<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbPembelianTitipian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_penjualan_titipan', function (Blueprint $table) {
         $table->increments('pt_no_faktur');
         $table->datetime('pt_tgl')->nullable();
         $table->integer('cus_kode')->nullable();
         $table->integer('pt_sales_person')->nullable();
         $table->integer('pt_checker')->nullable();
         $table->integer('pt_sopir')->nullable();
         $table->text('pt_catatan')->nullable();
         $table->datetime('pt_jatuh_tempo')->nullable();
         $table->datetime('pt_tgl_jatuh_tempo')->nullable();
         $table->integer('pt_subtotal')->nullable();
         $table->float('pt_disc')->nullable();
         $table->integer('pt_disc_nom')->nullable();
         $table->float('pt_ppn')->nullable();
         $table->integer('pt_ppn_nom')->nullable();
         $table->integer('pt_ongkos_angkut')->nullable();
         $table->integer('grand_total')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_penjualan_titipan');
     }
}
