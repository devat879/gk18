<table>
  @foreach($data as $row)
  <thead>
      <tr>
        <td colspan="9"><h5><b>Nama Pelanggan : {{$row->cus_nama}}</b></h5></td>
      </tr>
      <tr class="">
        <th style="font-size:12px">No </th>
        <th style="font-size:12px">Tanggal</th>
        <th style="font-size:12px">No. Faktur</th>
        <th style="font-size:12px">Barang Barkode</th>
        <th style="font-size:12px">Nama Barang</th>
        <th style="font-size:12px">QTY</th>
        <th style="font-size:12px">STN</th>
        <th style="font-size:12px">Harga</th>
        <th style="font-size:12px">Total</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($row->penjualan as $key)
        @foreach ($key->detail as $keyDet)
          <tr>
            <td style="font-size:12px"> {{ $no++ }}. </td>
            <td style="font-size:12px"> {{ date('Y-m-d', strtotime($key->pl_tgl)) }} </td>
            <td style="font-size:12px"> {{ $key->pl_no_faktur }} </td>
            <td style="font-size:12px"> {{ $keyDet->barang['brg_barcode'] }} </td>
            <td style="font-size:12px"> {{ $keyDet->barang['brg_nama'] }} </td>
            <td style="font-size:12px" align="right"> {{ number_format($keyDet->qty, 2, "." ,",") }} </td>
            <td style="font-size:12px"> {{ $keyDet->satuanJ['stn_nama'] }} </td>
            <td style="font-size:12px" align="right"> {{ number_format($keyDet->harga_net, 2, "." ,",") }} </td>
            <td style="font-size:12px" align="right"> {{ number_format($keyDet->total, 2, "." ,",") }} </td>
          </tr>
        @endforeach
      @endforeach
      <tr>
        <td colspan="8" align="right" style="font-weight:bold">Total Penjualan Ke {{$row->cus_nama}}</td>
        <td style="font-weight:bold" align="right">{{number_format($row->grand_total_cus, 2, "." ,",")}}</td>
      </tr>
      <tr>
        <td>
          <br>
        </td>
      </tr>
    @endforeach
  </tbody>
  <tr>
    <td colspan="8" align="right" style="font-weight:bold">Grand Total</td>
    <td style="font-weight:bold" align="right">{{number_format($grand_total, 2, "." ,",")}}</td>
  </tr>
</table>
