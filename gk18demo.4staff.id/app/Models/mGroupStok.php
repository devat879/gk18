<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mGroupStok extends Model
{
  public $incrementing = false;
  protected $table = 'tb_group_stok';
  protected $primaryKey = 'grp_kode';
  public $timestamps = false;
}
