<table>
  <thead>
    <tr>
      <th colspan="12">PENJUALAN LANGSUNG</th>
    </tr>
    <tr>
      <th>No </th>
      <th>Tanggal</th>
      <th>No. Invoice</th>
      <th>Nama</th>
      <th>Kasir</th>
      <th>Discount</th>
      <th>Ongkos Angkut</th>
      <th>Cash</th>
      <th>Transfer</th>
      <th>Cek/BG</th>
      <th>Piutang EDC</th>
      <th>Piutang Dagang</th>
    </tr>
  </thead>
  <tbody>
    <?php $__currentLoopData = $dataPL; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <tr>
        <td> <?php echo e($no++); ?>. </td>
        <td> <?php echo e(date('Y-m-d', strtotime($row->pl_tgl))); ?> </td>
        <td> <?php echo e($row->pl_no_faktur); ?> </td>
        <td> <?php echo e($row->customer['cus_nama']); ?> </td>
        <td> <?php echo e($row->karyawan['kry_nama']); ?> </td>
        <td> <?php echo e(number_format($row->pl_disc_nom, 2, "." ,",")); ?> </td>
        <td> <?php echo e(number_format($row->pl_ongkos_angkut, 2, "." ,",")); ?> </td>
        <td> <?php echo e(number_format(($row->cash - $row->kembalian_uang), 2, "." ,",")); ?> </td>
        <td> <?php echo e(number_format($row->transfer, 2, "." ,",")); ?> </td>
        <td> <?php echo e(number_format($row->cek_bg, 2, "." ,",")); ?> </td>
        <td> <?php echo e(number_format($row->edc, 2, "." ,",")); ?> </td>
        <td> <?php echo e(number_format($row->piutang, 2, "." ,",")); ?> </td>
      </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <tr>
      <td colspan="4">Total</td>
      <td><?php echo e(number_format($TotalPL, 2, "." ,",")); ?></td>
      <td><?php echo e(number_format($DiscPL, 2, "." ,",")); ?></td>
      <td><?php echo e(number_format($AngkutPL, 2, "." ,",")); ?></td>
      <td><?php echo e(number_format($TotalPLCash, 2, "." ,",")); ?></td>
      <td><?php echo e(number_format($TotalPLTransfer, 2, "." ,",")); ?></td>
      <td><?php echo e(number_format($TotalPLCek, 2, "." ,",")); ?></td>
      <td><?php echo e(number_format($TotalPLEdc, 2, "." ,",")); ?></td>
      <td><?php echo e(number_format($TotalPLPiutang, 2, "." ,",")); ?></td>
    </tr>
    <tr>
      <td>Total Penjualan</td>
      <td><?php echo e(number_format($TotalPenjualanL, 2, "." ,",")); ?></td>
    </tr>
  </tbody>
</table>

<table>
  <thead>
    <tr>
      <th colspan="12">PENJUALAN TITIPAN</th>
    </tr>
    <tr>
      <th>No </th>
      <th>Tanggal</th>
      <th>No. Invoice</th>
      <th>Nama</th>
      <th>Kasir</th>
      <th>Discount</th>
      <th>Ongkos Angkut</th>
      <th>Cash</th>
      <th>Transfer</th>
      <th>Cek/BG</th>
      <th>Piutang EDC</th>
      <th>Piutang Dagang</th>
    </tr>
  </thead>
  <tbody>
    <?php $__currentLoopData = $dataPT; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <tr>
        <td> <?php echo e($no++); ?>. </td>
        <td> <?php echo e(date('Y-m-d', strtotime($row->pt_tgl))); ?> </td>
        <td> <?php echo e($row->pt_no_faktur); ?> </td>
        <td> <?php echo e($row->customer['cus_nama']); ?> </td>
        <td> <?php echo e($row->karyawan['kry_nama']); ?> </td>
        <td> <?php echo e(number_format($row->pt_disc_nom, 2, "." ,",")); ?> </td>
        <td> <?php echo e(number_format($row->pt_ongkos_angkut, 2, "." ,",")); ?> </td>
        <td> <?php echo e(number_format(($row->cash - $row->kembalian_uang), 2, "." ,",")); ?> </td>
        <td> <?php echo e(number_format($row->transfer, 2, "." ,",")); ?> </td>
        <td> <?php echo e(number_format($row->cek_bg, 2, "." ,",")); ?> </td>
        <td> <?php echo e(number_format($row->edc, 2, "." ,",")); ?> </td>
        <td> <?php echo e(number_format($row->piutang, 2, "." ,",")); ?> </td>
      </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <tr>
      <td colspan="4">Total</td>
      <td><?php echo e(number_format($TotalPT, 2, "." ,",")); ?></td>
      <td><?php echo e(number_format($DiscPT, 2, "." ,",")); ?></td>
      <td><?php echo e(number_format($AngkutPT, 2, "." ,",")); ?></td>
      <td><?php echo e(number_format($TotalPTCash, 2, "." ,",")); ?></td>
      <td><?php echo e(number_format($TotalPTTransfer, 2, "." ,",")); ?></td>
      <td><?php echo e(number_format($TotalPTCek, 2, "." ,",")); ?></td>
      <td><?php echo e(number_format($TotalPTEdc, 2, "." ,",")); ?></td>
      <td><?php echo e(number_format($TotalPTPiutang, 2, "." ,",")); ?></td>
    </tr>
    <tr>
      <td>Total Penjualan</td>
      <td><?php echo e(number_format($TotalPenjualanT, 2, "." ,",")); ?></td>
    </tr>
  </tbody>
</table>

<table>
  <thead>
    <tr>
      <th colspan="12">RETUR PENJUALAN</th>
    </tr>
    <tr>
      <th>No </th>
      <th>Tanggal</th>
      <th>No. Invoice</th>
      <th>No. Retur</th>
      <th>Cash</th>
      <th>Transfer</th>
      <th>Cek/BG</th>
      <th>Piutang EDC</th>
      <th>Piutang Dagang</th>
    </tr>
  </thead>
  <tbody>
    <?php $__currentLoopData = $dataRetur; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <tr>
        <td> <?php echo e($no_3++); ?>. </td>
        <td> <?php echo e(date('Y-m-d', strtotime($row->tgl_pengembalian))); ?> </td>
        <td> <?php echo e($row->no_faktur); ?> </td>
        <td> <?php echo e($row->no_retur_penjualan); ?> </td>
        <td> <?php echo e(number_format(($row->cash - $row->kembalian_uang), 2, "." ,",")); ?> </td>
        <td> <?php echo e(number_format($row->transfer, 2, "." ,",")); ?> </td>
        <td> <?php echo e(number_format($row->cek_bg, 2, "." ,",")); ?> </td>
        <td> <?php echo e(number_format($row->edc, 2, "." ,",")); ?> </td>
        <td> <?php echo e(number_format($row->piutang, 2, "." ,",")); ?> </td>
      </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      <tr>
        <td colspan="4">Total</td>
        <td><?php echo e(number_format($TotalRetur, 2, "." ,",")); ?></td>
        <td><?php echo e(number_format($TotalReturCash, 2, "." ,",")); ?></td>
        <td><?php echo e(number_format($TotalReturTransfer, 2, "." ,",")); ?></td>
        <td><?php echo e(number_format($TotalReturCek, 2, "." ,",")); ?></td>
        <td><?php echo e(number_format($TotalReturEdc, 2, "." ,",")); ?></td>
        <td><?php echo e(number_format($TotalReturPiutang, 2, "." ,",")); ?></td>
      </tr>
      <tr>
        <td>Total Penjualan</td>
        <td><?php echo e(number_format($TotalPenjualanRetur, 2, "." ,",")); ?></td>
      </tr>
  </tbody>
</table>

<table>
  <thead>
    <tr>
      <th colspan="12">GRAND TOTAL</th>
    </tr>
    <tr>
      <th>Total Discount</th>
      <th>Total Ongkos Angkut</th>
      <th>Total Cash</th>
      <th>Total Transfer</th>
      <th>Total Cek/BG</th>
      <th>Total Piutang EDC</th>
      <th>Total Piutang Dagang</th>
    </tr>
  </thead>
  <tbody>
      <tr>
        <td><?php echo e(number_format($DiscPL + $DiscPT, 2, "." ,",")); ?></td>
        <td><?php echo e(number_format($AngkutPL + $AngkutPT, 2, "." ,",")); ?></td>
        <td><?php echo e(number_format(($TotalPLCash + $TotalPTCash) - $TotalReturCash, 2, "." ,",")); ?></td>
        <td><?php echo e(number_format(($TotalPLTransfer + $TotalPTTransfer) - $TotalReturTransfer, 2, "." ,",")); ?></td>
        <td><?php echo e(number_format(($TotalPLCek + $TotalPTCek) - $TotalReturCek, 2, "." ,",")); ?></td>
        <td><?php echo e(number_format(($TotalPLEdc + $TotalPTEdc) - $TotalReturEdc, 2, "." ,",")); ?></td>
        <td><?php echo e(number_format(($TotalPLPiutang + $TotalPTPiutang) - $TotalReturPiutang, 2, "." ,",")); ?></td>
      </tr>
      <tr>
        <td>Grand Total Penjualan</td>
        <td><?php echo e(number_format($TotalPenjualanL+$TotalPenjualanT-$TotalPenjualanRetur, 2, "." ,",")); ?></td>
      </tr>
  </tbody>
</table>


