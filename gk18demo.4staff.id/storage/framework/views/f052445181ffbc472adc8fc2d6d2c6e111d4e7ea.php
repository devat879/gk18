<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/global/plugins/bootstrap/css/bootstrap.min.css')); ?>">
    <style media="screen">
    .float{
      position:fixed;
      width:60px;
      height:60px;
      bottom:40px;
      right:40px;
      border-radius:50px;
      text-align:center;
      box-shadow: 2px 2px 3px #999;
      z-index: 100000;
    }
    .my-float{
      margin-top:22px;
    }
    </style>

    <script>
    function printDiv(divName){
      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
    }
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="glyphicon glyphicon-print"></i></button>
    </div>
    <div class="container-fluid" id='printMe'>
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4>Laporan Penjualan Stock Order</h4>
            <h4>Tanggal <?php echo e($start); ?> S/D <?php echo e($end); ?></h4>
          </div>
          <br>
          <div class="portlet light ">
            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <?php
                $no=1;
              ?>
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr>
                  <td colspan="9"><h5><b>Nama Pelanggan : <?php echo e($row->cus_nama); ?></b></h5></td>
                </tr>
                <tr class="">
                  <th style="font-size:12px">No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">No. Faktur</th>
                  <th style="font-size:12px">Barang Barkode</th>
                  <th style="font-size:12px">Nama Barang</th>
                  <th style="font-size:12px">QTY</th>
                  <th style="font-size:12px">STN</th>
                  <th style="font-size:12px">Harga</th>
                  <th style="font-size:12px">Total</th>
                </tr>
              </thead>
                <tbody>
                  <?php $__currentLoopData = $row->penjualan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php $__currentLoopData = $key->detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $keyDet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <tr>
                        <td style="font-size:12px"> <?php echo e($no++); ?>. </td>
                        <td style="font-size:12px"> <?php echo e(date('Y-m-d', strtotime($key->pt_tgl))); ?> </td>
                        <td style="font-size:12px"> <?php echo e($key->pt_no_faktur); ?> </td>
                        <td style="font-size:12px"> <?php echo e($keyDet->barang['brg_barcode']); ?> </td>
                        <td style="font-size:12px"> <?php echo e($keyDet->barang['brg_nama']); ?> </td>
                        <td style="font-size:12px" align="right"> <?php echo e(number_format($keyDet->qty, 2, "." ,",")); ?> </td>
                        <td style="font-size:12px"> <?php echo e($keyDet->satuanJ['stn_nama']); ?> </td>
                        <td style="font-size:12px" align="right"> <?php echo e(number_format($keyDet->harga_net, 2, "." ,",")); ?> </td>
                        <td style="font-size:12px" align="right"> <?php echo e(number_format($keyDet->total, 2, "." ,",")); ?> </td>
                      </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td colspan="8" align="right" style="font-weight:bold">Total Penjualan Ke <?php echo e($row->cus_nama); ?></td>
                      <td style="font-weight:bold" align="right"><?php echo e(number_format($row->grand_total_cus, 2, "." ,",")); ?></td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                
                <tr>
                  <td colspan="8" align="right" style="font-weight:bold">Grand Total</td>
                  <td style="font-weight:bold" align="right"><?php echo e(number_format($grand_total, 2, "." ,",")); ?></td>
                </tr>
              
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
