$(document).ready(function () {
  $('.form-send-penjualan').submit(function(e) {
      e.preventDefault();
      var ini = $(this);
      var kirim = $('input[name="pl_kirim_semua"]:checked').val();

      //Save penjualan
      $.ajax({
          url: ini.attr('action'),
          type: ini.attr('method'),
          data: ini.serialize(),
          success: function(data) {
              if(data.redirect) {
                  //Faktur Jual
                  var url_faktur = '/penjualanLangsung/report/faktur-jual'+'/'+data.faktur;
                  window.open(url_faktur, '_blank');

                  // if (kirim == 'ya') {
                  //   //Surat Jalan
                  //   var url_SJ = '/penjualanLangsung/report/surat-jalan'+'/'+data.faktur+'/'+data.surat_jalan;
                  //   window.open(url_SJ, '_blank');
                  //   //DO
                  //   var url_DO = '/penjualanLangsung/report/do'+'/'+data.faktur;
                  //   window.open(url_DO, '_blank');
                  // }

                  window.location.href = data.redirect;
              }
          },
          error: function(request, status, error) {
            swal({
              title: 'Perhatian',
              text: 'Data Gagal Disimpan!',
              type: 'error'
            });

            var json = JSON.parse(request.responseText);
            $('.form-group').removeClass('has-error');
            $('.help-block').remove();
            $.each(json.errors, function(key, value) {
              $('.form-send [name="'+key+'"]').parents('.form-group').addClass('has-error');
              $('.form-send [name="'+key+'"]').after('<span class="help-block">'+value+'</span>');
            });
          }
      });

      return false;
  });

  $('[name="pl_lama_kredit"]').change(function () {
      var pl_lama_kredit = $(this).val();
      var date = moment().add(pl_lama_kredit, 'days').calendar();
      $('[name="pl_tgl_jatuh_tempo"]').datepicker('setDate', date);
  });

  // var tableS1 = $('#sample_5').DataTable();
  // var tableS2 = $('#sample_6').DataTable();
  //
  // $('#btn-smpl2').on( 'click', function () {
  //     tableS2.destroy();
  // });
  // $('#btn-smpl1').on( 'click', function () {
  //     tableS1.destroy();
  // });

  $('#sample_6').on( 'draw.dt', function () {
    $('.btn-pilih-barang').click(function() {
      var ini = $('#modal-barang').data('barang');
      var brg_kode = $(this).data('brg-kode');
      var brg_barkode = $(this).data('brg-barkode');
      var brg_nama = $(this).data('brg-nama');
      var brg_kode_modal = ini.parents('div.input-group').children('input[name="brg_kode[]"]').val(brg_barkode).trigger('change');
      $('#modal-barang').modal('hide');
    });

    $('.btn-stok').click(function() {
      var href = $(this).data('href');
      $('#modal-stok').modal('show');
      $('#sample_3').DataTable({
        destroy : true,
        ajax : {
          url : href,
          dataSrc : ''
        },
        columns: [
          { data: "brg_no_seri" },
          { data: "QOH" },
          { data: "gdg_nama" },
          { data: "spl_nama" },
        ]
      });
    });
  });

  $('.btn-stok').click(function() {
    var href = $(this).data('href');
    $('#modal-stok').modal('show');
    $('#sample_3').DataTable({
      destroy : true,
      ajax : {
        url : href,
        dataSrc : ''
      },
      columns: [
        { data: "brg_no_seri" },
        { data: "QOH" },
        { data: "gdg_nama" },
        { data: "spl_nama" },
      ]
    });
  });

  $('#sample_5').on( 'draw.dt', function () {
    $('.btn-pilih-customer').click(function() {
      var cus_kode = $(this).data('cus-kode');
      var cus_nama = $(this).data('cus-nama');
      var cus_alamat = $(this).data('cus-alamat');
      var cus_telp = $(this).data('cus-telp');
      var cus_tipe = $(this).data('cus-tipe');
      $('[name="cus_kode"]').val(cus_kode).trigger('change');
      set_customer(cus_nama, cus_kode, cus_alamat, cus_telp, cus_tipe);
      $('#modal-customer').modal('hide');
    });

    $('.btn-piutang').click(function() {
      var href = $(this).data('href');
      $('#modal-piutang').modal('show');
      $('#sample_4').DataTable({
        destroy : true,
        ajax : {
          url : href,
          dataSrc : ''
        },
        columns: [
          { data: "pp_jatuh_tempo" },
          { data: null, render: function ( data, type, row )
            { return "PP"+data.pp_no_faktur; }
          },
          { data: "pp_amount" },
        ]
      });
    });

    $('.btn-pilih-customer-piutang').click(function() {
      var ini = $(this);
      $('#modal-piutang-pass').data('customer', ini).modal('show');
    });

    $('.btn-save-pilih-customer-piutang').click(function() {
      var password = $('input[name="password"]').val();
      var token = $('#data-back').data('form-token');
      var data_send = {
        password: password,
        _token: token
      };

      $.ajax({
        url: '/penjualanLangsung/piutang/pass',
        type: 'POST',
        data: data_send,
        success: function(data) {
          if (data == 'true') {
            var ini = $('#modal-piutang-pass').data('customer');
            var cus_kode = ini.data('cus-kode');
            var cus_nama = ini.data('cus-nama');
            var cus_alamat = ini.data('cus-alamat');
            var cus_telp = ini.data('cus-telp');
            var cus_tipe = ini.data('cus-tipe');
            $('[name="cus_kode"]').val(cus_kode).trigger('change');
            set_customer(cus_nama, cus_kode, cus_alamat, cus_telp, cus_tipe);
            $('#modal-piutang-pass').modal('hide');
            $('#modal-customer').modal('hide');
          }
          else {
            swal({
              title: 'Perhatian',
              text: 'Password Salah',
              type: 'warning'
            });
            // console.log(data);
          }
        }
      });
    });

    $('.btn-cek_bg').click(function() {
      var href = $(this).data('href');
      $('#modal-cek_bg').modal('show');
      $('#sample_7').DataTable({
        destroy : true,
        ajax : {
          url : href,
          dataSrc : ''
        },
        columns: [
          { data: "tgl_pencairan" },
          { data: "no_bg_cek" },
          // { data: null, render: function ( data, type, row )
          //   { return "PP"+data.pp_no_faktur; }
          // },
          { data: "cek_amount" },
        ]
      });
    });
  });

  $('.btn-piutang').click(function() {
    var href = $(this).data('href');
    $('#modal-piutang').modal('show');
    $('#sample_4').DataTable({
      destroy : true,
      ajax : {
        url : href,
        dataSrc : ''
      },
      columns: [
        { data: "pp_jatuh_tempo" },
        { data: null, render: function ( data, type, row )
          { return "PP"+data.pp_no_faktur; }
        },
        { data: "pp_amount" },
      ]
    });
  });

  $('.btn-pilih-customer-piutang').click(function() {
    var ini = $(this);
    $('#modal-piutang-pass').data('customer', ini).modal('show');
  });

  $('.btn-save-pilih-customer-piutang').click(function() {
    var password = $('input[name="password"]').val();
    var token = $('#data-back').data('form-token');
    var data_send = {
      password: password,
      _token: token
    };

    $.ajax({
      url: '/penjualanLangsung/piutang/pass',
      type: 'POST',
      data: data_send,
      success: function(data) {
        if (data == 'true') {
          var ini = $('#modal-piutang-pass').data('customer');
          var cus_kode = ini.data('cus-kode');
          var cus_nama = ini.data('cus-nama');
          var cus_alamat = ini.data('cus-alamat');
          var cus_telp = ini.data('cus-telp');
          var cus_tipe = ini.data('cus-tipe');
          $('[name="cus_kode"]').val(cus_kode).trigger('change');
          set_customer(cus_nama, cus_kode, cus_alamat, cus_telp, cus_tipe);
          $('#modal-piutang-pass').modal('hide');
          $('#modal-customer').modal('hide');
        }
        else {
          swal({
            title: 'Perhatian',
            text: 'Password Salah',
            type: 'warning'
          });
          // console.log(data);
        }
      }
    });
  });

  //penjualan langsung
  $('[name="cus_kode"]').change(function () {
    var cus_kode = $(this).val();
    var cus_nama = $(this).find(':selected').attr('data-cus-nama');
    var cus_alamat = $(this).find(':selected').attr('data-alamat');
    var cus_telp = $(this).find(':selected').attr('data-telp');
    var cus_tipe = $(this).find(':selected').attr('data-cus-tipe');

    set_customer(cus_nama, cus_kode, cus_alamat, cus_telp, cus_tipe);
  });

  $('[name="pl_transaksi"]').change(function () {
    var pl_transaksi = $(this).val();
    if (pl_transaksi === 'kredit') {
      $('.form-kredit').removeClass('hide');
    } else {
      $('.form-kredit').addClass('hide');
    }
  });

  $('.btn-modal-customer').click(function() {
    var tableS1 = $('#sample_5').DataTable();

    $('#modal-customer').modal('show');
  });

  $('.btn-pilih-customer').click(function() {
    var cus_kode = $(this).data('cus-kode');
    var cus_nama = $(this).data('cus-nama');
    var cus_alamat = $(this).data('cus-alamat');
    var cus_telp = $(this).data('cus-telp');
    var cus_tipe = $(this).data('cus-tipe');
    $('[name="cus_kode"]').val(cus_kode).trigger('change');
    set_customer(cus_nama, cus_kode, cus_alamat, cus_telp, cus_tipe);
    $('#modal-customer').modal('hide');
  });

  $('.btn-row-plus').click(function() {
    var cus_kode = $('[name="cus_kode"]').val();
    if(cus_kode == '-') {
      swal({
        title: 'Perhatian',
        text: 'Diharuskan Pilih Customer Terlebih Dahulu',
        type: 'warning'
      });
    } else {
      var row = $('.table-row-data tbody').html();
      $('.table-all-data tbody').append(row);
      select_row_barang();
      btn_modal_barang();
      btn_row_delete();
      enter_disc();
      select_harga();
      set_qty();
      detail_third_party();
    }
  });

  function btn_modal_barang() {
    $('.btn-modal-barang').click(function() {
      var tableS2 = $('#sample_6').DataTable();

      var ini = $(this);
      $('#modal-barang').data('barang', ini).modal('show');
    });

    $('.btn-pilih-barang').click(function() {
      var ini = $('#modal-barang').data('barang');
      var brg_kode = $(this).data('brg-kode');
      var brg_barkode = $(this).data('brg-barkode');
      var brg_nama = $(this).data('brg-nama');
      var brg_kode_modal = ini.parents('div.input-group').children('input[name="brg_kode[]"]').val(brg_barkode).trigger('change');
      $('#modal-barang').modal('hide');
    });
  }

  $('[name="pl_disc"], [name="pl_ppn"], [name="pl_ongkos_angkut"]').change(function() {
    kalkulasi_subtotal();
    kalkulasi_grandtotal();
  });

  function btn_row_delete() {
    $('.btn-row-delete').click(function() {
      $(this).parents('tr').remove();
      kalkulasi_subtotal();
      kalkulasi_total_hpp();
      kalkulasi_grandtotal();
    });
  }

  function select_row_barang() {
    var route_penjualan_langsung_barang_row = $('#data-back').data('route-penjualan-langsung-barang-row');
    $('input[name="brg_kode[]"]').change(function() {
      var brg_kode = $(this).val();
      var cus_kode = $('[name="cus_kode"]').val();
      var token = $('#data-back').data('form-token');
      var ini = $(this);
      var data_send = {
        brg_kode: brg_kode,
        cus_kode: cus_kode,
        _token: token
      };

      var nama = ini.parents('td').siblings('td.nama').children('input[name="nama[]"]');
      var satuan = ini.parents('td').siblings('td.satuan').children('input[name="satuan[]"]');
      var brg_hpp = ini.parents('td').siblings('td.brg_hpp').children('input[name="brg_hpp[]"]');
      var harga_jual = ini.parents('td').siblings('td.harga_jual').children('select[name="harga_jual[]"]');
      var brg_no_seri = ini.parents('td').siblings('td.brg_no_seri').children('select[name="brg_no_seri[]"]');
      var asal_gudang = ini.parents('td').siblings('td.gdg_kode').children('select[name="gdg_kode[]"]');
      var stok = ini.parents('td').siblings('td.qty').children('input[name="qty[]"]');

      asal_gudang.html('');
      stok.val('');
      nama.val('');
      satuan.val('');
      brg_hpp.val('');
      harga_jual.html('');
      brg_no_seri.html('');

      $.ajax({
        url: route_penjualan_langsung_barang_row,
        type: 'POST',
        data: data_send,
        success: function(data) {
          nama.val(data.nama);
          satuan.val(data.satuan);
          brg_hpp.val(data.brg_hpp);
          harga_jual.html(data.harga_jual);
          brg_no_seri.html(data.stok);
        }
      });
      // kalkulasi_total_hpp();
    });

    var route_gudang_row = $('#data-back').data('route-gudang-row');
    $('select[name="brg_no_seri[]"]').change(function() {
      var brg_no_seri = $(this).val();
      var brg_kode = $(this).parents('td').siblings('td.brg_kode').children('div.input-group').children('input').val();
      var token = $('#data-back').data('form-token');
      var ini = $(this);
      var data_send = {
        brg_kode: brg_kode,
        brg_no_seri: brg_no_seri,
        _token: token
      };

      var asal_gudang = ini.parents('td').siblings('td.gdg_kode').children('select[name="gdg_kode[]"]');
      var stok = ini.parents('td').siblings('td.qty').children('input[name="qty[]"]');

      asal_gudang.html('');
      stok.val('');

      $.ajax({
        url: route_gudang_row,
        type: 'POST',
        data: data_send,
        success: function(data) {
          asal_gudang.html(data.gudang);
        }
      });
      kalkulasi_total_hpp();
    });

    var route_stok_row = $('#data-back').data('route-stok-row');
    $('select[name="gdg_kode[]"]').change(function() {
      var gdg_kode = $(this).val();
      var brg_kode = $(this).parents('td').siblings('td.brg_kode').children('div.input-group').children('input').val();
      // var brg_kode = $(this).parents('td').siblings('td.brg_kode').children('select').val();
      var brg_no_seri = $(this).parents('td').siblings('td.brg_no_seri').children('select').val();
      var token = $('#data-back').data('form-token');
      var ini = $(this);
      var data_send = {
        brg_kode: brg_kode,
        brg_no_seri: brg_no_seri,
        gdg_kode: gdg_kode,
        _token: token
      };

      var stok = ini.parents('td').siblings('td.qty').children('input[name="qty[]"]');
      stok.val('');

      $.ajax({
        url: route_stok_row,
        type: 'POST',
        data: data_send,
        success: function(data) {
          if (data.stok != null) {
            stok.off('input');
            stok.on('input', function () {
              var value = $(this).val();
              $(this).val(Math.max(Math.min(value, data.stok), 0));
              // if ((value !== '') && (value.indexOf('.') === -1)) {
              // }
            });
          }
        }
      });
    });
  }

  function enter_disc() {
    $('[name="disc[]"]').change(function() {
      var form_disc_nom = $(this).parents('td').siblings('td.disc_nom').children('input');
      var form_harga_net = $(this).parents('td').siblings('td.harga_net').children('input');
      var form_total = $(this).parents('td').siblings('td.total').children('input');

      form_disc_nom.val(0);
      form_harga_net.val(0);

      var disc = $(this).val();
      var qty = $(this).parents('td').siblings('td.qty').children('input').val();
      var harga_jual = $(this).parents('td').siblings('td.harga_jual').children('select').val();
      var disc_nom =  (parseInt(disc) / parseInt(100)) * parseInt(harga_jual);
      var harga_net = parseInt(harga_jual) - disc_nom;
      var total = parseInt(harga_net) * parseInt(qty);

      form_disc_nom.val(disc_nom);
      form_harga_net.val(harga_net);
      form_total.val(total);

      kalkulasi_subtotal();
      kalkulasi_grandtotal();
    });
  }

  function select_harga() {
    $('[name="harga_jual[]"]').change(function() {
      var disc = $(this).parents('td').siblings('td.disc').children('input').val();
      var qty = $(this).parents('td').siblings('td.qty').children('input').val();
      var harga_jual = $(this).val();
      var disc_nom =  (parseInt(disc) / parseInt(100)) * parseInt(harga_jual);
      var harga_net = (parseInt(harga_jual) - disc_nom);
      var total = parseInt(harga_net) * parseInt(qty);
      $(this).parents('td').siblings('td.disc_nom').children('input').val(disc_nom);
      $(this).parents('td').siblings('td.harga_net').children('input').val(harga_net);
      $(this).parents('td').siblings('td.total').children('input').val(total);

      kalkulasi_subtotal();
      kalkulasi_grandtotal();
    });
  }

  function set_qty() {
    $('[name="qty[]"]').change(function() {
      var qty = $(this).val();
      // var harga_net = $(this).parents('td').siblings('td.harga_net').children('input').val();
      // var total = parseInt(qty) * parseInt(harga_net);
      // $(this).parents('td').siblings('td.total').children('input').val(total);
      //
      // kalkulasi_subtotal();
      // kalkulasi_grandtotal();

      var disc = $(this).parents('td').siblings('td.disc').children('input').val();
      var harga_jual = $(this).parents('td').siblings('td.harga_jual').children('select').val();
      var brg_hpp = $(this).parents('td').siblings('td.brg_hpp').children('input').val();
      var disc_nom =  (parseInt(disc) / parseInt(100)) * parseInt(harga_jual);
      var harga_net = (parseInt(harga_jual) - disc_nom);
      var total = parseInt(harga_net) * parseInt(qty);
      var total_hpp = parseInt(brg_hpp) * parseInt(qty);
      $(this).parents('td').siblings('td.disc_nom').children('input').val(disc_nom);
      $(this).parents('td').siblings('td.harga_net').children('input').val(harga_net);
      $(this).parents('td').siblings('td.total').children('input').val(total);
      $(this).parents('td').siblings('td.brg_hpp_total').children('input').val(total_hpp);

      kalkulasi_total_hpp();
      kalkulasi_subtotal();
      kalkulasi_grandtotal();
    });
  }

  function set_customer(cus_nama, cus_kode, cus_alamat, cus_telp, cus_tipe) {
    var kode_customer = $('#data-back').data('kode-customer');
    $('.cus_nama').val(cus_nama);
    $('.cus_alamat').val(cus_alamat);
    $('.cus_telp').val(cus_telp);
    $('[name="cus_kode_label"]').val(kode_customer+cus_kode);
    if (cus_kode == 0) {
      $('[name="cus_tipe"]').val('Non Member');
    }
    else {
      $('[name="cus_tipe"]').val(cus_tipe);
    }
  }

  function kalkulasi_subtotal() {
    var total = 0;
    $('[name="total[]"]').each(function(key, val) {
      var val = $(this).val();
      total = parseInt(total) + parseInt(val);
    });
    $('[name="pl_subtotal"]').val(total);
  }

  function kalkulasi_total_hpp() {
    var brg_hpp = 0;
    $('[name="brg_hpp_total[]"]').each(function(key, val) {
      var val = $(this).val();
      brg_hpp = parseInt(brg_hpp) + parseInt(val);
    });
    $('[name="pl_total_hpp"]').val(brg_hpp);
  }

  function kalkulasi_grandtotal() {
    var subtotal = $('[name="pl_subtotal"]').val();
    var disc = $('[name="pl_disc"]').val();
    var ppn = $('[name="pl_ppn"]').val();
    var ongkos_angkut = $('[name="pl_ongkos_angkut"]').val();

    var disc_kal = (parseInt(disc) / parseInt(100)) * parseInt(subtotal);
    var ppn_kal = (parseInt(ppn) / parseInt(100)) * parseInt(subtotal);

    var grand_total = parseInt(subtotal) - parseInt(disc_kal) + parseInt(ppn_kal) + parseInt(ongkos_angkut);

    $('[name="grand_total"]').val(grand_total);
    $('.nominal-grand-total').html(grand_total);
  }

  function detail_third_party() {
    //$('[name="gdg_kode[]"], [name="brg_kode[]"], [name="harga_jual[]"]').select2();
  }

  ////// JS PAYMENT

  $('.btn-row-payment-plus').click(function() {
    var row_payment = $('.table-row-payment tbody').html();
    var el = $(row_payment);
    $('.table-data-payment tbody').append(el);

    btn_row_delete_payment();
    btn_selectpickerx(el);
    payment();
    set_charge();
    setor();
    payment_third_party();
  });

  function btn_selectpickerx(el) {
    el.children('td:nth-child(1)').children('select').selectpicker();
  }

  function btn_row_delete_payment() {
    $('.btn-row-delete-payment').click(function() {
      $(this).parents('tr').remove();
      kalkulasi_sisa();
    });
  }

  function payment() {
    $('[name="payment[]"]').keyup(function() {
      var payment = $(this).val();

      $(this).parents('td').siblings('td.payment_total').children('input').val(payment);

      kalkulasi_sisa();
    });
  }

  function set_charge() {
    $('[name="charge[]"]').change(function() {
      var charge = $(this).val();
      var payment = $(this).parents('td').siblings('td.payment').children('input').val();
      var charge_nom = (parseInt(charge)/100) * parseInt(payment);
      var total = parseInt(payment) + parseInt(charge_nom);

      $(this).parents('td').siblings('td.charge_nom').html(charge_nom);
      $(this).parents('td').siblings('td.payment_total').children('input').val(total);

      kalkulasi_sisa();
    });
  }

  function setor() {
    $('[name="setor[]"]').change(function() {
      var setor = $(this).val();
      var total = $(this).parents('td').siblings('td.payment_total').children('input').val();
      var kembalian = parseInt(setor) - parseInt(total);

      $(this).parents('td').siblings('td.kembalian').html(kembalian);
    });
  }

  function kalkulasi_sisa() {
    var payment_total = 0;
    var grand_total = $('.nominal-grand-total').html();
    $('[name="payment_total[]"]').each(function(key, val) {
      var payment_total_ini = $(this).val();
      if(payment_total_ini != '') {
        payment_total = parseInt(payment_total) + parseInt(payment_total_ini);
      }
    });
    sisa = parseInt(grand_total) - parseInt(payment_total);
    $('.nominal-sisa').text(sisa);
  }

  function payment_third_party() {
    $('[name="tgl_pencairan[]"]').datepicker();
    //$('[name="master_id[]"]').select2();
  }

  //akhir dari penjualan langsung
});
