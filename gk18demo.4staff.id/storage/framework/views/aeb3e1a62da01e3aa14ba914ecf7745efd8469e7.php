<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />   
    <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/piutangPelanggan.js')); ?>" type="text/javascript"></script>

    <script src="<?php echo e(asset('assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-select2.min.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
<div class="page-content-inner">
    <div class="mt-content-body">
    <?php if($message = Session::get('success')): ?>
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong><?php echo e($message); ?></strong>
        </div>
    <?php endif; ?>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="portlet-body">
                            <!-- <div class="tab-content">
                                <div class="tab-pane fade active in" id="tab_jurnal_list"> -->
                                <div class="col-md-6 col-xs-6">
                                    <a style="font-size: 11px" class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button">
                                        Pilih Periode
                                    </a>
                                    <a style="font-size: 11px" type="button" class="btn btn-danger" href="<?php echo e(route('printLaporanPembelian', ['start_date'=>$start_date, 'end_date'=>$end_date, 'spl_id'=>$spl_id, 'mrk'=>$mrk, 'grp'=>$grp, 'tipe'=>'print'])); ?>" target="_blank">
                                        <span><i class="fa fa-print"></i></span> Print
                                    </a>
                                    <a style="font-size: 11px" type="button" class="btn btn-danger" href="<?php echo e(route('lapPembelianPrintExcel', ['start_date'=>$start_date, 'end_date'=>$end_date, 'spl_id'=>$spl_id,'mrk'=>$mrk,'grp' => $grp, 'tipe'=> 'pembelianStock'])); ?>" target="_blank">
                                        <span><i class="fa fa-print"></i></span> Excel
                                    </a>
                                </div>
                                    
                                    <br /><br />
                                    <div class="col-md-12">
                                        <h3><center>Laporan Pembelian Stock</center></h3>
                                        <h4><center><?php echo e(date('d M Y', strtotime($start_date))); ?> s/d <?php echo e(date('d M Y', strtotime($end_date))); ?></center></h4> 
                                        
                                    </div>
                                                                     
                                    
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" width="100%">
                                        <thead>
                                            <tr class="success">
                                                <th width="10" style="font-size: 12px"> No </th>
                                                <th style="font-size: 12px;text-align: center;"> Tanggal</th>
                                                <th style="font-size: 12px;text-align: center;"> No PO </th>
                                                <th style="font-size: 12px;text-align: center;"> No W/O </th>
                                                <th style="font-size: 12px;text-align: center;"> No Pembelian </th>
                                                <th style="font-size: 12px;text-align: center;"> Barcode </th>
                                                <th style="font-size: 12px;text-align: center;"> Nama Barang </th>
                                                <th style="font-size: 12px;text-align: center;"> Qty </th>
                                                <th style="font-size: 12px;text-align: center;"> Satuan </th>
                                                <th style="font-size: 12px;text-align: center;"> Harga </th>
                                                <th style="font-size: 12px;text-align: center;"> Total </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $grand_total     = 0;
                                            
                                        ?> 
                                        <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $grand_total_spl = 0;?>
                                            <?php if($jml_detail[$spl->spl_kode]>0): ?>
                                            <tr>
                                                <td style="font-size: 11px" colspan="11"><?php echo e($spl->spl_nama); ?></td>
                                            </tr>
                                            <?php endif; ?>
                                            <?php $__currentLoopData = $pembelian[$spl->spl_kode]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pembelian_spl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                            <tr>
                                                <td style="font-size: 11px"><?php echo e($no++); ?></td>
                                                <td style="font-size: 11px"><?php echo e(date('d M Y', strtotime($pembelian_spl->ps_tgl))); ?></td>
                                                <td style="font-size: 11px"><?php echo e($pembelian_spl->no_po); ?></td>
                                                <td style="font-size: 11px"><?php echo e($pembelian_spl->pos_no_po); ?> - <?php echo e(ucfirst($pembelian_spl->pb_kondisi)); ?></td>
                                                <td style="font-size: 11px"><?php echo e($pembelian_spl->no_pembelian); ?></td>
                                                <td style="font-size: 11px"><?php echo e($pembelian_spl->brg_barcode); ?></td>
                                                <td style="font-size: 11px"><?php echo e($pembelian_spl->brg_nama); ?></td>
                                                <td style="font-size: 11px" align="center"><?php echo e($pembelian_spl->qty); ?></td>
                                                <td style="font-size: 11px" align="center"><?php echo e($pembelian_spl->stn_nama); ?></td>
                                                <td align="right" style="font-size: 11px"><?php echo e(number_format($pembelian_spl->harga_net,2)); ?></td>
                                                <td align="right" style="font-size: 11px"><?php echo e(number_format($pembelian_spl->total,2)); ?></td>
                                            </tr>
                                            <?php
                                                $grand_total_spl = $grand_total_spl+$pembelian_spl->total;
                                                
                                            ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php $grand_total     = $grand_total+$grand_total_spl;?>
                                            <?php if($jml_detail[$spl->spl_kode]>0): ?>
                                            <tr>
                                                <td colspan="10" align="right" style="font-size: 12px"><strong>Grand Total</strong></td>
                                                <td align="right" style="font-size: 12px"><strong><?php echo e(number_format($grand_total_spl,2)); ?></strong></td>
                                            </tr>
                                            <?php endif; ?> 
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                                                     
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="10" align="right" style="font-size: 12px"><strong>Grand Total</strong></td>
                                                <td align="right" style="font-size: 12px"><strong><?php echo e(number_format($grand_total,2)); ?></strong></td>
                                            </tr> 
                                        </tfoot>
                                    </table>
                                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Pilih Periode
                </h4>
            </div>
            <div class="modal-body form">
                <form action="<?php echo e(route('pilihPeriodePembelian')); ?>" class="form-horizontal form-send" role="form" method="post">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <!-- <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date"/> -->
                                <select style="font-size: 11px" class="form-control select2" name="supplier_id">
                                    <option value="0">All Supplier</option>
                                    <?php $__currentLoopData = $suppliers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $supplier): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($supplier->spl_kode); ?>" <?php if($spl_id==$supplier->spl_kode) echo 'selected'?>><?php echo e($supplier->spl_nama); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Merek</label>
                            </div>
                            <div class="col-md-9">                                
                                <select style="font-size: 11px" class="form-control select2" name="mrk">
                                    <option value="0">All Merek</option>
                                    <?php $__currentLoopData = $mereks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $merek): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($merek->mrk_kode); ?>" <?php if($mrk==$merek->mrk_kode) echo 'selected'?>><?php echo e($merek->mrk_nama); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Group</label>
                            </div>
                            <div class="col-md-9">
                                <!-- <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date"/> -->
                                <select style="font-size: 11px" class="form-control select2" name="grp">
                                    <option value="0">All Group</option>
                                    <?php $__currentLoopData = $groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($group->grp_kode); ?>" <?php if($grp==$group->grp_kode) echo 'selected'?>><?php echo e($group->grp_nama); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date" value="<?php echo e($start_date); ?>" />
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                            <div class="col-md-2">
                                <h5><center>s/d</center></h5>

                            </div>
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date"  value="<?php echo e($end_date); ?>"/>
                                <input type="hidden" name="tipe_laporan" value="rekapStock">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                                <button type="submit" class="btn green">Search</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>