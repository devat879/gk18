<html>
  <head>
    <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ public_path('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ public_path('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ public_path('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ public_path('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}"> --}}
  </head>
  <body>
    <div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="portlet-body">
                            <!-- <div class="tab-content">
                                <div class="tab-pane fade active in" id="tab_jurnal_list"> -->
                                    <div class="col-md-12">
                                        <h1><center>Jurnal Umum</center></h1>
                                        @if($bulan=='1')
                                            <h2><center>Periode Januari {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='2')
                                            <h2><center>Periode Pebruari {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='3')
                                            <h2><center>Periode Maret {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='4')
                                            <h2><center>Periode April {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='5')
                                            <h2><center>Periode Mei {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='6')
                                            <h2><center>Periode Juni {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='7')
                                            <h2><center>Periode Juli {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='8')
                                            <h2><center>Periode Agustus {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='9')
                                            <h2><center>Periode September {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='10')
                                            <h2><center>Periode Oktober {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='11')
                                            <h2><center>Periode November {{$tahun_periode}}</center></h2> 
                                        @endif
                                        @if($bulan=='12')
                                            <h2><center>Periode Desember {{$tahun_periode}}</center></h2> 
                                        @endif
                                    </div>
                                    <table class="table table-striped table-bordered table-hover table-header-fixed">
                                        <thead>
                                            <tr class="">
                                                <th width="10"><center> No </center></th>
                                                <th><center> Tanggal </center></th>
                                                <th><center> No Bukti </center></th>
                                                <th><center> Keterangan </center></th>
                                                <th><center> No Akun </center></th>
                                                <th><center> Debet </center></th>
                                                <th><center> Kredit </center></th>
                                                <th><center> Catatan </center></th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr class="">
                                                <th width="10"></th>
                                                <th colspan="4" align="center"><h4><center><strong> TOTAL </strong></center></h4></th>
                                                <th> <h4><center><strong>{{number_format($jml_debet)}} </strong></center></h4></th>
                                                <th> <h4><center><strong>{{number_format($jml_kredit)}} </th>
                                                @if($jml_debet==$jml_kredit && $jml_debet>0 && $jml_kredit>0)
                                                <th>
                                                   <h4><strong>Status : <font color="green">Balance</font></strong></h4>
                                                </th>
                                                @endif
                                                @if($jml_debet!=$jml_kredit)
                                                <th>
                                                   <h4><strong>Status : <font color="red">Not Balance</font></strong></h4>
                                                </th>
                                                @endif
                                                @if($jml_debet==0 && $jml_kredit==0)
                                                <th>
                                                   <h4><strong>Status : <font color="red"></font></strong></h4>
                                                </th>
                                                @endif
                                                <th> </th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            @foreach($jurnalUmum as $jmu)
                                            <tr>
                                                <td align="center"> {{ $no++ }}. </td>
                                                    <td> {{ date('d M Y', strtotime($jmu->jmu_date_insert)) }} </td>
                                                    <td><center> {{ $jmu->no_invoice }} </center></td>
                                                    <td> {{ $jmu->jmu_keterangan }}</td>
                                                    <td colspan="5"></td>
                                            </tr>
                                            @foreach($jmu->transaksi as $trs)
                                            <tr>                                
                                                <td></td>
                                                <td></td>
                                                <td></td>                                
                                                <td>   {{ $trs->trs_nama_rekening }}</td>
                                                <td><center>   {{ $trs->trs_kode_rekening }}</center></td>
                                                <td align="right">   {{ number_format($trs->trs_debet) }} </td>
                                                <td align="right">   {{ number_format($trs->trs_kredit) }} </td>
                                                <td>   {{ $trs->trs_catatan }} </td>                                
                                            </tr>
                                            @endforeach
                                            @endforeach
                                        </tbody>
                                    </table>
                                <!-- </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>