<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  
  
  <title></title>
  <style>
  /* .tt  {border-collapse:collapse;border-spacing:0;width: 100%; }
  .tt td{font-family:Tahoma;font-size:11px;padding-top: 0px;overflow:hidden;word-break:normal;color:#333;background-color:#fff;}
  .tt th{font-family:Tahoma;font-size:11px;font-weight:bold;padding:1px 1px;overflow:hidden;word-break:normal;color:#333;background-color:#f0f0f0;}
  .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; padding:5px;}
  .tg td{font-family:Tahoma;font-size:11px;padding:5px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
  .tg th{font-family:Tahoma;font-size:12px;font-weight:bold;padding:5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;}
  .tg .tg-3wr7{font-weight:bold;font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
  .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
  .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;} */

  @media  print {
    html, body {
    display: block;
    font-family: "Tahoma";
    margin: 0px 0px 0px 0px;
    }

    @page  {
      size: 21.5cm 15cm;

    }
    #footer {
      position: fixed;
      bottom: 0;
    }
  }

  p {
    font-size: 14px;
    padding: 0 !important;
    margin: 0 !important;
  }
  table {
    border-collapse: collapse;
    padding: 0 !important;
    margin: 0 !important;
  }
  tr td{
    padding: 0 !important;
    margin: 0 !important;
  }
  </style>
</head>
<body>
  <table id="header" width="100%" border="0">
    <thead>
      <tr>
        <td colspan="8">
          <p><b>A.K.I.,</b> Jl. Gatot Subroto Barat No. 88A Tlp. (0361)416088, Fax.(0361)418933, No.HP. 0812 1611 8118, www.grahakita18.com</p>
        </td>
      </tr>
      <tr>
        <td width="15%"> <p>Syarat Pembayaran</p> </td>
        <td width="20%"> <p>: <?php echo e($faktur['pt_transaksi']); ?></p> </td>
        <td colspan="4"></td>
        <td width="10%"> <p>No. Ftr</p> </td>
        <td width="30%"> <p>: <?php echo e($faktur['kode_bukti_id']); ?></p> </td>
      </tr>
      <tr>
        <?php if($faktur['pt_transaksi'] == 'kredit'): ?>
          <td> <p>Tgl. Jatuh Tempo</p> </td>
          <td> <p>: <?php echo e($faktur['tglJT']); ?> (<?php echo e($faktur['pt_lama_kredit']); ?> hari)</p> </td>
          <td colspan="4"></td>
        <?php else: ?>
          <td colspan="6"></td>
        <?php endif; ?>
        <td> <p>Tgl.</p> </td>
        <td> <p>: <?php echo e($faktur['tglPrint']); ?></p> </td>
      </tr>

      <tr>
        <?php if($faktur['transfer'] != 0): ?>
          <td> <p>Transfer</p> </td>
          <td colspan="5"> <p>: Rp. <?php echo e(number_format(round($faktur['transfer']), 2, "." ,",")); ?></p> </td>
        <?php elseif($faktur['cek_bg'] != 0): ?>
          <td> <p>Cek/BG</p> </td>
          <td colspan="5"> <p>: Rp. <?php echo e(number_format(round($faktur['cek_bg']), 2, "." ,",")); ?></p> </td>
        <?php elseif($faktur['edc'] != 0): ?>
          <td> <p>EDC</p> </td>
          <td colspan="5"> <p>: Rp. <?php echo e(number_format(round($faktur['edc']), 2, "." ,",")); ?></p> </td>
        <?php else: ?>
          <td colspan="6"></td>
        <?php endif; ?>
        <td> <p>Pelanggan</p> </td>
        <td> <p>: <b><?php echo e($faktur['cus_nama']); ?></b></p> </td>
      </tr>
      <tr>
        <td> <p>Sales</p> </td>
        <td colspan="2"> <p>: <?php echo e($faktur->salesPrint['kry_nama']); ?></p> </td>
        <td colspan="4"></td>
        <td>
          <p> <?php echo e($faktur['cus_alamat']); ?>

            <?php if($faktur['cus_telp'] != null): ?>
              (<?php echo e($faktur['cus_telp']); ?>)
            <?php endif; ?>
          </p>
        </td>
      </tr>
    </thead>
  </table>

  <table id="body" width="100%" border="0">
    <thead>
      <tr>
        <td colspan="8" align="center"><p align="center" style="font-size: 18px;"><b>FAKTUR JUAL</b></p></td>
      </tr>
      <tr>
        <td colspan="8">
          <hr>
        </td>
      </tr>
      <tr>
        <th align="left"> <p>No</p> </th>
        <th align="left"> <p>Nama Barang</p> </th>
        <th align="right"> <p>Harga/sat(Rp.)</p> </th>
        <th align="right"> <p>Disc.%</p> </th>
        <th align="right"> <p>Disc.nom</p> </th>
        <th align="right"> <p>Harga Bersih</p> </th>
        <th align="right"> <p>Banyak</p> </th>
        <th align="right"> <p>Total</p> </th>
      </tr>
      <tr>
        <td colspan="8">
          <hr>
        </td>
      </tr>
    </thead>
    <tbody>
      <?php $__currentLoopData = $faktur->detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
          <td align="left"><p><?php echo e($faktur->no++); ?></p></td>
          <td align="left"><p><?php echo e($key['nama_barang']); ?></p></td>
          
          <td align="right"><p><?php echo e(number_format(round(($key->harga_jual * ($key->ppn/100)) + $key->harga_jual), 2, "." ,",")); ?></p></td>
          <td align="right"><p><?php echo e(number_format($key['disc'], 2, "." ,",")); ?></p></td>
          <td align="right"><p><?php echo e(number_format(round($key['disc_nom']), 2, "." ,",")); ?></p></td>
          <td align="right"><p><?php echo e(number_format(round($key['harga_net']), 2, "." ,",")); ?></p></td>
          <td align="right"><p><?php echo e(number_format($key['qty'], 2, "." ,",")); ?></p></td>
          <td align="right"><p><?php echo e(number_format(round($key['total']), 2, "." ,",")); ?></p></td>
        </tr>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
  </table>

  <table id="footer" width="100%" border="0">
    <thead>
      <tr>
        <td colspan="8">
          <hr>
        </td>
      </tr>

      <tr>
        <td colspan="8">
          <p>Terbilang : <?php echo e($faktur['terbilang']); ?></p>
        </td>
      </tr>
      <tr>
        <td colspan="8">
          <br>
        </td>
      </tr>

      <tr>
        <td rowspan="3" colspan="3">
          <p>Keterangan : <?php echo e($faktur['pt_catatan']); ?></p>
        </td>
        <td width="10%"> <p><b>Total</b></p> </td>
        <td width="2%"> <p>Rp.</p> </td>
        <td width="20%" colspan="2" align="right"> <p><?php echo e(number_format(round($faktur['pt_subtotal']), 2, "." ,",")); ?></p> </td>
      </tr>

      <?php if($faktur['pt_ongkos_angkut'] != 0): ?>
        <tr>
          <td> <p><b>Ongkos Angkut</b></p> </td>
          <td> <p>Rp.</p> </td>
          <td colspan="2" align="right"> <p><?php echo e(number_format(round($faktur['pt_ongkos_angkut']), 2, "." ,",")); ?></p> </td>
        </tr>
      <?php endif; ?>

      <?php if($faktur['charge_nom'] != 0): ?>
        <tr>
          <td> <p><b>Charge</b></p> </td>
          <td> <p>Rp.</p> </td>
          <td colspan="2" align="right"> <p><?php echo e(number_format(round($faktur['charge_nom']), 2, "." ,",")); ?></p> </td>
        </tr>
      <?php endif; ?>

      <tr>
        <?php if($faktur['charge_nom'] != 0 && $faktur['pt_ongkos_angkut'] != 0): ?>
          <td colspan="3">
            
          </td>
        <?php endif; ?>
        <td> <p><b>Discount</b></p> </td>
        <td> <p>Rp.</p> </td>
        <td colspan="2" align="right"> <p><?php echo e(number_format(round($faktur['pt_disc_nom']), 2, "." ,",")); ?></p> </td>
      </tr>

      <?php if($faktur['charge_nom'] != 0 && $faktur['pt_ongkos_angkut'] != 0): ?>
        <tr>
          <td colspan="4">
            
          </td>
          <td colspan="2">
            <hr>
          </td>
        </tr>
      <?php elseif($faktur['charge_nom'] != 0 || $faktur['pt_ongkos_angkut'] != 0): ?>
        <tr>
          <td colspan="4">
            
          </td>
          <td colspan="2">
            <hr>
          </td>
        </tr>
      <?php else: ?>
        <tr>
          <td colspan="1">
            
          </td>
          <td colspan="2">
            <hr>
          </td>
        </tr>
      <?php endif; ?>

      <tr>
        <td width="45%" rowspan="6" colspan="2">
          <p>- Pembayaran ditransfer ke Rek. BCA Cab. Sunset Road, A/C : 770 589 1818, A/C : PT. ANGSA KUSUMA INDAH</p>
          <p>- Pembayaran dengan CHEQUE/GIRO dianggap sah, setelah CHEQUE/GIRO tersebut telah dapat diuangkan/clearing</p>
          <p>- Barang yang telah di beli tidak dapat dikembalikan/ditukar</p>
        </td>
      </tr>

      <tr>
        <td align="center" colspan="1">
          <b><p>Penerima</p></b>
        </td>
        <td> <p><b>Grand Total</b></p> </td>
        <td> <p>Rp.</p> </td>
        <td colspan="2" align="right"> <p><?php echo e(number_format(round($faktur['grand_total']), 2, "." ,",")); ?></p> </td>
      </tr>

      

      <?php if($faktur['cash'] != 0): ?>
        <tr>
          <td colspan="1">
            <br>
          </td>
          <td> <p><b>Kas</b></p> </td>
          <td> <p>Rp.</p> </td>
          <td colspan="2" align="right"> <p><?php echo e(number_format(round($faktur['cash']), 2, "." ,",")); ?></p> </td>
        </tr>
      <?php endif; ?>

      <?php if($faktur['transfer'] != 0): ?>
        <tr>
          <td colspan="1">
            <br>
          </td>
          <td> <p><b>Transfer</b></p> </td>
          <td> <p>Rp.</p> </td>
          <td colspan="2" align="right"> <p><?php echo e(number_format(round($faktur['transfer']), 2, "." ,",")); ?></p> </td>
        </tr>
      <?php endif; ?>

      <?php if($faktur['cek_bg'] != 0): ?>
        <tr>
          <td colspan="1">
            <br>
          </td>
          <td> <p><b>Cek/BG</b></p> </td>
          <td> <p>Rp.</p> </td>
          <td colspan="2" align="right"> <p><?php echo e(number_format(round($faktur['cek_bg']), 2, "." ,",")); ?></p> </td>
        </tr>
      <?php endif; ?>

      <?php if($faktur['edc'] != 0): ?>
        <tr>
          <td colspan="1">
            <br>
          </td>
          <td> <p><b>EDC</b></p> </td>
          <td> <p>Rp.</p> </td>
          <td colspan="2" align="right"> <p><?php echo e(number_format(round($faktur['edc']), 2, "." ,",")); ?></p> </td>
        </tr>
      <?php endif; ?>

      <?php if($faktur['piutang'] != 0): ?>
        <tr>
          <td colspan="1">
            <br>
          </td>
          <td> <p><b>Piutang</b></p> </td>
          <td> <p>Rp.</p> </td>
          <td colspan="2" align="right"> <p><?php echo e(number_format(round($faktur['piutang']), 2, "." ,",")); ?></p> </td>
        </tr>
      <?php endif; ?>

      <tr>
        <td colspan="1">
          <br>
        </td>
        <td>
          <br>
        </td>
        <td colspan="2"><hr></td>
      </tr>

      

      

      <tr>
        <td align="center" colspan="1">
          <p>(......................................)</p>
        </td>
        
        <td> <p><b>Kembalian</b></p> </td>
        <td> <p>Rp.</p> </td>
        <td colspan="2" align="right"> <p><?php echo e(number_format(round($faktur['kembalian_uang']), 2, "." ,",")); ?></p> </td>
      </tr>
    </thead>
  </table>
</body>
</html>

<script type="text/javascript">
  window.print();
</script>
