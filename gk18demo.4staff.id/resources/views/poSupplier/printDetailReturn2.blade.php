<html moznomarginboxes mozdisallowselectionprint>
    <head>
        <title>Laporan Pembelian Supplier</title>
    </head>
    <body>
        <style type="text/css">
            @page {
                size: A4 potrait;
                margin: 2%;
              }
            .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
            .tg td{font-family:Arial;font-size:10px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
            .tg th{font-family:Arial;font-size:12px;font-weight:bold;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
            .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
        </style>
        <div class="container-fluid">
            <div class="row">
                <h3><center>A.K.I., Jl. Gatot Subroto No XX Tlp. 0361 xxx xxx</center></h3>
                <hr size="1px">
                <div class="row">
                    <!-- <div class="col-md-12"> -->
                        <table width="100%">
                            <tr>
                                <td style="font-size: 11px;padding-top: 0px;" width="25%">No. Faktur</td>
                                <td style="font-size: 11px;padding-top: 0px;" width="25%">: {{$returPembelian->ps_no_faktur}}</td>
                            </tr>
                            <tr>
                                <td style="font-size: 11px;padding-top: 0px;" width="25%">Tgl. Beli</td>
                                <td style="font-size: 11px;padding-top: 0px;" width="25%">: {{date('d M Y', strtotime($returPembelian->pembelianSupplier->ps_tgl))}}</td>
                            </tr>
                            <tr>
                                <td style="font-size: 11px;padding-top: 0px;" width="25%">No. Retur</td>
                                <td style="font-size: 11px;padding-top: 0px;" width="25%">: {{$returPembelian->no_return_pembelian}}</td>
                            </tr>
                            <tr>
                                <td style="font-size: 11px;padding-top: 0px;" width="25%">Tgl. Beli</td>
                                <td style="font-size: 11px;padding-top: 0px;" width="25%">: {{date('d M Y', strtotime($returPembelian->tgl_pengembalian))}}</td>
                                <td align="right" style="font-size: 12px;padding-left: : 50px;" width="25%">Supplier</td>
                                <td align="right" style="font-size: 12px;padding-top: 0px;" width="25%">: {{$returPembelian->supplier->spl_nama}}</td>
                            </tr>                            
                        </table>
                    <!-- </div> -->
                </div>
                <hr size="1px">
                <div class="row">
                    <div class="col-xs-12">
                        <table class="tg table table-bordered">
                            <thead>
                                <tr>
                                    <th style="font-size: 12px"><center> No </center></th>
                                    <th style="font-size: 12px"><center> Nama Stock </center></th>
                                    <th style="font-size: 12px"><center> No Seri </center></th>
                                    <th style="font-size: 12px"><center> QTY </center></th>
                                    <th style="font-size: 12px"><center> Harga Beli </center></th>
                                    <th style="font-size: 12px"><center> Total Retur </center></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($returPembelian->detailReturPembelian as $detail)
                                <tr>
                                    <td style="font-size: 11px"><center>{{$no++}} </center></td>
                                    <td style="font-size: 11px"><center> {{$detail->barang->brg_nama}} </center></td>
                                    <td style="font-size: 11px"><center> {{$detail->stok->brg_no_seri}} </center></td>
                                    <td style="font-size: 11px"><center> {{number_format($detail->qty_retur,2)}} </center></td>
                                    <td style="font-size: 11px"><center> {{number_format($detail->harga_jual,2)}} </center></td>
                                    <td style="font-size: 11px"><center> {{number_format($detail->total_retur,2)}} </center></td>
                                </tr>
                                @endforeach                                                
                            </tbody>
                        </table>
                        <table width="100%">
                            <tr>
                                    <td style="font-size: 11px;font-family:Arial;" align="right" width="85%">Total Retur</td>
                                    <td style="font-size: 11px;font-family:Arial;" align="right"> {{number_format($returPembelian->total_retur,2)}}</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 11px;font-family:Arial;" align="right">Discount</td>
                                    <td style="font-size: 11px;font-family:Arial;" align="right"> {{number_format(0,2)}}</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 11px;font-family:Arial;" align="right">PPN</td>
                                    <td style="font-size: 11px;font-family:Arial;" align="right"> {{number_format($returPembelian->ppn_nom,2)}}</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 11px;font-family:Arial;" align="right">Grand Total</td>
                                    <td style="font-size: 11px;font-family:Arial;" align="right"> {{number_format($returPembelian->total_retur+$returPembelian->ppn_nom,2)}}</td>
                                </tr>
                        </table>                                    
                    </div>
                </div>
                <br>
                <!-- <div class="row">
                        <table width="100%">
                            <tr>
                                <td style="font-size: 12px" width="40%">n.b : </td>
                                <td style="font-size: 12px" width="20%"></td>
                                <td style="font-size: 12px" width="40%" align="center">Denpasar</td>
                            </tr>
                            <tr>
                                <td style="font-size: 12px" width="40%">- </td>
                                <td style="font-size: 12px" width="20%"></td>
                                <td style="font-size: 12px" width="40%" align="center">GrahaKita 88</td>
                            </tr>
                        </table>
                </div> -->
            </div>
        </div>
        <script>
            window.print();
        </script>
    </body>
</html>
