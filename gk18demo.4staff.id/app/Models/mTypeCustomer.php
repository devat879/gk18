<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mTypeCustomer extends Model
{
  public $incrementing = false;
  protected $table = 'tb_type_customer';
  protected $primaryKey = 'type_cus_kode';
  public $timestamps = false;
}
