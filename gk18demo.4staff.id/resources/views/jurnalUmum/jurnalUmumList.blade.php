@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />   
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn-unlock-jurnal').click(function(){
                var id_jurnal = $(this).data('todo').id;
                $('[name="jurnal_umum_id"]').val(id_jurnal);
                $('#unlock-jurnal').modal("show");

            });
        });
    </script>
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($message = Session::get('warning'))
        <div class="alert alert-warning alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
        </div>
    @endif
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="portlet-body">
                            <div class="col-md-6">
                                <a style="font-size: 12px;" class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button" href="#modal-pilih-periode" >
                                        Pilih Periode
                                </a>
                                @if($jurnal=='jurnal')                                
                                <a style="font-size:12px;" type="button" class="btn btn-success" href="{{route('tambahJurnalUmum')}}">
                                    <span><i class="fa fa-plus"></i></span> Tambah Jurnal Umum
                                </a>
                                @endif
                                @if($jurnal=='laporan')
                                <a style="font-size:12px;" type="button" class="btn btn-danger" href="{{route('printJurnalUmum', ['bulan'=>$start_date, 'tahun'=>$end_date])}}" target="_blank">
                                    <span><i class="fa fa-print"></i></span> Print
                                </a>
                                <a style="font-size: 12px;" class="btn btn-info excel-btn" data-toggle="modal" type="button" href="#export-excel" >
                                        Excel
                                </a>
                                @endif
                            </div>
                            <!-- <div class="pull-right">                                
                                <a style="font-size:12px;" type="button" class="btn btn-primary" href="{{route('postingBukuBesar')}}" @if($bulan <= $bulan_aktif) disabled @endif>
                                    <span><i class="fa fa-history"></i></span> Posting Jurnal {{$tanggal[$bulan_aktif+1]}}
                                </a>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="portlet-body">
                                <div class="col-md-12">
                                        <h2><center>Jurnal Umum</center></h2>
                                        <h3><center>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</center></h3>
                                </div>
                                <div id="data">
                                    <?php
                                        $total_debet = 0;
                                        $total_kredit = 0;
                                        $no = 1;
                                    ?>
                                    <table class="table table-striped table-bordered table-hover table-header-fixed">
                                        <thead>
                                            <tr class="">
                                                <th width="10" style="font-size:12px;"><center> No </center></th>
                                                <th style="font-size:12px;"><center> Tanggal </center></th>
                                                <th style="font-size:12px;"><center> No Bukti </center></th>
                                                <th style="font-size:12px;"><center> Keterangan </center></th>
                                                <th style="font-size:12px;"><center> No Akun </center></th>
                                                <th style="font-size:12px;"><center> Debet </center></th>
                                                <th style="font-size:12px;"><center> Kredit </center></th>
                                                <th style="font-size:12px;"><center> Catatan </center></th>
                                                @if($jurnal=='jurnal')
                                                <th style="font-size:12px;"><center> Menu </center></th>
                                                @endif
                                            </tr>
                                        </thead>                                        
                                        <tbody>
                                            @foreach($jurnalUmum as $jmu)
                                            <tr>
                                                <td style="font-size:11px;" align="center"> {{ $no++ }}. </td>
                                                    <td style="font-size:11px;"> {{ date('d M Y', strtotime($jmu->jmu_tanggal)) }} </td>
                                                    <td style="font-size:11px;"><center> {{ $jmu->no_invoice }} </center></td>
                                                    @if($jurnal=='laporan')
                                                    <td style="font-size:11px;" colspan="5"> {{ $jmu->jmu_keterangan }}</td>
                                                    @else
                                                    <td style="font-size:11px;" colspan="6"> {{ $jmu->jmu_keterangan }}</td>
                                                    @endif
                                            </tr>
                                            <?php $this_ju_debet = 0; $this_ju_kredit=0;?>
                                            @foreach($jmu->transaksi as $trs)
                                            <tr>                                
                                                <td></td>
                                                <td></td>
                                                <td></td>                                
                                                <td style="font-size:11px;" <?php if($trs->trs_jenis_transaksi=='kredit') echo 'align="right"';?>>   {{ $trs->trs_nama_rekening }}</td>
                                                <td style="font-size:11px;" <?php if($trs->trs_jenis_transaksi=='kredit') echo 'align="right"';?>>{{ $trs->trs_kode_rekening }}</td>
                                                <td style="font-size:11px;" align="right">   {{ number_format($trs->trs_debet,2) }} </td>
                                                <td style="font-size:11px;" align="right">   {{ number_format($trs->trs_kredit,2) }} </td>
                                                <td style="font-size:11px;">   {{ $trs->trs_catatan }} </td>
                                                @if($jurnal=='jurnal')
                                                <td>
                                                    <div class="btn-group btn-group-xs">
                                                        <a style="font-size:11px;" class="btn btn-success" href="{{ route('editJurnalUmum', ['kode'=>$trs->jurnal_umum_id]) }}" <?php if($jmu->edit=='no') echo 'disabled';?>>
                                                            <span class="icon-pencil"></span>
                                                        </a>
                                                        @if(\Gate::allows('as_master'))
                                                        <a type="button" name="btn-unlock-jurnal" class="btn btn-primary btn-unlock-jurnal" data-toggle="modal" data-todo='{"id":{{$jmu->jurnal_umum_id}}}'>
                                                            <span class="fa fa-check"></span>
                                                        </a>
                                                        @endif
                                                    </div>
                                                </td>
                                                @endif                                    
                                            </tr>
                                            <?php
                                                $this_ju_debet += $trs->trs_debet;
                                                $this_ju_kredit += $trs->trs_kredit;
                                                $total_debet += $trs->trs_debet;
                                                $total_kredit += $trs->trs_kredit;
                                            ?>
                                            @endforeach
                                            <tr>                                
                                                <td></td>
                                                <td></td>
                                                <td></td>                                
                                                <td style="font-size:11px;"> </td>
                                                <td style="font-size:11px;"></td>
                                                <td style="font-size:11px;font-weight: bold;" align="right">   {{ number_format($this_ju_debet,2) }} </td>
                                                <td style="font-size:11px;font-weight: bold;" align="right">   {{ number_format($this_ju_kredit,2) }} </td>
                                                <td style="font-size:11px;">
                                                @if(number_format($this_ju_debet,2)==number_format($this_ju_kredit,2))
                                                    balance
                                                @else
                                                    not balance
                                                @endif  {{number_format($this_ju_debet-$this_ju_kredit,2)}}</td>
                                                @if($jurnal=='jurnal')
                                                <td>

                                                </td>
                                                @endif                                    
                                            </tr>
                                            <tr>
                                                <td colspan="8"></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr class="">
                                                <th width="10"></th>
                                                <th colspan="4" align="center"><center><strong> TOTAL </strong></center></th>
                                                <th><center><strong>{{number_format($total_debet,2)}} </strong></center></th>
                                                <th><center><strong>{{number_format($total_kredit,2)}} </th>
                                                @if($jml_debet==$jml_kredit || $total_debet==0 && $total_kredit==0)
                                                <th>
                                                    <strong>Status : <font color="green">Balance</font></strong>
                                                </th>
                                                @endif
                                                @if($jml_debet!=$jml_kredit)
                                                <th>
                                                    <strong>Status : <font color="red">Not Balance {{number_format(($total_debet-$total_kredit),2)}}</font></strong>
                                                </th>
                                                @endif
                                                @if($jurnal=='jurnal')
                                                <th> </th>
                                                @endif
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div id="dataJurnal">
                                    
                                </div>
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Filter By
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('pilihPeriode') }}" class="form-horizontal form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Supplier</label>
                            </div>
                            <div class="col-md-9">
                                <select style="font-size: 11px" class="form-control" name="jenis">
                                    <option value="0">All</option>
                                    <option value="beli">Pembelian</option>
                                    <option value="jual">Penjualan</option>
                                    <option value="bayar">Pembayaran</option>
                                    <option value="jurnal">Jurnal</option>
                                    <option value="asset">Asset</option>                                  
                                </select>
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Tanggal</label>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date" value="{{$start_date}}" />
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                            <div class="col-md-1">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date"  value="{{$end_date}}"/>
                                <!-- <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="tgl_pencairan" /> -->
                                <input type="hidden" name="jurnal" value="{{$jurnal}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Search</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="export-excel" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Filter By
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('jurnalPrintExcel') }}" class="form-horizontal" role="form" method="post"  target="_blank">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Supplier</label>
                            </div>
                            <div class="col-md-9">
                                <select style="font-size: 11px" class="form-control" name="jenis">
                                    <option value="0">All</option>
                                    <option value="beli">Pembelian</option>
                                    <option value="jual">Penjualan</option>
                                    <option value="bayar">Pembayaran</option>
                                    <option value="jurnal">Jurnal</option>
                                    <option value="asset">Asset</option>                                  
                                </select>
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Tanggal</label>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date" value="{{$start_date}}" />
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                            <div class="col-md-1">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date"  value="{{$end_date}}"/>
                                <!-- <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="tgl_pencairan" /> -->
                                <input type="hidden" name="tipe_laporan" value="rekapPembelian">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Search</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-tambah-jurnal-umum" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header bg-green-meadow bg-font-green-meadow">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Tambah Jurnal Umum
                </h4>
            </div>
            <div class="modal-body form-horizontal">
                <form action="" class="form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                      <div class="form-group">
                        <label class="col-md-3">Tanggal Transaksi</label>
                            <div class="col-md-4">
                                <select class="form-control form-control-inline input-medium">
                                    <option value="">--Pilih Tanggal--</option>
                                    @foreach($tanggal as $tgl)
                                    <option value="{{$tgl}}">{{$tgl}}</option>
                                    @endforeach
                                </select>
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3">Kode Bukti</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="no_bg_cek">
                            </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3">Keterangan</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="keterangan">
                            </div>
                      </div>
                      <div class="form-group">
                         <div class="form-group">
                      <div class="col-md-4" style="margin-top: -20px">
                        <br>
                          <button type="button" class="btn btn-success btn-row-payment-plus" data-toggle="modal"> 
                            <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                          </button>
                      </div>
                  </div>
                      </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-header-fixed table-data-payment">
                    <thead>
                        <tr>
                            <th>Kode Perkiraan</th>
                            <th>Jenis Transaksi</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Tipe Arus Kas</th>
                            <th>Catatan</th>
                            <th>Menu</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>

                    
                    
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="unlock-jurnal" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Unlock Jurnal
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('unlock-jurnal') }}" class="form-horizontal" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">                       
                        
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Password</label>
                            </div>
                            <input type="password" name="password" class="form-control">
                            <input type="text" name="username" class="form-control" value="{{auth()->user()->username}}">
                            <input type="text" name="jurnal_umum_id" class="form-control">
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Search</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@stop