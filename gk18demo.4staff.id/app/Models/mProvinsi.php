<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mProvinsi extends Model
{
  public $incrementing = false;
  protected $table = 'tb_provinsi';
  protected $primaryKey = 'prov_kode';
  public $timestamps = false;

  public function wilayah()
  {
    return $this->hasMany('App\Models\mWilayah', 'prov_kode');
  }
}
