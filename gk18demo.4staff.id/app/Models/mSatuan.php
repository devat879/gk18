<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mSatuan extends Model
{
  public $incrementing = false;
  protected $table = 'tb_satuan';
  protected $primaryKey = 'stn_kode';
  public $timestamps = false;
}
