@extends('main/index')

@section('css')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function () {
  $('[name="cus_kode"]').change(function () {
    var cus_kode = $(this).val();
    var cus_alamat = $(this).find(':selected').attr('data-alamat');
    $('.cus_alamat').text(cus_alamat);
  });

  $('[name="pt_transaksi"]').change(function () {
    var pt_transaksi = $(this).val();
    if (pt_transaksi === 'kredit') {
      $('.div-kredit').removeClass('hide');
    } else {
      $('.div-kredit').addClass('hide');
    }
  });
});
</script>
@stop

@section('body')

{{ csrf_field() }}
<div class="page-content-inner">
  <div class="mt-content-body">
    <div class="row">
      <div class="col-xs-12">
        <div class="portlet light ">
          {!! $form !!}

          <br />
          <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
            <thead>
              <tr class="">
                <th width="10"> No </th>
                <th> Kode Customer </th>
                <th> Nama </th>
                <th> Transaksi </th>
                <th> Disc(%) </th>
                <th> Disc Nom. </th>
                <th> PPN </th>
                <th> PPN Nom. </th>
                <th> Ongkos Angkut </th>
                <th> Total </th>
                <th> Kirim Semua </th>
                <th> Aksi </th>
              </tr>
            </thead>
            <tbody>
              @foreach($penjualanTitipan as $r)
              <?php $nama = DB::table('tb_customer')->select('cus_nama')->where('cus_kode', $r->cus_kode)->first()->cus_nama; ?>
              <tr>
                <td> {{ $no++ }} . </td>
                <td> {{ $kodeCustomer.$r->cus_kode }}</td>
                <td> {{ $nama }} </td>
                <td> {{ $r->pt_transaksi }} </td>
                <td> {{ $r->pt_disc.'%' }} </td>
                <td> {{ $r->pt_disc_no }} </td>
                <td> {{ $r->pt_ppn.'%' }} </td>
                <td> {{ $r->pt_ppn_nom }} </td>
                <td> {{ $r->pt_ongkos_angkut }} </td>
                <td>  </td>
                <td> {{ $r->pt_kirim_semua }} </td>
                <td>
                  <div class="btn-group-vertical btn-group-sm">
                    <a href="{{ route('penjualanTitipanEdit', ['kode'=>$r->pt_no_faktur]) }}" class="btn btn-success" >
                      <span class="icon-pencil"></span> Edit
                      </button>
                      <a href="{{ route('detailPenjualanTitipanList', ['kode'=>$r->pt_no_faktur]) }}" class="btn btn-primary" >
                        <span class="icon-drawer"></span> Detail
                      </a>
                      <button class="btn btn-danger btn-delete" data-href="{{ route('penjualanTitipanDelete', ['kode'=>$r->pt_no_faktur]) }}">
                        <span class="icon-trash"></span> Delete
                      </button>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>




<div class="modal draggable-modal" id="modal-tambah" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-full">
    <div class="modal-content">
      <div class="modal-header bg-blue-steel bg-font-blue-steel">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">
          <i class="fa fa-plus"></i> Payment
        </h4>
      </div>
      <div class="modal-body form">
        <form action="{{ route('gudangInsert') }}" class="form-horizontal form-send" role="form" method="post">
          {{ csrf_field() }}
          <div class="form-body">
            <div class="form-group">
              <label class="col-md-3 control-label">Nama Gudang</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="gdg_nama">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Alamat Gudang</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="gdg_alamat">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Keterangan</label>
              <div class="col-md-9">
                <textarea class="form-control" name="gdg_keterangan"></textarea>
              </div>
            </div>
          </div>
          <div class="form-actions">
            <div class="row">
              <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn green btn-lg">
                  <i class="fa fa-save"></i> Simpan Terakhir
                </button>
                <button type="button" class="btn default btn-lg" data-dismiss="modal">Tutup</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal draggable-modal" id="modal-edit" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-green-meadow bg-font-green-meadow">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">
          <i class="fa fa-pencil"></i> Edit Gudang
        </h4>
      </div>
      <div class="modal-body form">
        <form action="" class="form-horizontal form-send" role="form" method="put">
          {{ csrf_field() }}
          <div class="form-body">
            <div class="form-group">
              <label class="col-md-3 control-label">Nama Gudang</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="gdg_nama">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Alamat Gudang</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="gdg_alamat">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Keterangan</label>
              <div class="col-md-9">
                <textarea class="form-control" name="gdg_keterangan"></textarea>
              </div>
            </div>
          </div>
          <div class="form-actions">
            <div class="row">
              <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn green">Simpan</button>
                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@stop