<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mAcMaster extends Model
{
  protected $table = 'tb_ac_master';
  protected $primaryKey = 'master_id';
  public $timestamps = false;
}
