<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use App\Models\mPenjualanLangsung;
use App\Models\mDetailPenjualanLangsung;
use App\Models\mPenjualanTitipan;
use App\Models\mDetailPenjualanTitipan;
use App\Models\mCustomer;
use App\Models\mBarang;

class API_Penjualan extends Controller
{
  public function index()
  {
    $penjualan = mPenjualanLangsung::all();
    foreach ($penjualan as $key) {
      $key->customer;
      $key->karyawan;
      $detail = $key->detail_PL;
      foreach ($detail as $keyDet) {
        $keyDet->barang;
      }
    }
    return response()->json([
      'penjualan' => $penjualan
    ]);
  }

  public function detail_customer($id='')
  {
    $penjualan = mPenjualanLangsung::where('cus_kode', $id)->get();
    foreach ($penjualan as $key) {
      $key->customer;
      $key->karyawan;
      $detail = $key->detail_PL;
      foreach ($detail as $keyDet) {
        $keyDet->barang;
      }
    }
    return response()->json([
      'penjualan' => $penjualan
    ]);
  }

  public function detail_karyawan($id='')
  {
    $penjualan = mPenjualanTitipan::select('pt_no_faktur', 'pt_tgl',
    'pt_jatuh_tempo', 'pt_tgl_jatuh_tempo', 'grand_total', 'pt_transaksi', 'cus_alamat',
    'cus_telp', 'cus_nama')
    ->with(['detail_PT:pt_no_faktur,detail_pt_kode,brg_kode,nama_barang,harga_net,qty,terkirim,retur,tgl_kirim_terakhir'])
    // ->with(['sj:pt_no_faktur,sjt_kode,sjt_tgl', 'sj.det_sj:sjt_kode,det_sjt_kode,brg_kode,brg_no_seri,dikirim'])
    ->where('pt_sales_person', $id)
    ->orderBy('pt_no_faktur', 'desc')
    ->get();

    // foreach ($penjualan as $key) {
    //   $detail = $key->detail_PT;
    // }

    return response()->json([
      'penjualan' => $penjualan
    ]);
  }

  public function best_seller_product()
  {
    $product = mBarang::select('brg_kode', 'brg_nama', 'ktg_kode')->has('detailPL')
    ->groupBy('ktg_kode', 'brg_kode')
    ->get();

    foreach ($product as $key => $value) {
      $t_qty = 0;
      $det = $value->detailPL;
      // $value->kategoryProduct;
      foreach ($det as $keyDet => $valueDet) {
        $t_qty += $valueDet->qty;
      }
      unset($value->detailPL);
      $value['qty'] = $t_qty;
    }

    $sorted = $product->sortByDesc('qty');
    $sorted->values()->all();

    $barang = array();
    $i=0;
    foreach ($sorted as $key => $value) {
      $barang[$i]=$value;
      $i++;
    }

    return response()->json([
      'product' => $barang
    ]);
  }
}
