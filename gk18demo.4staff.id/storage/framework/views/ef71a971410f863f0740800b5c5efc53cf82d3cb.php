<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/piutangPelanggan.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
<div class="page-content-inner">
    <div class="mt-content-body">
        <?php if($message = Session::get('success')): ?>
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong><?php echo e($message); ?></strong>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <br /><br />
                        <!-- <div class="col-md-10">                         -->
                            <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                                <thead>
                                    <tr class="">
                                        <th style="font-size: 12px;font-family:Arial;"><center> No </center></th>                                        
                                        <th style="font-size: 12px;font-family:Arial;"><center> No Faktur Retur </center></th>
                                        <th style="font-size: 12px;font-family:Arial;"><center> Tanggal </center></th>
                                        <th style="font-size: 12px;font-family:Arial;"><center> No Faktur Pembelian </center></th>
                                        <th style="font-size: 12px;font-family:Arial;"><center> Barcode Barang </center></th>
                                        <th style="font-size: 12px;font-family:Arial;"><center> Nama Barang </center></th>
                                        <th style="font-size: 12px;font-family:Arial;"><center> Qty Retur </center></th>
                                        <th style="font-size: 12px;font-family:Arial;"><center> Satuan </center></th>
                                        <th style="font-size: 12px;font-family:Arial;"><center> Supplier </center></th>
                                        <th style="font-size: 12px;font-family:Arial;"><center> Total Retur </center></th>
                                        <th style="font-size: 12px;font-family:Arial;"><center> Aksi </center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detailRetur): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td style="font-size: 11px;font-family:Arial;" align="center"> <?php echo e($no++); ?>. </td>                                    
                                    <td style="font-size: 11px;font-family:Arial;"> <?php echo e($detailRetur->no_retur_pembelian); ?> </td>
                                    <td style="font-size: 11px;font-family:Arial;"> <?php echo e(date('d M Y', strtotime($detailRetur->returPembelian->tgl_pengembalian))); ?> </td>
                                    <td style="font-size: 11px;font-family:Arial;"> <?php echo e($detailRetur->returPembelian->ps_no_faktur); ?> </td>
                                    <td style="font-size: 11px;font-family:Arial;"> <?php echo e($detailRetur->barang->brg_kode); ?> </td>
                                    <td style="font-size: 11px;font-family:Arial;"> <?php echo e($detailRetur->barang->brg_nama); ?> </td>                                    
                                    <td style="font-size: 11px;font-family:Arial;" align="center"> <?php echo e(number_format($detailRetur->qty_retur,2)); ?> </td>
                                    <td style="font-size: 11px;font-family:Arial;"> <?php echo e($detailRetur->barang->satuan->stn_nama); ?> </td>
                                    <td style="font-size: 11px;font-family:Arial;"> <?php echo e($detailRetur->returPembelian->pembelianSupplier->supplier->spl_nama); ?> </td>
                                    <td style="font-size: 11px;font-family:Arial;"> <?php echo e(number_format(($detailRetur->returPembelian->total_retur)+($detailRetur->returPembelian->ppn_nom),2)); ?> </td>
                                    <td>
                                        <div class="btn-group btn-group-xs">
                                            <a type="button" name="btn-print" class="btn btn-danger" target="_blank" href="<?php echo e(route('detailReturPembelian',['kode'=>$detailRetur->returPembelian->id])); ?>">
                                                <span class="fa fa-print"></span>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>