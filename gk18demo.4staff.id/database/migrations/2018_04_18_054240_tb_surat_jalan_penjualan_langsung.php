<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbSuratJalanPenjualanLangsung extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_surat_jalan_penjualan_langsung', function (Blueprint $table) {
         $table->increments('sjl_kode');
         $table->datetime('sjl_tgl')->nullable();
         $table->integer('pl_no_faktur')->nullable();
         $table->integer('cus_kode')->nullable();
         $table->integer('sjl_sopir')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_surat_jalan_penjualan_langsung');
     }
}
