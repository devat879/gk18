@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
  <script src="{{ asset('js/transferStok.js') }}" type="text/javascript"></script>
  <script type="text/javascript">
  let today = new Date().toISOString().substr(0, 10);
  $('input[name="trf_tgl"]').val(today);
  </script>
@stop

@section('body')
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <ul class="nav nav-tabs">
              <li class="active">
                <a href="#tab_1" data-toggle="tab"> Transfer </a>
              </li>
              <li>
                <a href="#tab_2" data-toggle="tab"> History Transfer </a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active in" id="tab_1">
                <span id="data-back"
                data-form-token="{{ csrf_token() }}"
                data-route-transfer-stok-barang-row="{{ route('penjualanLangsungBarangRow') }}"
                data-route-transfer-stok-gudang-row="{{ route('penjualanLangsungGudangRow') }}"
                data-route-transfer-stok-row="{{ route('penjualanLangsungStokRow') }}"
                ></span>
                <form action="{{route('transferStokInsert')}}" class="form-horizontal form-send" role="form" method="post">
                  {{ csrf_field() }}
                  <div class="form-body">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          {{-- <div class="input-daterange"> --}}
                            <label class="col-md-5 control-label">Tanggal Transfer</label>
                            <div class="col-md-7">
                              <input type="date" class="form-control" name="trf_tgl">
                            </div>
                          {{-- </div> --}}
                        </div>
                        <div class="form-group">
                          <label class="col-md-5 control-label">Keterangan</label>
                          <div class="col-md-7">
                            <textarea name="trf_keterangan" class="form-control" rows="2" cols="40"></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="col-md-5 control-label">Tujuan</label>
                          <div class="col-md-7">
                            <select class="form-control" name="gdg_kode">
                              @foreach($gudang as $gdg)
                                <option value="{{ $gdg->gdg_kode }}">{{ $gdg->gdg_nama }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col-md-2">
                        <button type="button" class="btn btn-success btn-block btn-row-plus">
                          <span class="fa fa-plus"></span> Tambah Data
                        </button>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="portlet light ">
                        <table class="table table-striped table-bordered table-hover table-header-fixed table-all-data">
                          <thead>
                            <tr class="">
                              <th> Barang Barcode </th>
                              <th> No Seri Barang </th>
                              <th> Nama Barang </th>
                              <th> Satuan </th>
                              <th> Asal Barang </th>
                              <th> Stok </th>
                              <th> Action </th>
                            </tr>
                          </thead>
                          <tbody>
                            {{-- <tr>
                              <td colspan="7">
                                <button type="button" class="btn btn-success btn-block btn-row-plus">Tambah Data</button>
                              </td>
                            </tr> --}}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="portlet light">
                      <button type="submit" class="btn btn-success btn-lg">SAVE</button>
                      <a href="{{ route('transferStokList') }}" class="btn btn-warning btn-lg">Cancel</a>
                    </div>
                  </div>
                </form>
              </div>
              <div class="tab-pane" id="tab_2">
                <div class="row">
                  <div class="portlet light ">
                    <table class="table table-striped table-bordered table-hover table-header-fixed">
                      <thead>
                        <tr class="">
                          <th> No Transfer </th>
                          <th> Tanggal </th>
                          <th> Tujuan </th>
                          <th> Keterangan </th>
                          <th> Action </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($history as $row)
                          <tr>
                            <td> {{ $row->trf_kode }} </td>
                            <td> {{ $row->trf_tgl }} </td>
                            <td> {{ $row->gudang['gdg_nama'] }} </td>
                            <td> {{ $row->trf_keterangan }} </td>
                            <td>
                              <div class="btn-group-xs">
                                <button class="btn btn-primary btn-detail" data-href="{{ route('transferStokgetTransferDetail', ['kode'=>$row->trf_kode]) }}">
                                  <span class="glyphicon glyphicon-info-sign"></span> Detail
                                </button>
                                <a href="{{route('transferStokPrint', ['kode'=>$row->trf_kode])}}" target="_blank" class="btn btn-success">
                                  <i class="glyphicon glyphicon-print"></i>Print</a>
                                </div>
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    {{-- Data sisipan --}}
    <table class="table-row-data hide">
      <tbody>
        <tr>
          <td class="brg_kode">
            <div class="form-inline input-group">
              {{-- <input type="text" name="brg_kode[]" class="form-control" required=""> --}}
              <input type="text" name="brg_barcode[]" class="form-control" required="">
              <input type="hidden" name="brg_kode[]" class="form-control">
              {{-- <select name="brg_kode[]" class="form-control" data-placeholder="Pilih Barang" required="">
                <option value="0">Pilih Barang</option>
                @foreach($dataList as $r)
                  <option value="{{ $r->brg_kode }}">{{ $r->brg_barcode }}</option>
                @endforeach
              </select> --}}
              <span class="input-group-btn">
                <button type="button" class="btn btn-success btn-modal-barang" data-toggle="modal">
                  <span class="glyphicon glyphicon-search"></span>
                </button>
              </span>
            </div>
          </td>
          <td class="brg_no_seri">
            <select class="form-control" name="brg_no_seri[]" required=""></select>
          </td>
          <td class="nama">
            <input type="text" class="form-control" name="nama[]" readonly>
          </td>
          <td class="satuan">
            <input type="hidden" class="form-control" name="satuan[]" readonly>
            <input type="text" class="form-control" name="stn_nama[]" readonly>
          </td>
          <td class="asal_gudang">
            <select class="form-control" name="asal_gudang[]" required="">
            </select>
          </td>
          <td class="stok">
            <input type="number" min="0" class="form-control" name="stok[]" id="stok" required="">
          </td>
          <td>
            <button type="button" class="btn btn-danger btn-row-delete">Delete</button>
          </td>
        </tr>
      </tbody>
    </table>

    <div class="modal" id="modal-detail" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-green-meadow bg-font-green-meadow">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">
              <i class="fa fa-pencil"></i> Detail Transfer
            </h4>
          </div>
          <div class="modal-body form">
            <div class="portlet light">
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                <thead>
                  <tr class="">
                    <th> Kode </th>
                    <th> Barang No Seri </th>
                    <th> Nama </th>
                    <th> Stok Transfer </th>
                    <th> Satuan </th>
                    <th> Asal Barang </th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal bs-modal-lg" id="modal-barang" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="btn-smpl2"></button>
            <h4 class="modal-title"> Daftar Barang </h4>
          </div>
          <div class="modal-body">
            <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_6">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kode</th>
                  <th>Barkode</th>
                  <th>Nama Barang</th>
                  <th>Satuan</th>
                  <th>Menu</th>
                </tr>
              </thead>
              <tbody>
                @foreach($barang as $r)
                  <tr>
                    <td>{{ $no_2++ }}.</td>
                    <td>{{ $r->brg_kode }}</td>
                    <td>{{ $r->brg_barcode }}</td>
                    <td>{{ $r->brg_nama }}</td>
                    <td>{{ $r->stn_nama}}</td>
                    <td style="white-space: nowrap">
                      <div class="btn-group-md">
                        <button class="btn btn-info btn-stok"
                        {{-- data-toggle="modal" data-target="#modal-stok" --}}
                        data-href="{{ route('penjualanTitipanGetStok', ['kode'=>$r->brg_kode]) }}">
                          <span class="icon-eye"></span> Lihat Stok
                        </button>

                        <button class="btn btn-success btn-pilih-barang"
                        data-brg-kode="{{ $r->brg_kode }}"
                        data-brg-barkode="{{ $r->brg_barcode }}"
                        data-brg-nama="{{ $r->brg_nama }}">
                          <span class="icon-plus"></span> Pilih Barang
                        </button>
                      </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="modal" id="modal-stok" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">
              Stok
            </h4>
          </div>
          <div class="modal-body form">
            <div class="form-body">
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_3">
                <thead>
                  <tr class="">
                    <th> No Seri </th>
                    <th> QTY </th>
                    <th> Titipan </th>
                    <th> Gudang </th>
                    <th> Supplier </th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  @stop
