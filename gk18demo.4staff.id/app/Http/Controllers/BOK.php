<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mKendaraanOperasional;
use App\Models\mBiayaKendaraan;
use App\Models\mJPK;
use Carbon\Carbon;
use App\Models\mJurnalUmum;
use App\Models\mTransaksi;
use App\Models\mPerkiraan;
use App\Models\mSupplier;
use App\Models\mHutangLain;
use DataTables;

use Validator;

class BOK extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Biaya Operasional Kendaraan';
      $this->kodeLabel = 'biaya_operasional_kendaraan';
  }

  protected function createID() {
    $kode = $this->main->kodeLabel('biaya_operasional_kendaraan');
    $user = '.'.\Auth::user()->kode;
    $preffix = date('ymd').'.'.$kode.'.';

    $lastorder = mBiayaKendaraan::where('biaya_kendaraan_kode', 'like', '%'.$preffix.'%')
    ->max('biaya_kendaraan_kode');

    if ($lastorder != null) {
      $lastno = substr($lastorder, 11, 4);
      $now = $lastno+1;
      $no = str_pad($now, 4, '0', STR_PAD_LEFT);
    } else {
      $no = '0001';
    }

    return $preffix.$no.$user;
  }

  protected function CreateIDHutangLain()
  {
    $kode = $this->main->kodeLabel('hutangLain');
    $user = '.'.\Auth::user()->kode;
    $preffix = date('ymd').'-'.$kode.'-';

    $lastorder = mHutangLain::where('no_hutang_lain', 'like', '%'.$preffix.'%')
    ->max('no_hutang_lain');

    if ($lastorder != null) {
      $lastno = substr($lastorder, 11, 4);
      $now = $lastno+1;
      $no = str_pad($now, 4, '0', STR_PAD_LEFT);
    } else {
      $no = '0001';
    }

    return $preffix.$no.$user;
  }

  function index() {
      $breadcrumb = [
          'Biaya Operasional Kendaraan'=>route('BOKList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'biaya_operasional_kendaraan';
      $data['title'] = $this->title;
      $data['dataList'] = mKendaraanOperasional::all();
      return view('BOK/kendaraanList', $data);
  }

  function rules($request) {
      $rules = [
          'plat_no' => 'required',
          'nama_kendaraan' => 'required',
          'pemilik' => 'required',
          'km_awal' => 'required',
          'km_akhir' => 'required',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
      $this->rules($request->all());
      mKendaraanOperasional::insert($request->except('_token'));
      return [
          'redirect'=>route('BOKList')
      ];
  }

  function edit($kendaraan_operasional_kode='') {
      $response = [
          'action'=>route('BOKUpdate', ['kode'=>$kendaraan_operasional_kode]),
          'field'=>mKendaraanOperasional::find($kendaraan_operasional_kode)
      ];
      return $response;
  }

  function update(Request $request, $kendaraan_operasional_kode) {
      $this->rules($request->all());
      mKendaraanOperasional::where('kendaraan_operasional_kode', $kendaraan_operasional_kode)->update($request->except('_token'));
      return [
          'redirect'=>route('BOKList')
      ];
  }

  function delete($kendaraan_operasional_kode) {
    \DB::beginTransaction();
    try {
      mKendaraanOperasional::where('kendaraan_operasional_kode', $kendaraan_operasional_kode)->delete();
      $biaya = mBiayaKendaraan::where('kendaraan_operasional_kode', $kendaraan_operasional_kode)->get();
      foreach ($biaya as $key => $value) {
        mHutangLain::where('ref_kode', $value->biaya_kendaraan_kode)->delete();
        $ju = mJurnalUmum::where('no_invoice', $value->biaya_kendaraan_kode)->first();
        mTransaksi::where('jurnal_umum_id', $ju->jurnal_umum_id)->delete();
        mJurnalUmum::where('no_invoice', $value->biaya_kendaraan_kode)->delete();
      }
      mBiayaKendaraan::where('kendaraan_operasional_kode', $kendaraan_operasional_kode)->delete();

      \DB::commit();
    } catch (\Exception $e) {
      \DB::rollBack();
      throw $e;
    }
  }

  function index_biaya($kendaraan_operasional_kode='') {
      $breadcrumb = [
          'Biaya Operasional Kendaraan'=>route('BOKList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'biaya_operasional_kendaraan';
      $data['title'] = $this->title;
      $data['dataList'] = mBiayaKendaraan::where('kendaraan_operasional_kode', $kendaraan_operasional_kode)->get();
      $total_biaya = 0;
      foreach ($data['dataList'] as $key => $value) {
        $total_biaya += $value->total;
        $value->biaya;
        $value->pembayaran;
        $value->spl;
      }
      $data['kendaraan_operasional_kode'] = $kendaraan_operasional_kode;
      $data['jpkLabel'] = $this->main->kodeLabel('jpk');
      $data['jpk'] = mJPK::get();
      $data['spl'] = mSupplier::get();
      // $data['perkiraan'] = $this->main->perkiraanBiaya();
      $data['perkiraan'] = $this->main->perkiraan();
      $data['perkiraanP'] = $this->main->perkiraan();
      $data['total_biaya'] = $total_biaya;
      // return $data['perkiraan'];
      return view('BOK/biayaList', $data);
  }

  function rules_biaya($request) {
      $rules = [
        'tanggal' => 'required',
        'jpk_kode' => 'required',
        'nama_biaya' => 'required',
        'total' => 'required',
        'keterangan' => 'required',
        'biaya_kode' => 'required',
        'pembayaran_kode' => 'required',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert_biaya(Request $request) {
    // return $request;
    $label = $this->main->kodeLabel('biaya_operasional_kendaraan');

    $this->rules_biaya($request->all());
    // mBiayaKendaraan::insert($request->except('_token'));

    \DB::beginTransaction();
    try {
      $biaya = new mBiayaKendaraan();
      $biaya->biaya_kendaraan_kode = $this->createID();
      $biaya->kendaraan_operasional_kode = $request->kendaraan_operasional_kode;
      $biaya->jpk_kode = $request->jpk_kode;
      $biaya->nama_biaya = $request->nama_biaya;
      $biaya->total = $request->total;
      $biaya->keterangan = $request->keterangan;
      $biaya->biaya_kode = $request->biaya_kode;
      $biaya->pembayaran_kode = $request->pembayaran_kode;
      $biaya->tanggal = $request->tanggal;
      $biaya->jatuh_tempo = $request->jatuh_tempo;
      $biaya->km_awal = $request->km_awal;
      $biaya->km_akhir = $request->km_akhir;
      $biaya->spl_kode = $request->spl_kode;
      $biaya->save();

      // $kendaraan = mKendaraanOperasional::where('biaya_kendaraan_kode', $request->kendaraan_operasional_kode)->first();
      // $kendaraan->km_awal = $request->km_awal;
      // $kendaraan->km_akhir = $request->km_akhir;
      // $kendaraan->save();

      // General
      $date_db = date('Y-m-d H:i:s');
      $year = date('Y');
      $month = date('m');
      $day = date('d');
      $where = [
        'jmu_year'=>$year,
        'jmu_month'=>$month,
        'jmu_day'=>$day
      ];
      $jmu_no = mJurnalUmum::where($where)
      ->orderBy('jmu_no','DESC')
      ->select('jmu_no')
      ->limit(1);
      if($jmu_no->count() == 0) {
        $jmu_no = 1;
      } else {
        $jmu_no = $jmu_no->first()->jmu_no + 1;
      }

      $jurnal_umum_id = mJurnalUmum::orderBy('jurnal_umum_id','DESC')
      ->limit(1)
      ->first();
      $jurnal_umum_id = $jurnal_umum_id['jurnal_umum_id'] + 1;

      $data_jurnal = [
        'jurnal_umum_id'=>$jurnal_umum_id,
        // 'no_invoice'=>$label.$biaya->biaya_kendaraan_kode,
        'no_invoice'=>$biaya->biaya_kendaraan_kode,
        'jmu_no'=>$jmu_no,
        'jmu_tanggal'=>$request->tanggal,
        'jmu_year'=>$year,
        'jmu_month'=>$month,
        'jmu_day'=>$day,
        'jmu_keterangan'=>$request->keterangan,
        'jmu_date_insert'=>$date_db,
        'jmu_date_update'=>$date_db,
        ];
        mJurnalUmum::insert($data_jurnal);

        $masterB = mPerkiraan::where('master_id', $request->biaya_kode)->first();
        $trs_kode_rekeningB = $masterB->mst_kode_rekening;
        $trs_nama_rekeningB = $masterB->mst_nama_rekening;

        $data_biaya = [
        'jurnal_umum_id'=>$jurnal_umum_id,
        'master_id'=>$request->biaya_kode,
        'trs_jenis_transaksi'=>'debet',
        'trs_debet'=> $request->total,
        'trs_kredit'=>0,
        'user_id'=>0,
        'trs_year'=>$year,
        'trs_month'=>$month,
        'trs_kode_rekening'=>$trs_kode_rekeningB,
        'trs_nama_rekening'=>$trs_nama_rekeningB,
        'trs_tipe_arus_kas'=>'Operasi',
        'trs_catatan'=>$request->keterangan,
        'trs_date_insert'=>$date_db,
        'trs_date_update'=>$date_db,
        'trs_charge'=>0,
        'tgl_transaksi'=>$request->tanggal,
        ];
        mTransaksi::insert($data_biaya);

        $masterP = mPerkiraan::where('master_id', $request->pembayaran_kode)->first();
        $trs_kode_rekeningP = $masterP->mst_kode_rekening;
        $trs_nama_rekeningP = $masterP->mst_nama_rekening;

        $data_pembayaran = [
        'jurnal_umum_id'=>$jurnal_umum_id,
        'master_id'=>$request->pembayaran_kode,
        'trs_jenis_transaksi'=>'kredit',
        'trs_debet'=> 0,
        'trs_kredit'=>$request->total,
        'user_id'=>0,
        'trs_year'=>$year,
        'trs_month'=>$month,
        'trs_kode_rekening'=>$trs_kode_rekeningP,
        'trs_nama_rekening'=>$trs_nama_rekeningP,
        'trs_tipe_arus_kas'=>'Operasi',
        'trs_catatan'=>$request->keterangan,
        'trs_date_insert'=>$date_db,
        'trs_date_update'=>$date_db,
        'trs_charge'=>0,
        'tgl_transaksi'=>$request->tanggal,
        ];
        mTransaksi::insert($data_pembayaran);

        if ($trs_kode_rekeningP == '2104') {
          $piutangLain = new mHutangLain();
          $piutangLain->no_hutang_lain = $this->CreateIDHutangLain();
          $piutangLain->hl_jatuh_tempo = $request->jatuh_tempo;
          $piutangLain->hl_tgl = $request->tanggal;
          $piutangLain->hl_dari = $request->spl_kode;
          $piutangLain->hl_amount = $request->total;
          $piutangLain->hl_sisa_amount = $request->total;
          $piutangLain->hs_keterangan = $request->keterangan;
          $piutangLain->hs_status = 'Belum Bayar';
          $piutangLain->kode_perkiraan = $trs_kode_rekeningP;
          $piutangLain->created_by = auth()->user()->karyawan->kry_nama;
          $piutangLain->updated_by = auth()->user()->karyawan->kry_nama;
          $piutangLain->akun_debet = $request->biaya_kode;
          $piutangLain->ref_kode = $biaya->biaya_kendaraan_kode;
          $piutangLain->save();
        }

        \DB::commit();
        return [
        'redirect'=>route('BiayaBOKList', ['kode'=>$request->kendaraan_operasional_kode])
        ];
      } catch (\Exception $e) {
        \DB::rollBack();
        throw $e;
      }
    }

  function edit_biaya($biaya_kendaraan_kode='') {
      $response = [
          'action'=>route('BiayaBOKUpdate', ['kode'=>$biaya_kendaraan_kode]),
          'field'=>mBiayaKendaraan::find($biaya_kendaraan_kode)
      ];
      return $response;
  }

  function update_biaya(Request $request, $biaya_kendaraan_kode) {
      // $this->rules_biaya($request->all());
      // mBiayaKendaraan::where('biaya_kendaraan_kode', $biaya_kendaraan_kode)->update($request->except('_token'));
      // return [
      //     'redirect'=>route('BiayaBOKList', ['kode'=>$request->kendaraan_operasional_kode])
      // ];

      $label = $this->main->kodeLabel('biaya_operasional_kendaraan');

      $this->rules_biaya($request->all());

      \DB::beginTransaction();
      try {
        $biaya = mBiayaKendaraan::where('biaya_kendaraan_kode', $biaya_kendaraan_kode)->first();
        $biaya->kendaraan_operasional_kode = $request->kendaraan_operasional_kode;
        $biaya->jpk_kode = $request->jpk_kode;
        $biaya->nama_biaya = $request->nama_biaya;
        $biaya->total = $request->total;
        $biaya->keterangan = $request->keterangan;
        $biaya->biaya_kode = $request->biaya_kode;
        $biaya->pembayaran_kode = $request->pembayaran_kode;
        $biaya->tanggal = $request->tanggal;
        $biaya->jatuh_tempo = $request->jatuh_tempo;
        $biaya->km_awal = $request->km_awal;
        $biaya->km_akhir = $request->km_akhir;
        $biaya->spl_kode = $request->spl_kode;
        $biaya->save();

        // General
        $date_db = date('Y-m-d H:i:s');
        $year = date('Y', strtotime($request->tanggal));
        $month = date('m', strtotime($request->tanggal));
        $day = date('d', strtotime($request->tanggal));

        $kode_bukti_id = $biaya_kendaraan_kode;
        // $kode_bukti_id = $label.$biaya_kendaraan_kode;
        $jurnalID = mJurnalUmum::select('jurnal_umum_id')->where('no_invoice', $kode_bukti_id)->first();
        if ($jurnalID) {
          $jurnalID->jmu_tanggal = $request->tanggal;
          $jurnalID->jmu_year = $year;
          $jurnalID->jmu_month = $month;
          $jurnalID->jmu_day = $day;
          $jurnalID->jmu_keterangan = $request->keterangan;
          $jurnalID->save();
        }
        else {
          $kode_bukti_id = $label.$biaya_kendaraan_kode;
          $jurnalID = mJurnalUmum::select('jurnal_umum_id')->where('no_invoice', $kode_bukti_id)->first();
          $jurnalID->jmu_tanggal = $request->tanggal;
          $jurnalID->jmu_year = $year;
          $jurnalID->jmu_month = $month;
          $jurnalID->jmu_day = $day;
          $jurnalID->jmu_keterangan = $request->keterangan;
          $jurnalID->save();
        }

        mTransaksi::where('jurnal_umum_id', $jurnalID->jurnal_umum_id)->delete();

          $masterB = mPerkiraan::where('master_id', $request->biaya_kode)->first();
          $trs_kode_rekeningB = $masterB->mst_kode_rekening;
          $trs_nama_rekeningB = $masterB->mst_nama_rekening;

          $data_biaya = [
          'jurnal_umum_id'=>$jurnalID['jurnal_umum_id'],
          'master_id'=>$request->biaya_kode,
          'trs_jenis_transaksi'=>'debet',
          'trs_debet'=> $request->total,
          'trs_kredit'=>0,
          'user_id'=>0,
          'trs_year'=>$year,
          'trs_month'=>$month,
          'trs_kode_rekening'=>$trs_kode_rekeningB,
          'trs_nama_rekening'=>$trs_nama_rekeningB,
          'trs_tipe_arus_kas'=>'Operasi',
          'trs_catatan'=>$request->keterangan,
          'trs_date_insert'=>$date_db,
          'trs_date_update'=>$date_db,
          'trs_charge'=>0,
          'tgl_transaksi'=>$request->tanggal,
          ];
          mTransaksi::insert($data_biaya);

          $masterP = mPerkiraan::where('master_id', $request->pembayaran_kode)->first();
          $trs_kode_rekeningP = $masterP->mst_kode_rekening;
          $trs_nama_rekeningP = $masterP->mst_nama_rekening;

          $data_pembayaran = [
          'jurnal_umum_id'=>$jurnalID['jurnal_umum_id'],
          'master_id'=>$request->pembayaran_kode,
          'trs_jenis_transaksi'=>'kredit',
          'trs_debet'=> 0,
          'trs_kredit'=>$request->total,
          'user_id'=>0,
          'trs_year'=>$year,
          'trs_month'=>$month,
          'trs_kode_rekening'=>$trs_kode_rekeningP,
          'trs_nama_rekening'=>$trs_nama_rekeningP,
          'trs_tipe_arus_kas'=>'Operasi',
          'trs_catatan'=>$request->keterangan,
          'trs_date_insert'=>$date_db,
          'trs_date_update'=>$date_db,
          'trs_charge'=>0,
          'tgl_transaksi'=>$request->tanggal,
          ];
          mTransaksi::insert($data_pembayaran);

          if ($trs_kode_rekeningP == '2104') {
            $piutangUpdate = mHutangLain::where('ref_kode', $biaya_kendaraan_kode)->first();
            if ($piutangUpdate) {
              $piutangLain = mHutangLain::where('ref_kode', $biaya_kendaraan_kode)->first();
              $piutangLain->no_hutang_lain = $this->CreateIDHutangLain();
              $piutangLain->hl_jatuh_tempo = $request->jatuh_tempo;
              $piutangLain->hl_tgl = $request->tanggal;
              $piutangLain->hl_dari = $request->spl_kode;
              $piutangLain->hl_amount = $request->total;
              $piutangLain->hl_sisa_amount = $request->total;
              $piutangLain->hs_keterangan = $request->keterangan;
              $piutangLain->hs_status = 'Belum Bayar';
              $piutangLain->kode_perkiraan = $trs_kode_rekeningP;
              $piutangLain->created_by = auth()->user()->karyawan->kry_nama;
              $piutangLain->updated_by = auth()->user()->karyawan->kry_nama;
              $piutangLain->akun_debet = $request->biaya_kode;
              $piutangLain->save();
            }
            else {
              $piutangLain = new mHutangLain();
              $piutangLain->no_hutang_lain = $this->CreateIDHutangLain();
              $piutangLain->hl_jatuh_tempo = $request->jatuh_tempo;
              $piutangLain->hl_tgl = $request->tanggal;
              $piutangLain->hl_dari = $request->spl_kode;
              $piutangLain->hl_amount = $request->total;
              $piutangLain->hl_sisa_amount = $request->total;
              $piutangLain->hs_keterangan = $request->keterangan;
              $piutangLain->hs_status = 'Belum Bayar';
              $piutangLain->kode_perkiraan = $trs_kode_rekeningP;
              $piutangLain->created_by = auth()->user()->karyawan->kry_nama;
              $piutangLain->updated_by = auth()->user()->karyawan->kry_nama;
              $piutangLain->akun_debet = $request->biaya_kode;
              $piutangLain->ref_kode = $biaya->biaya_kendaraan_kode;
              $piutangLain->save();
            }
          }

          \DB::commit();
          return [
              'redirect'=>route('BiayaBOKList', ['kode'=>$request->kendaraan_operasional_kode])
          ];
        } catch (\Exception $e) {
          \DB::rollBack();
          throw $e;
        }
  }

  function delete_biaya($biaya_kendaraan_kode) {
    \DB::beginTransaction();
    try {
      mBiayaKendaraan::where('biaya_kendaraan_kode', $biaya_kendaraan_kode)->delete();
      mHutangLain::where('ref_kode', $biaya_kendaraan_kode)->delete();
      $ju = mJurnalUmum::where('no_invoice', $biaya_kendaraan_kode)->first();
      mTransaksi::where('jurnal_umum_id', $ju->jurnal_umum_id)->delete();
      mJurnalUmum::where('no_invoice', $biaya_kendaraan_kode)->delete();

      \DB::commit();
    } catch (\Exception $e) {
      \DB::rollBack();
      throw $e;
    }
  }

  private function request_date($request)
  {
    $time = Carbon::now();
    if ($request->start_date == null && $request->end_date == null) {
      $start_date = $time;
      $end_date = $time;
    }
    elseif ($request->start_date == null || $request->end_date == null) {
      if ($request->start_date == null) {
        $start_date = $time;
        $end_date = $request->end_date;
      }
      else {
        $start_date = $request->start_date;
        $end_date = $time;
      }
    }
    else {
      $start_date = $request->start_date;
      $end_date = $request->end_date;
    }

    return [
        'start' => $start_date,
        'end'=> $end_date
    ];
  }

  function BiayaRow(Request $request) {
    $date = $this->request_date($request);

    if ($request->master_id != 'all') {
      $data = mBiayaKendaraan::
      // select('jpk_kode', 'spl_nama', 'nama_biaya', 'total', 'keterangan', 'km_awal', 'km_akhir',
      // 'biaya.mst_kode_rekening as kode_rekening_biaya', 'biaya.mst_nama_rekening as rekening_biaya',
      // 'pembayaran.mst_kode_rekening as kode_rekening_pembayaran', 'pembayaran.mst_nama_rekening as rekening_pembayaran')
      // ->
      where('kendaraan_operasional_kode', $request->kendaraan_operasional_kode)
      ->leftJoin('tb_ac_master as biaya', 'tb_biaya_kendaraan.biaya_kode', '=', 'biaya.master_id')
      ->leftJoin('tb_ac_master as pembayaran', 'tb_biaya_kendaraan.pembayaran_kode', '=', 'pembayaran.master_id')
      ->leftJoin('tb_supplier as spl', 'tb_biaya_kendaraan.spl_kode', '=', 'spl.spl_kode')
      ->where('tb_biaya_kendaraan.created_at', '>=', $date['start']. ' 00:00:00')
      ->where('tb_biaya_kendaraan.created_at', '<=', $date['end']. ' 23:59:59')
      ->where('tb_biaya_kendaraan.biaya_kode', $request->master_id)
      ->get();
    }
    else {
      $data = mBiayaKendaraan::
      // select('jpk_kode', 'spl_nama', 'nama_biaya', 'total', 'keterangan', 'km_awal', 'km_akhir',
      // 'biaya.mst_kode_rekening as kode_rekening_biaya', 'biaya.mst_nama_rekening as rekening_biaya',
      // 'pembayaran.mst_kode_rekening as kode_rekening_pembayaran', 'pembayaran.mst_nama_rekening as rekening_pembayaran')
      // ->
      where('kendaraan_operasional_kode', $request->kendaraan_operasional_kode)
      ->leftJoin('tb_ac_master as biaya', 'tb_biaya_kendaraan.biaya_kode', '=', 'biaya.master_id')
      ->leftJoin('tb_ac_master as pembayaran', 'tb_biaya_kendaraan.pembayaran_kode', '=', 'pembayaran.master_id')
      ->leftJoin('tb_supplier as spl', 'tb_biaya_kendaraan.spl_kode', '=', 'spl.spl_kode')
      ->where('tb_biaya_kendaraan.created_at', '>=', $date['start']. ' 00:00:00')
      ->where('tb_biaya_kendaraan.created_at', '<=', $date['end']. ' 23:59:59')
      ->get();
    }

    $total_biaya = 0;
    foreach ($data as $key => $value) {
      $total_biaya += $value->total;
      $biaya = $value->biaya;
      // $data['rekening_biaya'] = $biaya->mst_nama_rekening;

      $pembayaran = $value->pembayaran;
      // $data['rekening_pembayran'] = $pembayaran->mst_nama_rekening;
      $value->spl;
    }

    // $data['grand_total'] = $total_biaya;
    // return $data;
    // return Datatables::of($data)->make(true);
    return [
        'data' => $data,
        'total_biaya'=> number_format($total_biaya, 2, "," ,".")
    ];
  }

  function BiayaNamaRow(Request $request) {
    $data = mJPK::select('jpk_nama')
    ->where('jpk_kode', $request->jpk_kode)
    ->first();

    return $data;
  }
}
