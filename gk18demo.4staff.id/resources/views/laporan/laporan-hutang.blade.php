<html>
  <head>
    {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}">
    <style media="screen">
    .float{
      position:fixed;
      width:60px;
      height:60px;
      bottom:40px;
      right:40px;
      border-radius:50px;
      text-align:center;
      box-shadow: 2px 2px 3px #999;
      z-index: 100000;
    }
    .my-float{
      margin-top:22px;
    }
    </style>

    <script>
    function printDiv(divName){
      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
    }
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="glyphicon glyphicon-print"></i></button>
    </div>
    <div class="container-fluid" id='printMe'>
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4>Laporan Hutang</h4>
            <h4>Tanggal {{$start}} S/D {{$end}}</h4>
          </div>
          <br>
          <div class="portlet light ">
            <h4>Hutang Supplier</h4>
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">No. Faktur</th>
                  <th style="font-size:12px">Supplier</th>
                  {{-- <th>Nama</th> --}}
                  <!-- <th style="font-size:12px">Discount</th>
                  <th style="font-size:12px">Biaya Lain</th> -->
                  <th style="font-size:12px">Total</th>
                </tr>
              </thead>
              <tbody>
                @foreach($dataHS as $row)
                  <tr>
                    <td style="font-size:12px"> {{ $no++ }}. </td>
                    <td style="font-size:12px"> {{ date('d M Y', strtotime($row->ps_tgl)) }} </td>
                    <td style="font-size:12px"> {{ $row->no_pembelian }} </td>
                    <td style="font-size:12px"> {{ $row->spl_nama }} </td>
                    {{-- <td> {{ $row->ps_sales_person }} </td> --}}
                    <!-- <td style="font-size:12px" align="rigth"> {{ number_format($row->ps_disc_nom, 2, "." ,",")  }} </td>
                    <td style="font-size:12px" align="rigth"> {{ number_format($row->biaya_lain, 2, "." ,",")  }} </td> -->
                    <td style="font-size:12px" align="rigth"> {{ number_format($row->hs_amount, 2, "." ,",") }} </td>
                  </tr>
                @endforeach
                {{-- <tfoot> --}}
                  <tr>
                    <td colspan="4" align="center" style="font-weight:bold">Total</td>
                    <!-- <td style="font-weight:bold" align="right">{{number_format($DiscHS, 2, "." ,",") }}</td>
                    <td style="font-weight:bold" align="right">{{number_format($AngkutHS, 2, "." ,",") }}</td> -->
                    <td style="font-weight:bold" align="right">{{number_format($TotalHS, 2, "." ,",") }}</td>
                  </tr>
                {{-- </tfoot> --}}
              </tbody>
            </table>
            <br>
            <h4>Hutang Biaya</h4>
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">No. Faktur</th>
                  <th style="font-size:12px">Supplier</th>
                  {{-- <th>Nama</th> --}}
                  <!-- <th style="font-size:12px">Discount</th>
                  <th style="font-size:12px">Biaya Lain</th> -->
                  <th style="font-size:12px">Total</th>
                </tr>
              </thead>
              <tbody>
                @foreach($dataHB as $rowHB)
                <?php $no_4=1;?>
                  <tr>
                    <td style="font-size:12px"> {{ $no_4++ }}. </td>
                    <td style="font-size:12px"> {{ date('d M Y', strtotime($rowHB->ps_tgl)) }} </td>
                    <td style="font-size:12px"> {{ $rowHB->no_pembelian }} </td>
                    <td style="font-size:12px"> {{ $rowHB->spl_nama }} </td>
                    {{-- <td> {{ $rowHB->ps_sales_person }} </td> --}}
                    <!-- <td style="font-size:12px" align="rigth"> {{ number_format($row->ps_disc_nom, 2, "." ,",")  }} </td>
                    <td style="font-size:12px" align="rigth"> {{ number_format($row->biaya_lain, 2, "." ,",")  }} </td> -->
                    <td style="font-size:12px" align="rigth"> {{ number_format($rowHB->hs_amount, 2, "." ,",") }} </td>
                  </tr>
                @endforeach
                {{-- <tfoot> --}}
                  <tr>
                    <td colspan="4" align="center" style="font-weight:bold">Total</td>
                    <!-- <td style="font-weight:bold" align="right">{{number_format($DiscHS, 2, "." ,",") }}</td>
                    <td style="font-weight:bold" align="right">{{number_format($AngkutHS, 2, "." ,",") }}</td> -->
                    <td style="font-weight:bold" align="right">{{number_format($TotalHB, 2, "." ,",") }}</td>
                  </tr>
                {{-- </tfoot> --}}
              </tbody>
            </table>
            <br>
            <h4>Hutang Cek/Bg</h4>
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">Nama</th>
                  <th style="font-size:12px">No Cek/Bg</th>
                  <th style="font-size:12px">Total</th>
                </tr>
              </thead>
              <tbody>
                @foreach($dataHC as $hutang_cek)
                  <tr>
                    <td style="font-size:12px"> {{ $no_2++ }}. </td>
                    <td style="font-size:12px"> {{ date('d M Y', strtotime($hutang_cek->tgl_cek)) }} </td>
                    <td style="font-size:12px"> {{ $hutang_cek->suppliers->spl_nama }} </td>
                    <td style="font-size:12px"> {{ $hutang_cek->no_cek_bg }} </td>
                    <td style="font-size:12px" align="right"> {{ number_format($hutang_cek->total_cek, 2, "." ,",")  }} </td>
                  </tr>
                @endforeach
                {{-- <tfoot> --}}
                  <tr>
                    <td colspan="4" align="center" style="font-weight:bold">Total</td>
                    <td style="font-weight:bold" align="right">{{ number_format($TotalHC, 2, "." ,",")}}</td>
                  </tr>
                {{-- </tfoot> --}}
              </tbody>
            </table>
            <br>
            <h4>Hutang Lain</h4>
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">Nama</th>
                  <th style="font-size:12px">Total</th>
                </tr>
              </thead>
              <tbody>
                @foreach($dataHL as $row)
                <?php $no_3=1;?>
                  <tr>
                    <td style="font-size:12px"> {{ $no_3++ }}. </td>
                    <td style="font-size:12px"> {{ date('d M Y', strtotime($row->hl_tgl)) }} </td>
                    <td style="font-size:12px"> {{ $row->spl->spl_nama }} </td>
                    <td style="font-size:12px" align="right"> {{ number_format($row->hl_amount, 2, "." ,",")  }} </td>
                  </tr>
                @endforeach
                {{-- <tfoot> --}}
                  <tr>
                    <td colspan="3" align="center" style="font-weight:bold">Total</td>
                    <td style="font-weight:bold" align="right">{{ number_format($TotalHL, 2, "." ,",")}}</td>
                  </tr>
                {{-- </tfoot> --}}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
