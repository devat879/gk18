<table>
  <thead>
    <tr>
      <th> No </th>
      <th> Date </th>
      <th> Kode Barang </th>
      <th> Nama Barang </th>
      <th> Satuan </th>
      <th> Kategory </th>
      <th> Group Stok </th>
      <th> Merek </th>
      <th> Lokasi </th>
      <th> Total In </th>
      <th> Total Out </th>
      <th> Last Stok </th>
      <th> Keterangan </th>
    </tr>
  </thead>
  <tbody>
    @foreach($dataList as $row)
      <tr>
        <td> {{ $row->ars_stok_kode }} </td>
        <td> {{ date('Y-m-d', strtotime($row->ars_stok_date)) }} </td>
        <td> {{ $row->brg_barcode }} </td>
        <td> {{ $row->brg_nama }} </td>
        <td> {{ $row->stn_nama }} </td>
        <td> {{ $row->ktg_nama }} </td>
        <td> {{ $row->grp_nama }} </td>
        <td> {{ $row->mrk_nama }} </td>
        <td> {{ $row->gdg_nama }} </td>
        @if ($row->stok_in == null)
          <td> - </td>
        @else
          <td> {{ $row->stok_in }} </td>
        @endif
        @if ($row->stok_out == null)
          <td> - </td>
        @else
          <td> {{ $row->stok_out }} </td>
        @endif
        <td> {{ $row->stok_prev }} </td>
        <td> {{ $row->keterangan }} </td>
      </tr>
    @endforeach
  </tbody>
</table>
