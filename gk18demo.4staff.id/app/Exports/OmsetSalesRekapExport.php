<?php

namespace App\Exports;

use App\Models\mKaryawan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class OmsetSalesRekapExport implements FromView
{
    private $start;
    private $end;
    private $mrk_kode;

    public function __construct($start, $end, $mrk_kode)
    {
      $this->start = $start;
      $this->end = $end;
      $this->mrk_kode = $mrk_kode;
    }

    public function view(): View
    {
      $start = $this->start;
      $end = $this->end;
      $mrk_kode = $this->mrk_kode;

      $data['no'] = 1;
      $data['no_2'] = 1;
      $data['no_3'] = 1;

      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $data['start'] = $start;
      $data['end'] = $end;

      $rekap_total_jualPL = 0;
      $rekap_total_jualPT = 0;

      if ($mrk_kode != 'all') {
        $rowPL = mKaryawan::
        select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', \DB::raw('sum(tb_detail_penjualan_langsung.total) as total_jual, sum(tb_detail_penjualan_langsung.qty) as qty_jual'))
        ->Join('tb_penjualan_langsung', 'tb_karyawan.kry_kode','=','tb_penjualan_langsung.pl_sales_person')
        ->Join('tb_detail_penjualan_langsung', 'tb_penjualan_langsung.pl_no_faktur','=','tb_detail_penjualan_langsung.pl_no_faktur')
        ->Join('tb_barang', 'tb_detail_penjualan_langsung.brg_kode','=','tb_barang.brg_kode')
        ->where('pl_tgl', '>=', $start)
        ->where('pl_tgl', '<=', $end)
        ->where('tb_barang.mrk_kode', '=', $mrk_kode)
        ->groupBy('kry_kode')
        ->orderBy('kry_kode')
        ->get();

        $rowPT = mKaryawan::
        select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', \DB::raw('sum(tb_detail_penjualan_titipan.total) as total_jual, sum(tb_detail_penjualan_titipan.qty) as qty_jual'))
        ->Join('tb_penjualan_titipan', 'tb_karyawan.kry_kode','=','tb_penjualan_titipan.pt_sales_person')
        ->Join('tb_detail_penjualan_titipan', 'tb_penjualan_titipan.pt_no_faktur','=','tb_detail_penjualan_titipan.pt_no_faktur')
        ->Join('tb_barang', 'tb_detail_penjualan_titipan.brg_kode','=','tb_barang.brg_kode')
        ->where('pt_tgl', '>=', $start)
        ->where('pt_tgl', '<=', $end)
        ->where('pt_tgl', '<=', $end)
        ->where('tb_barang.mrk_kode', '=', $mrk_kode)
        ->groupBy('kry_kode')
        ->orderBy('kry_kode')
        ->get();
      }
      else {
        $rowPL = mKaryawan::
        select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', \DB::raw('sum(tb_detail_penjualan_langsung.total) as total_jual, sum(tb_detail_penjualan_langsung.qty) as qty_jual'))
        ->Join('tb_penjualan_langsung', 'tb_karyawan.kry_kode','=','tb_penjualan_langsung.pl_sales_person')
        ->Join('tb_detail_penjualan_langsung', 'tb_penjualan_langsung.pl_no_faktur','=','tb_detail_penjualan_langsung.pl_no_faktur')
        ->where('pl_tgl', '>=', $start)
        ->where('pl_tgl', '<=', $end)
        ->groupBy('kry_kode')
        ->orderBy('kry_kode')
        ->get();

        $rowPT = mKaryawan::
        select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', \DB::raw('sum(tb_detail_penjualan_titipan.total) as total_jual, sum(tb_detail_penjualan_titipan.qty) as qty_jual'))
        ->Join('tb_penjualan_titipan', 'tb_karyawan.kry_kode','=','tb_penjualan_titipan.pt_sales_person')
        ->Join('tb_detail_penjualan_titipan', 'tb_penjualan_titipan.pt_no_faktur','=','tb_detail_penjualan_titipan.pt_no_faktur')
        ->where('pt_tgl', '>=', $start)
        ->where('pt_tgl', '<=', $end)
        ->groupBy('kry_kode')
        ->orderBy('kry_kode')
        ->get();
      }

      foreach ($rowPL as $key => $value) {
        $rekap_total_jualPL += $value->total_jual;
      }

      foreach ($rowPT as $key => $value) {
        $rekap_total_jualPT += $value->total_jual;
      }

      $data['rekapTJPL'] = $rekap_total_jualPL;
      $data['rekapTJPT'] = $rekap_total_jualPT;

      $data['dataPL'] = $rowPL;
      $data['dataPT'] = $rowPT;

      return view('export.laporan-omset-sales-rekap', $data);
    }
}
