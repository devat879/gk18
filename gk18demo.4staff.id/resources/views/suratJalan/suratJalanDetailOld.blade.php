@extends('main/index')

@section('css')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
{{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
{{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
{{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
{{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
{{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
{{-- <script src="{{ asset('js/suratJalan.js') }}" type="text/javascript"></script> --}}
<script type="text/javascript">
$('.btn-sj-kirim').click(function() {
  var ini = $(this);
  $('#modal-kirim').data('sj_kirim', ini).modal('show');
});

$('.btn-qty-kirim').click(function() {
  var ini = $('#modal-kirim').data('sj_kirim');
  var qty = $('[name="qty_kirim"]').val();
  ini.parents('tr').children('td').children('input[name="dikirim[]"]').val(qty);
  $('#modal-kirim').modal('hide');
});

$('.btn-sj-stok').click(function() {
  var ini = $(this);
  $('#modal-stok').data('sj_stok', ini).modal('show');
});

// $('.btn-qty-stok').click(function() {
//   var ini = $('#modal-stok').data('sj_stok');
//   var qty = $('[name="qty_stok"]').val();
//   // ini.parents('tr').children('td').children('input[name="dikirim[]"]').val(qty);
//   $('#modal-stok').modal('hide');
// });

$('.form-send-SJstok').submit(function(e) {
  e.preventDefault();
  var ini = $('#modal-stok').data('sj_stok');
  var kode = $('input[name="kode"]').val();
  var type_penjualan = ini.parents('tr').children('td').children('input[name="type"]').val();
  var gdg_kode = ini.parents('tr').children('td').children('input[name="gdg_kode[]"]').val();
  var brg_kode = ini.parents('tr').children('td').children('input[name="brg_kode[]"]').val();
  var detail_kode = ini.parents('tr').children('td').children('input[name="detail_kode[]"]').val();
  // $('input[name="type_modal[]"]').val(type);
  // $('input[name="detail_kode_modal[]"]').val(detail_kode);
  var qty = $('[name="qty_stok"]').val();
  var token = $('#data-back').data('form-token');
  var data_send = {
    kode: kode,
    type_penjualan: type_penjualan,
    gdg_kode: gdg_kode,
    brg_kode: brg_kode,
    detail_kode: detail_kode,
    qty: qty,
    _token: token
  };

  $.ajax({
    url: $(this).attr('action'),
    type: $(this).attr('method'),
    // data: $(this).serialize(),
    data: data_send,
    success: function(data) {
      // console.log(data);
      window.location.href = data.redirect;
    },
    error: function(request, status, error) {
      // var json = JSON.parse(request.responseText);
      // $('.form-group').removeClass('has-error');
      // $('.help-block').remove();
      // $.each(json.errors, function(key, value) {
      //   $('.form-send [name="'+key+'"]').parents('.form-group').addClass('has-error');
      //   $('.form-send [name="'+key+'"]').after('<span class="help-block">'+value+'</span>');
      // });
    }
  });
  return false;
});

$('.form-send-SJ').submit(function(e) {
    e.preventDefault();
    var ini = $(this);
    //Save penjualan
    $.ajax({
        url: ini.attr('action'),
        type: ini.attr('method'),
        data: ini.serialize(),
        success: function(data) {
          console.log(data);
            if(data.redirect) {
                if (data.tipe == 'langsung') {
                  //Surat Jalan
                  var url_SJ = '/suratJalan/surat-jalan/langsung/'+data.faktur+'/'+data.do;
                  window.open(url_SJ, '_blank');

                  //DO
                  var url_DO = '/suratJalan/do/langsung/'+data.faktur+'/'+data.do;
                  window.open(url_DO, '_blank');
                }
                else {
                  //Surat Jalan
                  var url_SJ = '/suratJalan/surat-jalan/titipan/'+data.faktur+'/'+data.do;
                  window.open(url_SJ, '_blank');

                  //DO
                  var url_DO = '/suratJalan/do/titipan/'+data.faktur+'/'+data.do;
                  window.open(url_DO, '_blank');
                }

                window.location.href = data.redirect;
            }
        },
        error: function(request, status, error) {
            var json = JSON.parse(request.responseText);
            $('.form-group').removeClass('has-error');
            $('.help-block').remove();
            $.each(json.errors, function(key, value) {
                $('.form-send [name="'+key+'"]').parents('.form-group').addClass('has-error');
                $('.form-send [name="'+key+'"]').after('<span class="help-block">'+value+'</span>');
            });
        }
    });
    return false;
});
</script>
@stop

@section('body')
<span id="data-back" data-form-token="{{ csrf_token() }}"></span>
<div class="page-content-inner">
  <div class="mt-content-body">
    <div class="row">
      <div class="col-xs-12">
        <div class="portlet light ">
          <ul class="nav nav-tabs">
            <li class="active">
              <a href="#tab_1_1" data-toggle="tab"> List Order </a>
            </li>
            <li>
              <a href="#tab_1_2" data-toggle="tab"> History Surat Jalan </a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active in" id="tab_1_1">
              <form action="{{ route('suratJalanInsert') }}" method="post" class="form-send-SJ">
                {{ csrf_field() }}
                <input type="hidden" name="kode_bukti_id" value="{{ $noFaktur }}">
                <input type="hidden" name="kode" value="{{ $kode }}">
                <input type="hidden" name="tipe" value="{{ $tipe }}">
                <input type="hidden" name="tanggal" value="{{ $tanggal }}">
                <input type="hidden" name="cus_kode" value="{{ $row['cus_kode'] }}">
                <input type="hidden" name="cus_alamat" value="{{ $row['cus_alamat'] }}">
                <input type="hidden" name="cus_telp" value="{{ $row['cus_telp'] }}">
              <div class="row">
                <div class="col-xs-12 col-sm-6">
                  <table style="border-collapse: separate; border-spacing: 20px;">
                    <tr>
                      <td>No Faktur</td>
                      <td>:</td>
                      <td>{{ $noFaktur }}</td>
                    </tr>
                    @if ($row['cus_nama'] == null)
                      <tr>
                        <td>Pelanggan</td>
                        <td>:</td>
                        <td>Guest</td>
                      </tr>
                    @else
                      <tr>
                        <td>Pelanggan</td>
                        <td>:</td>
                        <td>{{ $row['cus_nama'] }}</td>
                      </tr>
                    @endif
                    @if ($row['cus_alamat'] == null)
                      <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td>-</td>
                      </tr>
                    @else
                      <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td>{{ $row['cus_alamat'] }}</td>
                      </tr>
                    @endif
                    @if ($row['cus_telp'] == null)
                      <tr>
                        <td>Telp</td>
                        <td>:</td>
                        <td>-</td>
                      </tr>
                    @else
                      <tr>
                        <td>Telp</td>
                        <td>:</td>
                        <td>{{ $row['cus_telp'] }}</td>
                      </tr>
                    @endif
                  </table>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <table style="border-collapse: separate; border-spacing: 20px;">
                    <tr>
                      <td>Tanggal</td>
                      <td>:</td>
                      <td>{{ $tanggal }}</td>
                    </tr>
                    <tr>
                      <td>No. Surat Jalan</td>
                      <td>:</td>
                      <td>{{ $do }}</td>
                    </tr>
                    @if ($tipe == 'langsung')
                      <tr>
                        <td>Batas Kirim</td>
                        <td>:</td>
                        <td>{{ date('Y-m-d', strtotime($row->pl_batas_kirim)) }}</td>
                      </tr>
                    @endif
                  </table>
                </div>
              </div>
              <br /><br />
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                <thead>
                <tr class="">
                  <th width="10"> No </th>
                  <th> Kode Gudang </th>
                  <th> Kode Barang </th>
                  <th> Satuan </th>
                  <th> Qty </th>
                  <th> Terkirim </th>
                  <th> Dikirim </th>
                  <th> Action </th>
                </tr>
                </thead>
                <tbody>
                @foreach($list as $row)
                  @if ($row->qty > $row->terkirim)
                    <tr>
                      <td> {{ $no++ }}. </td>
                      @if ($tipe == 'langsung')
                        <td class="hide"> <input type="text" min="0" class="form-control" name="type" value="langsung"> </td>
                        <td class="hide"> <input type="text" min="0" class="form-control" name="detail_kode[]" value="{{$row->detail_pl_kode}}"> </td>
                      @else
                        <td class="hide"> <input type="text" min="0" class="form-control" name="type" value="titipan"> </td>
                        <td class="hide"> <input type="text" min="0" class="form-control" name="detail_kode[]" value="{{$row->detail_pt_kode}}"> </td>
                      @endif
                      <td class="hide"> <input type="text" min="0" class="form-control" name="brg_no_seri[]" value="{{$row->brg_no_seri}}"> </td>
                      <td class="hide"> <input type="text" min="0" class="form-control" name="brg_kode[]" value="{{$row->brg_kode}}"> </td>
                      <td class="hide"> <input type="text" min="0" class="form-control" name="gdg_kode[]" value="{{$row->gudang}}"> </td>
                      <td class="hide"> <input type="text" min="0" class="form-control" name="stn_kode[]" value="{{$row->satuanJ->stn_kode}}"> </td>
                      <td class="hide"> <input type="text" min="0" class="form-control" name="harga_jual[]" value="{{$row->harga_jual}}"> </td>
                      <td class="hide"> <input type="text" min="0" class="form-control" name="disc[]" value="{{$row->disc}}"> </td>
                      <td class="hide"> <input type="text" min="0" class="form-control" name="brg_hpp[]" value="{{$row->brg_hpp}}"> </td>
                      <td> {{ $kodeGudang.$row->gudang }} </td>
                      <td> {{ $kodeBarang.$row->brg_kode }} </td>
                      <td> {{ $row->satuanJ->stn_nama }} </td>
                      <td> {{ $row->qty }} </td>
                      <td> {{ $row->terkirim }} </td>
                      <td> <input type="number" min="0" class="form-control" name="dikirim[]" value="0" readonly> </td>
                      <td style="white-space: nowrap">
                        <div class="btn-group-md">
                          <button type="button" class="btn btn-success btn-sj-kirim" data-toggle="modal">
                            <span class="icon-pencil"></span> Kirim
                          </button>
                          <button type="button" class="btn btn-danger btn-sj-stok" data-toggle="modal">
                            <span class="icon-pencil"></span> Kembalikan Stok
                          </button>
                        </div>
                      </td>
                    </tr>
                  @endif
                @endforeach
                </tbody>
              </table>
              <div class="row">
                <div class="col-xs-12 col-sm-4">
                  <div class="form-group">
                    <label>Checker</label>
                    <select name="checker" class="form-control">
                      @foreach($karyawan as $r)
                        <option value="{{ $r->kry_kode }}">{{ $kodeKaryawan.$r->kry_kode.' - '.$r->kry_nama }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Sopir</label>
                    <select name="sopir" class="form-control">
                      @foreach($karyawan as $r)
                        <option value="{{ $r->kry_kode }}">{{ $kodeKaryawan.$r->kry_kode.' - '.$r->kry_nama }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Catatan</label>
                    <textarea class="form-control" name="catatan"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-xs-12 col-sm-8">
                    <br /><br /><br /><br /><br /><br /><br /><br /><br />
                    <button type="submit" class="btn btn-success btn-lg">Simpan</button>
                    <a href="{{ route('suratJalanList') }}" class="btn btn-warning btn-lg">Batal</a>
                  </div>
                </div>
              </div>
              </form>
            </div>
            <div class="tab-pane" id="tab_1_2">

              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                <thead>
                <tr class="">
                  <th width="10"> No </th>
                  <th> Kode </th>
                  <th> Tanggal </th>
                  <th> No. Faktur </th>
                  <th> Pelanggan </th>
                  <th> Alamat </th>
                  <th> Sopir </th>
                  <th> Menu</th>
                </tr>
                </thead>
                <tbody>
                @foreach($history as $r)
                    <?php $sopir = DB::table('tb_karyawan')->select('kry_nama')->where('kry_kode', $r->$idSopir)->first()->kry_nama; ?>
                    <tr>
                      <td> {{ $no_2++ }} . </td>
                      @if ($tipe == 'langsung')
                        <td> {{ $kodeSuratJalanPL.$r->$idSurat }}</td>
                      @else
                        <td> {{ $kodeSuratJalanPT.$r->$idSurat }}</td>
                      @endif
                      <td> {{ date('Y-m-d', strtotime($r->$tglSurat)) }} </td>
                      @if ($tipe == 'langsung')
                        <td> {{ $kodePenjualan.$r->pl_no_faktur }} </td>
                      @else
                        <td> {{ $kodePenjualan.$r->pt_no_faktur }} </td>
                      @endif
                      @if ($rowx['cus_nama'] == null)
                        <td> Guest </td>
                      @else
                        <td> {{ $rowx['cus_nama'] }} </td>
                      @endif

                      @if ($rowx['cus_alamat']== null)
                        <td> - </td>
                      @else
                        <td> {{ $rowx['cus_alamat'] }} </td>
                      @endif

                      @if ($sopir == null)
                        <td> - </td>
                      @else
                        <td> {{ $sopir }}</td>
                      @endif
                      <td>
                        <div class="btn-group-vertical btn-group-sm">
                          <a href="{{ route('suratJalanCetakView', ['kode'=>$r->$idSurat, 'tipe'=>$tipe]) }}" class="btn btn-success" >
                            <span class="icon-pencil"></span> Cetak Surat Jalan </a>
                        </div>
                      </td>
                    </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="modal-kirim" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-blue-steel bg-font-blue-steel">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">
          Kirim
        </h4>
      </div>
      <div class="modal-body form">
        <div class="form-body">
          <div class="form-group">
            <label class="col-md-3 control-label">QTY</label>
            <div class="col-md-9">
              <input type="number" min="0" class="form-control" name="qty_kirim">
            </div>
          </div>
        </div>
        <br>
        <div class="form-actions">
          <div class="row">
            <div class="col-md-offset-3 col-md-9">
              <button type="button" class="btn green btn-qty-kirim">Simpan</button>
              <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="modal-stok" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-red bg-font-red">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">
          Kembalikan Stok
        </h4>
      </div>
      <div class="modal-body form">
        <form action="{{route('suratJalanReturnStok')}}" class="form-horizontal form-send-SJstok" role="form" method="post">
          {{ csrf_field() }}
          <div class="form-body">
            <div class="form-group">
              <label class="col-md-3 control-label">QTY</label>
              <div class="col-md-9">
                <input type="hidden" class="form-control" name="type_modal">
                <input type="hidden" class="form-control" name="detail_kode_modal">
                <input type="number" min="0" class="form-control" name="qty_stok" value="0">
              </div>
            </div>
          </div>
          <br>
          <div class="form-actions">
            <div class="row">
              <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn green btn-qty-stok">Simpan</button>
                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@stop
