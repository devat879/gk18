<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mMerek extends Model
{
  public $incrementing = false;
  protected $table = 'tb_merek';
  protected $primaryKey = 'mrk_kode';
  public $timestamps = false;
}
