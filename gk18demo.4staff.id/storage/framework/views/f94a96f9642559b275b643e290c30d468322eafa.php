<table>
  <thead>
    <tr>
      <th> No </th>
      <th> Date </th>
      <th> Kode Barang </th>
      <th> Nama Barang </th>
      <th> Satuan </th>
      <th> Kategory </th>
      <th> Group Stok </th>
      <th> Merek </th>
      <th> Lokasi </th>
      <th> Total In </th>
      <th> Total Out </th>
      <th> Last Stok </th>
      <th> Keterangan </th>
    </tr>
  </thead>
  <tbody>
    <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <tr>
        <td> <?php echo e($row->ars_stok_kode); ?> </td>
        <td> <?php echo e(date('Y-m-d', strtotime($row->ars_stok_date))); ?> </td>
        <td> <?php echo e($row->brg_barcode); ?> </td>
        <td> <?php echo e($row->brg_nama); ?> </td>
        <td> <?php echo e($row->stn_nama); ?> </td>
        <td> <?php echo e($row->ktg_nama); ?> </td>
        <td> <?php echo e($row->grp_nama); ?> </td>
        <td> <?php echo e($row->mrk_nama); ?> </td>
        <td> <?php echo e($row->gdg_nama); ?> </td>
        <?php if($row->stok_in == null): ?>
          <td> - </td>
        <?php else: ?>
          <td> <?php echo e($row->stok_in); ?> </td>
        <?php endif; ?>
        <?php if($row->stok_out == null): ?>
          <td> - </td>
        <?php else: ?>
          <td> <?php echo e($row->stok_out); ?> </td>
        <?php endif; ?>
        <td> <?php echo e($row->stok_prev); ?> </td>
        <td> <?php echo e($row->keterangan); ?> </td>
      </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </tbody>
</table>
