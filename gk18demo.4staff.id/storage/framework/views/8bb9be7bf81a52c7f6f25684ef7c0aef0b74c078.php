<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/global/plugins/bootstrap/css/bootstrap.min.css')); ?>">
    <style media="screen">
    .float{
      position:fixed;
      width:60px;
      height:60px;
      bottom:40px;
      right:40px;
      border-radius:50px;
      text-align:center;
      box-shadow: 2px 2px 3px #999;
      z-index: 100000;
    }
    .my-float{
      margin-top:22px;
    }
    </style>

    <script>
    function printDiv(divName){
      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
    }
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="glyphicon glyphicon-print"></i></button>
    </div>
    <div class="container-fluid" id='printMe'>
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4>Laporan Rekap Omset Sales</h4>
            <h4>Tanggal <?php echo e($start); ?> S/D <?php echo e($end); ?></h4>
          </div>
          <br>
          <h4>Penjualan Langsung</h4>
          <div class="portlet light ">
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Nama</th>
                  <th style="font-size:12px">QTY Jual</th>
                  <th style="font-size:12px">Rp. Jual</th>
                  
                </tr>
              </thead>
                <tbody>
                  <?php $__currentLoopData = $dataPL; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td style="font-size:12px"> <?php echo e($no++); ?>. </td>
                      <td style="font-size:12px"> <?php echo e($row->kry_nama); ?> </td>
                      <td style="font-size:12px" align="right"> <?php echo e(number_format($row->qty_jual, 2, "." ,",")); ?> </td>
                      <td style="font-size:12px" align="right"> <?php echo e(number_format($row->total_jual, 2, "." ,",")); ?> </td>
                    </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  
                    <tr>
                      <td colspan="3" align="right" style="font-weight:bold">Grand Total</td>
                      <td style="font-weight:bold" align="right"><?php echo e(number_format($rekapTJPL, 2, "." ,",")); ?></td>
                    </tr>
                  
              </tbody>
            </table>
          </div>
          <br>
          <h4>Penjualan Titipan</h4>
          <div class="portlet light ">
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Nama</th>
                  <th style="font-size:12px">QTY Jual</th>
                  <th style="font-size:12px">Rp. Jual</th>
                  
                </tr>
              </thead>
                <tbody>
                  <?php $__currentLoopData = $dataPT; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td style="font-size:12px"> <?php echo e($no_2++); ?>. </td>
                      <td style="font-size:12px"> <?php echo e($row->kry_nama); ?> </td>
                      <td style="font-size:12px" align="right"> <?php echo e(number_format($row->qty_jual, 2, "." ,",")); ?> </td>
                      <td style="font-size:12px" align="right"> <?php echo e(number_format($row->total_jual, 2, "." ,",")); ?> </td>
                    </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  
                    <tr>
                      <td colspan="3" align="right" style="font-weight:bold">Grand Total</td>
                      <td style="font-weight:bold" align="right"><?php echo e(number_format($rekapTJPT, 2, "." ,",")); ?></td>
                    </tr>
                  
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
