<table>
  <thead>
    <tr>
      <th>Barang Kode</th>
      <th>Barcode</th>
      <th>Nama Barang</th>
      <th>Gudang</th>
      <th>Merek</th>
      <th>Kategori</th>
      <th>Satuan</th>
      <th>Group</th>
      <th>Supplier</th>
      <th>Stok Max</th>
      <th>Stok Min</th>
      <th>Harga Beli Tertinggi</th>
      <th>Harga Beli Terakhir</th>
      <th>PPN Supplier</th>
      <th>Harga Beli + PPN</th>
      <th>HPP</th>
      <th>Harga Jual Eceran</th>
      <th>Harga Jual Partai</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
    <?php $__currentLoopData = $barang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <tr>
        <td><?php echo e($item->brg_kode); ?></td>
        <td><?php echo e($item->brg_barcode); ?></td>
        <td><?php echo e($item->brg_nama); ?></td>
        <td><?php echo e($item->gudang['gdg_nama']); ?></td>
        <td><?php echo e($item->merek['mrk_nama']); ?></td>
        <td><?php echo e($item->kategory['ktg_nama']); ?></td>
        <td><?php echo e($item->satuan['stn_nama']); ?></td>
        <td><?php echo e($item->group['grp_nama']); ?></td>
        <td><?php echo e($item->supplier['spl_nama']); ?></td>
        <td><?php echo e($item->brg_stok_maximum); ?></td>
        <td><?php echo e($item->brg_stok_minimum); ?></td>
        <td><?php echo e($item->brg_harga_beli_tertinggi); ?></td>
        <td><?php echo e($item->brg_harga_beli_terakhir); ?></td>
        <td><?php echo e($item->brg_ppn_dari_supplier_persen); ?></td>
        <td><?php echo e($item->hargaPPN); ?></td>
        <td><?php echo e($item->brg_hpp); ?></td>
        <td><?php echo e($item->brg_harga_jual_eceran); ?></td>
        <td><?php echo e($item->brg_harga_jual_partai); ?></td>
        <td><?php echo e($item->brg_status); ?></td>
      </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </tbody>
</table>
