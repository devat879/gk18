<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/piutangPelanggan.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
<style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
    .tg td{font-family:Tahoma;font-size:12px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
    .tg th{font-family:Tahoma;font-size:14px;font-weight:bold;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;text-align: center;}
</style>
<div class="page-content-inner">
    <div class="mt-content-body">
    <?php if($message = Session::get('success')): ?>
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong><?php echo e($message); ?></strong>
        </div>
    <?php endif; ?>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="portlet light">
                    <div class="portlet-body">
                        <a style="font-size: 12px;" class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button" href="#modal-pilih-periode">
                            Pilih Periode
                        </a>
                        <a style="font-size: 12px;" type="button" class="btn btn-danger" href="<?php echo e(route('printNeraca', ['start_date'=>$start_date, 'end_date'=>$end_date,$tipe='print'])); ?>" target="_blank">
                                    <span><i class="fa fa-print"></i></span> Print
                        </a>
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                <h3><center><strong>NERACA</strong></center></h3>
                                <h4><center><strong><?php echo e(date('d M Y', strtotime($start_date))); ?> s/d <?php echo e(date('d M Y', strtotime($end_date))); ?></strong></center></h4>

                                <table class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr class="success">
                                            <th width="25%"><center>ASET</center></th>
                                            <th width="25%"><center>LIABILITAS</center></th>
                                            <th width="25%"><center>EKUITAS</center></th>
                                            <th width="25%"><center>STATUS</center></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?php echo e(number_format($total_asset,2)); ?></td>
                                            <td><?php echo e(number_format($total_liabilitas,2)); ?></td>
                                            <td><?php echo e(number_format($total_ekuitas,2)); ?></td>
                                            
                                            <?php if($status=='BALANCE'): ?>
                                            <td><font color="green"><strong><?php echo e($status); ?> : <?php echo e(number_format($selisih,2)); ?></strong></font></td>
                                            <?php else: ?>
                                            <td><font color="red"><strong><?php echo e($status); ?> : <?php echo e(number_format($selisih,2)); ?></strong></font></td>
                                            <?php endif; ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        <?php
                            $total_assets = 0;
                            $total_liabilitass = 0;
                            $total_ekuitass = 0;
                        ?>
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                <table class="tg" width="100%">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Activa</th>
                                        </tr>
                                        <tr>
                                            <th>Description</th>
                                            <th>Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody><?php $activa = 0;?>
                                        <?php $__currentLoopData = $detail_perkiraan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($detail->mst_neraca_tipe == 'asset' && $detail->mst_master_id == '0' && $detail->mst_nama_rekening != 'AKTIVA TETAP'): ?>
                                            <tr>
                                                <td style="font-weight: bold;"><?php echo e($detail->mst_kode_rekening); ?> <?php echo e($detail->mst_nama_rekening); ?></td>
                                                    <?php
                                                        
                                                        $total_assets = $total_assets+$asset[$detail->mst_kode_rekening];
                                                    ?>
                                                <td style="font-weight: bold;" align="right"><?php echo e(number_format($asset[$detail->mst_kode_rekening],2)); ?></td>
                                            </tr><?php $activa++;?>
                                            <?php $__currentLoopData = $detail->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $det_child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo $space2; ?><?php echo e($det_child->mst_kode_rekening); ?> <?php echo e($det_child->mst_nama_rekening); ?></td>
                                                <?php
                                                    $total_assets = $total_assets+$asset[$det_child->mst_kode_rekening];
                                                ?>
                                                <td align="right"><?php echo e(number_format($asset[$det_child->mst_kode_rekening],2)); ?></td>
                                            </tr><?php $activa++;?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php
                                            $total_aktiva = 0;
                                        ?>
                                        <?php $__currentLoopData = $detail_perkiraan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($detail->mst_neraca_tipe == 'asset' && $detail->mst_master_id == '0' && $detail->mst_nama_rekening == 'AKTIVA TETAP'): ?>
                                            <tr>
                                                <td style="font-weight: bold;"><?php echo e($detail->mst_kode_rekening); ?> <?php echo e($detail->mst_nama_rekening); ?></td>
                                                <td style="font-weight: bold;" align="right"><?php echo e(number_format($asset[$detail->mst_kode_rekening],2)); ?></td>
                                            </tr><?php $activa++;?>
                                            <?php $__currentLoopData = $detail->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $det_child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td style="font-weight: bold;"><?php echo $space2; ?><?php echo e($det_child->mst_kode_rekening); ?> <?php echo e($det_child->mst_nama_rekening); ?></td>
                                                <td style="font-weight: bold;" align="right"><?php echo e(number_format($asset[$det_child->mst_kode_rekening],2)); ?></td>
                                            </tr><?php $activa++;?>
                                            <?php $__currentLoopData = $det_child->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo $space3; ?><?php echo e($child->mst_kode_rekening); ?> <?php echo e($child->mst_nama_rekening); ?></td>
                                                <td align="right"><?php echo e(number_format($asset[$child->mst_kode_rekening],2)); ?></td>
                                            </tr><?php $activa++;?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Total Aktiva</th>
                                            <th><?php echo e(number_format($total_asset,2)); ?></th>
                                        </tr>
                                    </tfoot>                                    
                                </table>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                <table class="tg" width="100%">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Pasiva</th>
                                        </tr>
                                        <tr>
                                            <th>Description</th>
                                            <th>Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody><?php $passiva = 0;?>
                                        <?php $__currentLoopData = $detail_perkiraan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($detail->mst_neraca_tipe == 'liabilitas' && $detail->mst_master_id == '0'): ?>
                                            <tr>
                                                <td style="font-weight: bold;"><?php echo e($detail->mst_kode_rekening); ?> <?php echo e($detail->mst_nama_rekening); ?></td>
                                                    
                                                <td style="font-weight: bold;" align="right"><?php echo e(number_format($liabilitas[$detail->mst_kode_rekening],2)); ?></td>
                                            </tr>
                                            <?php $passiva++;?>
                                            <?php $__currentLoopData = $detail->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $det_child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo $space2; ?><?php echo e($det_child->mst_kode_rekening); ?> <?php echo e($det_child->mst_nama_rekening); ?></td>
                                                
                                                <td align="right"><?php echo e(number_format($liabilitas[$det_child->mst_kode_rekening],2)); ?></td>
                                            </tr>
                                            <?php $passiva++;?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php $__currentLoopData = $detail_perkiraan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($detail->mst_neraca_tipe == 'ekuitas' && $detail->mst_master_id == '0'): ?>
                                            <tr>
                                                <td style="font-weight: bold;"><?php echo e($detail->mst_kode_rekening); ?> <?php echo e($detail->mst_nama_rekening); ?></td>
                                                    
                                                <td style="font-weight: bold;" align="right"><?php echo e(number_format($ekuitas[$detail->mst_kode_rekening],2)); ?></td>
                                            </tr>
                                            <?php $passiva++;?>
                                            <?php $__currentLoopData = $detail->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $det_child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo $space2; ?><?php echo e($det_child->mst_kode_rekening); ?> <?php echo e($det_child->mst_nama_rekening); ?></td>
                                                
                                                <td align="right"><?php echo e(number_format($ekuitas[$det_child->mst_kode_rekening],2)); ?></td>
                                            </tr><?php $passiva++;?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php $selisih = $activa-$passiva;?>
                                        <?php for($i=1;$i<=$selisih;$i++): ?>
                                        <tr>
                                            <td></td>
                                            <td align="right">0</td>
                                        </tr>
                                        <?php endfor; ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Total Pasiva</th>
                                            <th><?php echo e(number_format($hitung,2)); ?></th>
                                        </tr>
                                    </tfoot>                                    
                                </table>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Pilih Periode
                </h4>
            </div>
            <div class="modal-body form">
                <form action="<?php echo e(route('pilihPeriodeNeraca')); ?>" class="form-horizontal form-send" role="form" method="post">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date" value="<?php echo e($start_date); ?>" />
                            </div>
                            <div class="col-md-2">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date" value="<?php echo e($end_date); ?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>