

<?php $__env->startSection('css'); ?>
  <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
  
  <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
  <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
  
  
  
  
  <script src="<?php echo e(asset('assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/components-select2.min.js')); ?>" type="text/javascript"></script>

  <script src="<?php echo e(asset('assets/global/plugins/moment.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('js/penjualanLangsung.js')); ?>" type="text/javascript"></script>

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
  <!-- (Optional) Latest compiled and minified JavaScript translation files -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
  
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>

  <span id="data-back" data-kode-customer="<?php echo e($kodeCustomer); ?>"
  data-form-token="<?php echo e(csrf_token()); ?>"
  data-route-penjualan-langsung-barang-row="<?php echo e(route('penjualanLangsungBarangRow')); ?>"
  data-route-gudang-row="<?php echo e(route('penjualanLangsungGudangRow')); ?>"
  data-route-stok-row="<?php echo e(route('penjualanLangsungStokRow')); ?>">
</span>

<form class="form-send-penjualan" action="<?php echo e(route('updatePenjualanLangsung')); ?>" method="put" autocomplete="off">
  <?php echo e(csrf_field()); ?>

  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="row form-horizontal">
              <div class="col-xs-12 col-sm-6 ">
                <div class="form-body">
                  <div class="form-group">
                    <div class="col-xs-3 hide">
                      <input type="text" class="form-control" placeholder="Kode" name="cus_kode_label" value="<?php echo e($kodeCustomer); ?>" disabled>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-<?php echo e($col_label); ?>">Pelanggan</label><br/>
                    <div class="col-xs-4 hide" style="margin-top: -20px">
                      <select name="cus_kode" class="form-control" title="Pilih Customer" required>
                        
                        <?php $__currentLoopData = $customer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option value="<?php echo e($r->cus_kode); ?>"
                            data-cus-nama="<?php echo e($r->cus_nama); ?>"
                            data-alamat="<?php echo e($r->cus_alamat); ?>"
                            data-cus-telp="<?php echo e($r->cus_telp); ?>"
                            data-cus-tipe="<?php echo e($r->cus_tipe); ?>"
                            data-kry_kode="<?php echo e($r->kry_kode); ?>"
                            <?php echo e(($penjualanLangsung->cus_kode == $r->cus_kode) ? 'selected="selected"' : ''); ?>>
                            <?php echo e($r->cus_nama); ?>

                          </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>
                    </div>
                    <div class="col-md-4" style="margin-top: -20px">
                      <input type="text" class="form-control cus_nama" name="cus_nama" value="<?php echo e($penjualanLangsung->cus_nama); ?>">
                    </div>
                    <div class="col-md-3" style="margin-top: -20px">
                      <input type="text" class="form-control" placeholder="Tipe" name="cus_tipe" value="<?php echo e($cus_tipe); ?>" readonly>
                    </div>
                    <div class="col-md-2" style="margin-top: -20px">
                      <button type="button" class="btn btn-success btn-block btn-modal-customer" data-toggle="modal">
                        <span class="glyphicon glyphicon-search"></span>
                      </button>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-<?php echo e($col_label); ?>">Alamat</label><br />
                    <div class="col-md-<?php echo e($col_form); ?>" style="margin-top: -20px">
                      <input type="text" class="form-control cus_alamat" name="cus_alamat" value="<?php echo e($penjualanLangsung->cus_alamat); ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-<?php echo e($col_label); ?>">No Telephone</label><br />
                    <div class="col-md-<?php echo e($col_form); ?>" style="margin-top: -20px">
                      <input type="text" class="form-control cus_telp" name="cus_telp" value="<?php echo e($penjualanLangsung->cus_telp); ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-<?php echo e($col_label); ?>">Kecamatan</label><br />
                    <div class="col-md-<?php echo e($col_form); ?>" style="margin-top: -20px">
                      <input type="text" class="form-control kecamatan" name="kecamatan" value="<?php echo e($penjualanLangsung->cus_kecamatan); ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-<?php echo e($col_label); ?>">Kabupaten</label><br />
                    <div class="col-md-<?php echo e($col_form); ?>" style="margin-top: -20px">
                      <input type="text" class="form-control kabupaten" name="kabupaten" value="<?php echo e($penjualanLangsung->cus_kabupaten); ?>">
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6">
                <div class="form-body">
                  <div class="form-group">
                    <label class="col-md-<?php echo e($col_label); ?>">No Faktur</label>
                    <div class="col-md-<?php echo e($col_form); ?>">
                      <input type="hidden" name="no_faktur" value="<?php echo e($no_faktur); ?>">
                      <input type="text" class="form-control" placeholder="No Faktur" value="<?php echo e($no_faktur); ?>" disabled="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-<?php echo e($col_label); ?>" style="padding-top: 10px;">Transaksi</label>
                    <div class="col-md-<?php echo e($col_form); ?>">
                      <div class="mt-radio-inline">
                        <label class="mt-radio mt-radio-outline">
                          <input type="radio" name="pl_transaksi" id="optionsRadios22" value="cash" <?php echo e(($penjualanLangsung->pl_transaksi == 'cash') ? 'checked' : ''); ?>> Cash
                          <span></span>
                        </label>
                        <label class="mt-radio mt-radio-outline">
                          <input type="radio" name="pl_transaksi" id="optionsRadios23" value="kredit" <?php echo e(($penjualanLangsung->pl_transaksi == 'kredit') ? 'checked' : ''); ?>> Kredit
                          <span></span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-2">
                <button type="button" class="btn btn-success btn-block btn-row-plus">
                  <span class="fa fa-plus"></span> Tambah Data
                </button>
              </div>
            </div>
            <br>
            <table class="table table-striped table-bordered table-hover table-header-fixed table-all-data ">
              <thead>
                <tr class="">
                  <th width="14%"> Barcode </th>
                  <th width="10%"> Nama </th>
                  <th width="10%"> Nomer Seri </th>
                  <th width="10%"> Gudang </th>
                  <th class="hide"> Satuan </th>
                  <th class="hide"> Barang HPP </th>
                  <th class="hide"> Barang HPP Total </th>
                  <th width="10%"> Harga Jual </th>
                  <th width="6%"> Disc (%) </th>
                  <th class="hide"> Disc Nom </th>
                  <th width="6%" class=""> PPN (%) </th>
                  <th class="hide"> PPN Nom </th>
                  <th width="8%"> Harga Net </th>
                  <th width="6%"> Qty </th>
                  <th width="10%"> Total </th>
                  
                    <th width="8%"> Aksi </th>
                  </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = $detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td class="brg_kode">
                        <div class="form-inline input-group">
                          <input type="text" name="brg_barcode[]" class="form-control" value="<?php echo e($value->barang->brg_barcode); ?>" required="">
                          <input type="hidden" name="brg_kode[]" class="form-control" value="<?php echo e($value->barang->brg_kode); ?>">
                          <span class="input-group-btn">
                            <button type="button" class="btn btn-success btn-modal-barang" data-toggle="modal">
                              <span class="glyphicon glyphicon-search"></span>
                            </button>
                          </span>
                        </div>
                      </td>
                      <td class="nama">
                        <input type="text" class="form-control" name="nama[]" value="<?php echo e($value->barang->brg_nama); ?>" readonly>
                      </td>
                      <td class="brg_no_seri">
                        <select name="brg_no_seri[]" class="form-control" data-placeholder="Pilih No Seri" required="">
                          <option value="<?php echo e($value->brg_no_seri); ?>"><?php echo e($value->brg_no_seri); ?></option>
                        </select>
                      </td>
                      <td class="gdg_kode">
                        <select name="gdg_kode[]" class="form-control" data-placeholder="Pilih Gudang" required="">
                          <?php if($value->gdg): ?>
                            <option value="<?php echo e($value->gudang); ?>"><?php echo e($value->gdg->gdg_nama); ?></option>
                          <?php else: ?>
                            <option value="<?php echo e($value->gudang); ?>">Gudang 0</option>
                          <?php endif; ?>
                        </select>
                      </td>
                      <td class="satuan hide">
                        <input type="text" name="satuan[]" class="form-control" value="<?php echo e($value->satuan); ?>" readonly>
                      </td>
                      <td class="brg_hpp hide">
                        <input type="number" class="form-control" name="brg_hpp[]" value="<?php echo e($value->brg_hpp); ?>" min="0" readonly>
                      </td>
                      
                      <td class="harga_jual">
                        <select class="form-control" name="harga_jual[]" required="">
                          <option value="<?php echo e($value->harga_jual); ?>"><?php echo e($value->harga_jual); ?></option>
                        </select>
                      </td>
                      <td class="disc">
                        <input type="number" class="form-control" name="disc[]" min="0" max="100" value="<?php echo e($value->disc); ?>">
                      </td>
                      <td class="disc_nom hide">
                        <input type="number" class="form-control" name="disc_nom[]" value="<?php echo e($value->disc_nom); ?>" readonly>
                      </td>
                      <td class="ppn">
                        <input type="number" class="form-control" name="ppn[]" min="0" max="100" value="<?php echo e($value->ppn); ?>" readonly>
                      </td>
                      <td class="ppn_nom hide">
                        <input type="number" class="form-control" name="ppn_nom[]" value="<?php echo e($value->ppn_nom); ?>" readonly>
                      </td>
                      <td class="harga_net">
                        <input type="number" class="form-control" name="harga_net[]" value="<?php echo e($value->harga_net); ?>" readonly>
                      </td>
                      <td class="qty">
                        <input type="number" class="form-control" step="0.01" name="qty[]" value="<?php echo e($value->qty); ?>" min="0" required>
                      </td>
                      <td class="brg_hpp_total hide">
                        <input type="number" class="form-control" name="brg_hpp_total[]" value="<?php echo e($value->brg_hpp * $value->qty); ?>" min="0" readonly>
                      </td>
                      <td class="total_nom_ppn hide">
                        <input type="number" class="form-control" name="total_nom_ppn[]" value="<?php echo e($value->total_nom_ppn); ?>" min="0" readonly>
                      </td>
                      <td class="total_nom_disc hide">
                        <input type="number" class="form-control" name="total_nom_disc[]" value="<?php echo e($value->disc_nom * $value->qty); ?>" min="0" readonly>
                      </td>
                      <td class="total_harga_jual hide">
                        <input type="number" class="form-control" name="total_harga_jual[]" value="<?php echo e($value->harga_jual * $value->qty); ?>" min="0" readonly>
                      </td>
                      <td class="total">
                        <input type="number" class="form-control" name="total[]" value="<?php echo e($value->total); ?>" min="0" readonly>
                      </td>
                      <td>
                        <button type="button" class="btn btn-danger btn-row-delete">Delete</button>
                      </td>
                    </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
              </table>

              <hr/>
              <div class="row">
                <div class="col-xs-12 col-sm-4 form-horizontal">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-<?php echo e($col_label); ?>">Sales Person</label>
                      <div class="col-md-<?php echo e($col_form); ?>">
                        <select name="pl_sales_person" class="select2" required>
                          <?php $__currentLoopData = $karyawan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($r->kry_kode); ?>" <?php echo e(($penjualanLangsung->pl_sales_person == $r->kry_kode) ? 'selected="selected"' : ''); ?>><?php echo e($kodeKaryawan.$r->kry_kode.' - '.$r->kry_nama); ?></option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group  hide">
                      <label class="col-md-<?php echo e($col_label); ?>">Checker</label>
                      <div class="col-md-<?php echo e($col_form); ?>">
                        <select name="pl_checker" class="select2">
                          <?php $__currentLoopData = $karyawan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($r->kry_kode); ?>" <?php echo e(($penjualanLangsung->pl_checker == $r->kry_kode) ? 'selected="selected"' : ''); ?>><?php echo e($kodeKaryawan.$r->kry_kode.' - '.$r->kry_nama); ?></option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group  hide">
                      <label class="col-md-<?php echo e($col_label); ?>">Sopir</label>
                      <div  class="col-md-<?php echo e($col_form); ?>">
                        <select name="pl_sopir" class="select2">
                          <?php $__currentLoopData = $karyawan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($r->kry_kode); ?>" <?php echo e(($penjualanLangsung->pl_sopir == $r->kry_kode) ? 'selected="selected"' : ''); ?>><?php echo e($kodeKaryawan.$r->kry_kode.' - '.$r->kry_nama); ?></option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label  class="col-md-3">DO/SJ</label>
                      <div  class="col-md-9">
                        <div class="mt-radio-inline">
                          <label class="mt-radio mt-radio-outline">
                            <input type="radio" name="pl_kirim_semua" id="optionsRadios23" value="tidak" <?php echo e(($penjualanLangsung->pl_kirim_semua == 'tidak') ? 'checked' : ''); ?>> Ya
                            <span></span>
                          </label>
                          <label class="mt-radio mt-radio-outline">
                            <input type="radio" name="pl_kirim_semua" id="optionsRadios22" value="ya" <?php echo e(($penjualanLangsung->pl_kirim_semua == 'ya') ? 'checked' : ''); ?>> Tidak
                            <span></span>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group hide">
                      <label class="col-md-<?php echo e($col_label); ?>">Batas Kirim</label>
                      <div class="col-md-<?php echo e($col_form); ?>">
                        <input type="text" name="pl_batas_kirim" class="form-control date-picker" value="<?php echo e($penjualanLangsung->pl_batas_kirim); ?>" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-<?php echo e($col_label); ?>">Catatan</label>
                      <div  class="col-md-<?php echo e($col_form); ?>">
                        <textarea class="form-control" name="pl_catatan"><?php echo e($penjualanLangsung->pl_catatan); ?></textarea>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-4 form-horizontal">
                  <?php if($penjualanLangsung->pl_transaksi == 'cash'): ?>
                    <div class="form-body form-kredit hide">
                  <?php else: ?>
                    <div class="form-body form-kredit">
                  <?php endif; ?>
                    <div class="form-group">
                      <label class="col-md-3">Lama Kredit</label>
                      <div class="col-md-9">
                        <select class="form-control" name="pl_lama_kredit" required>
                          <option value="<?php echo e($penjualanLangsung->pl_lama_kredit); ?>" selected><?php echo e($penjualanLangsung->pl_lama_kredit); ?> hari</option>
                          <option value="0">0 hari</option>
                          <option value="7">7 hari</option>
                          <option value="15">15 hari</option>
                          <option value="30">30 hari</option>
                          <option value="45">45 hari</option>
                          <option value="60">60 hari</option>
                        </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3">Jatuh Tempo</label>
                        <div class="col-md-9">
                          <input type="text" name="pl_tgl_jatuh_tempo" class="form-control date-picker" readonly>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4 form-horizontal">
                    <div class="form-body">
                      <div class="form-group hide">
                        <label class="col-md-3">Sub Total Barang</label>
                        <div class="col-md-9">
                          <input type="text" class="form-control" name="pl_subtotal_barang" value="0" readonly>
                        </div>
                      </div>
                      <div class="form-group hide">
                        <label class="col-md-3">Sub Total Nom Disc</label>
                        <div class="col-md-9">
                          <input type="text" class="form-control" name="pl_subtotal_nom_disc" value="0" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3">Sub Total</label>
                        <div class="col-md-9">
                          <input type="text" class="form-control" name="pl_subtotal" value="0" readonly>
                        </div>
                      </div>
                      <div class="form-group hide">
                        <label class="col-md-3">Total HPP</label>
                        <div class="col-md-9">
                          <input type="text" class="form-control" name="pl_total_hpp" value="0" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3">Disc (%)</label>
                        <div  class="col-md-9">
                          <input type="number" class="form-control" name="pl_disc" value="<?php echo e($penjualanLangsung->pl_disc); ?>" min="0" max="100">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3">Disc Nom</label>
                        <div  class="col-md-9">
                          <input type="text" class="form-control" name="pl_disc_nom" value="<?php echo e($penjualanLangsung->pl_disc_nom); ?>">
                        </div>
                      </div>
                      <div class="form-group hide">
                        <label class="col-md-3">Ppn</label>
                        <div  class="col-md-9">
                          
                          <select class="form-control" name="pl_ppn">
                            <option value="0" selected>0</option>
                            <option value="10">10</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group hide">
                        <label class="col-md-3">PPN Nom</label>
                        <div  class="col-md-9">
                          <input type="text" class="form-control" name="pl_ppn_nom" value="0" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3">Ongkos Angkut</label>
                        <div  class="col-md-9">
                          <input type="number" class="form-control" name="pl_ongkos_angkut" value="0">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3">Charge (%)</label>
                        <div  class="col-md-9">
                          <input type="number" min="0" max="100" class="form-control" name="charge_persen" step="0.01" value="<?php echo e($penjualanLangsung->charge_persen); ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3">Charge Nom</label>
                        <div  class="col-md-9">
                          <input type="text" class="form-control" name="charge_nom" step="0.01" value="<?php echo e($penjualanLangsung->charge_nom); ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3">Grand Total</label>
                        <div   class="col-md-9">
                          <input type="number" class="form-control" name="grand_total" value="0" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <button type="button" class="btn btn-success btn-lg btn-block" data-toggle="modal" href="#modal-payment">SAVE</button>
                        <a href="<?php echo e(route('daftarPenjualanLangsung')); ?>" class="btn btn-warning btn-lg btn-block">Batal</a>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal" id="modal-payment" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog modal-full">
          <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title"> Payment </h4>
            </div>
            <div class="modal-body form-horizontal">
              <div class="row">
                <div class="col-xs-12 col-sm-6">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-3">Kode Bukti</label>
                      <div class="col-md-9">
                        <input type="text" name="kode_bukti_id" class="form-control" value="<?php echo e($no_faktur); ?>" readonly>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-4">
                  <button type="button" class="btn btn-success btn-row-payment-plus">
                    <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                  </button>
                </div>
              </div>
              <br>
              <table class="table table-striped table-bordered table-hover table-header-fixed table-data-payment">
                <thead>
                  <tr>
                    <th>Kode Perkiraan</th>
                    <th>Payment</th>
                    <th class="hide">Charge(%)</th>
                    
                    <th>Total</th>
                    <th>No. Cek/BG</th>
                    <th>Tanggal Pencairan</th>
                    <th>Keterangan</th>
                    <th class="hide">Setor</th>
                    <th class="hide">Kembalian</th>
                    <th>Menu</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
              <br />
              <div class="row">
                <div class="col-xs-2 col-md-2">
                  <h3>Total</h3>
                </div>
                <div class="col-xs-2 col-md-2">
                  <h3 class="nominal-grand-total">0</h3>
                </div>
              </div>
              <div id="sisa_uang" class="row">
                <div class="col-xs-2 col-md-2">
                  <h3>Sisa</h3>
                </div>
                <div class="col-xs-2 col-md-2">
                  <h3 class="nominal-sisa">0</h3>
                  <input type="hidden" step="0.01" name="sisa_uang" value="">
                </div>
              </div>
              <div id="kembalian_uang" class="row">
                <div class="col-xs-2 col-md-2">
                  <h3>Kembalian</h3>
                </div>
                <div class="col-xs-2 col-md-2">
                  <h3 class="nominal-kembalian">0</h3>
                  <input type="hidden" step="0.01" name="kembalian_uang" value="">
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12 col-md-4 col-md-offset-8">
                  <div class="btn-group">
                    <button type="button" id="btn-modal-kertas" class="btn btn-success btn-lg">SAVE</button>
                    <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Cancel</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal" id="modal-kertas" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title">
                <i class="fa fa-print"></i> Pilih Ukuran Kertas
              </h4>
            </div>
            <div class="modal-body form">
              <div class="form-body">
                <div class="form-group">
                  <label class="col-md-3">Ukuran Kertas</label>
                  <div  class="col-md-9">
                    <div class="mt-radio-inline">
                      <label class="mt-radio mt-radio-outline">
                        <input type="radio" name="ukuran_kertas" value="besar" checked=""> Besar
                        <span></span>
                      </label>
                      <label class="mt-radio mt-radio-outline">
                        <input type="radio" name="ukuran_kertas" value="kecil"> Kecil
                        <span></span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <br>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">Cetak</button>
                    <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    <div class="modal bs-modal-lg" id="modal-customer" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="btn-smpl1"></button>
            <h4 class="modal-title"> Daftar Customer </h4>
          </div>
          <div class="modal-body">
            <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_5">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tipe</th>
                  <th>Kode</th>
                  <th>Nama</th>
                  <th>Alamat</th>
                  <th>No HP</th>
                  <th>Menu</th>
                </tr>
              </thead>
              <tbody>
                
                <?php $__currentLoopData = $customer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td><?php echo e($no++); ?>.</td>
                      <td><?php echo e($r->cus_tipe); ?></td>
                      <td><?php echo e($kodeCustomer.$r->cus_kode); ?></td>
                      <td><?php echo e($r->cus_nama); ?></td>
                      <td><?php echo e($r->cus_alamat); ?></td>
                      <td><?php echo e($r->cus_telp); ?></td>
                      <td style="white-space: nowrap">
                        <div class="btn-group-md">
                          <?php if($r->lewat > 0 || $r->bg > 0 || $r->banned != 0): ?>
                            <button class="btn btn-danger btn-pilih-customer-piutang"
                            data-toggle="modal"
                            data-cus-kode="<?php echo e($r->cus_kode); ?>"
                            data-cus-nama="<?php echo e($r->cus_nama); ?>"
                            data-cus-alamat="<?php echo e($r->cus_alamat); ?>"
                            data-cus-telp="<?php echo e($r->cus_telp); ?>"
                            data-cus-tipe="<?php echo e($r->cus_tipe); ?>">
                            <span class="icon-plus"></span> Pilih</button>
                          <?php else: ?>
                            <button class="btn btn-success btn-pilih-customer"
                            data-cus-kode="<?php echo e($r->cus_kode); ?>"
                            data-cus-nama="<?php echo e($r->cus_nama); ?>"
                            data-cus-alamat="<?php echo e($r->cus_alamat); ?>"
                            data-cus-telp="<?php echo e($r->cus_telp); ?>"
                            data-cus-tipe="<?php echo e($r->cus_tipe); ?>">
                            <span class="icon-plus"></span> Pilih</button>
                          <?php endif; ?>

                          <?php if($r->lewat > 0): ?>
                            <button class="btn btn-info btn-piutang"
                            data-href="<?php echo e(route('penjualanLangsungGetPiutang', ['kode'=>$r->cus_kode])); ?>">
                            <span class="icon-eye"></span> Piutang</button>
                          <?php endif; ?>

                          <?php if($r->bg > 0): ?>
                            <button class="btn btn-info btn-cek_bg"
                            data-href="<?php echo e(route('penjualanLangsungGetCheque', ['kode'=>$r->cus_kode])); ?>">
                            <span class="icon-eye"></span> Cek/BG</button>
                          <?php endif; ?>
                        </div>
                      </td>
                    </tr>
                    
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="modal bs-modal-lg" id="modal-barang" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="btn-smpl2"></button>
            <h4 class="modal-title"> Daftar Barang </h4>
          </div>
          <div class="modal-body">
            <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_6">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kode</th>
                  <th>Barkode</th>
                  <th>Nama Barang</th>
                  <th>Satuan</th>
                  <th>Menu</th>
                </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $barang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr>
                    <td><?php echo e($no_2++); ?>.</td>
                    <td><?php echo e($r->brg_kode); ?></td>
                    <td><?php echo e($r->brg_barcode); ?></td>
                    <td><?php echo e($r->brg_nama); ?></td>
                    <td><?php echo e($r->stn_nama); ?></td>
                    <td style="white-space: nowrap">
                      <div class="btn-group-md">
                        <button class="btn btn-info btn-stok"
                        
                        data-href="<?php echo e(route('penjualanTitipanGetStok', ['kode'=>$r->brg_kode])); ?>">
                          <span class="icon-eye"></span> Lihat Stok
                        </button>

                        <button class="btn btn-success btn-pilih-barang"
                        data-brg-kode="<?php echo e($r->brg_kode); ?>"
                        data-brg-barkode="<?php echo e($r->brg_barcode); ?>"
                        data-brg-nama="<?php echo e($r->brg_nama); ?>">
                          <span class="icon-plus"></span> Pilih Barang
                        </button>
                      </div>
                    </td>
                  </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <table class="table-row-payment hide">
      <tbody>
        <tr>
          <td class="master_id">
            <select name="master_id[]" class="form-control selectpickerx" data-live-search="true">
              <option value="">Tipe Pembayaran</option>
              <?php $__currentLoopData = $perkiraan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($r->master_id); ?>"
                  data-master-id="<?php echo e($r->master_id); ?>"
                  data-mst-kode-rekening="<?php echo e($r->mst_kode_rekening); ?>"
                  data-mst-nama-rekening="<?php echo e($r->mst_nama_rekening); ?>"
                  data-content="<?php echo e($r->mst_kode_rekening.' - '.$r->mst_nama_rekening); ?>">
                  <?php echo e($r->mst_kode_rekening.' - '.$r->mst_nama_rekening); ?>

                </option>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
          </td>
          <td class="payment">
            <input type="number" name="payment[]" step="0.01" class="form-control" value="0">
          </td>
          <td class="charge hide">
            <input type="number" name="charge[]" step="0.01" class="form-control" value="0">
          </td>
          
          <td class="payment_total">
            <input type="number" name="payment_total[]" step="0.01" class="form-control" value="0" readonly>
          </td>
          <td class="no_check_bg">
            <input type="text" name="no_check_bg[]" class="form-control" value="-" readonly>
          </td>
          <td>
            <input type="text" name="tgl_pencairan[]" class="form-control" data-date-format="yyyy-mm-dd" value="<?php echo e(date('Y-m-d')); ?>">
          </td>
          <td class="keterangan">
            <input type="text" name="keterangan[]" class="form-control">
          </td>
          <td class="setor hide">
            <input type="number" name="setor[]" class="form-control" value="0">
          </td>
          <td class="kembalian hide">
            -
          </td>
          <td>
            <button class="btn btn-danger btn-payment-delete btn-xs btn-row-delete-payment">Hapus</button>
          </td>
        </tr>
      </tbody>
    </table>

    
    <table class="table-row-data hide" id="table-data-barang">
      <tbody>
        <tr>
          <td class="brg_kode">
            <div class="form-inline input-group">
              <input type="text" name="brg_barcode[]" class="form-control" required="">
              <input type="hidden" name="brg_kode[]" class="form-control">
              
              <span class="input-group-btn">
                <button type="button" class="btn btn-success btn-modal-barang" data-toggle="modal">
                  <span class="glyphicon glyphicon-search"></span>
                </button>
              </span>
            </div>
          </td>
          <td class="nama">
            <input type="text" class="form-control" name="nama[]" readonly>
          </td>
          <td class="brg_no_seri">
            <select name="brg_no_seri[]" class="form-control" data-placeholder="Pilih No Seri" required="">
              
            </select>
          </td>
          <td class="gdg_kode">
            <select name="gdg_kode[]" class="form-control" data-placeholder="Pilih Gudang" required="">
              
            </select>
          </td>
          <td class="satuan hide">
            <input type="text" name="satuan[]" class="form-control" readonly>
          </td>
          <td class="brg_hpp hide">
            <input type="number" class="form-control" name="brg_hpp[]" value="0" min="0" readonly>
          </td>
          <td class="brg_hpp_total hide">
            <input type="number" class="form-control" name="brg_hpp_total[]" value="0" min="0" readonly>
          </td>
          
          <td class="harga_jual">
            <select class="form-control" name="harga_jual[]" required=""></select>
          </td>
          <td class="disc">
            <input type="number" class="form-control" name="disc[]" min="0" max="100" value="0">
          </td>
          <td class="disc_nom hide">
            <input type="number" class="form-control" name="disc_nom[]" readonly>
          </td>
          <td class="ppn">
            <input type="number" class="form-control" name="ppn[]" min="0" max="100" value="0" readonly>
          </td>
          <td class="ppn_nom hide">
            <input type="number" class="form-control" name="ppn_nom[]" readonly>
          </td>
          <td class="harga_net">
            <input type="number" class="form-control" name="harga_net[]" readonly>
          </td>
          <td class="qty">
            <input type="number" class="form-control" name="qty[]" value="0" min="0" required>
          </td>
          <td class="total_nom_ppn hide">
            <input type="number" class="form-control" name="total_nom_ppn[]" value="0" min="0" readonly>
          </td>
          <td class="total_nom_disc hide">
            <input type="number" class="form-control" name="total_nom_disc[]" value="0" min="0" readonly>
          </td>
          <td class="total_harga_jual hide">
            <input type="number" class="form-control" name="total_harga_jual[]" value="0" min="0" readonly>
          </td>
          <td class="total">
            <input type="number" class="form-control" name="total[]" value="0" min="0" readonly>
          </td>
          
          <td>
            <button type="button" class="btn btn-danger btn-row-delete">Delete</button>
          </td>
        </tr>
      </tbody>
    </table>

    <div class="modal" id="modal-stok" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">
              Stok
            </h4>
          </div>
          <div class="modal-body form">
            <div class="form-body">
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_3">
                <thead>
                  <tr class="">
                    <th> No Seri </th>
                    <th> QTY </th>
                    <th> Titipan </th>
                    <th> Gudang </th>
                    <th> Supplier </th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal" id="modal-piutang" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">
              Piutang
            </h4>
          </div>
          <div class="modal-body form">
            <div class="form-body">
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_4">
                <thead>
                  <tr class="">
                    <th> No </th>
                    <th> Jatuh Tempo </th>
                    <th> No Faktur </th>
                    
                    <th> Total </th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal" id="modal-piutang-pass" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-red bg-font-red">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">
              Piutang Password
            </h4>
          </div>
          <div class="modal-body form">
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Username</label>
                <div class="col-md-9">
                  <input type="username" class="form-control" name="username">
                </div>
              </div>
            </div>
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Password</label>
                <div class="col-md-9">
                  <input type="password" class="form-control" name="password">
                </div>
              </div>
            </div>
            <br>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="button" class="btn green btn-save-pilih-customer-piutang">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal" id="modal-cek_bg" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">
              Cek/BG
            </h4>
          </div>
          <div class="modal-body form">
            <div class="form-body">
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_7">
                <thead>
                  <tr class="">
                    <th> Tgl Pencairan </th>
                    <th> No BG Cek</th>
                    <th> Cek Amount </th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <style media="screen">
    .sweet-overlay {
      z-index: 100000 !important;
    }

    .sweet-alert {
      z-index: 100001 !important;
    }
    </style>
  <?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>