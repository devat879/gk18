<?php
 
use App\Models\mWoSupplier;
use App\Models\mCustomer;
use App\Models\mKaryawan;

$customer = mCustomer::all();
$karyawan = mKaryawan::all();
?>
<div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> Payment </h4>
            </div>
            <div class="modal-body form-horizontal">
                <form action="{{ route('pencairanPiutangCek') }}" method="post" id="form-payment-hutang-supplier">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3">Tgl Transaksi</label>
                            <div class="col-md-4">
                              <input class="form-control date-picker" size="16" type="text" name="tgl_transaksi" data-date-format="yyyy-mm-dd" required="required" />
                            <!-- </td> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">No Invoice</label>
                            <div class="col-md-4">
                              <input type="text" name="no_transaksi" class="form-control" value="{{$next_no_trs}}" readonly="readonly">
                              <input type="hidden" name="hs_kode" class="form-control" value="{{$piutang_cek->hs_kode}}">
                              <input type="hidden" name="id" class="form-control" value="{{$piutang_cek->id}}">
                              <input type="hidden" name="ps_no_faktur" class="form-control" value="{{$piutang_cek->ps_no_faktur}}">
                              <input type="hidden" name="spl_kode" class="form-control" value="{{$piutang_cek->spl_kode}}">
                            <!-- </td> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">No Cek/Bg</label>
                            <div class="col-md-4">
                              <input type="text" name="no_bg_cek" class="form-control" value="{{$piutang_cek->no_bg_cek}}" readonly="readonly">
                            <!-- </td> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <!-- <label class="col-md-3">Dari</label> -->
                            <div class="col-md-4">
                              <input type="hidden" name="cek_dari" class="form-control" value="{{$piutang_cek->cek_dari}}">
                            <!-- </td> -->
                            </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-4" style="margin-top: -20px">
                            <br>
                              <button type="button" class="btn btn-success btn-row-payment-plus"> 
                                <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                              </button>
                          </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-header-fixed table-data-payment">
                        <thead>
                            <tr>
                                <th>Kode Perkiraan</th>
                                <th>Jumlah</th>
                                <th>Total</th>
                                <th>Keterangan</th>
                                <th>Menu</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- <tr>
                                <td colspan="14">
                                    <button type="button" class="btn btn-success btn-block btn-row-payment-plus">
                                        <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                                    </button>
                                </td>
                            </tr> -->
                        </tbody>
                    </table>
                    <br />
                    <div class="row">
                        <div class="col-xs-12 col-md-3 col-lg-3">
                            <h2>Total</h2>
                        </div>
                        <div class="col-xs-12 col-md-3 col-lg-3">
                            <h2 class="nominal-grand-total" name="nominal-grand-total" id="nominal-grand-total">Rp. {{number_format($piutang_cek->sisa,2)}}</h2>
                            <input type="hidden" name="amount" step=".01" value="{{$piutang_cek->sisa}}">
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-3 col-lg-3">
                                <h2>Sisa</h2>
                            </div>
                            <div class="col-xs-12 col-md-3 col-lg-3">
                                <h2 class="nominal-sisa">Rp. {{number_format($piutang_cek->sisa,2)}}</h2>
                                <input type="hidden" name="amount_sisa" value="{{$piutang_cek->sisa}}" step=".01">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-4 col-md-offset-8">
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-success btn-lg" id="submit-payment-hutang-supplier">SAVE</button>
                                    <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <table class="table-row-payment hide">
              <tbody>
                <tr>
                  <td>
                    <select name="master_id[]" class="form-control selectpickerx" data-live-search="true" data-palceholder="Kode Akunting">
                      @foreach($perkiraan as $r)
                        <option value="{{ $r->master_id }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
                        @endforeach
                    </select>
                  </td>
                  <td class="payment">
                    <input type="number" name="payment[]" class="form-control" value="0" step=".01">
                  <td class="payment_total">
                    <input type="number" name="payment_total[]" class="form-control" value="0" readonly step=".01">
                  </td>
                  <td>
                    <input type="text" name="keterangan[]" class="form-control">
                  </td>
                  <td>
                    <button class="btn btn-danger btn-payment-delete btn-xs btn-row-delete-payment">Hapus</button>
                  </td>
                </tr>
              </tbody>
          </table>
<script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {

            $('#form-payment-hutang-supplier').submit(function(e) {
                e.preventDefault();
                var ini = $(this);
                
                $('#submit-payment-hutang-supplier').attr('disabled', true);
                var sisa = $('[name="sisa"]').val();
                // if(sisa > 0) {
                //     swal({
                //         title: 'Perhatian',
                //         text: 'Data Belum Balance',
                //         type: 'error'
                //     });
                //     $('#submit-payment-hutang-supplier').attr('disabled', false);
                // }
                // else{
                    $.ajax({
                      url: ini.attr('action'),
                      type: ini.attr('method'),
                      data: ini.serialize(),
                      success: function(data) {
                          if(data.redirect) {
                              window.location.href = data.redirect;
                          }
                      },
                      error: function(request, status, error) {
                        swal({
                          title: 'Perhatian',
                          text: 'Data Gagal Disimpan!',
                          type: 'error'
                        });

                        // var json = JSON.parse(request.responseText);
                        // $('.form-group').removeClass('has-error');
                        // $('.help-block').remove();
                        // $.each(json.errors, function(key, value) {
                        //   $('.form-send [name="'+key+'"]').parents('.form-group').addClass('has-error');
                        //   $('.form-send [name="'+key+'"]').after('<span class="help-block">'+value+'</span>');
                        // });
                      }
                  });

                // }

                return false;
            });
            
        });
</script>
<!-- <div class="modal-footer"> -->
    
<!-- </div> -->