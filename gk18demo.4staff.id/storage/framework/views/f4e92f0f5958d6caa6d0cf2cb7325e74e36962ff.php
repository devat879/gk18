

<?php $__env->startSection('css'); ?>
  <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/.css')); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
  <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/components-select2.min.js')); ?>" type="text/javascript"></script>

  <script src="<?php echo e(asset('assets/global/plugins/moment.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('js/buat-barcode.js')); ?>" type="text/javascript"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
  <span id="data-back"
  data-form-token="<?php echo e(csrf_token()); ?>"
  data-route-penjualan-langsung-barang-row="<?php echo e(route('penjualanLangsungBarangRow')); ?>"
  data-route-gudang-row="<?php echo e(route('penjualanLangsungGudangRow')); ?>"
  data-route-stok-row="<?php echo e(route('penjualanLangsungStokRow')); ?>"
  data-route-verifikasi="<?php echo e(route('BarangMix.verifikasi')); ?>"></span>

  <form class="form-send-penjualan" action="<?php echo e(route('BarangMix.store')); ?>" method="post">
    <?php echo e(csrf_field()); ?>

    <div class="page-content-inner">
      <div class="mt-content-body">
        <div class="row">
          <div class="col-xs-12">
            <div class="portlet light ">
              <div class="row form-horizontal">
                <div class="col-xs-12 col-sm-6 ">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-3">Barcode Cat Oplosan</label><br/>
                      <div class="row">
                        <div class="col-md-4" style="margin-top: -20px">
                          <input type="text" class="form-control" name="barcode_new" value="<?php echo e($barcode); ?>" readonly>
                        </div>
                        <div class="col-md-2" style="margin-top: -20px">
                          <button type="button" class="btn btn-success" id="verifikasi">Verifikasi</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              <br>
              <table class="table table-striped table-bordered table-hover table-header-fixed table-all-data ">
                <thead>
                  <tr class="">
                    <th width="14%"> Barcode </th>
                    <th width="10%"> Nama </th>
                    <th width="10%"> Nomer Seri </th>
                    <th width="10%"> Gudang </th>
                    <th class="hide"> Satuan </th>
                    <th class="hide"> Barang HPP </th>
                    
                    <th width="6%"> Qty </th>
                    
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="brg_kode">
                      <div class="form-inline input-group">
                        <input type="text" name="brg_kode[]" class="form-control" required="">
                        <span class="input-group-btn">
                          <button type="button" class="btn btn-success btn-modal-barang" data-toggle="modal">
                            <span class="glyphicon glyphicon-search"></span>
                          </button>
                        </span>
                      </div>
                    </td>
                    <td class="nama">
                      <input type="text" class="form-control" name="nama[]" readonly>
                    </td>
                    <td class="brg_no_seri">
                      <select name="brg_no_seri[]" class="form-control" data-placeholder="Pilih No Seri" required="">
                      </select>
                    </td>
                    <td class="gdg_kode">
                      <select name="gdg_kode[]" class="form-control" data-placeholder="Pilih Gudang" required="">
                      </select>
                    </td>
                    <td class="satuan hide">
                      <input type="text" name="satuan[]" class="form-control" readonly>
                    </td>
                    <td class="brg_hpp hide">
                      <input type="number" class="form-control" name="brg_hpp[]" value="0" min="0" readonly>
                    </td>
                    
                    <td class="qty">
                      <input type="number" class="form-control" name="qty[]" value="0" min="0" required>
                    </td>
                    
                  </tr>
                </tbody>
              </table>
              <br>
              <div class="row">
                <div class="col-md-6 form-horizontal">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-3">Nama Stock</label>
                      <div class="col-md-9">
                        <input type="text" class="form-control" name="nama_stok" value="">
                        <input type="hidden" class="form-control" name="brg_kode_combine" value="">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3">Kategory</label>
                      <div class="col-md-9">
                        <input type="text" class="form-control" name="ktg" value="" readonly>
                        <input type="hidden" class="form-control" name="id_ktg" value="">
                        <input type="hidden" class="form-control" name="id_grp" value="">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3">Merek</label>
                      <div class="col-md-9">
                        <input type="text" class="form-control" name="mrk" value="" readonly>
                        <input type="hidden" class="form-control" name="id_mrk" value="">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3">Satuan</label>
                      <div class="col-md-9">
                        <input type="text" class="form-control" name="stn" value="" readonly>
                        <input type="hidden" class="form-control" name="id_stn" value="">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3">Harga Jual</label>
                      <div class="col-md-9">
                        <input type="number" min="0" class="form-control" name="harga_jual_new" value="0">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-offset-2 col-md-4 form-horizontal">
                  <div class="form-body">
                    <div class="form-group">
                      <button type="submit" class="btn btn-success btn-lg btn-block" data-toggle="modal">SAVE</button>
                      <a href="#" class="btn btn-warning btn-lg btn-block">Batal</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>

  <div class="modal bs-modal-lg" id="modal-barang" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="btn-smpl2"></button>
          <h4 class="modal-title"> Daftar Barang </h4>
        </div>
        <div class="modal-body">
          <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_6">
            <thead>
              <tr>
                <th>No</th>
                <th>Kode</th>
                <th>Barkode</th>
                <th>Nama Barang</th>
                <th>Satuan</th>
                <th>Menu</th>
              </tr>
            </thead>
            <tbody>
              <?php $__currentLoopData = $barang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><?php echo e($no++); ?>.</td>
                  <td><?php echo e($r->brg_kode); ?></td>
                  <td><?php echo e($r->brg_barcode); ?></td>
                  <td><?php echo e($r->brg_nama); ?></td>
                  <td><?php echo e($r->satuan->stn_nama); ?></td>
                  <td style="white-space: nowrap">
                    <div class="btn-group-md">
                      <button class="btn btn-info btn-stok"
                      data-href="<?php echo e(route('penjualanTitipanGetStok', ['kode'=>$r->brg_kode])); ?>">
                      <span class="icon-eye"></span> Lihat Stok
                    </button>
                    <button class="btn btn-success btn-pilih-barang"
                    data-brg-kode="<?php echo e($r->brg_kode); ?>"
                    data-brg-barkode="<?php echo e($r->brg_barcode); ?>"
                    data-brg-nama="<?php echo e($r->brg_nama); ?>"
                    data-brg-ktg="<?php echo e($r->kategoryProduct->ktg_nama); ?>"
                    data-brg-grp="<?php echo e($r->groupStok->grp_nama); ?>"
                    data-brg-mrk="<?php echo e($r->merek->mrk_nama); ?>"
                    data-brg-stn="<?php echo e($r->satuan->stn_nama); ?>"
                    data-brg-id_ktg="<?php echo e($r->kategoryProduct->ktg_kode); ?>"
                    data-brg-id_grp="<?php echo e($r->groupStok->grp_kode); ?>"
                    data-brg-id_mrk="<?php echo e($r->merek->mrk_kode); ?>"
                    data-brg-id_stn="<?php echo e($r->satuan->stn_kode); ?>">
                    <span class="icon-plus"></span> Pilih Barang
                  </button>
                </div>
              </td>
            </tr>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>

<table class="table-row-data hide" id="table-data-barang">

</table>

<div class="modal" id="modal-stok" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-blue-steel bg-font-blue-steel">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">
          Stok
        </h4>
      </div>
      <div class="modal-body form">
        <div class="form-body">
          <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_3">
            <thead>
              <tr class="">
                <th> No Seri </th>
                <th> QTY </th>
                <th> Titipan </th>
                <th> Gudang </th>
                <th> Supplier </th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>