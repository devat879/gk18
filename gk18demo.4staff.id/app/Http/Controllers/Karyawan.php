<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mKaryawan;

use Validator;

class Karyawan extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Karyawan';
      $this->kodeLabel = 'karyawan';
  }

  function index() {
      $breadcrumb = [
          'Karyawan'=>route('karyawanList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'karyawan';
      $data['title'] = $this->title;
      $data['dataList'] = mKaryawan::all();
      return view('karyawan/karyawanList', $data);
  }

  function rules($request) {
      $rules = [
          'kry_nama' => 'required',
          'kry_alamat' => 'required',
          'kry_telp' => 'required',
          'kry_posisi' => 'required',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
      $this->rules($request->all());
      mKaryawan::insert($request->except('_token'));
      return [
          'redirect'=>route('karyawanList')
      ];
  }

  function edit($kry_kode='') {
      $response = [
          'action'=>route('karyawanUpdate', ['kode'=>$kry_kode]),
          'field'=>mKaryawan::find($kry_kode)
      ];
      return $response;
  }

  function update(Request $request, $kry_kode) {
      $this->rules($request->all());
      mKaryawan::where('kry_kode', $kry_kode)->update($request->except('_token'));
      return [
          'redirect'=>route('karyawanList')
      ];
  }

  function delete($kry_kode) {
      mKaryawan::where('kry_kode', $kry_kode)->delete();
  }
}
