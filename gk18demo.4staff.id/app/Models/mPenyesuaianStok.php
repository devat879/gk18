<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mPenyesuaianStok extends Model
{
  public $incrementing = false;
  protected $table = 'tb_penyesuaian_stok';
  protected $primaryKey = 'ps_kode';
  // public $timestamps = false;
}
