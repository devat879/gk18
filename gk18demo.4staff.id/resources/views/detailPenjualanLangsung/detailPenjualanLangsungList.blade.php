@extends('main/index')

@section('css')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
@stop

@section('body')
<div class="page-content-inner">
  <div class="mt-content-body">
    <div class="row">
      <div class="col-xs-12">
        <div class="portlet light ">
          <div class="portlet light">
            <div class="btn-group">
              <a href="{{ route('penjualanLangsungList') }}" class="btn btn-warning">
                <i class="fa fa-arrow-left"></i> Kembali
              </a>
              <button class="btn btn-primary" data-toggle="modal" href="#modal-tambah">
                <i class="fa fa-plus"></i> Tambah Detail Penjualan Langsung
              </button>
            </div>
            <br /><br />
            <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
              <thead>
                <tr class="">
                  <th width="10"> No </th>
                  <th> Kode Gudang </th>
                  <th> Kode Barang </th>
                  <th> Satuan </th>
                  <th> Harga Jual </th>
                  <th> Disc </th>
                  <th> Disc Nom </th>
                  <th> Harga Net </th>
                  <th> Qty </th>
                  <th> Terkirim </th>
                  <th> Aksi </th>
                </tr>
              </thead>
              <tbody>
                @foreach($dataList as $row)
                <tr>
                  <td> {{ $no++ }}. </td>
                  <td> {{ $kodeGudang.$row->gdg_kode }} </td>
                  <td> {{ $kodeBarang.$row->brg_kode }} </td>
                  <td> {{ $row->satuan }} </td>
                  <td> {{ $row->harga_jual }} </td>
                  <td> {{ $row->disc }} </td>
                  <td> {{ $row->disc_nom }} </td>
                  <td> {{ $row->harga_net }} </td>
                  <td> {{ $row->qty }} </td>
                  <td> {{ $row->terkirim }} </td>
                  <td>
                    <div class="btn-group-vertical btn-group-xs">
                      <button class="btn btn-success btn-edit" data-href="{{ route('detailPenjualanLangsungEdit', ['kode'=>$row->detail_pl_kode]) }}">
                        <span class="icon-pencil"></span> Edit
                      </button>
                      <button class="btn btn-danger btn-delete" data-href="{{ route('detailPenjualanLangsungDelete', ['kode'=>$row->detail_pl_kode]) }}">
                        <span class="icon-trash"></span> Delete
                      </button>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal draggable-modal" id="modal-tambah" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-blue-steel bg-font-blue-steel">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">
          <i class="fa fa-plus"></i> Tambah Detail Penjualan Langsung
        </h4>
      </div>
      <div class="modal-body form">
        <form action="{{ route('detailPenjualanLangsungInsert') }}" class="form-horizontal form-send" role="form" method="post">
          <input type="hidden" name="pl_no_faktur" value="{{ $pl_no_faktur }}">
          {{ csrf_field() }}
          <div class="form-body">
            <div class="form-group">
              <label class="col-md-3 control-label">Kode Gudang</label>
              <div class="col-md-9">
                <select name="gdg_kode" class="form-control">
                  @foreach($gudang as $r)
                  <option value="{{ $r->gdg_kode }}">{{ $kodeGudang.$r->gdg_kode }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Kode Barang</label>
              <div class="col-md-9">
                <select name="brg_kode" class="form-control">
                  @foreach($barang as $r)
                  <option value="{{ $r->brg_kode }}">{{ $kodeBarang.$r->brg_kode }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Satuan</label>
              <div class="col-md-9">
                <input type="number" class="form-control" name="satuan" min="0">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Harga Jual</label>
              <div class="col-md-9">
                <input type="number" class="form-control" name="harga_jual" min="0">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Disc</label>
              <div class="col-md-9">
                <input type="number" class="form-control" name="disc" min="0">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Disc Nominal</label>
              <div class="col-md-9">
                <input type="number" class="form-control" name="disc_nom" min="0">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Harga Net</label>
              <div class="col-md-9">
                <input type="number" class="form-control" name="harga_net" min="0">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Qty</label>
              <div class="col-md-9">
                <input type="number" class="form-control" name="qty" min="0">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Terikirim</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="terkirim">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Total</label>
              <div class="col-md-9">
                <input type="number" class="form-control" name="total" min="0">
              </div>
            </div>
          </div>
          <div class="form-actions">
            <div class="row">
              <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn green">Simpan</button>
                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal draggable-modal" id="modal-edit" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-green-meadow bg-font-green-meadow">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">
          <i class="fa fa-pencil"></i> Edit Detail Penjualan Langsung
        </h4>
      </div>
      <div class="modal-body form">
        <form action="" class="form-horizontal form-send" role="form" method="put">
          {{ csrf_field() }}
          <div class="form-body">
            <div class="form-group">
              <label class="col-md-3 control-label">Kode Gudang</label>
              <div class="col-md-9">
                <select name="gdg_kode" class="form-control">
                  @foreach($gudang as $r)
                    <option value="{{ $r->gdg_kode }}">{{ $kodeGudang.$r->gdg_kode }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Kode Barang</label>
              <div class="col-md-9">
                <select name="brg_kode" class="form-control">
                  @foreach($barang as $r)
                    <option value="{{ $r->brg_kode }}">{{ $kodeBarang.$r->brg_kode }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Satuan</label>
              <div class="col-md-9">
                <input type="number" class="form-control" name="satuan" min="0">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Harga Jual</label>
              <div class="col-md-9">
                <input type="number" class="form-control" name="harga_jual" min="0">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Disc</label>
              <div class="col-md-9">
                <input type="number" class="form-control" name="disc" min="0">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Disc Nominal</label>
              <div class="col-md-9">
                <input type="number" class="form-control" name="disc_nom" min="0">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Harga Net</label>
              <div class="col-md-9">
                <input type="number" class="form-control" name="harga_net" min="0">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Qty</label>
              <div class="col-md-9">
                <input type="number" class="form-control" name="qty" min="0">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Terikirim</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="terkirim">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Total</label>
              <div class="col-md-9">
                <input type="number" class="form-control" name="total" min="0">
              </div>
            </div>
          </div>
          <div class="form-actions">
            <div class="row">
              <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn green">Simpan</button>
                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@stop