<?php
 
use App\Models\mWoSupplier;
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">History Hutang</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <table>
                    <tbody>
                    <tr>
                        <td class="col-md-4">No Piutang</td>
                        <td>: {{$piutang->pl_invoice}}</td>
                    </tr>
                    <tr>
                        <td class="col-md-6">Atas Nama</td>
                        @if($piutang->id_tipe==$kode_customer)
                        <td> : {{ $kode_customer.$piutang->customer->cus_kode }} - {{ $piutang->customer->cus_nama }} </td>
                        @elseif($piutang->id_tipe==$kode_karyawan)
                        <td> : {{ $kode_karyawan.$piutang->karyawan->kry_kode }} - {{ $piutang->karyawan->kry_nama }} </td>
                        @endif
                    </tr>                    
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <table>
                    <tbody>
                    
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 15px;">
        <div class="col-md-12">
            <!-- <h4>Item Produksi</h4> -->
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th class="text-center">Tanggal</th>
                        <th>No Jurnal</th>
                        <th>Keterangan</th>
                        <th>Nama Rekening</th>
                        <th>Debet</th>
                        <th class="text-center">Kredit</th>
                    </tr>
                </thead>
                <tbody><?php $no=1;?>
                    @foreach($histories as $history)
                        <tr>
                            <td style="font-size: 12px"> {{ $no++ }} </td>
                            <td style="font-size: 12px"> {{ $history->jmu_tanggal }} </td>
                            <td style="font-size: 12px"> {{ $history->no_invoice }} </td>
                            <td style="font-size: 12px" colspan="4"> {{ $history->jmu_keterangan }} </td>
                        </tr>
                        @foreach($history->transaksi as $trs)
                        <tr>
                            <td colspan="3"></td>
                            <td style="font-size: 12px"> {{ $trs->trs_kode_rekening }} </td>
                            <td style="font-size: 12px"> {{ $trs->trs_nama_rekening }} </td>
                            <td style="font-size: 12px"> {{ number_format($trs->trs_debet,2) }} </td>
                            <td style="font-size: 12px"> {{ number_format($trs->trs_kredit,2) }} </td>
                        </tr>
                        @endforeach
                    @endforeach
                    
                </tbody>
            </table>

        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>            
        </div>
    </div>
</div>
<!-- <div class="modal-footer"> -->
    
<!-- </div> -->