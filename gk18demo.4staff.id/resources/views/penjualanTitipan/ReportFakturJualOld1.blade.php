<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all"/> --}}
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}"> --}}
    <title></title>
    <style>
    p {
      font-size: 10px;
    }
    table {
      border-collapse: collapse;
    }
    </style>
  </head>
  <body>
    <table width="100%" border="0">
      <thead>
        <tr>
          <td colspan="5">
            <p>Jl. Gatot Subroto Barat No. 88A Tlp. (0361)416088, Fax.(0361)418933, No.HP. 0812 1611 8118</p>
          </td>
        </tr>
        <tr>
          <td width="15%"> <p>Syarat Pembayaran</p> </td>
          <td width="5%"> <p>:</p> </td>
          <td width="10%"> <p>{{$faktur['pt_transaksi']}}</p> </td>

          <td width="40%" colspan="2"></td>

          <td width="10%"> <p>No. Ftr</p> </td>
          <td width="5%"> <p>:</p> </td>
          <td width="15%"> <p>{{$faktur['kode_bukti_id']}}</p> </td>
        </tr>
        <tr>
          <td width="15%"> <p>Sales</p> </td>
          <td width="5%"> <p>:</p> </td>
          <td width="10%"> <p>{{$faktur->salesPrint['kry_nama']}}</p> </td>

          <td width="40%" colspan="2"></td>

          <td width="10%"> <p>Tgl.</p> </td>
          <td width="5%"> <p>:</p> </td>
          <td width="15%"> <p>{{$faktur['tglPrint']}}</p> </td>
        </tr>

        <tr>
          <td width="70%" colspan="5"></td>

          <td width="10%"> <p>Pelanggan</p> </td>
          <td width="5%"> <p>:</p> </td>
          <td width="15%"> <p>{{$faktur['cus_nama']}}</p> </td>
        </tr>
        <tr>
          <td width="100%" colspan="8" align="center"><p align="center"><b>Faktur Jual</b></p></td>
        </tr>
        <br>
        <tr>
          <th width="10%"> <p>No</p> </th>
          <th width="15%"> <p>Nama Barang</p> </th>
          <th width="15%"> <p>Harga/sat(Rp.)</p> </th>
          <th width="10%"> <p>Disc.%</p> </th>
          <th width="15%"> <p>Disc.nom</p> </th>
          <th width="15%"> <p>Harga Bersih</p> </th>
          <th width="10%"> <p>Banyak</p> </th>
          <th width="10%"> <p>Total</p> </th>
        </tr>
        <tr>
          <td colspan="8">
            <hr>
          </td>
        </tr>
      </thead>
      <tbody>
        @foreach ($faktur->detail as $key)
          <tr>
            <td align="center"><p>{{$faktur->no++}}</p></td>
            <td align="center"><p>{{$key['nama_barang']}}</p></td>
            <td align="center"><p>{{number_format($key['harga_jual'], 2, "." ,",")}}</p></td>
            <td align="center"><p>{{number_format($key['disc'], 2, "." ,",")}}</p></td>
            <td align="center"><p>{{number_format($key['disc_nom'], 2, "." ,",")}}</p></td>
            <td align="center"><p>{{number_format($key['harga_net'], 2, "." ,",")}}</p></td>
            <td align="center"><p>{{number_format($key['qty'], 2, "." ,",")}}</p></td>
            <td align="center"><p>{{number_format($key['total'], 2, "." ,",")}}</p></td>
          </tr>
        @endforeach
        <tr>
          <td colspan="8">
            <hr>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="8">
            <br>
          </td>
        </tr>

        <tr>
          <td width="30%" rowspan="6" colspan="4">
            <p>- Pembayaran dengan CHEQUE/GIRO dianggap sah, setelah CHEQUE/GIRO tersebut telah dapat diuangkan/clearing</p>
            <p>- Barang yang telah di beli tidak dapat dikembalikan/ditukar</p>
          </td>
        </tr>

        <tr>
          <td width="30%" align="center" colspan="1">
            <b><p>Penerima</p></b>
          </td>
          <td width="10%"></td>
          <td width="10%"> <p><b>Total</b></p> </td>
          <td width="10%" align="right"> <p>{{number_format($faktur['pt_subtotal'], 2, "." ,",")}}</p> </td>
        </tr>

        <tr>
          <td colspan="1">
            <br>
          </td>
          <td width="10%"></td>
          <td width="10%"> <p><b>Discount</b></p> </td>
          <td width="10%" align="right"> <p>{{number_format($faktur['pt_disc_nom'], 2, "." ,",")}}</p> </td>
        </tr>

        <tr>
          <td colspan="1">
            <br>
          </td>
          <td width="10%"></td>
          <td width="10%"></td>
          <td width="10%"><hr></td>
        </tr>

        <tr>
          <td colspan="1">
            <br>
          </td>
          <td width="10%"></td>
          <td width="10%"> <p><b>Grand Total</b></p> </td>
          <td width="10%" align="right"> <p>{{number_format($faktur['grand_total'], 2, "." ,",")}}</p> </td>
        </tr>

        <tr>
          <td width="20%" align="center" colspan="1">
            <p>(......................................)</p>
          </td>
          {{-- <td width="10%"></td>
          <td width="10%"> <p><b>Kembali</b></p> </td>
          <td width="10%" align="right"> <p>{{number_format(0, 2, "." ,",")}}</p> </td> --}}
        </tr>
      </tfoot>
    </table>
  </body>
</html>
