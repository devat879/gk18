<?php

namespace App\Exports;

use App\Models\mPembelianSupplier;
use App\Models\mPiutangPelanggan;
use App\Models\mSupplier;
use App\Models\mDetailPembelianSupplier;
use App\Helpers\Main;
// use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

// class BarangExport implements FromCollection
class LaporanPembelianExport implements FromView
{
    // public function collection()
    // {
    //   return mBarang::all();
    // }
        private $start;
        private $end;
        private $spl_id;
        private $mrk;
        private $tipe;
        private $grp;
        private $main;

    public function __construct($start_date,$end_date,$spl_id,$mrk,$grp,$tipe)
    {
        $this->start    = $start_date;
        $this->end      = $end_date;
        $this->spl_id   = $spl_id;
        $this->mrk      = $mrk;
        $this->tipe     = $tipe;
        $this->grp      = $grp;
        $this->main     = new Main();
    }

    public function view(): View
    {
        $tipe       = $this->tipe;
        if($tipe=='rekapPembelian'){

            
            $start_date = $this->start;
            $end_date   = $this->end;
            $spl_id     = $this->spl_id;
            $mrk        = $this->mrk;
            $grp        = $this->grp;

            if($tipe=='rekapPembelian'){
                
                $data['dataList']       = mPembelianSupplier::leftJoin('tb_detail_ps_supplier','tb_pembelian_supplier.no_pembelian','=','tb_detail_ps_supplier.ps_no_faktur')->leftJoin('tb_barang','tb_barang.brg_kode','=','tb_detail_ps_supplier.brg_kode')->leftJoin('tb_satuan','tb_satuan.stn_kode','=','tb_detail_ps_supplier.satuan')->leftJoin('tb_merek','tb_barang.mrk_kode','=','tb_merek.mrk_kode')->leftJoin('tb_group_stok','tb_barang.grp_kode','=','tb_group_stok.grp_kode')->whereBetween('ps_tgl',[$start_date,$end_date])->orderBy('tb_pembelian_supplier.ps_no_faktur','DESC');

                if($spl_id>0){
                    $data['spl']        = $spl_id;
                    $data['dataList']   = $data['dataList']->where('tb_pembelian_supplier.spl_id',$spl_id);
                }

                if($mrk>0){
                    $data['mrk']        = $mrk;
                    $data['dataList']   = $data['dataList']->where('tb_merek.mrk_kode',$mrk);
                }

                if($grp>0){
                    $data['grp']        = $grp;
                    $data['dataList']   = $data['dataList']->where('tb_group_stok.grp_kode',$grp);
                }
                
                $data['dataList']     = $data['dataList']->get();
                $data['jml_pembelian']= $data['dataList']->count();
                $data['no']           = 1;

                return view('export.lap-rekap-pembelian', $data);
            }      

        }
        if($tipe=='pembelianStock'){
            $data['start_date']   = $this->start;
            $data['end_date']     = $this->end;
            $data['spl_id']       = $this->spl_id;
            $data['brg']          = $this->start;
            $mrk          = $this->mrk;
            $grp          = $this->grp;

            $data['dataList']     = mSupplier::whereExists(function ($query) {
                                    $query->select()->from('tb_pembelian_supplier')
                                    ->whereRaw('tb_pembelian_supplier.spl_id = tb_supplier.spl_kode');
                                  });

            if($data['spl_id']>0){
                $data['dataList']   = $data['dataList']->leftJoin('tb_pembelian_supplier','tb_pembelian_supplier.spl_id','=','tb_supplier.spl_kode')->where('tb_pembelian_supplier.spl_id',$data['spl_id'])->groupBy('tb_pembelian_supplier.spl_id');
            }

            $data['dataList']   = $data['dataList']->get();

            $data['jml_pembelian']     = $data['dataList']->count();
            if($data['jml_pembelian']>0){
                foreach ($data['dataList'] as $spl) {
                    $data['pembelian'][$spl->spl_kode]    = mDetailPembelianSupplier::leftJoin('tb_pembelian_supplier','tb_pembelian_supplier.no_pembelian','=','tb_detail_ps_supplier.ps_no_faktur')->leftJoin('tb_barang','tb_barang.brg_kode','=','tb_detail_ps_supplier.brg_kode')->leftJoin('tb_satuan','tb_satuan.stn_kode','=','tb_detail_ps_supplier.satuan')->leftJoin('tb_wo_pembelian','tb_pembelian_supplier.pos_no_po','=','tb_wo_pembelian.no_wo')->whereBetween('ps_tgl',[$data['start_date'],$data['end_date']])->where('tb_pembelian_supplier.spl_id', $spl->spl_kode);
              
                    if($mrk>0){
                        $data['mrk']                        = $mrk;
                        $data['pembelian'][$spl->spl_kode]  = $data['pembelian'][$spl->spl_kode]->where('tb_barang.mrk_kode',$mrk);
                    }
                    if($grp>0){
                        $data['grp']                        = $grp;
                        $data['pembelian'][$spl->spl_kode]  = $data['pembelian'][$spl->spl_kode]->where('tb_barang.grp_kode',$grp);
                    }
                    $data['jml_detail'][$spl->spl_kode]   = $data['pembelian'][$spl->spl_kode]->count();
                    $data['pembelian'][$spl->spl_kode]    = $data['pembelian'][$spl->spl_kode]->orderBy('ps_tgl','ASC')->get();
                }
            }         

            $data['kodePembelian']     = $this->main->kodeLabel('pembelianSupplier');

            return view('export.lap-pembelian', $data);
        }

        if($tipe=='pembelianTunaiKredit'){
            $data['start_date']    = $this->start;
            $data['end_date']       = $this->end;
            $spl_id                 = $this->spl_id;

            $queryPembelianPpn                 = mPembelianSupplier::where('ps_ppn','>',0)->whereBetween('ps_tgl',[$data['start_date'],$data['end_date']]);
            $queryPembelianNonPpn              = mPembelianSupplier::where('ps_ppn','=',0)->whereBetween('ps_tgl',[$data['start_date'],$data['end_date']]);

            if($spl_id > 0){
                $data['spl_kode']     = $spl_id;
                $queryPembelianPpn                 = mPembelianSupplier::where('ps_ppn','>',0)->where('spl_id',$spl_id)->whereBetween('ps_tgl',[$data['start_date'],$data['end_date']]);
                $queryPembelianNonPpn              = mPembelianSupplier::where('ps_ppn','=',0)->where('spl_id',$spl_id)->whereBetween('ps_tgl',[$data['start_date'],$data['end_date']]);
            }
              
            $data['dataListPpn']            = $queryPembelianPpn->get();
            $data['dataListNonPpn']         = $queryPembelianNonPpn->get();
            $data['jml_pembelian_cash']     = $queryPembelianPpn->count();
            $data['jml_pembelian_credit']   = $queryPembelianNonPpn->count();
            
            $data['kodePembelian']          = $this->main->kodeLabel('pembelianSupplier');

            return view('export.lap-pembelian-tunai-kredit', $data);
        }

    }
}
