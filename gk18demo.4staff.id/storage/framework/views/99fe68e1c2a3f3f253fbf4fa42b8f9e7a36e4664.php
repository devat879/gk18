

<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/piutangPelanggan.js')); ?>" type="text/javascript"></script>

    <script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn-paymen').click(function() {
                var href = $(this).data('href');
                $.ajax({
                    url: href,
                    success: function(data) {
                        $.each(data.field, function(field, value) {
                            $('#modalPayment [name="'+field+'"]').val(value);

                            if(field=='pp_sisa_amount'){
                                const formatter = new Intl.NumberFormat()
                                var balance = formatter.format(value); // "$1,000.00"
                                // $('#balance').html('Rp. '+balance);

                                $('.nominal-grand-total').html('Rp. '+balance);
                                $('.nominal-sisa').html('Rp. '+balance);
                                $('#modalPayment [name="amount"]').val(value);
                                $('#modalPayment [name="amount_sisa"]').val(value);
                            }

                        });
                        $('#modalPayment form').attr('action', data.action);
                        $('#modalPayment').modal('show');
                    }
                });
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet-body">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#langsung" data-toggle="tab"> Piutang Langsung </a>
                            </li>
                            <li>
                                <a href="#titipan" data-toggle="tab"> Piutang Titipan </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="langsung">
                                <h4><center><strong>Piutang Penjualan Langsung</strong></center></h4>
                                <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                                    <thead>
                                        <tr class="">
                                            <th style="font-size:12px"> No </th>
                                            <th style="font-size:12px"> No Invoice Piutang </th>
                                            <th style="font-size:12px"> Jatuh Tempo</th>
                                            <th style="font-size:12px"> No Faktur Penjualan </th>
                                            <th style="font-size:12px"> Tanggal Faktur Penjualan </th>
                                            <!-- <th style="font-size:12px"> Sales Person </th> -->
                                            <th style="font-size:12px"> Pelanggan </th>
                                            <th style="font-size:12px"> Tipe Pelanggan </th>                                    
                                            <th style="font-size:12px"> Alamat </th>
                                            <th style="font-size:12px"> Telepon </th>
                                            <th style="font-size:12px"> Sisa </th>
                                            <th style="font-size:12px"> Keterangan </th>
                                            <th style="font-size:12px"> Status </th>
                                            <th style="font-size:12px"> Aksi </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $piutang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td style="font-size:10px"> <?php echo e($no++); ?>. </td>
                                        <td style="font-size:10px"> <?php echo e($piutang->no_piutang_pelanggan); ?> </td>
                                        <td style="font-size:10px"> <?php echo e(date('d M Y',strtotime($piutang->pp_jatuh_tempo))); ?> </td>
                                        <td style="font-size:10px"> <?php echo e($piutang->pp_no_faktur); ?> </td>
                                        <td style="font-size:10px"> <?php echo e(date('d M Y',strtotime($piutang->tgl_piutang))); ?> </td>
                                        <td style="font-size:10px"> <?php echo e($piutang->customers->cus_nama); ?></td>
                                        <td style="font-size:10px"> <?php echo e($piutang->customers->cus_tipe); ?> </td>
                                        <td style="font-size:10px"> <?php echo e($piutang->customers->cus_alamat); ?></td>
                                        <td style="font-size:10px"> <?php echo e($piutang->customers->cus_telp); ?></td>
                                        <td style="font-size:10px"> <?php echo e(number_format($piutang->pp_sisa_amount,2)); ?> </td>
                                        <td style="font-size:10px"> <?php echo e($piutang->pp_keterangan); ?> </td>
                                        <td style="font-size:10px"> <?php echo e($piutang->pp_status); ?> </td>
                                        <td>
                                            <!-- <div class="btn-group btn-group-xs"> -->
                                                <a type="button" name="btn-cart" class="btn btn-primary btn-xs" href="<?php echo e(route('printPiutang', ['kode'=>$piutang->pp_invoice])); ?>" target="_blank">
                                                    <span class="fa fa-print"></span>
                                                </a>
                                                <?php if($piutang->pp_sisa_amount!=0): ?>                                                
                                                <a class="btn btn-danger btn-payment btn-xs" href="<?php echo e(route('getpiutangPelanggan', ['kode'=>$piutang->pp_invoice])); ?>" data-target="#modalPayment" data-toggle="modal">
                                                    <span class="fa fa-money"></span>
                                                </a>
                                                <?php endif; ?>
                                            <!-- </div> -->
                                        </td>
                                    </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="titipan">
                                <h4><center><strong>Piutang Penjualan Titipan</strong></center></h4>
                                <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                    <thead>
                                        <tr class="">
                                            <th style="font-size:12px"> No </th>
                                            <th style="font-size:12px"> No Invoice Piutang </th>
                                            <th style="font-size:12px"> Jatuh Tempo</th>
                                            <th style="font-size:12px"> No Faktur Penjualan </th>
                                            <th style="font-size:12px"> Tanggal Faktur Penjualan </th>
                                            <!-- <th style="font-size:12px"> Sales Person </th> -->
                                            <th style="font-size:12px"> Pelanggan </th>
                                            <th style="font-size:12px"> Tipe Pelanggan </th>                                    
                                            <th style="font-size:12px"> Alamat </th>
                                            <th style="font-size:12px"> Telepon </th>
                                            <th style="font-size:12px"> Sisa </th>
                                            <th style="font-size:12px"> Keterangan </th>
                                            <th style="font-size:12px"> Status </th>
                                            <th style="font-size:12px"> Aksi </th>
                                        </tr>
                                    </thead>
                                    <tbody><?php $n=1;?>
                                    <?php $__currentLoopData = $titipan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ptp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td style="font-size:10px"> <?php echo e($n++); ?>. </td>
                                        <td style="font-size:10px"> <?php echo e($ptp->no_piutang_pelanggan); ?> </td>
                                        <td style="font-size:10px"> <?php echo e(date('d M Y',strtotime($ptp->pp_jatuh_tempo))); ?> </td>
                                        <td style="font-size:10px"> <?php echo e($ptp->pp_no_faktur); ?> </td>
                                        <td style="font-size:10px"> <?php echo e(date('d M Y',strtotime($ptp->tgl_piutang))); ?> </td>
                                        <td style="font-size:10px"> <?php echo e($ptp->customers->cus_nama); ?></td>
                                        <td style="font-size:10px"> <?php echo e($ptp->customers->cus_tipe); ?> </td>
                                        <td style="font-size:10px"> <?php echo e($ptp->customers->cus_alamat); ?></td>
                                        <td style="font-size:10px"> <?php echo e($ptp->customers->cus_telp); ?></td>
                                        <td style="font-size:10px"> <?php echo e(number_format($ptp->pp_sisa_amount,2)); ?> </td>
                                        <td style="font-size:10px"> <?php echo e($ptp->pp_keterangan); ?> </td>
                                        <td style="font-size:10px"> <?php echo e($ptp->pp_status); ?> </td>
                                        <td>
                                            <!-- <div class="btn-group btn-group-xs"> -->
                                                <a type="button" name="btn-cart" class="btn btn-primary btn-xs" href="<?php echo e(route('printPiutang', ['kode'=>$ptp->pp_invoice])); ?>" target="_blank">
                                                    <span class="fa fa-print"></span>
                                                </a>
                                                <?php if($piutang->pp_sisa_amount!=0): ?>
                                                <a class="btn btn-danger btn-payment btn-xs" href="<?php echo e(route('getpiutangPelanggan', ['kode'=>$ptp->pp_invoice])); ?>" data-target="#modalPayment" data-toggle="modal">
                                                    <span class="fa fa-money"></span>
                                                </a>
                                                <?php endif; ?>
                                            <!-- </div> -->
                                        </td>
                                    </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modalPayment" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog modal-full">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"> Payment </h4>
          </div>
          <div class="modal-body form-horizontal">
            

        </div>
      </div>
    </div>
</div>
</form>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>