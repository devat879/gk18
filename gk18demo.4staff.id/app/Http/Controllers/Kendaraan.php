<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mKaryawan;
use App\Models\mKendaraan;

use Validator;

class Kendaraan extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Kendaraan';
      $this->kodeLabel = 'kendaraan';
  }

  function index() {
      $breadcrumb = [
          'Kendaraan'=>route('kendaraanList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'kendaraan';
      $data['title'] = $this->title;
      $data['dataList'] = mKendaraan::leftJoin('tb_karyawan', 'tb_kendaraan.kry_kode', '=', 'tb_karyawan.kry_kode')->get();
      $data['karyawan'] = mKaryawan::all();
      return view('kendaraan/kendaraanList', $data);
  }

  function rules($request) {
      $rules = [
          'kdn_nama' => 'required',
          'kdn_description' => 'required',
          'kry_kode' => 'required',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
      $this->rules($request->all());
      mKendaraan::insert($request->except('_token'));
      return [
          'redirect'=>route('kendaraanList')
      ];
  }

  function edit($kdn_kode='') {
      $response = [
          'action'=>route('kendaraanUpdate', ['kode'=>$kdn_kode]),
          'field'=>mKendaraan::find($kdn_kode)
      ];
      return $response;
  }

  function update(Request $request, $kdn_kode) {
      $this->rules($request->all());
      mKendaraan::where('kdn_kode', $kdn_kode)->update($request->except('_token'));
      return [
          'redirect'=>route('kendaraanList')
      ];
  }

  function delete($kdn_kode) {
      mKendaraan::where('kdn_kode', $kdn_kode)->delete();
  }
}
