<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mHutangSuplier;
use Carbon\Carbon;
use App\Models\mPerkiraan;
use App\Models\mJurnalUmum;
use App\Models\mTransaksi;
use App\Models\mSupplier;
use App\Models\mHutangCek;
use App\Models\mHutangLain;

use Validator;

class HutangSuplier extends Controller
{
    private $main;
    private $title;
    private $kodeLabel;

    function __construct() {
        $this->main         = new Main();
        $this->title        = 'List Hutang Suplier';
        $this->kodeLabel    = 'hutangSuplier';
    }

    function index() {
        $breadcrumb         = [
            'Hutang Suplier'=>route('hutangSuplier'),
            'Daftar'        =>''
        ];
        $data               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']       = 'hutangsuplier';
        $data['title']      = $this->title;
        $data['dataList']   = mHutangSuplier::where('sisa_amount','>',0)->get();
        $data['perkiraan']  = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();
        $kode               = $this->main->kodeLabel('bayar');
        $data['no_transaksi'] = $this->main->getNoTransaksiPayment($kode);
        return view('hutang/hutangSuplierList', $data);
    }

    function rules($request) {
        $rules              = [
          'master_id'       => 'required',
          'payment'         => 'required',
          'payment_total'   => 'required',
          'keterangan'      => 'required',
        //   'setor'           => 'required',
        ];

        $customeMessage = [
          'required' => 'Kolom diperlukan'
        ];
        Validator::make($request, $rules, $customeMessage)->validate();
    }

    public function insert(Request $request){
        $this->rules($request->all());
        \DB::beginTransaction();
        try{
            $date_db        = date('Y-m-d H:i:s');
            // Jurnal Umum

            // Transaksi
            $master_id      = $request->input('master_id');
            $payment        = $request->input('payment');
            $charge         = $request->input('charge');
            $payment_total  = $request->input('payment_total');
            $no_check_bg    = $request->input('no_check_bg');
            $tgl_pencairan  = $request->input('tgl_pencairan');
            $setor          = $request->input('setor');
            $keterangan     = $request->input('keterangan');
            $tipe_arus_kas  = 'Operasi';
            $hs_kode        = $request->input('hs_kode');
            $kode_perkiraan = $request->input('kode_perkiraan');
            $no_faktur      = $request->input('ps_no_faktur');
            $no_faktur_hutang      = $request->input('no_faktur_hutang');
            $spl_kode       = $request->input('spl_kode');
            $spl            = mSupplier::find($spl_kode);
            $spl_nama       = $spl->spl_nama;
            $no_cek_bg     = $request->input('no_cek_bg');
            $tgl_pencairan     = $request->input('tgl_pencairan');
            $nama_bank     = $request->input('nama_bank');
            $no_transaksi      = $request->input('no_transaksi');
            $tgl_transaksi  = $request->input('tgl_transaksi');

            // General
            $year           = date('Y',strtotime($tgl_transaksi));
            $month          = date('m',strtotime($tgl_transaksi));
            $day            = date('d',strtotime($tgl_transaksi));
            $where          = [
                'jmu_year'  => $year,
                'jmu_month' => $month,
                'jmu_day'   => $day
            ];
            $jmu_no         = mJurnalUmum::where($where)
                            ->orderBy('jmu_no','DESC')
                            ->select('jmu_no')
                            ->limit(1);

            if($jmu_no->count() == 0) {
                $jmu_no     = 1;
            } else {
                $jmu_no     = $jmu_no->first()->jmu_no + 1;
            }


            $jurnal_umum_id = mJurnalUmum::orderBy('jurnal_umum_id','DESC')
                            ->limit(1)
                            ->first();
            $jurnal_umum_id = $jurnal_umum_id['jurnal_umum_id'] + 1;

            $data_jurnal            = [
                'jurnal_umum_id'    => $jurnal_umum_id,
                // 'no_invoice'        => $no_faktur_hutang,
                'no_invoice'        => $no_transaksi,
                'id_pel'            => 'SPL'.$spl_kode,
                'jmu_tanggal'       => $tgl_transaksi,
                'jmu_no'            => $jmu_no,
                'jmu_year'          => $year,
                'jmu_month'         => $month,
                'jmu_day'           => $day,
                'jmu_keterangan'    => 'Pembayaran Hutang Supplier : '.$spl_nama.' No Faktur Hutang : '.$no_faktur_hutang,
                'jmu_date_insert'   => $date_db,
                'jmu_date_update'   => $date_db,
                'reference_number'  => $no_faktur_hutang,
            ];
            mJurnalUmum::insert($data_jurnal);

            $amountAwal             = mHutangSuplier::where('hs_kode',$hs_kode)->first();
            $hs_amount              = $amountAwal['sisa_amount'];
            $total_payment          = 0;

            foreach($master_id as $key => $val) {

                $master                 = mPerkiraan::find($master_id[$key]);
                $trs_kode_rekening      = $master->mst_kode_rekening;
                $trs_nama_rekening      = $master->mst_nama_rekening;

                $data_transaksi         = [
                    'jurnal_umum_id'        => $jurnal_umum_id,
                    'master_id'             => $master_id[$key],
                    'trs_jenis_transaksi'   => 'kredit',
                    'trs_debet'             => 0,
                    'trs_kredit'            => $payment[$key],
                    'user_id'               => 0,
                    'trs_year'              => $year,
                    'trs_month'             => $month,
                    'trs_kode_rekening'     => $trs_kode_rekening,
                    'trs_nama_rekening'     => $trs_nama_rekening,
                    'trs_tipe_arus_kas'     => $tipe_arus_kas,
                    'trs_catatan'           => $keterangan[$key],
                    'trs_date_insert'       => $date_db,
                    'trs_date_update'       => $date_db,
                    'trs_charge'            => $charge[$key],
                    'trs_no_check_bg'       => $no_cek_bg[$key],
                    'trs_tgl_pencairan'     => $tgl_pencairan[$key],
                    'trs_setor'             => $setor[$key],
                    'tgl_transaksi'         => $tgl_transaksi
                ];

                mTransaksi::insert($data_transaksi);

                $hs_amount            = $hs_amount-$payment[$key];
                $total_payment        = $total_payment+$payment[$key];
                $angka_hs_amount      = number_format($hs_amount,2);
                
                if($angka_hs_amount=='0.00'){
                    mHutangSuplier::where('hs_kode', $hs_kode)->update(['sisa_amount'=>$hs_amount,'hs_status'=>'Lunas']);
                }else{
                    mHutangSuplier::where('hs_kode', $hs_kode)->update(['sisa_amount'=>$hs_amount]);
                }

                if($trs_kode_rekening=='2103'){
                    $hutang_cek            = new mHutangCek([
                        'no_cek_bg'             => $no_cek_bg[$key],
                        'tgl_pencairan'         => date('Y-m-d', strtotime($tgl_pencairan[$key])),
                        'tgl_cek'               => date('Y-m-d'),
                        'total_cek'             => $payment[$key],
                        'sisa'                  => $payment[$key],
                        'nama_bank'             => $nama_bank[$key],
                        'cek_untuk'             => $spl_kode,
                        'keterangan_cek'        => $keterangan[$key],
                        'created_at'            => date('Y-m-d'),
                        'created_by'            => auth()->user()->Karyawan->kry_nama,
                        'updated_at'            => date('Y-m-d'),
                        'updated_by'            => auth()->user()->Karyawan->kry_nama
                    ]);
        
                    $hutang_cek->save();
                }
            }

            $master_hutang                 = mPerkiraan::where('mst_kode_rekening', $kode_perkiraan)->first();
            $master_id_hutang              = $master_hutang->master_id;
            $trs_kode_rekening_hutang      = $master_hutang->mst_kode_rekening;
            $trs_nama_rekening_hutang      = $master_hutang->mst_nama_rekening;

            $data_transaksi_hutang    = [
                'jurnal_umum_id'      => $jurnal_umum_id,
                'master_id'           => $master_id_hutang,
                'trs_jenis_transaksi' => 'debet',
                'trs_debet'           => $total_payment,
                'trs_kredit'          => 0,
                'user_id'             => 0,
                'trs_year'            => $year,
                'trs_month'           => $month,
                'trs_kode_rekening'   => $trs_kode_rekening_hutang,
                'trs_nama_rekening'   => $trs_nama_rekening_hutang,
                'trs_tipe_arus_kas'   => $tipe_arus_kas,
                'trs_catatan'         => "Pembayaran Hutang",
                'trs_date_insert'     => $date_db,
                'trs_date_update'     => $date_db,
                'trs_charge'          => 0,
                'trs_no_check_bg'     => 0,
                'trs_tgl_pencairan'   => date('Y-m-d'),
                'trs_setor'           => $total_payment,
                'tgl_transaksi'       => $tgl_transaksi
            ];
            mTransaksi::insert($data_transaksi_hutang);

            \DB::commit();

            return [
                'redirect'=>route('hutangSuplier')
            ];
        }catch(Exception $e) {
            throw $e;
            \DB::rollBack();    
                
        }

        
    }

    function getData($kode='') {
        // $response = [
        //     'action'=>route('hutangSuplierInsert'),
        //     'field'=>mHutangSuplier::leftJoin('tb_supplier','tb_supplier.spl_kode','=','tb_hutang_supplier.spl_kode')->find($kode)
        // ];
        // return $response;
        $data['hutang'] = mHutangSuplier::leftJoin('tb_supplier','tb_supplier.spl_kode','=','tb_hutang_supplier.spl_kode')->find($kode);
        $data['perkiraan']  = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();
        $kode               = $this->main->kodeLabel('bayar');
        $data['no_transaksi'] = $this->main->getNoTransaksiPayment($kode);
        return view('hutang/payment-hutang-supplier', $data)->render();
    }


    function kartuHutang($kode='',$start_date='',$end_date='', $coa='' ,$tipe=''){
        $breadcrumb         = [
            'Hutang/Piutang'        => '',
            'Kartu Hutang Supplier' =>route('kartuHutangSupplier')
        ];
        $data               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']       = 'hutangsuplier';
        $data['title']      = 'Kartu Hutang';
        $data['spl_id']        = 'all';        
        $data['dataList']   = mSupplier::all();
        if($kode!=''){
            if($kode!='all'){
                $data['dataList'] = mSupplier::where('spl_kode',$kode)->get();
                $data['spl_id']        = $kode;
            }
        }
        // $data['dataList']       = $data['dataList']->get();
        $kode_spl = $this->main->kodeLabel('supplier');
        $data['start_date'] = date('Y-m-d');
        $data['end_date']   = date('Y-m-d');
        $data['coa']        = '2101';

        if($start_date!='' || $end_date!=''){
            $data['start_date'] = $start_date;
            $data['end_date']   = $end_date;
        }
        if($coa!=''){
            $data['coa']        = $coa;
        }
        foreach ($data['dataList'] as $spl) {
            $data['begining_balance'][$spl->spl_kode] = 0;
            

            $trs = mTransaksi::leftJoin('tb_ac_jurnal_umum','tb_ac_jurnal_umum.jurnal_umum_id','=','tb_ac_transaksi.jurnal_umum_id')->where('tb_ac_jurnal_umum.id_pel','=',$kode_spl.$spl->spl_kode)->where('trs_kode_rekening',$data['coa'])->whereBetween('tb_ac_jurnal_umum.jmu_tanggal',[$data['start_date'],$data['end_date']]);

            $query              = mTransaksi::leftJoin('tb_ac_jurnal_umum','tb_ac_jurnal_umum.jurnal_umum_id','=','tb_ac_transaksi.jurnal_umum_id')->where('tb_ac_jurnal_umum.id_pel','=',$kode_spl.$spl->spl_kode)->where('trs_kode_rekening',$data['coa'])->where('tb_ac_transaksi.tgl_transaksi','<',$data['start_date']);
            
            $debet              = $query->sum('trs_debet');
            $kredit             = $query->sum('trs_kredit');
            $data['begining_balance'][$spl->spl_kode] = $kredit-$debet;
            $data['trs'][$spl->spl_kode] = $trs->get();
        }
        $data['kodeSupplier']   = $this->main->kodeLabel('supplier');
        $data['supplier']       = mSupplier::all();
        $data['kode_coa']       = [
            '2101' => 'Hutang Dagang',
            '2103' => 'Hutang Cek/Bg',
            '2104' => 'Hutang Biaya',
            '2107' => 'Hutang Lain-Lain',
        ];
        if($tipe=='print'){
            return view('hutang/printKartuHutangSupplier', $data);
        }else{
            return view('hutang/kartuHutangSupplier', $data);
        }
    }

    function getKartuHutang($kode='', $start_date='',$end_date=''){

        $breadcrumb         = [
            'Hutang/Piutang'               => '',
            'Kartu Hutang Supplier'        =>route('kartuHutangSupplier'),
            'Detail Kartu Hutang '         => ''
        ];
        $data               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['supplier']   = mSupplier::find($kode);
        $data['menu']       = 'hutangsuplier';
        $data['title']      = 'Kartu Hutang '.$data['supplier']->spl_nama;
        $data['kodeSupplier'] = $this->main->kodeLabel('supplier');
        $data2   = mJurnalUmum::where('id_pel',$data['kodeSupplier'].$kode)->leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('trs_kode_rekening','2101');
        $data['start_date'] = 0;
        $data['end_date']   = 0;
        $data['begining_balance'] = 0;

        if($start_date!='' && $end_date!=''){
            $query              = mJurnalUmum::where('id_pel',$data['kodeSupplier'].$kode)->leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('trs_kode_rekening','2101')->where('tb_ac_jurnal_umum.jmu_tanggal','<',$start_date);
            $debet              = $query->sum('trs_debet');
            $kredit             = $query->sum('trs_kredit');
            $data['begining_balance'] = $kredit-$debet;

            $data2   = $data2->whereBetween('tb_ac_jurnal_umum.jmu_tanggal',[$start_date,$end_date]);
            $data['start_date'] = $start_date;
            $data['end_date']   = $end_date;

        }
        $data['dataList'] = $data2->get(); 
        $data['count'] = $data2->count();       


        return view('hutang/getkartuHutangSupplier', $data);
    }

    function pilihPeriodeKartuHutang(Request $request){
        $start_sate     = $request->input('start_date');
        $end_date       = $request->input('end_date');
        $supplier_id    = $request->input('spl_id');
        $coa            = $request->input('coa');

        return [
          'redirect'=>route('getkartuHutangSuppliers', ['kode'=>$supplier_id,'start_date'=>$start_sate, 'end_date'=>$end_date, 'coa'=>$coa]),
        ];
    }

    function umur_hutang($start_date='', $end_date='',$coa='', $spl='', $tipe=''){
        $breadcrumb         = [
            'Hutang/Piutang'        => '',
            'Laporan'               => '',
            'Umur Hutang'           => route('umurHutang'),
        ];
        $data                   = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']           = 'hutangsuplier';
        $data['title']          = 'Umur Hutang';
        
        $data['kodeSupplier']   = $this->main->kodeLabel('supplier');
        $data['start_date']     = date('Y-m-d');
        $data['end_date']       = date('Y-m-d');
        $data['coa']            = 0;
        $data['spl_id']         = 0;
        $data['kode_coa']       = [
            '2101' => 'Hutang Dagang',
            '2104' => 'Hutang Biaya',
            '2107' => 'Hutang Lain-Lain',
            '2103' => 'Hutang Cek/Bg',
        ];
        $data['supplier']       = mSupplier::all();
        $data['data']           = mHutangSuplier::where('hs_status','Belum Lunas');
        $data['data_hl']        = mHutangLain::where('hs_status','Belum Bayar');
        $data['data_hc']        = mHutangCek::where('sisa','>',0);
        $data['spl_nama']       = 'All Supplier';
        if($start_date!='' || $end_date!=''){
            $data['start_date']     = $start_date;
            $data['end_date']       = $end_date;
        }
        if($coa!=0){
            $data['coa']            = $coa;
            $data['data_hl']        = $data['data_hl']->where('kode_perkiraan', $coa);
            $data['data']           = $data['data']->where('kode_perkiraan', $coa);
            
            // if($coa=='2103'){
            //     $data['data_hc']        = mHutangCek::where('sisa','>',0);
            // }
        }
        if($spl!=0){
            $data['spl_id']     = $spl;
            $data['data']       = $data['data']->where('spl_kode', $spl);
            $data['data_hl']    = $data['data_hl']->where('hl_dari', $spl);
            $data['data_hc']    = $data['data_hc']->where('cek_untuk', $spl);            
            $spl                = mSupplier::find($spl);
            $data['spl_nama']   = $spl->spl_nama;
        }
        $data['data']           = $data['data']->get();
        $data['data_hl']        = $data['data_hl']->get();
        if($data['coa']==0 || $data['coa']=='2103'){
            $data['data_hc']        = $data['data_hc']->get();
        }
        

        if($tipe=='print'){
            return view('hutang/printUmurHutang', $data);
        }else{
            return view('hutang/umurHutang', $data);
        }
        
    }

    function periode(Request $request) {
      $start_sate     = $request->input('start_date');
      $end_date       = $request->input('end_date');
      $coa            = $request->input('kode_coa');
      $spl_id         = $request->input('spl_id');

      return [
        'redirect'=>route('getUmurHutang', ['start_date'=>$start_sate, 'end_date'=>$end_date, 'coa'=>$coa, 'spl'=>$spl_id]),
      ];
    }

    public function show_history($id)
    {
        $data['hutang'] = mHutangSuplier::where('hs_kode', $id)->firstOrFail();
        // $data['histories'] = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('reference_number',$data['hutang']->no_hutang_lain)->get();
        $data['histories'] = mJurnalUmum::where('reference_number',$data['hutang']->no_faktur_hutang)->get();
        return view('hutang/show-history-hutang-supplier', $data)->render();
    }

    function edit_hutang($kode='') {
        // $response = [
        //     'action'=>route('hutangSuplierInsert'),
        //     'field'=>mHutangSuplier::leftJoin('tb_supplier','tb_supplier.spl_kode','=','tb_hutang_supplier.spl_kode')->find($kode)
        // ];
        // return $response;
        $data['hutang'] = mHutangSuplier::leftJoin('tb_supplier','tb_supplier.spl_kode','=','tb_hutang_supplier.spl_kode')->find($kode);
        $data['jurnal']         = mJurnalUmum::leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('no_invoice',$data['hutang']->no_faktur_hutang)->where('trs_jenis_transaksi','debet')->get();
        $data['id_hutang']      = $kode;
        return view('hutang/edit-hutang-supplier', $data)->render();
    }

    public function update_hutang(Request $request,$kode){
        // $this->rules($request->all());
        \DB::beginTransaction();
        try{
            $data_hutang = [
              'no_faktur_hutang'  => $request->no_faktur_hutang,
              'js_jatuh_tempo'    => $request->ps_jatuh_tempo,//sementara
              'ps_no_faktur'      => $request->ps_no_faktur,
              'spl_kode'          => $request->spl_kode,
              'hs_amount'         => $request->hs_amount,
              'sisa_amount'       => $request->hs_amount,
              'hs_keterangan'     => $request->hs_keterangan,
              'hs_status'         =>'Belum Lunas',
              'kode_perkiraan'    => $request->kode_perkiraan,
              // 'created_at'        => date('Y-m-d H:i:s'),
              // 'created_by'        => auth()->user()->karyawan->kry_nama,
              'updated_at'        => date('Y-m-d H:i:s'),
              'updated_by'        => auth()->user()->karyawan->kry_nama,
              'tgl_hutang'        => $request->tgl_hutang,
            ];
            mHutangSuplier::where('hs_kode',$kode)->update($data_hutang);

            $jurnal_umum = mJurnalUmum::where('no_invoice',$request->no_faktur_hutang)->firstOrFail();
            mTransaksi::where('jurnal_umum_id',$jurnal_umum->jurnal_umum_id)->delete();

            //insert akun_kredit
            $amount                 = $request->hs_amount;
            $year                   = date('Y', strtotime($jurnal_umum->jmu_tanggal));
            $month                  = date('m', strtotime($jurnal_umum->jmu_tanggal));
            $date_db                = $jurnal_umum->jmu_tanggal;
            $tipe_arus_kas          = "Operasi";
            $keterangan             = $request->hs_keterangan;
            $master_piutang                 = mPerkiraan::where('mst_kode_rekening',$request->kode_perkiraan)->first();
            $master_id_piutang              = $master_piutang->master_id;
            $trs_kode_rekening_piutang      = $master_piutang->mst_kode_rekening;
            $trs_nama_rekening_piutang      = $master_piutang->mst_nama_rekening;

            $data_transaksi_persediaan           = [
                'jurnal_umum_id'      => $jurnal_umum->jurnal_umum_id,
                'master_id'           => $master_id_piutang,
                'trs_jenis_transaksi' => 'kredit',
                'trs_debet'           => 0,
                'trs_kredit'          => $amount,
                'user_id'             => 0,
                'trs_year'            => $year,
                'trs_month'           => $month,
                'trs_kode_rekening'   => $trs_kode_rekening_piutang,
                'trs_nama_rekening'   => $trs_nama_rekening_piutang,
                'trs_tipe_arus_kas'   => $tipe_arus_kas,
                'trs_catatan'         => $keterangan,
                'trs_date_insert'     => $date_db,
                'trs_date_update'     => $date_db,
                'trs_charge'          => 0,
                'trs_no_check_bg'     => 0,
                'trs_tgl_pencairan'   => date('Y-m-d'),
                'trs_setor'           => 0,
                'tgl_transaksi'       => date('Y-m-d', strtotime($jurnal_umum->jmu_tanggal))
            ];
            mTransaksi::insert($data_transaksi_persediaan);
            //end insert data yg dipilih ke tb_ac_transaksi

            //insert data piutang lain-lain yang di dalam tabel transaksi
            $master             = $request->kode_debet;
            $jenis_transaksi    = "debet";
            $debet              = $request->hs_amount;
            $kredit             = 0;
            $tipe_arus_kas2     = "Operasi";
            $catatan            = $keterangan;

            foreach($master as $key=>$val) {
                $coa = mPerkiraan::where('mst_kode_rekening',$master[$key])->first();
                $trs_kode_rekening = $coa->mst_kode_rekening;
                $trs_nama_rekening = $coa->mst_nama_rekening;
            
                $data_transaksi             = [
                    'jurnal_umum_id'        => $jurnal_umum->jurnal_umum_id,
                    'master_id'             => $coa->master_id,
                    'trs_jenis_transaksi'   => $jenis_transaksi,
                    'trs_debet'             => $debet,
                    'trs_kredit'            => $kredit,
                    'user_id'               => 0,
                    'trs_year'              => $year,
                    'trs_month'             => $month,
                    'trs_kode_rekening'     => $trs_kode_rekening,
                    'trs_nama_rekening'     => $trs_nama_rekening,
                    'trs_tipe_arus_kas'     => $tipe_arus_kas2,
                    'trs_catatan'           => $catatan,
                    'trs_date_insert'       => $date_db,
                    'trs_date_update'       => $date_db,
                    'trs_charge'            => 0,
                    'trs_no_check_bg'       => '',
                    'trs_tgl_pencairan'     => date('Y-m-d'),
                    'trs_setor'             => 0,
                    'tgl_transaksi'         => date('Y-m-d', strtotime($jurnal_umum->jmu_tanggal))
                ];
                
                mTransaksi::insert($data_transaksi);
            }
            mTransaksi::where('jurnal_umum_id',$jurnal_umum->jurnal_umum_id)->where('trs_debet',0)->where('trs_kredit',0)->delete();
                //end insert data hutang lain-lain otomatis ke tabel tb_ac_transaksi
            // mJurnalUmum::where('jurnal_umum_id',$jurnal_umum->jurnal_umum_id)->delete();
            \DB::commit();

            return [
                'redirect'=>route('hutangSuplier')
            ];
        }catch(Exception $e) {
            throw $e;
            \DB::rollBack();    
                
        }
        
    }      
}
