
                                    <?php
                                        $total_debet = 0;
                                        $total_kredit = 0;
                                        $no = 1;
                                    ?>
                                    <table class="table table-striped table-bordered table-hover table-header-fixed">
                                        <thead>
                                            <tr class="">
                                                <th width="10"><center> No </center></th>
                                                <th><center> Tanggal </center></th>
                                                <th><center> No Bukti </center></th>
                                                <th><center> Keterangan </center></th>
                                                <th><center> No Akun </center></th>
                                                <th><center> Debet </center></th>
                                                <th><center> Kredit </center></th>
                                                <th><center> Catatan </center></th>
                                                <th><center> Menu </center></th>
                                            </tr>
                                        </thead>                                        
                                        <tbody>
                                            @foreach($jurnalUmum as $jmu)
                                            <tr>
                                                <td align="center"> {{ $no++ }}. </td>
                                                    <td> {{ date('d M Y', strtotime($jmu->jmu_date_insert)) }} </td>
                                                    <td><center> {{ $jmu->no_invoice }} </center></td>
                                                    <td> {{ $jmu->jmu_keterangan }}</td>
                                                    <td colspan="5"></td>
                                            </tr>
                                            @foreach($jmu->transaksi as $trs)
                                            <tr>                                
                                                <td></td>
                                                <td></td>
                                                <td></td>                                
                                                <td>   {{ $trs->trs_nama_rekening }}</td>
                                                <td><center>   {{ $trs->trs_kode_rekening }}</center></td>
                                                <td align="right">   {{ number_format($trs->trs_debet,2) }} </td>
                                                <td align="right">   {{ number_format($trs->trs_kredit,2) }} </td>
                                                <td>   {{ $trs->trs_catatan }} </td>
                                                <td>
                                                    <div class="btn-group btn-group-xs">
                                                        <a class="btn btn-success" href="{{ route('editJurnalUmum', ['kode'=>$trs->jurnal_umum_id]) }}">
                                                            <span class="icon-pencil"></span>
                                                        </a>
                                                    </div>
                                                </td>                                    
                                            </tr>
                                            <?php
                                                $total_debet += $trs->trs_debet;
                                                $total_kredit += $trs->trs_kredit;
                                            ?>
                                            @endforeach
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr class="">
                                                <th width="10"></th>
                                                <th colspan="4" align="center"><center><strong> TOTAL </strong></center></th>
                                                <th><center><strong>{{number_format($total_debet,2)}} </strong></center></th>
                                                <th><center><strong>{{number_format($total_kredit,2)}} </th>
                                                @if($total_debet==$total_kredit && $total_debet>0 && $total_kredit>0)
                                                <th>
                                                    <strong>Status : <font color="green">Balance</font></strong>
                                                </th>
                                                @endif
                                                @if($total_debet!=$total_kredit)
                                                <th>
                                                    <strong>Status : <font color="red">Not Balance</font></strong>
                                                </th>
                                                @endif
                                                @if($total_debet==0 && $total_kredit==0)
                                                <th>
                                                    <strong>Status : <font color="red"></font></strong>
                                                </th>
                                                @endif
                                                <th> </th>
                                            </tr>
                                        </tfoot>
                                    </table>
<script>
    $('[data-toggle="tooltip"]').tooltip();
</script>