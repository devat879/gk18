
<div class="modal-header bg-blue-steel bg-font-blue-steel">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
  <h4 class="modal-title"> Unclock Edit Pembelian </h4>
</div>
            <div class="modal-body form-horizontal">
                <form action="<?php echo e(route('unlock-edit-pembelian')); ?>" class="form-horizontal" role="form" method="post">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-body">
                        <div class="form-group">
                            <div style="padding-top: 10px" class="col-md-3">
                                <label>Username</label>
                            </div>
                            <div style="padding-top: 5px" class="col-md-9">
                              <input type="text" name="username" class="form-control">
                            </div>
                        </div>                      
                        
                        <div class="form-group">
                            <div style="padding-top: 10px" class="col-md-3">
                                <label>Password</label>
                            </div>
                            <div style="padding-top: 5px" class="col-md-9">
                              <input type="password" name="password" class="form-control">
                            </div>
                            <input type="hidden" name="id_pembelian" class="form-control" value="<?php echo e($id); ?>">
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Login</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
<!-- <div class="modal-footer"> -->
    
<!-- </div> -->