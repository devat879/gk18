<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/piutangPelanggan.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
<div class="page-content-inner">
    <div class="mt-content-body">
    <?php if($message = Session::get('success')): ?>
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong><?php echo e($message); ?></strong>
        </div>
    <?php endif; ?>
        <div class="row">
            <div class="col-md-12>
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="portlet-body">
                            <!-- <div class="tab-content">
                                <div class="tab-pane fade active in" id="tab_jurnal_list"> -->
                                <div class="col-md-6 col-xs-6">
                                    <a style="font-size:12px;" class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button">
                                        Pilih Periode
                                    </a>
                                    <a style="font-size: 11px" type="button" class="btn btn-danger" href="<?php echo e(route('printlapReturnPembelianTunaiKreditCutOff', ['start_date'=>$start_date, 'end_date'=>$end_date, 'spl_id'=>$spl,'tipe'=>'print'])); ?>" target="_blank">
                                        <span><i class="fa fa-print"></i></span> Print
                                    </a>
                                </div>
                                    
                                    <br /><br />
                                    <div class="col-md-12">
                                        <h3><center>Laporan Retur Pembelian Tunai Kredit</center></h3>
                                        <h4><center><?php echo e(date('d M Y', strtotime($start_date))); ?> s/d <?php echo e(date('d M Y', strtotime($end_date))); ?></center></h4> 
                                        
                                    </div>
                                                                     
                                    
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" width="100%">
                                        <thead>
                                            <tr class="success">
                                                <th style="font-size:12px;" align="center"><center> No </center></th>
                                                <th style="font-size:12px;" align="center"><center> Tanggal </center></th>
                                                <th style="font-size:12px;" align="center"><center> No Jurnal </center></th>
                                                <th style="font-size:12px;" align="center"><center> No Invoice </center></th>
                                                <th style="font-size:12px;" align="center"><center> Nama </center></th>
                                                <th style="font-size:12px;" align="center"><center> Harga Beli (DPP) </center></th>
                                                <th style="font-size:12px;" align="center"><center> Diskon </center></th>
                                                <th style="font-size:12px;" align="center"><center> PPN </center></th>
                                                <th style="font-size:12px;" align="center"><center> Cash </center></th>
                                                <th style="font-size:12px;" align="center"><center> Credit </center></th>
                                                <th style="font-size:12px;" align="center"><center> Tgl Jatuh Tempo </center></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $ppn_cash      = 0;
                                                $diskon_cash   = 0;
                                                $ppn_credit    = 0;
                                                $diskon_credit = 0; 
                                            ?>
                                            <?php $__currentLoopData = $dataListCash; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td style="font-size:11px;" align="center"><?php echo e($no++); ?></td>
                                                <td style="font-size:11px;"><?php echo e($data->tgl_pengembalian); ?></td>
                                                <td style="font-size:11px;"><?php echo e($data->no_return_pembelian); ?></td>
                                                <td style="font-size:11px;"><?php echo e($data->pembelianSupplier->no_invoice); ?></td>
                                                <td style="font-size:11px;"><?php echo e($data->supplier->spl_nama); ?></td>
                                                <td style="font-size:11px;" align="right"><?php echo e(number_format($data->total_retur,2)); ?></td>
                                                <td style="font-size:11px;" align="right"><?php echo e(number_format($data->pembelianSupplier->ps_disc_nom,2)); ?></td>
                                                <td style="font-size:11px;" align="right"><?php echo e(number_format($data->ppn_nom,2)); ?></td>
                                                <td style="font-size:11px;" align="right"><?php echo e(number_format($data->total_retur+$data->pembelianSupplier->ps_disc_nom+$data->ppn_nom,2)); ?></td>
                                                <td style="font-size:11px;">-</td>
                                                <td style="font-size:11px;">-</td>
                                            </tr> 
                                            <?php
                                                $ppn_cash        = $ppn_cash+$data->ppn_nom;
                                                $diskon_cash     = $diskon_cash+$data->pembelianSupplier->ps_disc_nom;
                                            ?> 
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td style="font-size:12px;"><strong>Subtotal</strong></td>
                                                <td style="font-size:12px;"><strong><?php echo e(number_format($dataListCash->sum('total_retur'),2)); ?></strong></td>
                                                <td style="font-size:12px;"><strong><?php echo e(number_format($diskon_cash,2)); ?></strong></td>
                                                <td style="font-size:12px;"><strong><?php echo e(number_format($ppn_cash,2)); ?></strong></td>
                                                <td style="font-size:12px;"><strong><?php echo e(number_format($dataListCash->sum('total_retur')+$ppn_cash+$diskon_cash,2)); ?></strong></td>
                                                <td style="font-size:12px;"><strong>-</strong></td>
                                                <td></td>
                                            </tr> 
                                            <?php $__currentLoopData = $dataListCredit; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dataCredit): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td style="font-size:11px;" align="center"><?php echo e($no++); ?></td>
                                                <td style="font-size:11px;"><?php echo e($dataCredit->tgl_pengembalian); ?></td>
                                                <td style="font-size:11px;"><?php echo e($kodePembelian.$dataCredit->no_return_pembelian); ?></td>
                                                <td style="font-size:11px;"><?php echo e($dataCredit->pembelianSupplier->no_invoice); ?></td>
                                                <td style="font-size:11px;"><?php echo e($dataCredit->supplier->spl_nama); ?></td>
                                                <td style="font-size:11px;" align="right"><?php echo e(number_format($dataCredit->total_retur,2)); ?></td>
                                                <td style="font-size:11px;" align="right"><?php echo e(number_format($dataCredit->pembelianSupplier->ps_disc_nom,2)); ?></td>
                                                <td style="font-size:11px;" align="right"><?php echo e(number_format($dataCredit->ppn_nom,2)); ?></td>
                                                <td style="font-size:11px;">-</td>
                                                <td style="font-size:11px;"><?php echo e(number_format($dataCredit->total_retur+$dataCredit->pembelianSupplier->ps_disc_nom+$dataCredit->ppn_nom,2)); ?></td>
                                                <td style="font-size:11px;"><?php echo e($dataCredit->ps_tgl); ?></td>
                                            </tr>
                                            <?php
                                                $ppn_credit        = $ppn_credit+$dataCredit->ppn_nom;
                                                $diskon_credit     = $diskon_credit+$dataCredit->pembelianSupplier->ps_disc_nom;
                                            ?> 
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td style="font-size:12px;"><strong>Subtotal</strong></td>
                                                <td style="font-size:12px;"><strong><?php echo e(number_format($dataListCredit->sum('total_retur'),2)); ?></strong></td>
                                                <td style="font-size:12px;"><strong><?php echo e(number_format($diskon_credit,2)); ?></strong></td>
                                                <td style="font-size:12px;"><strong><?php echo e(number_format($ppn_credit,2)); ?></strong></td>
                                                <td style="font-size:12px;"><strong>-</strong></td>
                                                <td style="font-size:12px;"><strong><?php echo e(number_format($dataListCredit->sum('total_retur')+$ppn_credit+$diskon_credit,2)); ?></strong></td>
                                                <td></td>
                                            </tr>                                                                                 
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                    </table>
                                    
                                <!-- </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Pilih Periode
                </h4>
            </div>
            <div class="modal-body form">
                <form action="<?php echo e(route('pilihPeriodePembelian')); ?>" class="form-horizontal form-send" role="form" method="post">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-body">
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Supplier</label>
                            </div>
                            <div class="col-md-9">
                                <!-- <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date"/> -->
                                <select style="font-size: 11px" class="form-control select2" name="supplier_id">
                                    <option value="0">All Supplier</option>
                                    <?php $__currentLoopData = $suppliers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $supplier): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($supplier->spl_kode); ?>" <?php if($spl==$supplier->spl_kode) echo 'selected'?>><?php echo e($supplier->spl_nama); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date"/>
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                            <div class="col-md-2">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date"/>                                
                                <input type="hidden" name="tipe_laporan" value="returnPembelianTunaiKredit">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>