

<?php $__env->startSection('css'); ?>
  <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
  
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
  <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
  
  
  
  
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="portlet light">
              <button class="btn btn-primary" data-toggle="modal" href="#modal-tambah">
                <i class="fa fa-plus"></i> Tambah User
              </button>
              <br /><br />
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                <thead>
                  <tr class="">
                    <th style="font-size:10px" width="10"> No </th>
                    <th style="font-size:10px"> Kode User </th>
                    <th style="font-size:10px"> Username </th>
                    <th style="font-size:10px"> Role </th>
                    <th style="font-size:10px"> Karyawan </th>
                    <th style="font-size:10px"> Inisial </th>
                    <th style="font-size:10px"> Action </th>
                  </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td style="font-size:10px"> <?php echo e($no++); ?>. </td>
                      <td style="font-size:10px"> <?php echo e($kodeLabel.$row->user_kode); ?> </td>
                      <td style="font-size:10px"> <?php echo e($row->username); ?> </td>
                      <td style="font-size:10px"> <?php echo e(ucwords($row->role)); ?> </td>
                      <td style="font-size:10px"> <?php echo e($row->kry_nama); ?> </td>
                      <td style="font-size:10px"> <?php echo e($row->kode); ?> </td>
                      <td style="font-size:10px">
                        <div class="btn-group-xs">
                          <button class="btn btn-success btn-edit" data-href="<?php echo e(route('userEdit', ['kode'=>$row->user_kode])); ?>">
                            <span class="icon-pencil"></span> Edit
                          </button>
                          <button class="btn btn-danger btn-delete-new" data-href="<?php echo e(route('userDelete', ['kode'=>$row->user_kode])); ?>">
                            <span class="icon-trash"></span> Delete
                          </button>
                        </div>
                      </td>
                    </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modal-tambah" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-plus"></i> Tambah User
          </h4>
        </div>
        <div class="modal-body form">
          <form action="<?php echo e(route('userInsert')); ?>" class="form-horizontal form-send" role="form" method="post">
            <?php echo e(csrf_field()); ?>

            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Username</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="username">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Password</label>
                <div class="col-md-9">
                  <input type="password" class="form-control" name="password">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Inisial</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="kode" maxlength="2">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Role</label>
                <div class="col-md-9">
                  <select class="form-control" name="role">
                    <option value="master">Master</option>
                    <option value="admin">Admin</option>
                    <option value="accounting">Accounting</option>
                    <option value="logistik">Logistik</option>
                    <option value="penjualan">Penjualan</option>
                    <option value="purchasing">Pembelian</option>
                    <option value="sales_person">Sales Person</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Karyawan</label>
                <div class="col-md-9">
                  <select class="form-control" name="kry_kode">
                    <?php $__currentLoopData = $karyawan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($kry->kry_kode); ?>"><?php echo e($kry->kry_nama); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal" id="modal-edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Edit User
          </h4>
        </div>
        <div class="modal-body form">
          <form action="" class="form-horizontal form-send" role="form" method="put">
            <?php echo e(csrf_field()); ?>

            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Username</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="username">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Password</label>
                <div class="col-md-9">
                  <input type="password" class="form-control" name="password">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Inisial</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="kode" maxlength="2">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Role</label>
                <div class="col-md-9">
                  <select class="form-control" name="role">
                    <option value="master">Master</option>
                    <option value="admin">Admin</option>
                    <option value="accounting">Accounting</option>
                    <option value="logistik">Logistik</option>
                    <option value="penjualan">Penjualan</option>
                    <option value="purchasing">Pembelian</option>
                    <option value="sales_person">Sales Person</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Karyawan</label>
                <div class="col-md-9">
                  <select class="form-control" name="kry_kode">
                    <?php $__currentLoopData = $karyawan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($kry->kry_kode); ?>"><?php echo e($kry->kry_nama); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>