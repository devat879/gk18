<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mDetailSuratJalanPT extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_detail_surat_jalan_penjualan_titipan';
  protected $primaryKey = 'det_sjt_kode';
  // public $timestamps = false;

  public function SuratJalan()
  {
    return $this->belongsTo('App\Models\mSuratJalanPT', 'sjt_kode');
  }

  public function Barang()
  {
    return $this->belongsTo('App\Models\mBarang', 'brg_kode');
  }

  public function Gudang()
  {
    return $this->belongsTo('App\Models\mGudang', 'gdg_kode');
  }

  public function satuanJ()
  {
    return $this->belongsTo('App\Models\mSatuan', 'stn_kode')->select('stn_kode', 'stn_nama');
  }
}
