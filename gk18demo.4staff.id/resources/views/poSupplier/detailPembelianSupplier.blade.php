@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="../assets/pages/css/invoice.min.css" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                            <p>A.K.I., Jl. Gatot Subroto No XX Tlp. 0361 xxx xxx</p>
                            <hr/>
                            <div class="row">
                                <div class="col-xs-4">
                                    <ul class="list-unstyled">
                                        <li> No. Faktur : {{$pembelian->no_pembelian}} </li>
                                        <li> No Invoice : {{$pembelian->no_invoice}}</li>
                                        <li> Lampiran : 1 Lembar</li>
                                    </ul>
                                </div>
                                <div class="col-xs-4">
                                </div>
                                <div class="col-xs-4">
                                    <h5></h5>
                                    <ul class="list-unstyled">
                                        <li> Supplier : </li>
                                        <li> {{$pembelian->supplier->spl_nama}} </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th style="font-size: 12px"><center> No </center></th>
                                                <th style="font-size: 12px"><center> Nama Barang </center></th>
                                                <th style="font-size: 12px"><center> No Seri </center></th>
                                                <th style="font-size: 12px" class="hidden-xs"><center> Satuan </center></th>
                                                <th style="font-size: 12px" class="hidden-xs"><center> QTY </center></th>
                                                <th style="font-size: 12px" class="hidden-xs"><center> Harga </center></th>
                                                <th style="font-size: 12px" class="hidden-xs"><center> Total </center></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($pembelian->detailPembelian as $detail)   
                                            <tr>
                                                <td style="font-size: 11px"><center> {{ $no++ }} </center></td>
                                                <td style="font-size: 11px"> {{ $detail->nama_barang }} </td>
                                                <td style="font-size: 11px"> {{ $detail->stok->brg_no_seri }} </td>
                                                <td style="font-size: 11px"><center> {{ $detail->satuans->stn_nama }} <center></td>
                                                <td style="font-size: 11px"><center> {{ $detail->qty }} </center></td>
                                                <td style="font-size: 11px"> {{ number_format($detail->harga_net,2) }} </td>
                                                <td style="font-size: 11px" align="right"> {{ number_format($detail->total,2) }} </td>
                                            </tr>                                            
                                        @endforeach
                                            <tr height="2px">
                                                <td style="font-size: 12px" colspan="6" align="right"><strong>Subtotal</strong></td>
                                                <td style="font-size: 12px" align="right"><strong>{{number_format($pembelian->ps_subtotal,2)}}</strong></td>
                                            </tr>
                                            @if($pembelian->ps_ppn_nom > 0)
                                            <tr>
                                                <td style="font-size: 12px" colspan="6" align="right">Diskon</td>
                                                <td style="font-size: 12px" align="right">{{$pembelian->ps_disc}}% - {{number_format($pembelian->ps_disc_nom,2)}}</td>
                                            </tr>
                                            @endif
                                            @if($pembelian->ps_disc_nom > 0)
                                            <tr>
                                                <td style="font-size: 12px" colspan="6" align="right">PPN</td>
                                                <td style="font-size: 12px" align="right">{{$pembelian->ps_ppn}}% - {{number_format($pembelian->ps_ppn_nom,2)}}</td>
                                            </tr>
                                            @endif
                                            @if($pembelian->biaya_lain > 0)
                                            <tr>
                                                <td style="font-size: 12px" colspan="6" align="right">Biaya Lain-Lain</td>
                                                <td style="font-size: 12px" align="right">{{number_format($pembelian->biaya_lain,2)}}</td>
                                            </tr>
                                            @endif                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td style="font-size: 12px" colspan="6" align="right">Grand Total</td>
                                                <td style="font-size: 12px" align="right">{{number_format($pembelian->grand_total,2)}}</td>
                                            </tr>
                                        </foot>
                                    </table>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <table class="col-md-12" width="100%">
                                        <tr>
                                            <td colspan="5" width="45%">n.b : </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" width="45%"> - {{$pembelian->ps_catatan}} </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-3">
                                    <table class="col-md-12" width="100%">
                                        <tr><td>Denpasar</td></tr>
                                        <tr><td>GrahaKita 88</td></tr>
                                        <tr></tr>
                                    </table>
                                </div>
                            </div>
                            <hr>
                            <!-- <a style="font-size: 12px;" class="btn btn-lg blue hidden-print margin-bottom-5" href="{{route('printdetailPembelian',['id'=>$id, 'tipe'=>'print'])}}" target="_blank"> Print
                                <i class="fa fa-print"></i>
                            </a> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop