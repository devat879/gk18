<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mWilayah extends Model
{
  public $incrementing = false;
  protected $table = 'tb_wilayah';
  protected $primaryKey = 'wlh_kode';
  public $timestamps = false;

  public function provinsi()
  {
    return $this->belongsTo('App\Models\mProvinsi', 'prov_kode');
  }
}
