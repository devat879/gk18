<?php

namespace App\Http\Controllers;

use App\Models\mHistoryCetakFaktur;
use App\Helpers\Main;
use Carbon\Carbon;
use App\Models\mUser;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;

class HistoryCetakFaktur extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'History Cetak Faktur';
      $this->kodeLabel = 'history_cetak_faktur';
  }

  function index() {
      $breadcrumb = [
          'History Cetak Faktur'=>route('HistoryCetakFakturList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'history_cetak_faktur';
      $data['title'] = $this->title;
      $data['dataList'] = mHistoryCetakFaktur::select(\DB::raw('no_faktur, count(no_faktur) as count'))->groupBy('no_faktur')->get();
      return view('history/CetakFakturList', $data);
  }

  function detail($id='') {
      $breadcrumb = [
          'History Cetak Faktur'=>route('HistoryCetakFakturList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'history_cetak_faktur';
      $data['title'] = $this->title;
      $data['dataList'] = mHistoryCetakFaktur::where('no_faktur', $id)->orderBy('tgl', 'desc')->get();
      return view('history/DetailCetakFakturList', $data);
  }

  function store(Request $request) {
    // return $request;
    \DB::beginTransaction();
    try {
      $target = mUser::where('role', 'master')->where('username', $request->username)->first();
      if (Hash::check($request->password, $target->password)) {
        $time = Carbon::now();

        $data = new mHistoryCetakFaktur();
        $data->no_faktur = $request->no_faktur;
        $data->keterangan = $request->keterangan;
        $data->tgl = $time;
        $data->person = $request->person;
        $data->save();

        \DB::commit();
        return 'true';
      }
      else {
        return 'false';
      }

      // return [
      //   'redirect'=>route('HistoryCetakFakturList')
      // ];
    } catch (\Exception $e) {
      \DB::rollBack();
      throw $e;
    }
  }
}
