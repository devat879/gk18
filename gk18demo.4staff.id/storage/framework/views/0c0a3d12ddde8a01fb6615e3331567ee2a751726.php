<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

    <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />   
    <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/piutangPelanggan.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
    <script src="<?php echo e(asset('assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-select2.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('.date-picker').datepicker()
            .on('changeDate', function(ev){                 
                $('.date-picker').datepicker('hide');
            });
            
        });
    </script>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
<style type="text/css">
    .table .head{font-size: 13px;font-family: Tahoma;text-align: center;vertical-align: middle !important;padding-top: 1px;font-weight: bold;}
    .table td{font-size: 12px;font-family: Tahoma;padding-top: 1px;}
</style>
<div class="page-content-inner">
    <div class="mt-content-body">        
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet-body">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#langsung" data-toggle="tab"> Hutang Supplier </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="langsung">
                                <div class="col-md-6 col-xs-6">
                                    <a style="font-size: 11px" class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button">
                                            Pilih Periode
                                    </a>
                                    <a style="font-size: 11px" type="button" class="btn btn-danger" href="<?php echo e(route('printUmurHutang',['start_date'=>$start_date,'end_date'=>$end_date, 'coa'=>$coa, 'spl'=>$spl_id, 'tipe'=>'print'])); ?>" target="_blank">
                                        <span><i class="fa fa-print"></i></span> Print
                                    </a>
                                    <a style="font-size: 12px;" class="btn btn-info excel-btn" data-toggle="modal" type="button" href="#export-excel" >
                                        Excel
                                    </a>
                                </div>
                                <div class="portlet light">
                                    <?php $tgl_akhir= $end_date;?>                        
                                    <h3 style="font-family: Tahoma"><center>Umur Hutang</center></h3>
                                    <h4 style="font-family: Tahoma"><center><?php echo e(date('d M Y', strtotime($start_date))); ?> s/d <?php echo e(date('d M Y', strtotime($end_date))); ?></center></h4>
                                    <br /><br />
                                    <h6 style="font-family: Tahoma;font-weight: bold">Kode Perkiraan : <?php if($coa!=0): ?> <?php echo e($coa); ?> - <?php echo e($kode_coa[$coa]); ?><?php else: ?> All <?php endif; ?> | Supplier : <?php echo e($spl_nama); ?></h6>
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr class="success">
                                                <td rowspan="3" class="head"> No </td>
                                                <td rowspan="3" class="head"> Nama Supplier </td>
                                                <td rowspan="3" class="head"> Tgl Inv</td>
                                                <td rowspan="3" class="head"> No Inv </td>
                                                <td rowspan="3" class="head"> Tempo </td>
                                                <td rowspan="3" class="head"> Tgl Jth Temp </td>
                                                <td colspan="5" class="head">Umur Hutang</td>
                                            </tr>
                                            <tr class="success">
                                                <td colspan="2" class="head">Belum Jatuh Tempo</td>
                                                <td colspan="3" class="head">Sudah Jatuh Tempo</td>
                                            </tr>
                                            <tr class="success">
                                                <td class="head">1-30 days</td>
                                                <td class="head">30-60 days</td>
                                                <td class="head">1-30 days</td>
                                                <td class="head">30-60 days</td>
                                                <td class="head">60-90 days</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $ttl_a=0;$ttl_b=0;$ttl_c=0;$ttl_d=0;$ttl_e=0;?>
                                        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hutang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($no++); ?></td>
                                                <td><?php echo e($hutang->suppliers->spl_nama); ?></td>
                                                <td><?php echo e(date('d M Y', strtotime($hutang->tgl_hutang))); ?></td>
                                                <td><?php echo e($hutang->ps_no_faktur); ?></td>
                                                <?php
                                                    $tgl_beli           = new DateTime($hutang->tgl_hutang);
                                                    $jth_tmp            = new DateTime($hutang->js_jatuh_tempo);
                                                    $difference         = $tgl_beli->diff($jth_tmp);
                                                    $tmp                = $difference->days;
                                                    // $today = date('Y-m-d');
                                                    // $today              = $end_date;
                                                    $today              = $tgl_akhir;
                                                    $a='-';$b='-';$c='-';$d='-';$e='-';

                                                    if(strtotime($hutang->js_jatuh_tempo)> strtotime($today)){
                                                        //belum jtuh tempo
                                                        $tgl_beli       = new DateTime($hutang->tgl_hutang);
                                                        $today          = new DateTime($today);
                                                        $difference     = $tgl_beli->diff($today);
                                                        $days           = $difference->days;
                                                        if($days<=30){
                                                            $aa         = $hutang->sisa_amount;      
                                                            $a          = number_format($hutang->sisa_amount,2);
                                                            $ttl_a      = $ttl_a+$aa;
                                                        }else{
                                                            $bb         = $hutang->sisa_amount;
                                                            $b          = number_format($hutang->sisa_amount,2);
                                                            $ttl_b      = $ttl_b+$bb;
                                                        }                                            
                                                    }else{
                                                        //sudah jatuh tempo
                                                        $jth_tmp        = new DateTime($hutang->js_jatuh_tempo);
                                                        $end_date       = new DateTime($today);
                                                        $difference     = $jth_tmp->diff($end_date);
                                                        $days           = $difference->days;
                                                        if($days<=30){
                                                            $cc         = $hutang->sisa_amount;
                                                            $c          = number_format($hutang->sisa_amount,2);
                                                            $ttl_c      = $ttl_c+$cc;
                                                        }elseif($days<=60){
                                                            $dd         = $hutang->sisa_amount;
                                                            $d          = number_format($hutang->sisa_amount,2);
                                                            $ttl_d      = $ttl_d+$dd;
                                                        }else{
                                                            $ee         = $hutang->sisa_amount;
                                                            $e          = number_format($hutang->sisa_amount,2);
                                                            $ttl_e      = $ttl_e+$ee;
                                                        }  
                                                    }                                        
                                                ?>
                                                <td><?php echo e($tmp); ?> days</td>
                                                <td><?php echo e(date('d M Y', strtotime($hutang->js_jatuh_tempo))); ?></td>
                                                <!--belum jatuh tempo-->                                    
                                                <td><?php echo e($a); ?></td>
                                                <td><?php echo e($b); ?></td>
                                                <!--sudah jatuh tempo-->
                                                <td><?php echo e($c); ?></td>
                                                <td><?php echo e($d); ?></td>
                                                <td><?php echo e($e); ?></td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php $__currentLoopData = $data_hl; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($no++); ?></td>
                                                <td><?php echo e($hl->spl->spl_nama); ?></td>
                                                <td><?php echo e(date('d M Y', strtotime($hl->hl_tgl))); ?></td>
                                                <td><?php echo e($hl->no_hutang_lain); ?></td>
                                                <?php
                                                    $tgl_beli           = new DateTime($hl->hl_tgl);
                                                    $jth_tmp            = new DateTime($hl->hl_jatuh_tempo);
                                                    $difference         = $tgl_beli->diff($jth_tmp);
                                                    $tmp                = $difference->days;
                                                    // $today = date('Y-m-d');
                                                    // $today              = $end_date;
                                                    $today              = $tgl_akhir;
                                                    $a='-';$b='-';$c='-';$d='-';$e='-';
                                                    

                                                    if(strtotime($hl->hl_jatuh_tempo)> strtotime($today)){
                                                        //belum jtuh tempo
                                                        $tgl_beli       = new DateTime($hl->hl_tgl);
                                                        $today          = new DateTime($today);
                                                        $difference     = $tgl_beli->diff($today);
                                                        $days           = $difference->days;
                                                        if($days<=30){
                                                            $aa         = $hl->hl_sisa_amount;      
                                                            $a          = number_format($hl->hl_sisa_amount,2);
                                                            $ttl_a      = $ttl_a+$aa;
                                                        }else{
                                                            $bb         = $hl->hl_sisa_amount;
                                                            $b          = number_format($hl->hl_sisa_amount,2);
                                                            $ttl_b      = $ttl_b+$bb;
                                                        }                                            
                                                    }else{
                                                        //sudah jatuh tempo
                                                        $jth_tmp        = new DateTime($hl->hl_jatuh_tempo);
                                                        $end_date       = new DateTime($today);
                                                        $difference     = $jth_tmp->diff($end_date);
                                                        $days           = $difference->days;
                                                        if($days<=30){
                                                            $cc         = $hl->hl_sisa_amount;
                                                            $c          = number_format($hl->hl_sisa_amount,2);
                                                            $ttl_c      = $ttl_c+$cc;
                                                        }elseif($days<=60){
                                                            $dd         = $hl->hl_sisa_amount;
                                                            $d          = number_format($hl->hl_sisa_amount,2);
                                                            $ttl_d      = $ttl_d+$dd;
                                                        }else{
                                                            $ee         = $hl->hl_sisa_amount;
                                                            $e          = number_format($hl->hl_sisa_amount,2);
                                                            $ttl_e      = $ttl_e+$ee;
                                                        }  
                                                    }                                        
                                                ?>
                                                <td><?php echo e($tmp); ?> days</td>
                                                <td><?php echo e(date('d M Y', strtotime($hl->hl_jatuh_tempo))); ?></td>
                                                <!--belum jatuh tempo-->                                    
                                                <td><?php echo e($a); ?></td>
                                                <td><?php echo e($b); ?></td>
                                                <!--sudah jatuh tempo-->
                                                <td><?php echo e($c); ?></td>
                                                <td><?php echo e($d); ?></td>
                                                <td><?php echo e($e); ?></td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php $__currentLoopData = $data_hc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($no++); ?></td>
                                                <td><?php echo e($hc->suppliers->spl_nama); ?></td>
                                                <td><?php echo e(date('d M Y', strtotime($hc->tgl_cek))); ?></td>
                                                <td><?php echo e($hc->no_cek_bg); ?></td>
                                                <?php
                                                    $tgl_beli           = new DateTime($hc->tgl_cek);
                                                    $jth_tmp            = new DateTime($hc->tgl_pencairan);
                                                    $difference         = $tgl_beli->diff($jth_tmp);
                                                    $tmp                = $difference->days;
                                                    // $today = date('Y-m-d');
                                                    // $today              = $end_date;
                                                    $today              = $tgl_akhir;
                                                    $a='-';$b='-';$c='-';$d='-';$e='-';
                                                    

                                                    if(strtotime($hc->tgl_pencairan)> strtotime($today)){
                                                        //belum jtuh tempo
                                                        $tgl_beli       = new DateTime($hc->tgl_cek);
                                                        $today          = new DateTime($today);
                                                        $difference     = $tgl_beli->diff($today);
                                                        $days           = $difference->days;
                                                        if($days<=30){
                                                            $aa         = $hc->sisa;      
                                                            $a          = number_format($hc->sisa,2);
                                                            $ttl_a      = $ttl_a+$aa;
                                                        }else{
                                                            $bb         = $hc->sisa;
                                                            $b          = number_format($hc->sisa,2);
                                                            $ttl_b      = $ttl_b+$bb;
                                                        }                                            
                                                    }else{
                                                        //sudah jatuh tempo
                                                        $jth_tmp        = new DateTime($hc->tgl_pencairan);
                                                        $end_date       = new DateTime($today);
                                                        $difference     = $jth_tmp->diff($end_date);
                                                        $days           = $difference->days;
                                                        if($days<=30){
                                                            $cc         = $hc->sisa;
                                                            $c          = number_format($hc->sisa,2);
                                                            $ttl_c      = $ttl_c+$cc;
                                                        }elseif($days<=60){
                                                            $dd         = $hc->sisa;
                                                            $d          = number_format($hc->sisa,2);
                                                            $ttl_d      = $ttl_d+$dd;
                                                        }else{
                                                            $ee         = $hc->sisa;
                                                            $e          = number_format($hc->sisa,2);
                                                            $ttl_e      = $ttl_e+$ee;
                                                        }  
                                                    }                                        
                                                ?>
                                                <td><?php echo e($tmp); ?> days</td>
                                                <td><?php echo e(date('d M Y', strtotime($hc->tgl_pencairan))); ?></td>
                                                <!--belum jatuh tempo-->                                    
                                                <td><?php echo e($a); ?></td>
                                                <td><?php echo e($b); ?></td>
                                                <!--sudah jatuh tempo-->
                                                <td><?php echo e($c); ?></td>
                                                <td><?php echo e($d); ?></td>
                                                <td><?php echo e($e); ?></td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td colspan="6" style="font-size: 13px;font-weight: bold;">Total</td>
                                            <td style="font-size: 13px;font-weight: bold;"><?php echo e(number_format($ttl_a,2)); ?></td>
                                            <td style="font-size: 13px;font-weight: bold;"><?php echo e(number_format($ttl_b,2)); ?></td>
                                            <td style="font-size: 13px;font-weight: bold;"><?php echo e(number_format($ttl_c,2)); ?></td>
                                            <td style="font-size: 13px;font-weight: bold;"><?php echo e(number_format($ttl_d,2)); ?></td>
                                            <td style="font-size: 13px;font-weight: bold;"><?php echo e(number_format($ttl_e,2)); ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    Date
                </h4>
            </div>
            <div class="modal-body form">
                <form action="<?php echo e(route('periodeUmurHutang')); ?>" class="form-horizontal form-send" role="form" method="post">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-3">
                                <label style="padding-top: 5px;">COA</label>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control select2" name="kode_coa">
                                    <option value="0">All</option>
                                    <?php foreach ($kode_coa as $key => $value) {?>
                                    <option value="<?php echo e($key); ?>" <?php if($coa==$key) echo 'selected';?> ><?php echo e($key); ?> - <?php echo e($value); ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label style="padding-top: 5px;">Supplier</label>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control select2" name="spl_id">
                                    <option value="0">All Supplier</option>
                                    <?php foreach ($supplier as $spl) {?>
                                    <option value="<?php echo e($spl->spl_kode); ?>" <?php if($spl_id==$spl->spl_kode) echo 'selected';?> ><?php echo e($spl->spl_nama); ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date_pembelian" value="<?php echo e(date('Y-m-d', strtotime($start_date))); ?>" />
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                                <!-- <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date"/> -->
                            </div>
                            <div class="col-md-2">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date_pembelian" value="<?php echo e(date('Y-m-d')); ?>"/>
                                <input type="hidden" name="tipe_laporan" value="rekapStock">

                            </div>
                        </div>
                        
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-4 col-md-4">
                                <button type="submit" class="btn green col-md-6">Search</button>
                                <button type="button" class="btn default col-md-6" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="export-excel" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Filter By
                </h4>
            </div>
            <div class="modal-body form">
                <form action="<?php echo e(route('umurHutangPrintExcel')); ?>" class="form-horizontal" role="form" method="post"  target="_blank">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-3">
                                <label style="padding-top: 5px;">COA</label>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control select2" name="kode_coa">
                                    <option value="0">All</option>
                                    <?php foreach ($kode_coa as $key => $value) {?>
                                    <option value="<?php echo e($key); ?>" <?php if($coa==$key) echo 'selected';?> ><?php echo e($key); ?> - <?php echo e($value); ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label style="padding-top: 5px;">Supplier</label>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control select2" name="spl_id">
                                    <option value="0">All Supplier</option>
                                    <?php foreach ($supplier as $spl) {?>
                                    <option value="<?php echo e($spl->spl_kode); ?>" <?php if($spl_id==$spl->spl_kode) echo 'selected';?> ><?php echo e($spl->spl_nama); ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Tanggal</label>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date" value="<?php echo e($start_date); ?>" />
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                            <div class="col-md-1">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date"  value="<?php echo e($tgl_akhir); ?>"/>
                                <!-- <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="tgl_pencairan" /> -->
                                <input type="hidden" name="tipe_laporan" value="rekapPembelian">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Search</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>