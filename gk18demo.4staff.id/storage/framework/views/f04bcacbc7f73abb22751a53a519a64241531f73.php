

<?php $__env->startSection('css'); ?>
  <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
  <style media="screen">
  td.wrapok {
    white-space:nowrap
  }

  td.fontsize{
    font-size:10px
  }

  td.details-control {
      background: url("<?php echo e(asset('img/icon/details_open.png')); ?>") no-repeat center center;
      cursor: pointer;
  }
  tr.shown td.details-control {
      background: url("<?php echo e(asset('img/icon/details_close.png')); ?>") no-repeat center center;
  }
  </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
  <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
  
  
  
  
  
  <script type="text/javascript">
  $(document).ready(function() {
  var l = window.location;
  var base_url = l.protocol + "//" + l.host + "/";
  var table_barang = $('#sample_x').DataTable({
    destroy : true,
    processing: true,
    serverSide: true,
    ajax: base_url+'datatable/barang',
    columns: [
      {
        "className"     : 'details-control',
        "orderable"     : false,
        "data"          : null,
        "render"        : '<img src="<?php echo e(asset('img/icon/details_open.png')); ?>" />',
        "defaultContent": '<img src="<?php echo e(asset('img/icon/details_open.png')); ?>" />'
      },
      // {data: 'no', class:"fontsize"},
      {data: 'brg_kode', class:"wrapok fontsize"},
      {data: 'brg_barcode', class:"wrapok fontsize"},
      {data: 'brg_nama', class:"wrapok fontsize"},
      // {data: 'stn_nama', class:"wrapok fontsize"},
      // {data: 'ktg_nama', class:"wrapok fontsize"},
      // {data: 'grp_nama', class:"wrapok fontsize"},
      {data: 'mrk_nama', class:"wrapok fontsize"},
      // {data: 'spl_nama', class:"wrapok fontsize"},
      // {data: 'brg_harga_beli_terakhir', class:"fontsize", render: function ( data, type, row ) {
      //   return parseFloat(data).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
      //   }
      // },
      // {data: 'brg_harga_beli_tertinggi', class:"fontsize", render: function ( data, type, row ) {
      //   return parseFloat(data).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
      //   }
      // },
      // {data: 'hargaPPN', class:"fontsize", render: function ( data, type, row ) {
      //   return parseFloat(data).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
      //   }
      // },
      // {data: 'brg_hpp', class:"fontsize", render: function ( data, type, row ) {
      //   return parseFloat(data).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
      //   }
      // },
      // {data: 'brg_harga_jual_eceran', class:"fontsize", render: function ( data, type, row ) {
      //   return parseFloat(data).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
      //   }
      // },
      {data: 'eceranPPN', class:"fontsize", render: function ( data, type, row ) {
        return parseFloat(data).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
        }
      },
      // {data: 'brg_harga_jual_partai', class:"fontsize", render: function ( data, type, row ) {
      //   return parseFloat(data).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
      //   }
      // },
      {data: 'retailPPN', class:"fontsize", render: function ( data, type, row ) {
        return parseFloat(data).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
        }
      },
      {data: 'QOH', class:"fontsize"},
      {data: 'QOH_titipan', class:"fontsize"},
      // {data: 'brg_status', class:"wrapok fontsize"},
      { data: null, orderable: false, searchable: false, render: function ( data, type, row ) {
        return '<div style="font-size:10px; white-space: nowrap" class="btn-group-xs"> <button class="btn btn-info btn-print-barcode" data-href="'+base_url+'barcode/'+data.brg_kode+'"><span class="fa fa-print"></span> Barcode</button> <button class="btn btn-info btn-edit-stok" data-href="'+base_url+'barang/stok/'+data.brg_kode+'/edit"><span class="icon-pencil"></span> Stok</button><button class="btn btn-success btn-edit" data-href="'+base_url+'barang/'+data.brg_kode+'"><span class="icon-pencil"></span> Edit</button><button class="btn btn-danger btn-delete-new" data-href="'+base_url+'barang/delete/'+data.brg_kode+'"><span class="icon-trash"></span> Delete</button> </div>';
        }
      },
    ]
  });

  $('#sample_x tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table_barang.row( tr );

    if ( row.child.isShown() ) {
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      row.child( format(row.data()) ).show();
      tr.addClass('shown');
    }
  });
});

  function format ( d ) {
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
    '<tr>'+
    '<td>Satuan :</td>'+
    '<td>'+d.stn_nama+'</td>'+
    '</tr>'+
    '<tr>'+
    '<td>Kategory :</td>'+
    '<td>'+d.ktg_nama+'</td>'+
    '</tr>'+
    '<tr>'+
    '<td>Group :</td>'+
    '<td>'+d.grp_nama+'</td>'+
    '</tr>'+
    '<tr>'+
    '<td>Harga Beli Akhir :</td>'+
    '<td>'+parseFloat(d.brg_harga_beli_terakhir).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2})+'</td>'+
    '</tr>'+
    '<tr>'+
    '<td>Harga Beli Max:</td>'+
    '<td>'+parseFloat(d.brg_harga_beli_tertinggi).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2})+'</td>'+
    '</tr>'+
    '<tr>'+
    '<td>Harga Beli + PPN:</td>'+
    '<td>'+parseFloat(d.hargaPPN).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2})+'</td>'+
    '</tr>'+
    '<tr>'+
    '<td>HPP :</td>'+
    '<td>'+parseFloat(d.brg_hpp).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2})+'</td>'+
    '</tr>'+
    '</table>';
  }
  </script>
  <script src="<?php echo e(asset('js/barang.js')); ?>" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
  <span id="data-back"
  data-href_create_id="<?php echo e(route('barang.createID')); ?>">
</span>
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          
            <div class="portlet light">
              <button class="btn btn-primary btn-modal-tambah-barang">
              
                <i class="fa fa-plus"></i> Tambah Barang
              </button>
              <div class="btn-group-md pull-right">
                <button class="btn btn-success btn-print" data-toggle="modal" href="#modal-print">
                  <i class="glyphicon glyphicon-print"></i> Print to PDF
                </button>
                <button class="btn btn-success btn-print" data-toggle="modal" href="#modal-print-excel">
                  <i class="glyphicon glyphicon-print"></i> Print to Excel
                </button>
              </div>
              <br /><br />
              <table class="table table-striped table-bordered table-hover table-header-fixed display" id="sample_x">
                <thead>
                  <tr class="">
                    <th></th>
                    
                    <th style="font-size:10px"> Kode Barang </th>
                    <th style="font-size:10px"> Barcode </th>
                    <th style="font-size:10px"> Nama Barang </th>
                    
                    
                    
                    <th style="font-size:10px"> Merek </th>
                    
                    
                    
                    
                    
                    
                    <th style="font-size:10px"> H.Jual Retail + PPN </th>
                    
                    <th style="font-size:10px"> H.Jual Partai + PPN </th>
                    <th style="font-size:10px"> QOH </th>
                    <th style="font-size:10px"> QOH Titipan</th>
                    
                    <th style="font-size:10px"> Action </th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
          
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modal-tambah" tabindex="-1" role="basic" aria-hidden="true">
    <div class="">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-plus"></i> Tambah Barang
          </h4>
        </div>
        <div class="modal-body form">
          <form id="form-tambah" action="<?php echo e(route('barangInsert')); ?>" class="form-horizontal form-send-barang" role="form" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <div class="form-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group ktg">
                    <label class="col-md-3 control-label">Kategory</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="ktg_kode" required>
                        <option value="" selected>Pilih Kategory</option>
                        <?php $__currentLoopData = $k_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option value="<?php echo e($kategory->ktg_kode); ?>"><?php echo e($kategory->ktg_nama); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group grp">
                    <label class="col-md-3 control-label">Group</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="grp_kode" required>
                        <option value="">Pilih Group</option>
                        <?php $__currentLoopData = $g_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option value="<?php echo e($group->grp_kode); ?>"><?php echo e($group->grp_nama); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group mrk">
                    <label class="col-md-3 control-label">Merk</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="mrk_kode" required>
                        <option value="">Pilih Merk</option>
                        <?php $__currentLoopData = $merek; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $merk): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option value="<?php echo e($merk->mrk_kode); ?>"><?php echo e($merk->mrk_nama); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Gudang</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="gdg_kode" required>
                        <?php $__currentLoopData = $gudang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gdg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option value="<?php echo e($gdg->gdg_kode); ?>"><?php echo e($gdg->gdg_nama); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group kode">
                    <label class="col-md-3 control-label">Kode Barang</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" name="brg_kode" required readonly>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Barcode</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" name="brg_barcode" required readonly>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Nama</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" name="brg_nama" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Satuan</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="stn_kode">
                        <?php $__currentLoopData = $satuan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stn): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option value="<?php echo e($stn->stn_kode); ?>"><?php echo e($stn->stn_nama); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Product Image</label>
                    <div class="col-md-9">
                      <img id="imgScr" src="<?php echo e(asset('img/icon/no_image.png')); ?>" alt="image" width="100">
                      <div id="message"></div>
                      <input type="file" min="0" class="form-control" name="brg_product_img" id="imgBtn">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3 control-label">Stok Maximum</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required min="0" class="form-control" name="brg_stok_maximum">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Stok Minimum</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required min="0" class="form-control" name="brg_stok_minimum">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Harga Beli Terakhir</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_harga_beli_terakhir">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Harga Beli Tertinggi</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_harga_beli_tertinggi">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">HPP</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_hpp">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Harga Jual Eceran</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_harga_jual_eceran">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">harga Jual Partai</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_harga_jual_partai">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Supplier</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="spl_kode">
                        <?php $__currentLoopData = $supplier; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option value="<?php echo e($spl->spl_kode); ?>"><?php echo e($spl->spl_nama); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">PPN</label>
                    <div class="col-md-8">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_ppn_dari_supplier_persen">
                    </div>
                    <label class="control-label">%</label>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Status</label>
                    <div class="col-md-2">
                      <input type="checkbox" class="form-control" name="brg_status" value="Aktif">
                    </div>
                    <label class="control-label">Aktif</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="text-center">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>


  <div class="modal" id="modal-edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Edit Barang
          </h4>
        </div>
        <div class="modal-body form">
          <form action="" class="form-horizontal form-send-barang-edit" role="form" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <div class="form-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group ktg">
                    <label class="col-md-3 control-label">Kategory</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="ktg_kode" required>
                        <option value="">Pilih Kategory</option>
                        <?php $__currentLoopData = $k_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option value="<?php echo e($kategory->ktg_kode); ?>"><?php echo e($kategory->ktg_nama); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group grp">
                    <label class="col-md-3 control-label">Group</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="grp_kode" required>
                        <option value="">Pilih Group</option>
                        <?php $__currentLoopData = $g_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option value="<?php echo e($group->grp_kode); ?>"><?php echo e($group->grp_nama); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group mrk">
                    <label class="col-md-3 control-label">Merk</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="mrk_kode" required>
                        <option value="">Pilih Merk</option>
                        <?php $__currentLoopData = $merek; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $merk): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option value="<?php echo e($merk->mrk_kode); ?>"><?php echo e($merk->mrk_nama); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Gudang</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="gdg_kode" required>
                        <?php $__currentLoopData = $gudang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gdg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option value="<?php echo e($gdg->gdg_kode); ?>"><?php echo e($gdg->gdg_nama); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group kode">
                    <label class="col-md-3 control-label">Kode Barang</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" name="brg_kode" required readonly>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Barcode</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" name="brg_barcode" required readonly>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Nama</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" name="brg_nama" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Satuan</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="stn_kode">
                        <?php $__currentLoopData = $satuan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stn): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option value="<?php echo e($stn->stn_kode); ?>"><?php echo e($stn->stn_nama); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Product Image</label>
                    <div class="col-md-9">
                      <img id="imgScrEdit" alt="image" width="100">
                      <div id="messageEdit"></div>
                      <input type="file" min="0" class="form-control" name="brg_product_img" id="imgBtnEdit">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-3 control-label">Stok Maximum</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required min="0" class="form-control" name="brg_stok_maximum">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Stok Minimum</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required min="0" class="form-control" name="brg_stok_minimum">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Harga Beli Terakhir</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_harga_beli_terakhir">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Harga Beli Tertinggi</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_harga_beli_tertinggi">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">HPP</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_hpp">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Harga Jual Eceran</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_harga_jual_eceran">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">harga Jual Partai</label>
                    <div class="col-md-9">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_harga_jual_partai">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Supplier</label>
                    <div class="col-md-9">
                      <select class="form-control selectpicker" data-live-search="true" name="spl_kode">
                        <?php $__currentLoopData = $supplier; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option value="<?php echo e($spl->spl_kode); ?>"><?php echo e($spl->spl_nama); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">PPN</label>
                    <div class="col-md-8">
                      <input type="number" value="0" required step="0.01" min="0" class="form-control" name="brg_ppn_dari_supplier_persen">
                    </div>
                    <label class="control-label">%</label>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Status</label>
                    <div class="col-md-2">
                      <input type="checkbox" class="form-control" name="brg_status" value="Aktif">
                    </div>
                    <label class="control-label">Aktif</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="text-center">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal" id="modal-edit-stok" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Stok
          </h4>
        </div>
        <div class="modal-body form">
          <form action="" name="stok-form" class="form-horizontal form-stok" role="form" method="post">
            <?php echo e(csrf_field()); ?>

            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">No Seri</label>
                <div class="col-md-9">
                  <input id="stok-brg_kode" type="hidden" name="brg_kode" class="form-control" readonly>
                  <input id="stok-spl_kode" type="hidden" name="spl_kode" class="form-control" readonly>
                  
                  <input id="stok-brg_no_seri" type="text" name="brg_no_seri" class="form-control" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">QTY</label>
                <div class="col-md-9">
                  <input id="stok-stok" type="number" min="0" class="form-control" name="stok" value="0" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Stok HPP</label>
                <div class="col-md-9">
                  <input id="stok-stk_hpp" type="number" min="0" name="stk_hpp" class="form-control" value="0" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Gudang</label>
                <div class="col-md-9">
                  <select id="stok-gdg_kode" class="form-control" name="gdg_kode" required>
                    <?php $__currentLoopData = $gudang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gdg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($gdg->gdg_kode); ?>"><?php echo e($gdg->gdg_nama); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
                </div>
              </div>
              
              <br>
              <br>
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                <thead>
                  <tr class="">
                    <th> No Seri </th>
                    <th> QTY </th>
                    <th> Titipan </th>
                    <th> Gudang </th>
                    <th> Supplier </th>
                    <th> Action </th>
                  </tr>
                </thead>
              </table>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="text-center">
                  <button type="button" class="btn green stk_btn_save">Simpan</button>
                  
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modal-print" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Print
          </h4>
        </div>
        <div class="modal-body form">
          <form action="<?php echo e(route('barangPrint')); ?>" class="form-horizontal" role="form" method="post" target="_blank">
            <?php echo e(csrf_field()); ?>

            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Type</label>
                <div class="col-md-9">
                  <select class="form-control" name="type">
                    <option value="General">General</option>
                    <option value="Detail">Detail</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Kategory</label>
                <div class="col-md-9">
                  <select name="ktg_kode" class="form-control selectpicker" required data-live-search="true">
                    <option value="0">---Semua---</option>
                    <?php $__currentLoopData = $k_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($kategory->ktg_kode); ?>"><?php echo e($kategory->ktg_nama); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Group</label>
                <div class="col-md-9">
                  <select name="grp_kode" class="form-control selectpicker" required data-live-search="true">
                    <option value="0">---Semua---</option>
                    <?php $__currentLoopData = $g_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($group->grp_kode); ?>"><?php echo e($group->grp_nama); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Merek</label>
                <div class="col-md-9">
                  <select name="mrk_kode" class="form-control selectpicker" required data-live-search="true">
                    <option value="0">---Semua---</option>
                    <?php $__currentLoopData = $merek; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $merk): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($merk->mrk_kode); ?>"><?php echo e($merk->mrk_nama); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Supplier</label>
                <div class="col-md-9">
                  <select name="spl_kode" class="form-control selectpicker" required data-live-search="true">
                  <option value="0">---Semua---</option>
                    <?php $__currentLoopData = $supplier; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($spl->spl_kode); ?>"><?php echo e($spl->spl_nama); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="text-center">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modal-print-excel" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Print
          </h4>
        </div>
        <div class="modal-body form">
          <form action="<?php echo e(route('barangPrintExcel')); ?>" class="form-horizontal" role="form" method="post" target="_blank">
            <?php echo e(csrf_field()); ?>

            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Kategory</label>
                <div class="col-md-9">
                  <select name="ktg_kode" class="form-control selectpicker" required data-live-search="true">
                    <option value="0">---Semua---</option>
                    <?php $__currentLoopData = $k_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($kategory->ktg_kode); ?>"><?php echo e($kategory->ktg_nama); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Group</label>
                <div class="col-md-9">
                  <select name="grp_kode" class="form-control selectpicker" required data-live-search="true">
                    <option value="0">---Semua---</option>
                    <?php $__currentLoopData = $g_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($group->grp_kode); ?>"><?php echo e($group->grp_nama); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Merek</label>
                <div class="col-md-9">
                  <select name="mrk_kode" class="form-control selectpicker" required data-live-search="true">
                    <option value="0">---Semua---</option>
                    <?php $__currentLoopData = $merek; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $merk): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($merk->mrk_kode); ?>"><?php echo e($merk->mrk_nama); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Supplier</label>
                <div class="col-md-9">
                  <select name="spl_kode" class="form-control selectpicker" required data-live-search="true">
                  <option value="0">---Semua---</option>
                    <?php $__currentLoopData = $supplier; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($spl->spl_kode); ?>"><?php echo e($spl->spl_nama); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="text-center">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modal-stok_sample" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Stok Sample
          </h4>
        </div>
        <div class="modal-body form">
          <form action="<?php echo e(route('stokbarangStokSample')); ?>" class="form-horizontal form-stok-sample" role="form" method="post">
            <?php echo e(csrf_field()); ?>

            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Qty</label>
                <div class="col-md-9">
                  <input class="form-control" type="hidden" name="stk_kode_sample">
                  <input class="form-control" type="hidden" name="brg_no_seri_sample">
                  <input class="form-control" type="hidden" name="brg_kode_sample">
                  <input class="form-control" type="hidden" name="brg_barcode_sample">
                  <input class="form-control" type="hidden" name="gdg_kode_sample">
                  <input class="form-control" type="hidden" name="spl_kode_sample">
                  <input class="form-control" type="number" min="1" name="qty_sample" value="0">
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="text-center">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modal-stok_reject" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Stok Reject
          </h4>
        </div>
        <div class="modal-body form">
          <form action="<?php echo e(route('stokbarangStokReject')); ?>" class="form-horizontal form-stok-sample" role="form" method="post">
            <?php echo e(csrf_field()); ?>

            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Qty</label>
                <div class="col-md-9">
                  <input class="form-control" type="hidden" name="stk_kode_reject">
                  <input class="form-control" type="number" min="1" name="qty_reject" value="0">
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="text-center">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>