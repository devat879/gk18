<table>
  <tr>
    <td>Kartu Hutang</td>
  </tr>
  <tr>
    <?php $akhir_tgl=$end_date;?>
    <td>{{date('d M Y', strtotime($start_date))}} - {{date('d M Y', strtotime($end_date))}}</td>
  </tr>
</table>
@if($id_tipe==$kodeCust)
                        @foreach($dataList as $user)
                                               
                        <table>
                            <tbody>
                                <tr>
                                    <td colspan="7">{{$kodeCust.$user->cus_kode}} - {{ $user->cus_nama }}</td>
                                </tr>
                                <tr>
                                    <td colspan="7"> {{$coa}} - {{$kode_coa[$coa]}}</td>
                                </tr>
                                <?php 
                                    // use use App\Models\mJurnalUmum;
                                    // $data2   = mJurnalUmum::where('id_pel',$kodeSupplier.$spl->spl_kode)->leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('trs_kode_rekening','2101')->get();
                                ?>
                                <tr class="">
                                    <td> No </td>
                                    <td> Tanggal </td>
                                    <td> No Invoice</td>
                                    <td> Keterangan </td>
                                    <td> Debet </td>
                                    <td> Kredit </td>
                                    <td> Saldo </td>
                                </tr>
                                <?php $saldo = $begining_balance[$kodeCust.$user->cus_kode];?>
                                <tr>
                                    <td> 1</td>
                                    <td>  </td>
                                    <td>  </td>
                                    <td> begining balance </td>
                                    <td> {{ number_format($begining_balance[$kodeCust.$user->cus_kode],2) }} </td>
                                    <td>  </td>
                                    <td> {{ number_format($saldo,2) }} </td>
                                </tr>
                                <?php $no=2;$ttl_kredit=0;$ttl_debet=$begining_balance[$kodeCust.$user->cus_kode];?>
                                @if($jml_detail[$kodeCust.$user->cus_kode]>0)
                                @foreach($trs[$kodeCust.$user->cus_kode] as $spl_trs)
                                <?php 
                                    $saldo = $saldo+$spl_trs->trs_debet-$spl_trs->trs_kredit;
                                    $ttl_debet+=$spl_trs->trs_debet;
                                    $ttl_kredit+=$spl_trs->trs_kredit
                                ?>
                                <tr>
                                    <td> {{ $no++ }}. </td>
                                    <td> {{ date('d M Y', strtotime($spl_trs->jmu_tanggal)) }} </td>
                                    <td> {{ $spl_trs->no_invoice }} </td>
                                    <td> {{ $spl_trs->jmu_keterangan }} </td>
                                    <td> {{ number_format($spl_trs->trs_debet,2) }} </td>
                                    <td> {{ number_format($spl_trs->trs_kredit,2) }} </td>
                                    <td> {{ number_format($saldo,2) }} </td>                                
                                </tr>
                                @endforeach
                                @endif
                                <tr>
                                    <td colspan="4">Sub Total Account</td>
                                    <td>{{number_format($ttl_debet,2)}}</td>
                                    <td>{{number_format($ttl_kredit,2)}}</td>
                                </tr>
                                
                            </tbody>
                        </table>
                        
                        @endforeach
                        @elseif($id_tipe==$kodeKry)
                        @foreach($dataList as $user)
                        
                        <table class="table table-striped table-bordered">
                            <tbody>
                                <tr>
                                    <td colspan="7">{{$kodeKry.$user->kry_kode}} - {{ $user->kry_nama }}</td>
                                </tr>
                                <tr>
                                    <td colspan="7"> {{$coa}} - {{$kode_coa[$coa]}}</td>
                                </tr>
                                <?php 
                                    // use use App\Models\mJurnalUmum;
                                    // $data2   = mJurnalUmum::where('id_pel',$kodeSupplier.$spl->spl_kode)->leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('trs_kode_rekening','2101')->get();
                                ?>
                                <tr class="">
                                    <td> No </td>
                                    <td> Tanggal </td>
                                    <td> No Invoice</td>
                                    <td> Keterangan </td>
                                    <td> Debet </td>
                                    <td> Kredit </td>
                                    <td> Saldo </td>
                                </tr>
                                <?php $saldo = $begining_balance[$kodeKry.$user->kry_kode];?>
                                <tr>
                                    <td> 1</td>
                                    <td>  </td>
                                    <td>  </td>
                                    <td> begining balance </td>
                                    <td> {{ number_format($begining_balance[$kodeKry.$user->kry_kode],2) }} </td>
                                    <td>  </td>
                                    <td> {{ number_format($saldo,2) }} </td>
                                </tr>
                                <?php $no=2;$ttl_kredit=0;$ttl_debet=$begining_balance[$kodeKry.$user->kry_kode];?>
                                @if($jml_detail[$kodeKry.$user->kry_kode]>0)
                                @foreach($trs[$kodeKry.$user->kry_kode] as $spl_trs)
                                <?php 
                                    $saldo = $saldo+$spl_trs->trs_debet-$spl_trs->trs_kredit;
                                    $ttl_debet+=$spl_trs->trs_debet;
                                    $ttl_kredit+=$spl_trs->trs_kredit
                                ?>
                                <tr>
                                    <td> {{ $no++ }}. </td>
                                    <td> {{ date('d M Y', strtotime($spl_trs->jmu_tanggal)) }} </td>
                                    <td> {{ $spl_trs->no_invoice }} </td>
                                    <td> {{ $spl_trs->jmu_keterangan }} </td>
                                    <td> {{ number_format($spl_trs->trs_debet,2) }} </td>
                                    <td> {{ number_format($spl_trs->trs_kredit,2) }} </td>
                                    <td> {{ number_format($saldo,2) }} </td>                                
                                </tr>
                                @endforeach
                                @endif
                                <tr>
                                    <td colspan="4">Sub Total Account</td>
                                    <td>{{number_format($ttl_debet,2)}}</td>
                                    <td>{{number_format($ttl_kredit,2)}}</td>
                                </tr>
                                
                            </tbody>
                        </table>
                        
                        @endforeach
                        @endif
