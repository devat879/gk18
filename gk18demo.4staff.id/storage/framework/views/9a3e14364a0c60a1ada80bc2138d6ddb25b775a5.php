<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />   
    <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/piutangPelanggan.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>

    <script src="<?php echo e(asset('assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-select2.min.js')); ?>" type="text/javascript"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $('[name="start_date"],[name="end_date"]').datepicker()
            .on('changeDate', function(ev){                 
                $('[name="start_date"],[name="end_date"]').datepicker('hide');
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
<div class="page-content-inner">
    <div class="mt-content-body">
        
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="col-md-12">
                            <div class="portlet light ">
                                <a style="font-size: 11px" class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button">
                                    Pilih Periode
                                </a>
                                <a style="font-size: 11px" type="button" class="btn btn-danger" href="<?php echo e(route('printkartuHutangSupplier',['kode'=>$spl_id, 'start_date'=>$start_date, 'end_date'=>$end_date, 'coa'=>$coa, 'tipe'=>'print'])); ?>" target="_blank">
                                    <span><i class="fa fa-print"></i></span> Print
                                </a>
                                <a style="font-size: 11px" type="button" class="btn btn-danger" href="<?php echo e(route('kartuHutangExcel',['kode'=>$spl_id, 'start_date'=>$start_date, 'end_date'=>$end_date, 'coa'=>$coa])); ?>" target="_blank">
                                    <span><i class="fa fa-print"></i></span> Excel
                                </a>
                            </div>
                        </div>
                        <h4 style="font-family: Tahoma">Kartu Hutang</h4>
                        <h5 style="font-family: Tahoma"><?php echo e(date('d M Y', strtotime($start_date))); ?> s/d <?php echo e(date('d M Y', strtotime($end_date))); ?></h5>
                        <br /><br />
                        <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <table class="table table-striped table-bordered">
                            <tbody>
                                <tr>
                                    <td align="left" colspan="7" style="font-size: 11px;font-weight: bold;"><?php echo e($spl->spl_nama); ?> </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="7" style="font-size: 11px;font-weight: bold;"> <?php echo e($coa); ?> - <?php echo e($kode_coa[$coa]); ?></td>
                                </tr>
                                <?php 
                                    // use use App\Models\mJurnalUmum;
                                    // $data2   = mJurnalUmum::where('id_pel',$kodeSupplier.$spl->spl_kode)->leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('trs_kode_rekening','2101')->get();
                                ?>
                                <tr class="">
                                    <td style="font-size: 12px" width="10"> No </td>
                                    <td style="font-size: 12px"> Tanggal </td>
                                    <td style="font-size: 12px"> No Invoice</td>
                                    <td style="font-size: 12px"> Keterangan </td>
                                    <td style="font-size: 12px"> Debet </td>
                                    <td style="font-size: 12px"> Kredit </td>
                                    <td style="font-size: 12px"> Saldo </td>
                                </tr>
                                <?php $saldo = $begining_balance[$spl->spl_kode];?>
                                <tr>
                                    <td style="font-size: 11px" align="center"> 1</td>
                                    <td style="font-size: 11px">  </td>
                                    <td style="font-size: 11px">  </td>
                                    <td style="font-size: 11px"> begining balance </td>
                                    <td style="font-size: 11px;text-align: right;">  </td>
                                    <td style="font-size: 11px;text-align: right;"> <?php echo e(number_format($begining_balance[$spl->spl_kode],2)); ?> </td>
                                    <td style="font-size: 11px;text-align: right;"> <?php echo e(number_format($saldo,2)); ?> </td>
                                </tr>
                                <?php $no=2;$ttl_debet=0;$ttl_kredit=$begining_balance[$spl->spl_kode];?>
                                <?php $__currentLoopData = $trs[$spl->spl_kode]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spl_trs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php 
                                    $saldo = $saldo+$spl_trs->trs_kredit-$spl_trs->trs_debet;
                                    $ttl_debet+=$spl_trs->trs_debet;
                                    $ttl_kredit+=$spl_trs->trs_kredit
                                ?>
                                <tr>
                                    <td style="font-size: 11px" align="center"> <?php echo e($no++); ?>. </td>
                                    <td style="font-size: 11px"> <?php echo e(date('d M Y', strtotime($spl_trs->jmu_tanggal))); ?> </td>
                                    <td style="font-size: 11px"> <?php echo e($spl_trs->no_invoice); ?> </td>
                                    <td style="font-size: 11px"> <?php echo e($spl_trs->jmu_keterangan); ?> </td>
                                    <td style="font-size: 11px;text-align: right;"> <?php echo e(number_format($spl_trs->trs_debet,2)); ?> </td>
                                    <td style="font-size: 11px;text-align: right;"> <?php echo e(number_format($spl_trs->trs_kredit,2)); ?> </td>
                                    <td style="font-size: 11px;text-align: right;"> <?php echo e(number_format($saldo,2)); ?> </td>                                
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td style="font-size: 12px;font-weight: bold;" align="left" colspan="4">Sub Total Account</td>
                                    <td style="font-size: 12px;font-weight: bold;" align="left"><?php echo e(number_format($ttl_debet,2)); ?></td>
                                    <td style="font-size: 12px;font-weight: bold;" align="left"><?php echo e(number_format($ttl_kredit,2)); ?></td>
                                </tr>
                                <tr>
                                    <td style="font-size: 11px" align="center" colspan="6">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                            
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-clock"></i> Pilih Periode
                </h4>
            </div>
            <div class="modal-body form">
                <form action="<?php echo e(route('pilihPeriodeKartuHutang')); ?>" class="form-horizontal form-send" role="form" method="post">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-3">
                                <label style="padding-top: 7px">Kode COA</label>
                            </div>
                            <div class="col-md-9">
                                <!-- <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date"/> -->
                                <select style="font-size: 11px" class="form-control select2" name="coa">
                                    <?php $__currentLoopData = $kode_coa; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($key); ?>" <?php if($key==$coa) echo 'selected';?>><?php echo e($value); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label style="padding-top: 7px">Supplier</label>
                            </div>
                            <div class="col-md-9">
                                <!-- <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date"/> -->
                                <select style="font-size: 11px" class="form-control select2" name="spl_id">
                                    <option value="all"  <?php if($spl_id=='all') echo 'selected';?>>All Supplier</option>
                                    <?php $__currentLoopData = $supplier; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $supplier): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($supplier->spl_kode); ?>" <?php if($spl_id==$supplier->spl_kode) echo 'selected';?>><?php echo e($supplier->spl_nama); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date" value="<?php echo e($start_date); ?>" />
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                            <div class="col-md-2">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date"  value="<?php echo e($end_date); ?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row col-md-offset-3">
                                <button type="submit" class="btn green">Search</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>