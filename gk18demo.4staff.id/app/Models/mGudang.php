<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mGudang extends Model
{
    protected $table = 'tb_gudang';
    protected $primaryKey = 'gdg_kode';
    public $timestamps = false;

    public function stok()
    {
      return $this->hasMany('App\Models\mStok', 'gdg_kode');
    }
}
