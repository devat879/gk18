<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mCheque;
use App\Models\mJurnalUmum;
use App\Models\mTransaksi;
use App\Models\mPerkiraan;
use App\Models\mDetailPerkiraan;
use App\Models\mPeriode;
use App\Models\mAsset;
use App\Models\mKategoryAsset;
use App\Models\mPenyusutanAsset;
use App\Models\mPenjualanLangsung;
use App\Models\mBarang;
use App\Models\mDetailPenjualanLangsung;
use App\Models\mDetailPenjualanTitipan;
use App\Models\mReturPenjualanDetail;
use App\Models\mDetailSuratJalanPT;
use App\Models\mSuratJalanPenjualanTitipan;
use Carbon\Carbon;

use Validator;
use Redirect;
use DB;

class BukuBesar extends Controller
{
    private $main;
    private $title;
    private $kodeLabel;

    function __construct() {
        $this->main         = new Main();
        $this->title        = 'Buku Besar';
        $this->kodeLabel    = '';
    }

    function index($start_date='', $end_date='', $coa='', $tipe='') {
        $breadcrumb         = [
            'Buku Besar'   => route('bukuBesar'),
            'Daftar'        => ''
        ];
        $data               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']       = 'buku_besar';
        $periode                    = mPeriode::first();
        $data['bulan']              = $periode->bulan_aktif;
        $data['tahun_periode']      = $periode->tahun_aktif;
        $data['kode_perkiraan']     = 0;
        $data['title']      = $this->title;
        $data['tanggal']    = [
                                '1' => 'Januari','2' => 'Februari','3' => 'Maret','4' => 'April','5' => 'Mei','6' => 'Juni',
            '7' => 'Juli','8' => 'Agustus','9' => 'September','10' => 'Oktober','11' => 'November','12' => 'Desember'
        ];
        $data['tahun']      = [
                                '2017','2018','2019','2020','2021','2022','2023','2025'
                                ];        
        $data['start_date']         = date('Y-m-d');
        $data['end_date']           = date('Y-m-d');
        $data['coa']                = 'all';
        $data['perkiraan']          = mPerkiraan::orderBy('master_id','ASC')->get();
        $data['kode_coa']           = mPerkiraan::orderBy('master_id','ASC')->get();
        

        if($start_date!='' && $end_date!=''){
            $data['start_date']         = $start_date;
            $data['end_date']           = $end_date;
        }
        if($coa!=''){
            if($coa!='all'){
                $data['perkiraan']  = mPerkiraan::where('master_id',$coa)->get();
                $data['coa']        = $coa;
            }            
        }
        
        foreach ($data['perkiraan'] as $perkiraan) {
            if($perkiraan->mst_normal=='debet'){
                $debet      = $perkiraan->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_debet');
                $kredit     = $perkiraan->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_kredit');
                $saldo_awal = $debet - $kredit;
                $data['saldo_awal'][$perkiraan->mst_kode_rekening] = $saldo_awal;
            }else{
                $debet      = $perkiraan->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_debet');
                $kredit     = $perkiraan->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_kredit');
                $saldo_awal = $kredit - $debet;
                $data['saldo_awal'][$perkiraan->mst_kode_rekening] = $saldo_awal;
            }
        }
        $data['tgl_saldo_awal']     = date('Y-m-d', strtotime('-1 days', strtotime($data['start_date'])));

        if($tipe=='print'){
            return view('bukuBesar/printBukuBesar', $data);
        }else{
            return view('bukuBesar/LapBukuBesar', $data);
        }
        
    }

    function rules($request) {
        $rules = [
            'tgl_transaksi'             => 'required',
            'kode_bukti'                => 'required',
            'keterangan'                => 'required',
        ];

        $customeMessage = [
            'required'  =>'Kolom diperlukan'
        ];
        Validator::make($request, $rules, $customeMessage)->validate();
    }

    function pilihPeriode(Request $request) {
        $month  = $request->input('start_date');
        $year   = $request->input('end_date');
        $coa   = $request->input('kode_perkiraan');
    
        return [
          'redirect'=>route('BukuBesarCutOff', ['start_date'=>$month, 'end_date'=>$year, 'coa'=>$coa]),
        ];
    }

    function rugiLaba($month='', $year='',$tipe=''){
        $breadcrumb         = [
            'Accounting'    => '',
            'Rugi Laba'     => route('rugiLaba')
        ];
        $data               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']       = 'rugi_laba';
        $periode                    = mPeriode::first();
        $data['bulan']              = $periode->bulan_aktif+1;
        $data['tahun_periode']      = $periode->tahun_aktif;
        $data['start_date']    = date('Y-m-d');
        $data['end_date']      = date('Y-m-d');
        $data['title']      = 'Rugi Laba';
        $data['tahun']      = [
                                '2017','2018','2019','2020','2021','2022','2023','2025'
        ];
        $data['tanggal']    = [
                                '1' => 'Januari','2' => 'Februari','3' => 'Maret','4' => 'April','5' => 'Mei','6' => 'Juni',
            '7' => 'Juli','8' => 'Agustus','9' => 'September','10' => 'Oktober','11' => 'November','12' => 'Desember'
        ];
        if($month!='' || $year!=''){
            $data['start_date']    = $month;
            $data['end_date']      = $year;
        }
        
        //transaksi pendapatan
        $data['kode_pendapatan']  = mPerkiraan::where('mst_kode_rekening','3100')->get();
        $biaya = 0;
        foreach($data['kode_pendapatan'] as $pendapatan){
            $total_pendapatanUsaha = 0;
            foreach($pendapatan->childs as $pendapatanUsaha){
                $total_penjualan = 0;
                foreach($pendapatanUsaha->childs as $penjualan){
                    $total_penjualanChilds = 0;
                    foreach($penjualan->childs as $penjualanChilds){
                        if($penjualanChilds->mst_normal == 'kredit'){                                                        
                            $data['biaya'][$penjualanChilds->mst_kode_rekening]=$penjualanChilds->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_kredit')-$penjualanChilds->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_debet');
                            $total_penjualanChilds=$total_penjualanChilds+$data['biaya'][$penjualanChilds->mst_kode_rekening];
                        }if($penjualanChilds->mst_normal == 'debet'){
                            $data['biaya'][$penjualanChilds->mst_kode_rekening]=$penjualanChilds->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_debet')-$penjualanChilds->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_kredit');
                            $total_penjualanChilds=$total_penjualanChilds-$data['biaya'][$penjualanChilds->mst_kode_rekening];
                        }
                    }
                    if($penjualan->mst_normal == 'kredit'){
                        $total_childs = $total_penjualanChilds;                                                       
                        $data['biaya'][$penjualan->mst_kode_rekening]=$total_childs+$penjualan->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                        $total_penjualan = $total_penjualan+$data['biaya'][$penjualan->mst_kode_rekening];
                    }if($penjualan->mst_normal == 'debet'){
                        $total_childs = $total_penjualanChilds;
                        $data['biaya'][$penjualan->mst_kode_rekening]=$total_childs-$penjualan->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                        $total_penjualan = $total_penjualan-$data['biaya'][$penjualan->mst_kode_rekening];
                    }
                }
                if($pendapatanUsaha->mst_normal == 'kredit'){
                    $penjualan_total =$total_penjualan;                                                        
                    $data['biaya'][$pendapatanUsaha->mst_kode_rekening]=$penjualan_total+$pendapatanUsaha->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                    $total_pendapatanUsaha = $total_pendapatanUsaha+$data['biaya'][$pendapatanUsaha->mst_kode_rekening];
                }if($pendapatanUsaha->mst_normal == 'debet'){
                    $penjualan_total =$total_penjualan;
                    $data['biaya'][$pendapatanUsaha->mst_kode_rekening]=$penjualan_total-$pendapatanUsaha->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                    $total_pendapatanUsaha = $total_pendapatanUsaha-$data['biaya'][$pendapatanUsaha->mst_kode_rekening];
                }
            }
            if($pendapatan->mst_normal == 'kredit'){   
                $total_pendapatan = $total_pendapatanUsaha;                                                      
                $data['biaya'][$pendapatan->mst_kode_rekening]=$total_pendapatanUsaha+$pendapatan->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
            }if($pendapatan->mst_normal == 'debet'){
                $total_pendapatan = $total_pendapatanUsaha;                
                $data['biaya'][$pendapatan->mst_kode_rekening]=$total_pendapatanUsaha-$pendapatan->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
            }
        }
        //transaksi pendapatan

        //transaksi pendapatan diluar usaha
        $data['kode_pendapatan_diluar_usaha']  = mPerkiraan::where('mst_kode_rekening','3120')->get();
        foreach($data['kode_pendapatan_diluar_usaha'] as $pendapatanDiluarUsaha){
            $total_pendapatanDiluarUsahaChilds = 0;
            foreach($pendapatanDiluarUsaha->childs as $pendapatanDiluarUsahaChilds){
                if($pendapatanDiluarUsahaChilds->mst_normal == 'kredit'){
                                                                          
                    $data['biaya'][$pendapatanDiluarUsahaChilds->mst_kode_rekening]=$pendapatanDiluarUsahaChilds->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_kredit')-$pendapatanDiluarUsahaChilds->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_debet');
                    $total_pendapatanDiluarUsahaChilds = $total_pendapatanDiluarUsahaChilds+$data['biaya'][$pendapatanDiluarUsahaChilds->mst_kode_rekening];
                }if($pendapatanDiluarUsahaChilds->mst_normal == 'debet'){
                    
                    $data['biaya'][$pendapatanDiluarUsahaChilds->mst_kode_rekening]=$pendapatanDiluarUsahaChilds->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_debet')-$pendapatanDiluarUsahaChilds->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_kredit');
                    $total_pendapatanDiluarUsahaChilds = $total_pendapatanDiluarUsahaChilds-$data['biaya'][$pendapatanDiluarUsahaChilds->mst_kode_rekening];
                }
            }
            if($pendapatanDiluarUsaha->mst_normal == 'kredit'){   
                $total_pendapatanDiluarUsaha = $total_pendapatanDiluarUsahaChilds;                                                      
                $data['biaya'][$pendapatanDiluarUsaha->mst_kode_rekening]=$total_pendapatanDiluarUsaha+$pendapatanDiluarUsaha->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_kredit');
            }if($pendapatanDiluarUsaha->mst_normal == 'debet'){
                $total_pendapatanDiluarUsaha = $total_pendapatanDiluarUsahaChilds;                
                $data['biaya'][$pendapatanDiluarUsaha->mst_kode_rekening]=$total_pendapatanDiluarUsaha-$pendapatanDiluarUsaha->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_debet');
            }
        }
        //transaksi pendapatan diluar usaha

        //transaksi hpp
        $data['kode_hpp']  = mPerkiraan::where('mst_kode_rekening','4100')->get();
        foreach($data['kode_hpp'] as $hpp){
            $total_penjualan = 0;
            foreach($hpp->childs as $hppChilds){
                $total_hppChild = 0;
                foreach($hppChilds->childs as $hppChild){
                    if($hppChild->mst_normal == 'kredit'){                                                        
                        $data['biaya'][$hppChild->mst_kode_rekening]=$hppChild->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_kredit')-$hppChild->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_debet');
                        $total_hppChild=$total_hppChild-$data['biaya'][$hppChild->mst_kode_rekening];
                    }if($hppChild->mst_normal == 'debet'){
                        $data['biaya'][$hppChild->mst_kode_rekening]=$hppChild->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_debet')-$hppChild->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_kredit');
                        $total_hppChild=$total_hppChild+$data['biaya'][$hppChild->mst_kode_rekening];
                    }
                }
                if($hppChilds->mst_normal == 'kredit'){
                    $total_hppChilds = $total_hppChild;                                                       
                    $data['biaya'][$hppChilds->mst_kode_rekening]=$total_hppChilds-$hppChilds->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_kredit');
                    $total_hppChilds = $total_hppChilds-$data['biaya'][$hppChilds->mst_kode_rekening];
                }if($hppChilds->mst_normal == 'debet'){
                    $total_hppChilds = $total_hppChild;
                    $data['biaya'][$hppChilds->mst_kode_rekening]=$total_hppChilds+$hppChilds->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_debet');
                    $total_hppChilds = $data['biaya'][$hppChilds->mst_kode_rekening];
                }
            }
            if($hpp->mst_normal == 'kredit'){
                $total_hpp = $total_hppChilds;                                                       
                $data['biaya'][$hpp->mst_kode_rekening]=$total_hpp-$hpp->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_kredit');
                // $total_pendapatanUsaha = $total_pendapatanUsaha+$data['biaya'][$hpp->mst_kode_rekening];
            }if($hpp->mst_normal == 'debet'){
                $total_hpp = $total_hppChilds;
                $data['biaya'][$hpp->mst_kode_rekening]=$total_hpp+$hpp->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_debet');
                // $total_hpp = $total_hpp-$data['biaya'][$hpp->mst_kode_rekening];
            }
        }
        //transaksi hpp

        //transaksi biaya-biaya
        $data['kode_biaya']  = mPerkiraan::where('mst_kode_rekening','5100')->get();
        foreach($data['kode_biaya'] as $biaya){
            $total_biayaChild = 0;
            foreach($biaya->childs as $biayaChild){
                $total_biayaChild2 = 0;
                foreach($biayaChild->childs as $biayaChild2){
                    if($biayaChild2->mst_normal == 'kredit'){                                                        
                        $data['biaya'][$biayaChild2->mst_kode_rekening]=$biayaChild2->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_kredit')-$biayaChild2->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_debet');
                        $total_biayaChild2=$total_biayaChild2-$data['biaya'][$biayaChild2->mst_kode_rekening];
                    }if($biayaChild2->mst_normal == 'debet'){
                        $data['biaya'][$biayaChild2->mst_kode_rekening]=$biayaChild2->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_debet')-$biayaChild2->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_kredit');
                        $total_biayaChild2=$total_biayaChild2+$data['biaya'][$biayaChild2->mst_kode_rekening];
                    }
                }

                if($biayaChild->mst_normal == 'kredit'){                                                       
                    $data['biaya'][$biayaChild->mst_kode_rekening]=$total_biayaChild-$hppChilds->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_kredit')-$hppChilds->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_debet');
                    $total_biayaChild = $total_biayaChild-$data['biaya'][$biayaChild->mst_kode_rekening];
                }if($biayaChild->mst_normal == 'debet'){
                    $data['biaya'][$biayaChild->mst_kode_rekening]=$total_biayaChild2+$biayaChild->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_debet')-$biayaChild->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_kredit');
                    $total_biayaChild = $total_biayaChild+$data['biaya'][$biayaChild->mst_kode_rekening];
                }
            }
            if($biaya->mst_normal == 'kredit'){
                $total_hpp = $total_biayaChild;                                                       
                $data['biaya'][$biaya->mst_kode_rekening]=$total_hpp-$biaya->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_kredit')-$biaya->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_debet');
                
            }if($hpp->mst_normal == 'debet'){
                $total_hpp = $total_biayaChild;
                $data['biaya'][$biaya->mst_kode_rekening]=$total_hpp+$biaya->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_debet')-$biaya->transaksi->where('tgl_transaksi','>=',$data['start_date'])->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_kredit');
                
            }
        }
        //transaksi biaya-biaya

        if($tipe=='print'){
            return view('bukuBesar/printRugiLaba', $data);
        }else{
            return view('bukuBesar/rugiLaba', $data);
        }
    }

    function pilihPeriodeRugiLaba(Request $request) {
        $month  = $request->input('start_date');
        $year   = $request->input('end_date');
    
        return [
          'redirect'=>route('rugiLabaCutOff', ['bulan'=>$month, 'tahun'=>$year]),
        ];
    }

    // function neraca($start_date='',$end_date='',$tipe=''){
    //     $breadcrumb         = [
    //         'Accounting'    => '',
    //         'Neraca'        => route('neraca'),
    //         'Daftar'        => ''
    //     ];
    //     $data               = $this->main->data($breadcrumb, $this->kodeLabel);
    //     $data['menu']       = 'neraca';
    //     $periode                    = mPeriode::first();
    //     $data['bulan']              = $periode->bulan_aktif;
    //     $data['tahun_periode']      = $periode->tahun_aktif;
    //     $data['title']              = 'Neraca';
    //     $data['tanggal']    = [
    //                             '1','2','3','4','5','6','7','8','9','10','11','12'
    //                             ];
    //     $data['tahun']      = [
    //                             '2017','2018','2019','2020','2021','2022','2023','2025'
    //     ];

    //     // $data['detail_perkiraan']  = mDetailPerkiraan::where('msd_year',$data['tahun_periode'])->where('msd_month',$data['bulan'])->get();
    //     $data['detail_perkiraan']  = mPerkiraan::all();
    //     $data['start_date']         = date('Y-m-d');
    //     $data['end_date']           = date('Y-m-d');
    //     if($start_date!=''||$end_date!=''){
    //         $data['start_date']         = $start_date;
    //         $data['end_date']           = $end_date;
    //     }
    

    //     $data['total_asset']             = 0;
    //     $data['total_liabilitas']        = 0;
    //     $data['total_ekuitas']           = 0;
    //     $data['data_asset']         = mPerkiraan::where('mst_neraca_tipe','asset')->get();
    //     $data['data_ekuitas']         = mPerkiraan::where('mst_neraca_tipe','ekuitas')->get();
    //     $data['data_liabilitas']         = mPerkiraan::where('mst_neraca_tipe','liabilitas')->get();

    //     foreach ($data['data_asset'] as $detail_asset) {
    //         if($detail_asset->mst_normal == 'kredit'){
    //             $query              = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('tb_ac_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_ac_transaksi.trs_kode_rekening',$detail_asset->mst_kode_rekening);
    //             $kredit             = $query->sum('trs_kredit');
    //             $debet              = $query->sum('trs_debet');
    //             $asset              = $kredit-$debet;
    //             $data['asset'][$detail_asset->mst_kode_rekening] = $asset;
    //             $data['total_asset']     = $data['total_asset']-$asset;
    //         }
    //         if($detail_asset->mst_normal == 'debet'){
    //             $query              = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('tb_ac_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_ac_transaksi.trs_kode_rekening',$detail_asset->mst_kode_rekening);
    //             $kredit             = $query->sum('trs_kredit');
    //             $debet              = $query->sum('trs_debet');
    //             $asset              = $debet-$kredit;
    //             $data['asset'][$detail_asset->mst_kode_rekening] = $asset;
    //             $data['total_asset']     = $data['total_asset']+$asset;                       
    //         }
    //     }

    //     foreach ($data['data_ekuitas'] as $data_ekuitas) {
    //         if($data_ekuitas->mst_normal == 'kredit'){
    //             $query              = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('tb_ac_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_ac_transaksi.trs_kode_rekening',$data_ekuitas->mst_kode_rekening);
    //             $kredit             = $query->sum('trs_kredit');
    //             $debet              = $query->sum('trs_debet');
    //             $ekuitas            = $kredit-$debet;
    //                         // $data['ekuitas'][$detail->mst_kode_rekening] = $ekuitas;
    //         }
    //         if($data_ekuitas->mst_normal == 'debet'){
    //             $query              = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('tb_ac_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_ac_transaksi.trs_kode_rekening',$data_ekuitas->mst_kode_rekening);
    //             $kredit             = $query->sum('trs_kredit');
    //             $debet              = $query->sum('trs_debet');
    //             $ekuitas              = $debet-$kredit;
    //                         // $data['ekuitas'][$detail->mst_kode_rekening] = $ekuitas;                       
    //         }   
    //         $data['ekuitas'][$data_ekuitas->mst_kode_rekening] = $ekuitas;                                        
    //                 // }       
    //         if($data_ekuitas->mst_kode_rekening=='2403'){
    //             $ekuitas                = $this->count_rugi_laba($data['end_date']);
    //             $data['ekuitas'][$data_ekuitas->mst_kode_rekening] = $ekuitas;
    //         }      

    //         $data['total_ekuitas']     = $data['total_ekuitas']+$ekuitas;
    //     }

    //     foreach ($data['data_liabilitas'] as $data_liabilitas) {
    //         if($data_liabilitas->mst_normal == 'kredit'){
    //             $query              = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('tb_ac_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_ac_transaksi.trs_kode_rekening',$data_liabilitas->mst_kode_rekening);
    //             $kredit             = $query->sum('trs_kredit');
    //             $debet              = $query->sum('trs_debet');
    //             $liabilitas            = $kredit-$debet;
    //                         // $data['ekuitas'][$detail->mst_kode_rekening] = $ekuitas;
    //         }
    //         if($data_liabilitas->mst_normal == 'debet'){
    //             $query              = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('tb_ac_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_ac_transaksi.trs_kode_rekening',$data_liabilitas->mst_kode_rekening);
    //             $kredit             = $query->sum('trs_kredit');
    //             $debet              = $query->sum('trs_debet');
    //             $liabilitas              = $debet-$kredit;
    //                         // $data['ekuitas'][$detail->mst_kode_rekening] = $ekuitas;                       
    //         }   
    //         $data['liabilitas'][$data_liabilitas->mst_kode_rekening] = $liabilitas;                                        
    //                 // }       
                

    //         $data['total_liabilitas']     = $data['total_liabilitas']+$liabilitas;
    //     }

    //     $data['hitung']         = $data['total_liabilitas']+$data['total_ekuitas'];
    //     if(number_format($data['hitung'],2)==number_format($data['total_asset'],2)){
    //         $data['status']         = 'BALANCE';
    //         $data['selisih']         = 0;
    //     }else{
    //         $data['status']         = 'NOT BALANCE';
    //         $data['selisih']         = $data['total_asset']-$data['hitung'];
    //     }

    //     // $data['status']         = $data['total_asset']-$data['hitung'];
    //     // $data['status']         = 0;
    //     $data['space1'] = $this->main->perkiraan_space(1);
    //     $data['space2'] = $this->main->perkiraan_space(2);
    //     $data['space3'] = $this->main->perkiraan_space(3);
    //     $data['space4'] = $this->main->perkiraan_space(4);


        
    //     // return view('bukuBesar/neraca', $data);
    //     if($tipe=='print'){
    //         return view('bukuBesar/printNeraca', $data);
    //     }else{
    //         return view('bukuBesar/neraca-copy', $data);
    //     }
        

    // }

    function neraca($start_date='',$end_date='',$tipe=''){
        $breadcrumb         = [
            'Accounting'    => '',
            'Neraca'        => route('neraca'),
            'Daftar'        => ''
        ];
        $data               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']       = 'neraca';
        $periode                    = mPeriode::first();
        $data['bulan']              = $periode->bulan_aktif;
        $data['tahun_periode']      = $periode->tahun_aktif;
        $data['title']              = 'Neraca';
        $data['tanggal']    = [
                                '1','2','3','4','5','6','7','8','9','10','11','12'
                                ];
        $data['tahun']      = [
                                '2017','2018','2019','2020','2021','2022','2023','2025'
        ];

        // $data['detail_perkiraan']  = mDetailPerkiraan::where('msd_year',$data['tahun_periode'])->where('msd_month',$data['bulan'])->get();
        $data['detail_perkiraan']  = mPerkiraan::all();
        $data['start_date']         = date('Y-m-d');
        $data['end_date']           = date('Y-m-d');
        if($start_date!=''||$end_date!=''){
            $data['start_date']         = $start_date;
            $data['end_date']           = $end_date;
        }
    

        $data['total_asset']             = 0;
        $data['total_liabilitas']        = 0;
        $data['total_ekuitas']           = 0;
        $data['data_asset']              = mPerkiraan::where('mst_neraca_tipe','asset')->where('mst_master_id',0)->get();
        $data['data_ekuitas']            = mPerkiraan::where('mst_neraca_tipe','ekuitas')->where('mst_master_id',0)->get();
        $data['data_liabilitas']         = mPerkiraan::where('mst_neraca_tipe','liabilitas')->get();

        foreach ($data['data_asset'] as $detail_asset) {
            $total_detail_asset = 0;
            foreach ($detail_asset->childs as $asset_childs) {
                $total_asset_childs = 0;
                foreach ($asset_childs->childs as $asset_childs_2) {
                    if($asset_childs_2->mst_normal == 'kredit'){
                        $query              = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('tb_ac_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_ac_transaksi.master_id',$asset_childs_2->master_id);
                        $kredit             = $query->sum('trs_kredit');
                        $debet              = $query->sum('trs_debet');
                        $asset              = $kredit-$debet;
                        $data['asset'][$asset_childs_2->mst_kode_rekening] = $asset;
                        $data['total_asset']     = $data['total_asset']-$asset;
                        $total_asset_childs = $total_asset_childs-$asset;

                    }
                    if($asset_childs_2->mst_normal == 'debet'){
                        $query              = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('tb_ac_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_ac_transaksi.master_id',$asset_childs_2->master_id);
                        $kredit             = $query->sum('trs_kredit');
                        $debet              = $query->sum('trs_debet');
                        $asset              = $debet-$kredit;
                        $data['asset'][$asset_childs_2->mst_kode_rekening] = $asset;
                        $data['total_asset']     = $data['total_asset']+$asset;
                        $total_asset_childs = $total_asset_childs+$asset;                       
                    }
                    
                }

                if($asset_childs->mst_normal == 'kredit'){
                    $query              = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('tb_ac_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_ac_transaksi.master_id',$asset_childs->master_id);
                    $kredit             = $query->sum('trs_kredit');
                    $debet              = $query->sum('trs_debet');
                    $asset              = $kredit-$debet;
                    $data['asset'][$asset_childs->mst_kode_rekening] = $asset-$total_asset_childs;
                    $data['total_asset']     = $data['total_asset']-$asset;
                    $total_detail_asset = $total_detail_asset-$data['asset'][$asset_childs->mst_kode_rekening];
                }
                if($asset_childs->mst_normal == 'debet'){
                    $query              = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('tb_ac_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_ac_transaksi.master_id',$asset_childs->master_id);
                    $kredit             = $query->sum('trs_kredit');
                    $debet              = $query->sum('trs_debet');
                    $asset              = $debet-$kredit;
                    $data['asset'][$asset_childs->mst_kode_rekening] = $asset+$total_asset_childs;
                    $data['total_asset']     = $data['total_asset']+$asset;
                    $total_detail_asset = $total_detail_asset+$data['asset'][$asset_childs->mst_kode_rekening];                       
                }
            }
            if($detail_asset->mst_normal == 'kredit'){
                $query              = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('tb_ac_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_ac_transaksi.master_id',$detail_asset->master_id);
                $kredit             = $query->sum('trs_kredit');
                $debet              = $query->sum('trs_debet');
                $asset              = $kredit-$debet;
                $data['asset'][$detail_asset->mst_kode_rekening] = $asset-$total_detail_asset;
                $data['total_asset']     = $data['total_asset']-$asset;
            }
            if($detail_asset->mst_normal == 'debet'){
                $query              = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('tb_ac_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_ac_transaksi.master_id',$detail_asset->master_id);
                $kredit             = $query->sum('trs_kredit');
                $debet              = $query->sum('trs_debet');
                $asset              = $debet-$kredit;
                $data['asset'][$detail_asset->mst_kode_rekening] = $asset+$total_detail_asset;
                $data['total_asset']     = $data['total_asset']+$asset;                       
            }
        }

        foreach ($data['data_ekuitas'] as $data_ekuitas) {
            $total_data_ekuitas = 0;
            foreach ($data_ekuitas->childs as $ekuitas_childs) {
                if($ekuitas_childs->mst_normal == 'kredit'){
                    $query              = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('tb_ac_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_ac_transaksi.master_id',$ekuitas_childs->master_id);
                    $kredit             = $query->sum('trs_kredit');
                    $debet              = $query->sum('trs_debet');
                    $ekuitas            = $kredit-$debet;
                    $data['ekuitas'][$ekuitas_childs->mst_kode_rekening] = $ekuitas;
                    $data['total_ekuitas']     = $data['total_ekuitas']+$ekuitas;
                    $total_data_ekuitas = $total_data_ekuitas+$ekuitas;
                }
                if($ekuitas_childs->mst_normal == 'debet'){
                    $query              = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('tb_ac_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_ac_transaksi.master_id',$ekuitas_childs->master_id);
                    $kredit             = $query->sum('trs_kredit');
                    $debet              = $query->sum('trs_debet');
                    $ekuitas              = $debet-$kredit;
                    $data['ekuitas'][$ekuitas_childs->mst_kode_rekening] = $ekuitas;                    
                    $data['total_ekuitas']     = $data['total_ekuitas']-$ekuitas;
                    $total_data_ekuitas = $total_data_ekuitas-$ekuitas;                     
                }
                if($ekuitas_childs->mst_kode_rekening=='2403'){
                    $ekuitas                = $this->count_rugi_laba($data['end_date']);
                    $data['ekuitas'][$ekuitas_childs->mst_kode_rekening] = $ekuitas;                    
                    $data['total_ekuitas']     = $data['total_ekuitas']+$ekuitas;
                    $total_data_ekuitas = $total_data_ekuitas+$ekuitas;
                }
            }
            if($data_ekuitas->mst_normal == 'kredit'){
                $query              = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('tb_ac_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_ac_transaksi.master_id',$data_ekuitas->master_id);
                $kredit             = $query->sum('trs_kredit');
                $debet              = $query->sum('trs_debet');
                $ekuitas            = $kredit-$debet;
                $data['ekuitas'][$data_ekuitas->mst_kode_rekening] = $ekuitas+$total_data_ekuitas;
            }
            if($data_ekuitas->mst_normal == 'debet'){
                $query              = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('tb_ac_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_ac_transaksi.master_id',$data_ekuitas->master_id);
                $kredit             = $query->sum('trs_kredit');
                $debet              = $query->sum('trs_debet');
                $ekuitas              = $debet-$kredit;
                $data['ekuitas'][$data_ekuitas->mst_kode_rekening] = $ekuitas-$total_data_ekuitas;                      
            }   

        }

        foreach ($data['data_liabilitas'] as $data_liabilitas) {
            $total_data_liabilitas = 0;
            foreach ($data_liabilitas->childs as $liabilitas_childs) {
                if($liabilitas_childs->mst_normal == 'kredit'){
                    $query              = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('tb_ac_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_ac_transaksi.master_id',$liabilitas_childs->master_id);
                    $kredit             = $query->sum('trs_kredit');
                    $debet              = $query->sum('trs_debet');
                    $liabilitas            = $kredit-$debet;
                    $data['liabilitas'][$liabilitas_childs->mst_kode_rekening] = $liabilitas;
                    if($data_liabilitas->mst_normal == 'kredit'){
                        $total_data_liabilitas = $total_data_liabilitas+$liabilitas;
                    }else{
                        $total_data_liabilitas = $total_data_liabilitas-$liabilitas;
                    }
                    
                }
                if($liabilitas_childs->mst_normal == 'debet'){
                    $query              = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('tb_ac_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_ac_transaksi.master_id',$liabilitas_childs->master_id);
                    $kredit             = $query->sum('trs_kredit');
                    $debet              = $query->sum('trs_debet');
                    $liabilitas              = $debet-$kredit;
                    $data['liabilitas'][$liabilitas_childs->mst_kode_rekening] = $liabilitas;
                    if($data_liabilitas->mst_normal == 'kredit'){
                        $total_data_liabilitas = $total_data_liabilitas-$liabilitas;
                    }else{
                        $total_data_liabilitas = $total_data_liabilitas+$liabilitas;
                    }                     
                } 
            }
            if($data_liabilitas->mst_normal == 'kredit'){
                $query              = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('tb_ac_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_ac_transaksi.master_id',$data_liabilitas->master_id);
                $kredit             = $query->sum('trs_kredit');
                $debet              = $query->sum('trs_debet');
                $liabilitas            = $kredit-$debet;
                $data['liabilitas'][$data_liabilitas->mst_kode_rekening] = $liabilitas+$total_data_liabilitas;
            }
            if($data_liabilitas->mst_normal == 'debet'){
                $query              = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('tb_ac_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_ac_transaksi.master_id',$data_liabilitas->master_id);
                $kredit             = $query->sum('trs_kredit');
                $debet              = $query->sum('trs_debet');
                $liabilitas              = $debet-$kredit; 
                $data['liabilitas'][$data_liabilitas->mst_kode_rekening] = $liabilitas-$total_data_liabilitas;                    
            }  
                

            $data['total_liabilitas']     = $data['total_liabilitas']+$liabilitas;
        }

        $data['hitung']         = $data['total_liabilitas']+$data['total_ekuitas'];
        if(number_format($data['hitung'],2)==number_format($data['total_asset'],2)){
            $data['status']         = 'BALANCE';
            $data['selisih']         = 0;
        }else{
            $data['status']         = 'NOT BALANCE';
            $data['selisih']         = $data['total_asset']-$data['hitung'];
        }
        // $data['status']         = 0;
        $data['space1'] = $this->main->perkiraan_space(1);
        $data['space2'] = $this->main->perkiraan_space(2);
        $data['space3'] = $this->main->perkiraan_space(3);
        $data['space4'] = $this->main->perkiraan_space(4);


        
        // return view('bukuBesar/neraca', $data);
        if($tipe=='print'){
            return view('bukuBesar/printNeraca', $data);
        }else{
            return view('bukuBesar/neraca-copy', $data);
        }
        

    }

    function pilihPeriodeNeraca(Request $request) {
        $month  = $request->input('start_date');
        $year   = $request->input('end_date');
    
        return [
          'redirect'=>route('neracaCutOff', ['start_date'=>$month, 'end_date'=>$year]),
        ];
    }

    function neracaCutOff($month='', $year=''){
        $breadcrumb         = [
            'Accounting'    => '',
            'Neraca'        => route('neraca'),
            'Daftar'        => ''
        ];
        $data               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']       = 'neraca';
        $periode                    = mPeriode::first();
        $data['bulan']              = $month;
        $data['tahun_periode']      = $year;
        $data['title']              = 'Neraca';
        $data['tanggal']    = [
                                '1','2','3','4','5','6','7','8','9','10','11','12'
                                ];
        $data['tahun']      = [
                                '2017','2018','2019','2020','2021','2022','2023','2025'
        ];

        $data['detail_perkiraan']  = mDetailPerkiraan::where('msd_year',$data['tahun_periode'])->where('msd_month',$data['bulan'])->get();

        $data['total_asset']             = 0;
        $data['total_liabilitas']        = 0;
        $data['total_ekuitas']           = 0;

        foreach($data['detail_perkiraan'] as $detail){
            if($detail->perkiraan->mst_neraca_tipe == 'ekuitas'){
                if($detail->perkiraan->mst_normal == 'kredit'){
                    $ekuitas  = $detail->msd_awal_kredit;
                }
                if($detail->perkiraan->mst_normal == 'debet'){
                    $ekuitas  = $detail->msd_awal_debet;
                }
                foreach($detail->perkiraan->transaksi->where('trs_year',$data['tahun_periode'])->where('trs_month',$data['bulan']) as $transaksi){
                    if($detail->perkiraan->mst_normal == 'kredit'){
                        $ekuitas  = $ekuitas+$transaksi->trs_kredit-$transaksi->trs_debet;
                    }
                    if($detail->perkiraan->mst_normal == 'debet'){
                        $ekuitas  = $ekuitas-$transaksi->trs_kredit+$transaksi->trs_debet;
                        
                    }
                                    
                }
                $data['total_ekuitas']     = $data['total_ekuitas']+$ekuitas;
            }

            if($detail->perkiraan->mst_neraca_tipe == 'asset'){
                if($detail->perkiraan->mst_normal == 'kredit'){
                    $asset  = $detail->msd_awal_kredit;
                }
                if($detail->perkiraan->mst_normal == 'debet'){
                    $asset  = $detail->msd_awal_debet;
                }
                foreach($detail->perkiraan->transaksi->where('trs_year',$data['tahun_periode'])->where('trs_month',$data['bulan']) as $transaksi){
                    if($detail->perkiraan->mst_normal == 'kredit'){
                        $asset  = $asset+$transaksi->trs_kredit-$transaksi->trs_debet;
                    }
                    if($detail->perkiraan->mst_normal == 'debet'){
                        $asset  = $asset-$transaksi->trs_kredit+$transaksi->trs_debet;
                        
                    }
                                    
                }
                $data['total_asset']     = $data['total_asset']+$asset;
            }

            if($detail->perkiraan->mst_neraca_tipe == 'liabilitas'){
                if($detail->perkiraan->mst_normal == 'kredit'){
                    $liabilitas  = $detail->msd_awal_kredit;
                }
                if($detail->perkiraan->mst_normal == 'debet'){
                    $liabilitas  = $detail->msd_awal_debet;
                }
                foreach($detail->perkiraan->transaksi->where('trs_year',$data['tahun_periode'])->where('trs_month',$data['bulan']) as $transaksi){
                    if($detail->perkiraan->mst_normal == 'kredit'){
                        $liabilitas  = $liabilitas+$transaksi->trs_kredit-$transaksi->trs_debet;
                    }
                    if($detail->perkiraan->mst_normal == 'debet'){
                        $liabilitas  = $liabilitas-$transaksi->trs_kredit+$transaksi->trs_debet;
                        
                    }
                                    
                }
                $data['total_liabilitas']     = $data['total_liabilitas']+$liabilitas;
            }
        }

        $data['status']         = $data['total_asset']-$data['total_liabilitas']-$data['total_ekuitas'];
        // $data['status']         = 0;


        if($data['bulan']=='1'){
            $data['bln']='Januari';
        }
        if($data['bulan']=='2'){
            $data['bln']='Pebruari';
        }
        if($data['bulan']=='3'){
            $data['bln']='Maret';
        }
        if($data['bulan']=='4'){
            $data['bln']='April';
        }
        if($data['bulan']=='5'){
            $data['bln']='Mei';
        }
        if($data['bulan']=='6'){
            $data['bln']='Juni';
        }
        if($data['bulan']=='7'){
            $data['bln']='Juli';
        }
        if($data['bulan']=='8'){
            $data['bln']='Agustus';
        }
        if($data['bulan']=='9'){
            $data['bln']='September';
        }
        if($data['bulan']=='10'){
            $data['bln']='Oktober';
        }
        if($data['bulan']=='11'){
            $data['bln']='November';
        }
        if($data['bulan']=='12'){
            $data['bln']='Desember';
        }
        return view('bukuBesar/neraca', $data);
        

    }

    function printNeraca($month='', $year=''){

        $breadcrumb         = [
            'Accounting'    => '',
            'Neraca'        => route('neraca'),
            'Print'        => ''
        ];
        $data               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']       = 'neraca';
        $periode                    = mPeriode::first();
        $data['bulan']              = $month;
        $data['tahun_periode']      = $year;
        $data['title']              = 'Neraca';
        $data['tanggal']    = [
                                '1','2','3','4','5','6','7','8','9','10','11','12'
                                ];
        $data['tahun']      = [
                                '2017','2018','2019','2020','2021','2022','2023','2025'
        ];

        $data['detail_perkiraan']  = mDetailPerkiraan::where('msd_year',$data['tahun_periode'])->where('msd_month',$data['bulan'])->get();

        $data['total_asset']             = 0;
        $data['total_liabilitas']        = 0;
        $data['total_ekuitas']           = 0;

        foreach($data['detail_perkiraan'] as $detail){
            if($detail->perkiraan->mst_neraca_tipe == 'ekuitas'){
                if($detail->perkiraan->mst_normal == 'kredit'){
                    $ekuitas  = $detail->msd_awal_kredit;
                }
                if($detail->perkiraan->mst_normal == 'debet'){
                    $ekuitas  = $detail->msd_awal_debet;
                }
                foreach($detail->perkiraan->transaksi->where('trs_year',$data['tahun_periode'])->where('trs_month',$data['bulan']) as $transaksi){
                    if($detail->perkiraan->mst_normal == 'kredit'){
                        $ekuitas  = $ekuitas+$transaksi->trs_kredit-$transaksi->trs_debet;
                    }
                    if($detail->perkiraan->mst_normal == 'debet'){
                        $ekuitas  = $ekuitas-$transaksi->trs_kredit+$transaksi->trs_debet;
                        
                    }
                                    
                }
                $data['total_ekuitas']     = $data['total_ekuitas']+$ekuitas;
            }

            if($detail->perkiraan->mst_neraca_tipe == 'asset'){
                if($detail->perkiraan->mst_normal == 'kredit'){
                    $asset  = $detail->msd_awal_kredit;
                }
                if($detail->perkiraan->mst_normal == 'debet'){
                    $asset  = $detail->msd_awal_debet;
                }
                foreach($detail->perkiraan->transaksi->where('trs_year',$data['tahun_periode'])->where('trs_month',$data['bulan']) as $transaksi){
                    if($detail->perkiraan->mst_normal == 'kredit'){
                        $asset  = $asset+$transaksi->trs_kredit-$transaksi->trs_debet;
                    }
                    if($detail->perkiraan->mst_normal == 'debet'){
                        $asset  = $asset-$transaksi->trs_kredit+$transaksi->trs_debet;
                        
                    }
                                    
                }
                $data['total_asset']     = $data['total_asset']+$asset;
            }

            if($detail->perkiraan->mst_neraca_tipe == 'liabilitas'){
                if($detail->perkiraan->mst_normal == 'kredit'){
                    $liabilitas  = $detail->msd_awal_kredit;
                }
                if($detail->perkiraan->mst_normal == 'debet'){
                    $liabilitas  = $detail->msd_awal_debet;
                }
                foreach($detail->perkiraan->transaksi->where('trs_year',$data['tahun_periode'])->where('trs_month',$data['bulan']) as $transaksi){
                    if($detail->perkiraan->mst_normal == 'kredit'){
                        $liabilitas  = $liabilitas+$transaksi->trs_kredit-$transaksi->trs_debet;
                    }
                    if($detail->perkiraan->mst_normal == 'debet'){
                        $liabilitas  = $liabilitas-$transaksi->trs_kredit+$transaksi->trs_debet;
                        
                    }
                                    
                }
                $data['total_liabilitas']     = $data['total_liabilitas']+$liabilitas;
            }
        }

        $data['status']         = $data['total_asset']-$data['total_liabilitas']-$data['total_ekuitas'];
        // $data['status']         = 0;


        if($data['bulan']=='1'){
            $data['bln']='Januari';
        }
        if($data['bulan']=='2'){
            $data['bln']='Pebruari';
        }
        if($data['bulan']=='3'){
            $data['bln']='Maret';
        }
        if($data['bulan']=='4'){
            $data['bln']='April';
        }
        if($data['bulan']=='5'){
            $data['bln']='Mei';
        }
        if($data['bulan']=='6'){
            $data['bln']='Juni';
        }
        if($data['bulan']=='7'){
            $data['bln']='Juli';
        }
        if($data['bulan']=='8'){
            $data['bln']='Agustus';
        }
        if($data['bulan']=='9'){
            $data['bln']='September';
        }
        if($data['bulan']=='10'){
            $data['bln']='Oktober';
        }
        if($data['bulan']=='11'){
            $data['bln']='November';
        }
        if($data['bulan']=='12'){
            $data['bln']='Desember';
        }
        // return view('bukuBesar/printNeraca', $data);
        return view('bukuBesar/printNeracaGood', $data);
    }

    function arus_kas($start_date='', $end_date='', $tipe=''){
        $breadcrumb         = [
            'Accounting'    => '',
            'Arus Kas'        => route('arusKas')
        ];
        $data               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']       = 'arus_kas';
        $periode                    = mPeriode::first();
        $data['bulan']              = date('m');
        $data['tahun_periode']      = $periode->tahun_aktif;
        $data['title']              = 'Arus Kas';
        $data['start_date']         = date('Y-m-d');
        $data['end_date']           = date('Y-m-d');        
        $master                     = mPerkiraan::where('mst_kode_rekening','1101')->orWhere('mst_kode_rekening','=','1102')->get();
        if($start_date!='' || $end_date!=''){
            $data['start_date']         = $start_date;
            $data['end_date']           = $end_date; 
        }
        $data['transaksi']      = mTransaksi::whereBetween('tgl_transaksi',[$data['start_date'],$data['end_date']]);
        
        $data['kas_awal']           = 0;
            // foreach($master as $mst){
            //     if($mst->mst_normal=='debet'){
            //         $debet      = $mst->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_debet');
            //         $kredit     = $mst->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_kredit');
            //         $saldo_awal = $debet - $kredit;
            //         $data['kas_awal'] = $data['kas_awal']+$saldo_awal;
            //     }
            // }
        // }
        //saldoawalkaskecil
        $debet                  = mTransaksi::where('tgl_transaksi' ,'<', $data['start_date'])->where('trs_kode_rekening','1101')->sum('trs_debet');
        $kredit                 = mTransaksi::where('tgl_transaksi' ,'<', $data['start_date'])->where('trs_kode_rekening','1101')->sum('trs_kredit');
        $kas_kecil               = $debet-$kredit;

        //saldoawalkasbesar
        $debet_kas_besar        = mTransaksi::where('tgl_transaksi' ,'<', $data['start_date'])->where('trs_kode_rekening','1102')->sum('trs_debet');
        $kredit_kas_besar       = mTransaksi::where('tgl_transaksi' ,'<', $data['start_date'])->where('trs_kode_rekening','1102')->sum('trs_kredit');
        $kas_besar               = $debet_kas_besar-$kredit_kas_besar;
        $data['kas_awal']       = $kas_kecil+$kas_besar;

        $data['transaksi']                = $data['transaksi']->where('trs_kode_rekening','=','1101')->get();
        $data['transaksi_kas_besar']      = mTransaksi::whereBetween('tgl_transaksi',[$data['start_date'],$data['end_date']])->where('trs_kode_rekening','=','1102')->get();
        // ->orWhere('trs_kode_rekening','=','1102')
        if($tipe=='print'){
            return view('bukuBesar/printArusKas', $data);
        }else{
            return view('bukuBesar/arusKas', $data);
        }
        

    }

    function pilihPeriodeArusKas(Request $request) {
        $month  = $request->input('start_date');
        $year   = $request->input('end_date');
    
        return [
          'redirect'=>route('ArusKasCutOff', ['start_date'=>$month, 'end_date'=>$year]),
        ];
    }

    public function daftarAsset(){
        $breadcrumb                 = [
            'Accounting'            => '',
            'Asset'                 => '',
            'Daftar Asset'          => route('daftarAsset')
        ];
        $data                       = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']               = 'aset';
        $data['kodeAsset']          = $this->main->kodeLabel('asset');
        $data['kodekategoryAsset']  = $this->main->kodeLabel('kategoryAsset');
        $data['title']              = 'Asset';

        $data['kode_asset']         = mAsset::max('asset_id');
        $data['next_kode_asset']    = $data['kode_asset']+1;
        $data['assets']             = mAsset::all();
        $data['kategoriAssets']     = mKategoryAsset::all();
        $data['perkiraan']          = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();
        //buat no faktur pembelian
        $tahun                    = date('Y');
        $thn                      = substr($tahun,2,2);
        $bulan                    = date('m');
        $no_po_last               = mAsset::where('kode_asset','like','%/'.$data['kodeAsset'].'/'.$bulan.$thn)->max('kode_asset');
        $lastNoUrut               = substr($no_po_last,0,4);//mengambil string dari $lastNoFaktur dari index ke-8, yg diambil hanya 3 index saja
        if($lastNoUrut==0){
          $nextNoUrut                   = 0+1;
        }else{
          $nextNoUrut                   = $lastNoUrut+1;
        }    
        $nextNoTransaksi              = sprintf('%04s',$nextNoUrut).'/'.$data['kodeAsset'].'/'.$bulan.$thn;
        $data['next_no_asset']    = $nextNoTransaksi;
        //buat no faktur pembelian

        return view('bukuBesar/daftarAsset', $data);

    }

    public function save_asset(Request $request){
        // $this->rules($request->all());
        \DB::beginTransaction();
        try {
            $data_asset = [
                'kode_asset'                => $request->input('asset_id'),
                'nama'                      => strtoupper($request->input('nama')),
                'kategori'                  => $request->input('kategori'),
                'tanggal_beli'              => $request->input('tanggal_beli_2'),
                'tanggal_beli_2'            => $request->input('tanggal_beli'),
                'qty'                       => $request->input('qty'),
                'harga_beli'                => $request->input('harga_beli'),
                'total_beli'                => $request->input('total_beli'),
                'nilai_residu'              => $request->input('nilai_residu'),
                'umur_ekonomis'             => $request->input('umur_ekonomis'),
                'lokasi'                    => strtoupper($request->input('lokasi')),
                'akumulasi_beban'           => $request->input('akumulasi_beban'),
                'terhitung_tanggal'         => $request->input('terhitung_tanggal'),
                'nilai_buku'                => $request->input('nilai_buku'),
                'beban_perbulan'            => $request->input('beban_perbulan'),
                'metode'                    => $request->input('metode'),
                'tanggal_pensiun'           => $request->input('tanggal_pensiun'),
                'kode_akun_asset'           => $request->input('kode_akun_asset'),
                'kode_akun_akumulasi'       => $request->input('kode_akun_akumulasi'),
                'kode_akun_depresiasi'      => $request->input('kode_akun_depresiasi'),
                'profisi'                   => $request->input('asset_profisi'),
                'jenis_pembayaran'          => $request->input('jenis_pembayaran'),
                'created_by'                => auth()->user()->karyawan->kry_nama,
                'updated_by'                => auth()->user()->karyawan->kry_nama,
            ];

            mAsset::create($data_asset);

            $date_db          = date('Y-m-d');
            $year             = date('Y');
            $month            = date('m');
            $day              = date('d');
            $where            = [
                'jmu_year'    => $year,
                'jmu_month'   => $month,
                'jmu_day'     => $day
            ];
            $jmu_no           = mJurnalUmum::where($where)
                                  ->orderBy('jmu_no','DESC')
                                  ->select('jmu_no')
                                  ->limit(1);
            if($jmu_no->count() == 0) {
                $jmu_no         = 1;
            } else {
                $jmu_no         = $jmu_no->first()->jmu_no + 1;
            }

            $jurnal                 = new mJurnalUmum();
            $jurnal->no_invoice     = $request->input('asset_id');
            $jurnal->jmu_tanggal    = date('Y-m-d');
            $jurnal->jmu_no          = $jmu_no;
            $jurnal->jmu_year        = $year;
            $jurnal->jmu_month       = $month;
            $jurnal->jmu_day         = $day;
            $jurnal->jmu_keterangan  = 'Pengadaan asset No : '.$request->input('asset_id');
            $jurnal->jmu_date_insert = $date_db;
            $jurnal->jmu_date_update = $date_db;
            $jurnal->save();

            $jurnal_umum_id = $jurnal->jurnal_umum_id;

            $coa = [$request->input('kode_akun_asset'),'516002'];
            foreach ($coa as $key) {
                $master_piutang_lain                = mPerkiraan::where('mst_kode_rekening', $key)->first();
                $master_id_piutang_lain             = $master_piutang_lain->master_id;
                $trs_kode_rekening_piutang_lain     = $master_piutang_lain->mst_kode_rekening;
                $trs_nama_rekening_piutang_lain     = $master_piutang_lain->mst_nama_rekening;

                if($key=='516002'){
                    $amount = $request->input('asset_profisi');
                }else{
                    // $amount = parseFloat($request->input('harga_beli_2'))-parseFloat($request->input('asset_profisi'));
                    $amount = $request->input('harga_beli_2');
                }

                $data_transaksi_piutang    = [
                    'jurnal_umum_id'      => $jurnal_umum_id,
                    'master_id'           => $master_id_piutang_lain,
                    'trs_jenis_transaksi' => 'debet',
                    'trs_debet'           => $amount,
                    'trs_kredit'          => 0,
                    'user_id'             => 0,
                    'trs_year'            => $year,
                    'trs_month'           => $month,
                    'trs_kode_rekening'   => $trs_kode_rekening_piutang_lain,
                    'trs_nama_rekening'   => $trs_nama_rekening_piutang_lain,
                    'trs_tipe_arus_kas'   => 'Investasi',
                    'trs_catatan'         => 'Pengadaan Asset',
                    'trs_date_insert'     => $date_db,
                    'trs_date_update'     => $date_db,
                    'trs_charge'          => 0,
                    'trs_no_check_bg'     => 0,
                    'trs_tgl_pencairan'   => date('Y-m-d'),
                    'trs_setor'           => 0,
                    'tgl_transaksi'           => date('Y-m-d')
                ];
                mTransaksi::insert($data_transaksi_piutang);
            }

            $coa_jns_pembayaran                 = mPerkiraan::where('mst_kode_rekening', $request->input('jenis_pembayaran'))->first();
            $jns_pembayaran    = [
                'jurnal_umum_id'      => $jurnal_umum_id,
                'master_id'           => $coa_jns_pembayaran->master_id,
                'trs_jenis_transaksi' => 'kredit',
                'trs_debet'           => 0,
                'trs_kredit'          => $request->input('harga_beli'),
                'user_id'             => 0,
                'trs_year'            => $year,
                'trs_month'           => $month,
                'trs_kode_rekening'   => $coa_jns_pembayaran->mst_kode_rekening,
                'trs_nama_rekening'   => $coa_jns_pembayaran->mst_nama_rekening,
                'trs_tipe_arus_kas'   => 'Investasi',
                'trs_catatan'         => 'Pengadaan Asset',
                'trs_date_insert'     => $date_db,
                'trs_date_update'     => $date_db,
                'trs_charge'          => 0,
                'trs_no_check_bg'     => 0,
                'trs_tgl_pencairan'   => date('Y-m-d'),
                'trs_setor'           => 0,
                'tgl_transaksi'           => date('Y-m-d')
            ];
            mTransaksi::insert($jns_pembayaran);
            \DB::commit();
            return [
                'redirect'=>route('daftarAsset')
            ];
        } catch (Exception $e) {
            throw $e;
            \DB::rollBack();
        }
        
    }

    function pilih_asset($asset_id='') {
        $response               = [
            'action'            => route('postingAssetToJurnal', ['kode'=>$asset_id]),
            'field'             => mAsset::find($asset_id)
        ];
    
        return $response;
    }

    function posting_asset_to_jurnal(Request $request, $asset_id=''){
        \DB::beginTransaction();
        try{
            $date_db          = date('Y-m-d H:i:s');
        
            $kode_akun_depresiasi        = $request->input('kode_akun_depresiasi');
            $kode_akun_akumulasi         = $request->input('kode_akun_akumulasi');
            $kode_akun_asset             = $request->input('kode_akun_asset');
            $akumulasi_beban             = $request->input('akumulasi_beban');
            $id_asset                    = $request->input('asset_id');
            $asset_kode                  = $request->input('kode_asset');
            $nama_asset                  = $request->input('nama');
            $nilai_buku                  = $request->input('nilai_buku');
            $beban_perbulan              = $request->input('beban_perbulan');

            //data jurnal umum
            $tgl_transaksi      = $request->input('tgl_transaksi');
            
            
            // General
            $year             = date('Y', strtotime($tgl_transaksi));
            $month            = date('m', strtotime($tgl_transaksi));
            $day              = date('d', strtotime($tgl_transaksi));
            $where            = [
                'jmu_year'    => $year,
                'jmu_month'   => $month,
                'jmu_day'     => $day
            ];
            $jmu_no           = mJurnalUmum::where($where)
                                  ->orderBy('jmu_no','DESC')
                                  ->select('jmu_no')
                                  ->limit(1);
            if($jmu_no->count() == 0) {
                $jmu_no         = 1;
            } else {
                $jmu_no         = $jmu_no->first()->jmu_no + 1;
            }
            
            
            $jurnal_umum_id     = mJurnalUmum::orderBy('jurnal_umum_id','DESC')
                                           ->limit(1)
                                            ->first();
            $jurnal_umum_id     = $jurnal_umum_id['jurnal_umum_id'] + 1;

            $kode_asset            = mPerkiraan::where('mst_kode_rekening',$kode_akun_asset)->first();
            
            $data_jurnal          = [
                'jurnal_umum_id'  => $jurnal_umum_id,
                'no_invoice'      => $asset_kode,
                'jmu_tanggal'     => date('Y-m-d',strtotime($tgl_transaksi)),
                'jmu_no'          => $jmu_no,
                'jmu_year'        => $year,
                'jmu_month'       => $month,
                'jmu_day'         => $day,
                'jmu_keterangan'  => 'Akumulasi Penyusutan '.$kode_asset->mst_nama_rekening.' Asset No : '.$asset_kode,
                'jmu_date_insert' => $date_db,
                'jmu_date_update' => $date_db,
            ];
            mJurnalUmum::insert($data_jurnal);
            
                             
                $kode_depresiasi            = mPerkiraan::where('mst_kode_rekening',$kode_akun_depresiasi)->first();

                $data_transaksi           = [
                    'jurnal_umum_id'      => $jurnal_umum_id,
                    'master_id'           => $kode_depresiasi->master_id,
                    'trs_jenis_transaksi' => 'debet',
                    'trs_debet'           => $beban_perbulan,
                    'trs_kredit'          => 0,
                    'user_id'             => 0,
                    'trs_year'            => $year,
                    'trs_month'           => $month,
                    'trs_kode_rekening'   => $kode_depresiasi->mst_kode_rekening,
                    'trs_nama_rekening'   => $kode_depresiasi->mst_nama_rekening,
                    'trs_tipe_arus_kas'   => 'Operasi',
                    'trs_catatan'         => $kode_depresiasi->mst_nama_rekening.' '.$nama_asset,
                    'trs_date_insert'     => $date_db,
                    'trs_date_update'     => $date_db,
                    'trs_charge'          => 0,
                    'trs_no_check_bg'     => '',
                    'trs_tgl_pencairan'   => date('Y-m-d'),
                    'trs_setor'           => 0,
                    'tgl_transaksi'     => date('Y-m-d',strtotime($tgl_transaksi)),
                ];

                mTransaksi::insert($data_transaksi);
            
                $kode_akumulasi            = mPerkiraan::where('mst_kode_rekening',$kode_akun_akumulasi)->first();
                $data_transaksi_akumulasi           = [
                    'jurnal_umum_id'      => $jurnal_umum_id,
                    'master_id'           => $kode_akumulasi->master_id,
                    'trs_jenis_transaksi' => 'kredit',
                    'trs_debet'           => 0,
                    'trs_kredit'          => $beban_perbulan,
                    'user_id'             => 0,
                    'trs_year'            => $year,
                    'trs_month'           => $month,
                    'trs_kode_rekening'   => $kode_akumulasi->mst_kode_rekening,
                    'trs_nama_rekening'   => $kode_akumulasi->mst_nama_rekening,
                    'trs_tipe_arus_kas'   => 'operasi',
                    'trs_catatan'         => $kode_akumulasi->mst_nama_rekening.' '.$nama_asset,
                    'trs_date_insert'     => $date_db,
                    'trs_date_update'     => $date_db,
                    'trs_charge'          => 0,
                    'trs_no_check_bg'     => '',
                    'trs_tgl_pencairan'   => date('Y-m-d'),
                    'trs_setor'           => 0,
                    'tgl_transaksi'     => date('Y-m-d',strtotime($tgl_transaksi)),
                ];

                mTransaksi::insert($data_transaksi_akumulasi);

        

                // return Redirect::to('/jurnalUmum')->with('success', 'Posting Asset ke Jurnal Umum Berhasil');
                $data_asset         = [
                    'akumulasi_beban'       => $akumulasi_beban,
                    'nilai_buku'            => $nilai_buku
                ];

                mAsset::where('asset_id',$id_asset)->update($data_asset);


                //insert ke tb_penyusutan_asset
                $data_penyusutan = [
                    'asset_id'              => $id_asset,
                    'penyusutan_perbulan'   => $beban_perbulan,
                    'bulan'                 => $month,
                    'tahun'                 => $year
                ];
                mPenyusutanAsset::create($data_penyusutan);

                \DB::commit();

                return [
                    'redirect'=>route('jurnalUmum')
                ];
        } catch (Exception $e) {
            throw $e;
            \DB::rollBack();
        }
        
    }

    function laporan_penyusutan_asset(){
        $breadcrumb                 = [
            'Accounting'            => '',
            'Penyusutan Asset'      => route('penyusutanAsset')
        ];

        $data                       = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']               = 'penyusutan_asset';
        $data['title']              = 'Penyusutan Asset';
        $data['bln']                = date('m');
        $data['thn']                = date('Y');
        $data['kategoriAsset']      = mKategoryAsset::all();

        return view('bukuBesar/penyusutanAsset', $data);
    }

    function printPenyusutanAsset($year=''){
        $breadcrumb                 = [
            'Accounting'            => '',
            'Penyusutan Asset'      => route('penyusutanAsset')
        ];

        $data                       = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']               = 'penyusutan_asset';
        $data['title']              = 'Penyusutan Asset';
        $data['bln']                = date('m');
        $data['thn']                = $year;
        $data['kategoriAsset']      = mKategoryAsset::all();

        return view('bukuBesar/printPenyusutanAsset', $data);
    }

    function rugiLabaPerbarang($start_date='', $end_date,$tipe=''){
        $breadcrumb         = [
            'Accounting'    => '',
            'Rugi/Laba Per Barang'     => route('rugiLabaPerBarang')
        ];
        $data               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']       = 'rugi_laba';
        $periode                    = mPeriode::first();
        $data['start_date']         = date('Y-m-d');
        $data['end_date']           = date('Y-m-d');
        $data['bulan']              = $periode->bulan_aktif;
        $data['tahun_periode']      = $periode->tahun_aktif;
        $today                      = date('Y-m-d');
        $data['title']      = 'Rugi Laba Per Barang';
        if($start_date!='' || $end_date!=''){
            $data['start_date']         = $start_date;
            $data['end_date']           = $end_date;
        }
        
        
        // $data['penjualan']  = mPenjualanLangsung::whereBetween('pl_no_faktur',['1','10'])->get();
        $data['penjualan']  = mPenjualanLangsung::whereMonth('pl_tgl',$data['bulan'])->get();
        $data['barang']     = mBarang::all();
        foreach ($data['barang'] as $brg) {
            //jual langsung
            $query_langsung         = mDetailPenjualanLangsung::leftJoin('tb_penjualan_langsung', 'tb_penjualan_langsung.pl_no_faktur', '=', 'tb_detail_penjualan_langsung.pl_no_faktur')->where('brg_kode',$brg->brg_kode)->whereBetween('tb_penjualan_langsung.pl_tgl',[$data['start_date'],$data['end_date']])->get();
            // $qty_jual           = $query_langsung->sum('qty');
            // $hpp_jual           = $brg->brg_hpp;
            // $total_jual_langsung = $query_langsung->sum('total');
            $qty_jual = 0;
            $hpp_jual = 0;
            $total_jual_langsung = 0;
            foreach ($query_langsung as $langsung) {                
                $qty_jual  = $qty_jual+$langsung->qty;
                $hpp_jual  = $hpp_jual+($langsung->qty*$langsung->brg_hpp);
                $total_jual_langsung = $total_jual_langsung+($langsung->qty*$langsung->harga_jual)-($langsung->qty*$langsung->disc_nom);
            }

            //jual titipan
            $query_titipan         = mDetailSuratJalanPT::leftJoin('tb_surat_jalan_penjualan_titipan','tb_surat_jalan_penjualan_titipan.sjt_kode','=','tb_detail_surat_jalan_penjualan_titipan.sjt_kode')->leftJoin('tb_detail_penjualan_titipan','tb_detail_surat_jalan_penjualan_titipan.det_kode','=','tb_detail_penjualan_titipan.detail_pt_kode')->where('tb_detail_surat_jalan_penjualan_titipan.brg_kode',$brg->brg_kode)->whereBetween('tb_surat_jalan_penjualan_titipan.sjt_tgl',[$data['start_date'],$data['end_date']])->get();
            $qty_titipan = 0;
            $hpp_titipan = 0;
            $total_jual_titipan = 0;
            foreach ($query_titipan as $titipan) {
                $qty_titipan  = $qty_titipan+$titipan->dikirim;
                $hpp_titipan  = $hpp_titipan+($titipan->dikirim*$titipan->brg_hpp);
                $total_jual_titipan = $total_jual_titipan+($titipan->dikirim*$titipan->harga_jual)-($titipan->dikirim*$titipan->disc_nom);
                
            }
            
            $data['item']['total_jual'.$brg->brg_kode] = $total_jual_langsung+$total_jual_titipan;
            $data['item']['hpp_jual'.$brg->brg_kode] = $hpp_titipan+$hpp_jual;

            //retur
            $query_retur            = mReturPenjualanDetail::leftJoin('tb_retur_penjualan', 'tb_retur_penjualan.no_retur_penjualan', '=', 'tb_detail_retur_penjualan.no_retur_penjualan')->where('brg_kode',$brg->brg_kode)->whereBetween('tb_retur_penjualan.tgl_pengembalian',[$data['start_date'],$data['end_date']])->get();
            // $qty_retur           = $query_retur->sum('qty_retur');
            // $hpp_retur           = $brg->brg_hpp;
            // $data['item']['total_retur'.$brg->brg_kode] = $query_retur->sum('tb_detail_retur_penjualan.total_retur');
            // $data['item']['hpp_retur'.$brg->brg_kode] = $qty_retur*$hpp_retur;

            $qty_retur = 0;
            $hpp_retur = 0;
            $total_retur = 0;
            foreach ($query_retur as $retur) {                
                $qty_retur  = $qty_retur+$retur->qty_retur;
                $hpp_retur  = $hpp_retur+($retur->qty_retur*$brg->brg_hpp);
                $total_retur = $total_retur+($retur->qty_retur*$retur->harga_jual);
            }
            $data['item']['total_retur'.$brg->brg_kode] = $total_retur;
            $data['item']['hpp_retur'.$brg->brg_kode] = $hpp_retur;
        }

        $diskon_jual_langsung = mPenjualanLangsung::whereBetween('pl_tgl',[$data['start_date'],$data['end_date']])->sum('pl_disc_nom');
        $diskon_titipan       = mSuratJalanPenjualanTitipan::leftJoin('tb_penjualan_titipan','tb_penjualan_titipan.pt_no_faktur','=','tb_surat_jalan_penjualan_titipan.pt_no_faktur')->whereBetween('tb_surat_jalan_penjualan_titipan.sjt_tgl',[$data['start_date'],$data['end_date']])->sum('tb_penjualan_titipan.pt_disc_nom');
        $data['disc_nom']     = 0-($diskon_jual_langsung+$diskon_titipan);

        
        if($tipe=='print'){
            return view('bukuBesar/printRugiLabaPerBarang', $data);
        }else{
            return view('bukuBesar/rugiLabaPerBarang', $data);
        }      
        
        
    }

    function pilihPeriodeRugiLabaPerBarang(Request $request) {
        $start_sate  = $request->input('start_date');
        $end_date   = $request->input('end_date');
    
        return [
          'redirect'=>route('rugiLabaPerBarangCutOff', ['start_date'=>$start_sate, 'end_date'=>$end_date]),
        ];
    }

    function rugiLabaPerBarangCutOff($start_date='',$end_date=''){
        $breadcrumb         = [
            'Accounting'    => '',
            'Rugi/Laba Per Barang'     => route('rugiLabaPerBarang')
        ];
        $data               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']       = 'rugi_laba';
        $periode                    = mPeriode::first();
        $data['start_date']         = $start_date;
        $data['end_date']           = $end_date;
        $data['bulan']              = $periode->bulan_aktif;
        $data['tahun_periode']      = $periode->tahun_aktif;
        $today                      = date('Y-m-d');
        $data['title']      = 'Rugi Laba';
        
        
        // $data['penjualan']  = mPenjualanLangsung::whereBetween('pl_no_faktur',['1','10'])->get();
        $data['penjualan']  = mPenjualanLangsung::whereMonth('pl_tgl',$data['bulan'])->get();
        $data['barang']     = mBarang::all();

        $data['penjualan'] = DB::table('tb_penjualan_langsung')
            ->join('tb_detail_penjualan_langsung', 'tb_penjualan_langsung.pl_no_faktur', '=', 'tb_detail_penjualan_langsung.pl_no_faktur')
            ->join('tb_barang', 'tb_barang.brg_kode', '=', 'tb_detail_penjualan_langsung.brg_kode')
            ->select('tb_penjualan_langsung.*', 'tb_detail_penjualan_langsung.*', 'tb_barang.*', DB::raw('SUM(harga_net) as hrg_net'),DB::raw('SUM(tb_detail_penjualan_langsung.brg_hpp) as hrg_hpp'))
            // ->whereDate('tb_penjualan_langsung.pl_tgl',$data['today'])
            ->whereBetween('tb_penjualan_langsung.pl_tgl',[$data['start_date'],$data['end_date']])
            ->groupBy('tb_detail_penjualan_langsung.brg_kode')
            ->get();
        
        $data['jml_penjualan'] = $data['penjualan']->count();
        
        
        return view('bukuBesar/rugiLabaPerBarang', $data);
    }

    function printRugiLabaPerBarang($start_date='',$end_date=''){
        $breadcrumb         = [
            'Accounting'    => '',
            'Rugi/Laba Per Barang'     => route('rugiLabaPerBarang')
        ];
        $data               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']       = 'rugi_laba';
        $periode                    = mPeriode::first();
        $data['start_date']         = $start_date;
        $data['end_date']           = $end_date;
        $data['bulan']              = $periode->bulan_aktif;
        $data['tahun_periode']      = $periode->tahun_aktif;
        $today                      = date('Y-m-d');
        $data['thn']                = date('Y');
        $data['title']      = 'Rugi Laba';
        
        // $data['penjualan']  = mPenjualanLangsung::whereBetween('pl_no_faktur',['1','10'])->get();
        $data['penjualan']  = mPenjualanLangsung::whereMonth('pl_tgl',$data['bulan'])->get();
        $data['barang']     = mBarang::all();

        $data['penjualan'] = DB::table('tb_penjualan_langsung')
            ->join('tb_detail_penjualan_langsung', 'tb_penjualan_langsung.pl_no_faktur', '=', 'tb_detail_penjualan_langsung.pl_no_faktur')
            ->join('tb_barang', 'tb_barang.brg_kode', '=', 'tb_detail_penjualan_langsung.brg_kode')
            ->select('tb_penjualan_langsung.*', 'tb_detail_penjualan_langsung.*', 'tb_barang.*', DB::raw('SUM(harga_net) as hrg_net'),DB::raw('SUM(tb_detail_penjualan_langsung.brg_hpp) as hrg_hpp'))
            // ->whereDate('tb_penjualan_langsung.pl_tgl',$data['today'])
            ->whereBetween('tb_penjualan_langsung.pl_tgl',[$data['start_date'],$data['end_date']])
            ->groupBy('tb_detail_penjualan_langsung.brg_kode')
            ->get();
        
        $data['jml_penjualan'] = $data['penjualan']->count();

        
        
        
        return view('bukuBesar/printRugiLabaPerBarang', $data);
    }

    function filterByKodePerkiraan(Request $request) {
        $month      = $request->input('bulan');
        $year       = $request->input('tahun');
        $master_id  = $request->input('master_id');
    
        return [
          'redirect'=>route('BukuBesarCutOffKodePerkiraan', ['bulan'=>$month, 'tahun'=>$year, 'master_id'=>$master_id]),
        ];
    }

    function BukuBesarCutOffKodePerkiraan($month='', $year='',$master_id=''){
        $breadcrumb         = [
            'Buku Besar'   => route('bukuBesar'),
            'Daftar'        => ''
        ];
        $data               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']       = 'buku_besar';
        $periode                    = mPeriode::first();
        $data['bulan']              = $month;
        $data['tahun_periode']      = $year;
        $data['kode_perkiraan']     = $master_id;
        $data['title']      = $this->title;
        $data['tanggal']    = [
                                '1' => 'Januari','2' => 'Februari','3' => 'Maret','4' => 'April','5' => 'Mei','6' => 'Juni',
            '7' => 'Juli','8' => 'Agustus','9' => 'September','10' => 'Oktober','11' => 'November','12' => 'Desember'
        ];
        $data['tahun']      = [
                                '2017','2018','2019','2020','2021','2022','2023','2025'
                                ];
        
        $data['perkiraan']  = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();
        $data['detailPerkiraan'] = mDetailPerkiraan::where('msd_year',$data['tahun_periode'])->where('msd_month',$data['bulan']);

        if($master_id > 0){
            $data['detailPerkiraan'] = $data['detailPerkiraan']->where('master_id',$master_id);
        }

        $data['detailPerkiraan']    = $data['detailPerkiraan']->get();
        $data['jml_detailPerkiraan'] = $data['detailPerkiraan']->count();

        $data['transaksi'] = mTransaksi::where('trs_year',$data['tahun_periode'])->where('trs_month',$data['bulan'])->get();
        return view('bukuBesar/bukuBesar', $data);
    }

    function count_rugi_laba($tanggal){
        $data['end_date']          = $tanggal;
        $laba_rugi                 = 0;
        //transaksi pendapatan
        $data['kode_pendapatan']  = mPerkiraan::where('mst_posisi','laba rugi')->get();
        foreach ($data['kode_pendapatan'] as $perkiraan) {
            if($perkiraan->mst_normal == 'kredit'){
                $kredit             = $perkiraan->transaksi->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_kredit');
                $debet              = $perkiraan->transaksi->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_debet');                                                     
                $data['biaya'][$perkiraan->mst_kode_rekening]=$kredit-$debet;
                $laba_rugi=$laba_rugi+$data['biaya'][$perkiraan->mst_kode_rekening];

            }if($perkiraan->mst_normal == 'debet'){
                $kredit             = $perkiraan->transaksi->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_kredit');
                $debet              = $perkiraan->transaksi->where('tgl_transaksi','<=',$data['end_date'])->sum('trs_debet');

                $data['biaya'][$perkiraan->mst_kode_rekening]=$debet-$kredit;
                $laba_rugi=$laba_rugi-$data['biaya'][$perkiraan->mst_kode_rekening];
            }
        }
        return $laba_rugi;
    }

    function get_asset($kode=''){
        $data['asset']      = mAsset::find($kode);
        $data['id']         = $kode;
        // $data = 0;
        return view('bukuBesar/input-penyusutan-asset', $data)->render();
    }

    // function index() {
    //     $breadcrumb         = [
    //         'Buku Besar'   => route('bukuBesar'),
    //         'Daftar'        => ''
    //     ];
    //     $data               = $this->main->data($breadcrumb, $this->kodeLabel);
    //     $data['menu']       = 'buku_besar';
    //     $periode                    = mPeriode::first();
    //     $data['bulan']              = $periode->bulan_aktif;
    //     $data['tahun_periode']      = $periode->tahun_aktif;
    //     $data['kode_perkiraan']     = 0;
    //     $data['title']      = $this->title;
    //     $data['tanggal']    = [
    //                             '1' => 'Januari','2' => 'Februari','3' => 'Maret','4' => 'April','5' => 'Mei','6' => 'Juni',
    //         '7' => 'Juli','8' => 'Agustus','9' => 'September','10' => 'Oktober','11' => 'November','12' => 'Desember'
    //     ];
    //     $data['tahun']      = [
    //                             '2017','2018','2019','2020','2021','2022','2023','2025'
    //                             ];
    //     $data['jurnalUmum'] = mJurnalUmum::where('jmu_month',$data['bulan'])->where('jmu_year',$data['tahun_periode'])->get();
    //     $data['jml_debet']  = mTransaksi::where('trs_month',$data['bulan'])->where('trs_year',$data['tahun_periode'])->sum('trs_debet');
    //     $data['jml_kredit'] = mTransaksi::where('trs_month',$data['bulan'])->where('trs_year',$data['tahun_periode'])->sum('trs_kredit');
    //     // $data['perkiraan']  = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();
    //     $data['perkiraan']  = mPerkiraan::orderBy('master_id','ASC')->get();
    //     $data['detailPerkiraan'] = mDetailPerkiraan::where('msd_year',$data['tahun_periode'])->where('msd_month',$data['bulan'])->get();
    //     $data['jml_detailPerkiraan'] = mDetailPerkiraan::where('msd_year',$data['tahun_periode'])->where('msd_month',$data['bulan'])->count();
    //     $data['transaksi'] = mTransaksi::where('trs_year',$data['tahun_periode'])->where('trs_month',$data['bulan'])->get();
    //     return view('bukuBesar/bukuBesar', $data);
    // }
    

}
