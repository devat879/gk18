<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mCheque extends Model
{
    //public $incrementing = false;
  	protected $table = 'tb_cheques';
  	protected $primaryKey = 'id';
  	//public $timestamps = false;

  	protected $guarded=[];

  

  	public function dataList() 
  	{
   	 	return static::all();
  	}

  	public function customer()
	{
	    return $this->belongsTo('App\Models\mCustomer', 'cek_dari', 'cus_kode');
	}
}
