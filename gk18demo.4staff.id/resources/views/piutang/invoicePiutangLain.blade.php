@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="../assets/pages/css/invoice.min.css" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="invoice">
                            <p>A.K.I., Jl. Gatot Subroto No XX Tlp. 0361 xxx xxx</p>
                            <hr/>
                            <div class="row">
                                <div class="col-xs-4">
                                    <h5>Kepada :</h5>
                                    <ul class="list-unstyled">
                                        <li> #{{$piutangLain->pl_dari}} </li>
                                    </ul>
                                </div>
                                <div class="col-xs-4">
                                </div>
                                <div class="col-xs-4">
                                    <h5></h5>
                                    <ul class="list-unstyled">
                                        <li> No Invoice : {{$piutangLain->pl_invoice}}</li>
                                        <li> Tanggal Invoice : {{ date('d M Y') }}</li>
                                        <li> Jatuh Tempo : {{date('d M Y',strtotime($piutangLain->pl_jatuh_tempo))}}</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="col-md-12" width="100%">
                                        <tr>
                                                <td colspan="5" width="45%">Terbilang : {{ucwords($terbilang)}} Rupiah</td>
                                                <td width="8%">Total</td>
                                                <td width="10%">Rp. {{number_format($piutangLain->pl_amount)}}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" rowspan="3">Keterangan</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <!-- <td colspan="5"></td> -->                                                
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <!-- <td colspan="5"></td>  -->                                               
                                                <td border="1"><strong>Grand Total</strong></td>
                                                <td><strong>Rp. {{number_format($piutangLain->pl_amount)}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="5">-{{$piutangLain->pl_keterangan}}</td>                                               
                                                <td><strong></strong></td>
                                                <td><strong></strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="5"></td>                                                
                                                <td><strong></strong></td>
                                                <td><strong></strong></td>
                                            </tr>
                                    </table>
                                </div>
                            </div>
                            <hr>
                            <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();"> Print
                                        <i class="fa fa-print"></i>
                                    </a>
                                    <a class="btn btn-lg green hidden-print margin-bottom-5"> Submit Your Invoice
                                        <i class="fa fa-check"></i>
                                    </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop