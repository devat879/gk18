@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
  <script type="text/javascript">
  $(document).ready(function (e) {
    $(function() {
      $("#imgBtn").change(function() {
        $("#message").empty();
        var file = this.files[0];
        var imagefile = file.type;
        var match= ["image/jpeg","image/png","image/jpg"];
        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
        {
          $('#imgScr').attr('src', "{{asset('img/icon/no_image.png')}}");
          $("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
          return false;
        }
        else
        {
          var reader = new FileReader();
          reader.onload = imageIsLoaded;
          reader.readAsDataURL(this.files[0]);
        }
      });
    });
    function imageIsLoaded(e) {
      $('#imgScr').attr('src', e.target.result);
      $('#imgScr').attr('width', '100px');
      $('#imgScr').attr('height', '100px');
    };
  });
  </script>
@stop

@section('body')
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-4">
          <div class="portlet light ">
            <div class="portlet light">
              <h4>Tambah Slider</h4>
              <form action="{{ route('sliderInsert') }}" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-body">
                  <div class="form-group">
                    <div class="col-md-3">
                      {{-- <label class="control-label">Preview</label> --}}
                    </div>
                    <div class="col-md-9">
                      <img id="imgScr" src="{{asset('img/icon/no_image.png')}}" alt="image" width="100">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Slider Image</label>
                    <div class="col-md-9">
                      <div id="message"></div>
                      <input type="file" min="0" class="form-control" name="sldr_img" id="imgBtn">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Slider Deskripsi</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" name="sldr_deskripsi">
                    </div>
                  </div>
                </div>
                <div class="form-actions">
                  <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                      <button type="submit" class="btn green">Simpan</button>
                      <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="col-xs-8">
          <div class="portlet light ">
            <div class="portlet light">
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                <thead>
                  <tr class="">
                    <th style="font-size:10px"> No </th>
                    {{-- <th style="font-size:10px"> Kode </th> --}}
                    <th style="font-size:10px"> Date </th>
                    <th style="font-size:10px"> Deskripsi </th>
                    <th style="font-size:10px"> Image </th>
                    <th style="font-size:10px"> Action </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($dataList as $row)
                    <tr>
                      <td style="font-size:10px"> {{ $no++ }}. </td>
                      {{-- <td style="font-size:10px"> {{ $kodeLabel.$row->sldr_kode }} </td> --}}
                      <td style="font-size:10px"> {{ $row->sldr_tgl }} </td>
                      <td style="font-size:10px"> {{ $row->sldr_deskripsi }} </td>
                      <td class="text-center" style="font-size:10px"> <img src="{{ asset($row->sldr_img) }}" alt="slider" style="width: 100px; height: 100px;"></td>
                      <td style="font-size:10px">
                        <div class="btn-group-xs">
                          <button class="btn btn-danger btn-delete-new" data-href="{{ route('sliderDelete', ['kode'=>$row->sldr_kode]) }}">
                            <span class="icon-trash"></span> Delete
                          </button>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
