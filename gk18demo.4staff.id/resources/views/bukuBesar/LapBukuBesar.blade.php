@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />   
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong>{{ $message }}</strong>
        </div>
    @endif
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-md-4" style="padding-top: 15px;" align="left">
                                    <a style="font-size: 12px;" class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button">
                                        Pilih Periode
                                    </a>
                                    <a style="font-size: 12px;" type="button" class="btn btn-danger" href="{{route('printBukuBesar', ['bulan'=>$start_date, 'tahun'=>$end_date, 'master_id'=>$coa, 'tipe'=>'print'])}}" target="_blank">
                                        <span><i class="fa fa-print"></i></span> Print
                                    </a>
                                    <a style="font-size: 12px;" class="btn btn-info excel-btn" data-toggle="modal" type="button" href="#export-excel" >
                                        Excel
                                    </a>
                                </div>
                            </div>
                                                                   
                                    <br /><br />
                                    <div class="col-md-12">
                                        <h3><center>Buku Besar</center></h3>
                                        <h4><center>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</center></h4> 
                                    </div>
                                    @foreach($perkiraan as $pkr)
                                    <hr>
                                    <br>
                                    <table width="100%" id="{{$pkr->mst_nama_rekening}}">
                                        <tbody>
                                        <tr>
                                            <td style="font-size: 12px;" width="50%"><strong>Perkiraan : {{$pkr->mst_nama_rekening}}</strong></td>
                                            <td style="font-size: 12px;" align="right" width="50%"><strong>Kode Rek : {{$pkr->mst_kode_rekening}}</strong></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-striped table-bordered table-hover table-header-fixed">
                                        <thead>
                                            <tr class="success">
                                                <th style="font-size: 12px;"><center> Tanggal </center></th>
                                                <th style="font-size: 12px;"><center> No Bukti </center></th>
                                                <th style="font-size: 12px;"><center> Keterangan </center></th>
                                                <th style="font-size: 12px;"><center> Debet </center></th>
                                                <th style="font-size: 12px;"><center> Kredit </center></th>
                                                @if($pkr->mst_normal == 'kredit')
                                                <th style="font-size: 12px;"><center> Saldo(Kredit)</center></th>
                                                @endif
                                                @if($pkr->mst_normal == 'debet')
                                                <th style="font-size: 12px;"><center> Saldo(Debet)</center></th>
                                                @endif
                                                
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <tr>
                                                <?php $kode = $pkr->mst_kode_rekening;?>
                                                <td style="font-size: 11px;">{{ date('d M Y', strtotime($tgl_saldo_awal)) }}</td>
                                                <td style="font-size: 11px;"></td>
                                                <td style="font-size: 11px;">Saldo awal</td>
                                                <td style="font-size: 11px;" align="right">{{number_format(0,2)}}</td>
                                                <td style="font-size: 11px;" align="right">{{number_format(0,2)}}</td>
                                                <td style="font-size: 11px;" align="right">{{number_format($saldo_awal[$pkr->mst_kode_rekening],2)}}</td>
                                            </tr>
                                            <?php
                                                $saldo = $saldo_awal[$pkr->mst_kode_rekening];
                                                $total_debet = 0;
                                                $total_kredit = 0;

                                            ?>
                                            @foreach($pkr->transaksi->where('tgl_transaksi','>=',$start_date)->where('tgl_transaksi','<=',$end_date) as $transaksi)
                                            <?php
                                                $total=0;
                                                
                                                if($pkr->mst_normal == 'kredit'){
                                                    // $saldo_awal=$pkr->msd_awal_kredit;
                                                    $saldo=$saldo+$transaksi->trs_kredit-$transaksi->trs_debet;
                                                }
                                                if($pkr->mst_normal == 'debet'){
                                                    // $saldo_awal=$pkr->msd_awal_debet;
                                                    $saldo=$saldo-$transaksi->trs_kredit+$transaksi->trs_debet;
                                                }
                                                $total          = $total+$saldo;
                                                $total_debet    = $total_debet+$transaksi->trs_debet;
                                                $total_kredit   = $total_kredit+$transaksi->trs_kredit;                                                
                                            ?>
                                            <tr>
                                                <td style="font-size: 11px;">{{ date('d M Y', strtotime($transaksi->jurnalUmum->jmu_tanggal)) }}</td>
                                                <td style="font-size: 11px;">{{ $transaksi->jurnalUmum->no_invoice }}</td>
                                                <td style="font-size: 11px;">{{ $transaksi->jurnalUmum->jmu_keterangan }}</td>
                                                <td style="font-size: 11px;" align="right">{{ number_format($transaksi->trs_debet,2) }}</td>
                                                <td style="font-size: 11px;" align="right">{{ number_format($transaksi->trs_kredit,2) }}</td>
                                                <td style="font-size: 11px;" align="right">{{number_format($saldo,2)}}</td>
                                            </tr>
                                            @endforeach
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr class="">
                                                <td style="font-size: 12px;" colspan="3" align="right"><strong> TOTAL </strong></td>
                                                <td style="font-size: 12px;" align="right"><strong>{{number_format($total_debet,2)}}</strong></td>
                                                <td style="font-size: 12px;" align="right"><strong>{{number_format($total_kredit,2)}}</strong></td>
                                                <td style="font-size: 12px;" align="right"><strong>{{number_format($saldo,2)}}</strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    @endforeach
                                    
                                <!-- </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    Pilih Periode
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('pilihPeriodeBukuBesar') }}" class="form-horizontal form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                    <select class="form-control select2" name="kode_perkiraan">
                                        <option value="all">All COA</option>
                                        @foreach($kode_coa as $r)
                                            <option value="{{ $r->master_id }}" <?php if($coa==$r->master_id) echo 'selected';?>>{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date" value="{{$start_date}}" />
                            </div>
                            <div class="col-md-2">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date" value="{{$end_date}}"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">search</button>
                                <button type="button" class="btn default" data-dismiss="modal">close</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="export-excel" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Filter By
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('bukuBesarPrintExcel') }}" class="form-horizontal" role="form" method="post"  target="_blank">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <select class="form-control" name="kode_perkiraan" data-placeholder="Kode Rekening">
                                        <option value="all"<?php if($coa=='all') echo 'selected';?>>All COA</option>
                                        @foreach($kode_coa as $r)
                                            <option value="{{ $r->master_id }}" <?php if($coa==$r->master_id) echo 'selected';?>>{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Tanggal</label>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date" value="{{$start_date}}" />
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                            <div class="col-md-1">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date"  value="{{$end_date}}"/>
                                <!-- <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="tgl_pencairan" /> -->
                                <input type="hidden" name="tipe_laporan" value="rekapPembelian">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Search</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@stop