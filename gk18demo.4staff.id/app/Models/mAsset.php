<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mAsset extends Model
{
    
  	protected $table = 'tb_asset';
  	protected $primaryKey = 'asset_id';

  	protected $guarded = [];

	  
	public function kategoriAsset(){
		return $this->belongsTo(mKategoryAsset::class,'kategori','ka_kode');
	}

	public function kodePerkiraanAsset(){
		return $this->belongsTo(mPerkiraan::class,'kode_akun_asset','mst_kode_rekening');
	}

	public function kodePerkiraanAkumulasi(){
		return $this->belongsTo(mPerkiraan::class,'kode_akun_akumulasi','mst_kode_rekening');
	}

	public function kodePerkiraanDepresiasi(){
		return $this->belongsTo(mPerkiraan::class,'kode_akun_depresiasi','mst_kode_rekening');
	}

	public function penyusutanAsset(){
		return $this->hasMany(mPenyusutanAsset::class,'asset_id','asset_id');
	}
}
