<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/global/plugins/bootstrap/css/bootstrap.min.css')); ?>">
    <style media="screen">
    .float{
      position:fixed;
      width:60px;
      height:60px;
      bottom:40px;
      right:40px;
      border-radius:50px;
      text-align:center;
      box-shadow: 2px 2px 3px #999;
      z-index: 100000;
    }
    .my-float{
      margin-top:22px;
    }
    </style>

    <script>
    function printDiv(divName){
      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
    }
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="glyphicon glyphicon-print"></i></button>
    </div>
    <div class="container-fluid" id='printMe'>
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4>Laporan Kartu Stok</h4>
            <h4>Tanggal <?php echo e($start); ?> S/D <?php echo e($end); ?></h4>
          </div>
          <br>
          <div class="portlet light ">
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr>
                  <th> No </th>
                  <th> Date </th>
                  <th> Kode Barang </th>
                  <th> Nama Barang </th>
                  <th> Satuan </th>
                  <th> Kategory </th>
                  <th> Group Stok </th>
                  <th> Merek </th>
                  <th> Lokasi </th>
                  <th> Total In </th>
                  <th> Total Out </th>
                  <th> Last Stok </th>
                  <th> Keterangan </th>
                </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr>
                    <td> <?php echo e($row->ars_stok_kode); ?> </td>
                    <td> <?php echo e(date('Y-m-d', strtotime($row->ars_stok_date))); ?> </td>
                    <td> <?php echo e($row->brg_barcode); ?> </td>
                    <td> <?php echo e($row->brg_nama); ?> </td>
                    <td> <?php echo e($row->stn_nama); ?> </td>
                    <td> <?php echo e($row->ktg_nama); ?> </td>
                    <td> <?php echo e($row->grp_nama); ?> </td>
                    <td> <?php echo e($row->mrk_nama); ?> </td>
                    <td> <?php echo e($row->gdg_nama); ?> </td>
                    <?php if($row->stok_in == null): ?>
                      <td> - </td>
                    <?php else: ?>
                      <td> <?php echo e($row->stok_in); ?> </td>
                    <?php endif; ?>
                    <?php if($row->stok_out == null): ?>
                      <td> - </td>
                    <?php else: ?>
                      <td> <?php echo e($row->stok_out); ?> </td>
                    <?php endif; ?>
                    <td> <?php echo e($row->stok_prev); ?> </td>
                    <td> <?php echo e($row->keterangan); ?> </td>
                  </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
