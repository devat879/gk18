<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mDetailReturPembelian extends Model
{
  public $incrementing = false;
  protected $table = 'tb_detail_retur_pembelian';
  protected $primaryKey = 'no_detail_retur_pembelian';
  public $timestamps = false;

  protected $fillable=[
    'no_detail_retur_pembelian',
    'brg_kode',
    'stok_id',
    'harga_jual',
    'qty_retur',
    'total_retur',
    'no_retur_pembelian'
  ];

  public function supplier()
  {
    return $this->belongsTo('App\Models\mSupplier', 'spl_kode');
  }

  public function hutangSupplier()
  {
    return $this->hasMany('App\Models\mHutangSupplier', 'ps_no_faktur');
  }

  public function returPembelian()
  {
    return $this->belongsTo('App\Models\mReturPembelian', 'no_retur_pembelian','no_return_pembelian');
  }

  public function barang()
  {
    return $this->belongsTo('App\Models\mBarang', 'brg_kode','brg_kode');
  }

  public function stok(){
    return $this->belongsTo(mStok::class,'stok_id','stk_kode');
  }
}
