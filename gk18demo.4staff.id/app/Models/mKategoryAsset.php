<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mKategoryAsset extends Model
{
    public $incrementing = false;
    protected $table = 'tb_kategori_asset';
    protected $primaryKey = 'ka_kode';
    public $timestamps = false;
      
    public function Asset(){
        return $this->hasMany(mAsset::class,'kategori','ka_kode');
    }
}
