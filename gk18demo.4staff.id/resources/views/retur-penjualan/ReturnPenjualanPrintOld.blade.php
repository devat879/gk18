<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all"/> --}}
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}"> --}}
    <title></title>
    <style>
    p {
      font-size: 10px;
    }
    table {
      border-collapse: collapse;
    }
    </style>
  </head>
  <body>
    <div>
      <div>
        <div>
          <table width="100%">
            <tr>
              <td>
                <img src="http://grahakita.ganesh-demo.online/img/icon/aki.png" width="70">
              </td>
              <td colspan="3">
                <p>Jl. Gatot Subroto Barat No. 88A Tlp. (0361)416088, Fax.(0361)418933, No.HP. 0812 1611 8118</p>
              </td>
            </tr>
          </table>
          <div style="float: left; width: 60%;">
            <table width="100%" border="0">
              <tr>
                <td width="30%"> <p>Nama Pelanggan</p> </td>
                <td width="5%"> <p>:</p> </td>
                <td> <p>{{$faktur->penjualanL->cus_nama}}</p> </td>
              </tr>
            </table>
          </div>

          <div style="float: right; width: 40%;">
            <table width="100%" border="0">
              <tr>
                <td width="10%"> <p>No. Ftr</p> </td>
                <td width="10%"> <p>:</p> </td>
                <td width="10%"> <p>{{$faktur->kode_bukti_id}}</p> </td>
              </tr>
              <tr>
                <td width="10%"> <p>Tgl. Jual</p> </td>
                <td width="10%"> <p>:</p> </td>
                <td width="10%"> <p>{{$faktur->tglFaktur}}</p> </td>
              </tr>
              <tr>
                <td width="10%"> <p>No. Retur</p> </td>
                <td width="10%"> <p>:</p> </td>
                <td width="10%"> <p>{{$faktur->retur_id}}</p> </td>
              </tr>
              <tr>
                <td width="10%"> <p>Tgl. Retur</p> </td>
                <td width="10%"> <p>:</p> </td>
                <td width="10%"> <p>{{$faktur->tglRetur}}</p> </td>
              </tr>
            </table>
          </div>

          <div class="index" style="clear: both; margin: 0pt; padding: 0pt;">
            {{-- <h5 align="center">Faktur Jual</h5> --}}
            <p align="center"><b>Faktur Jual</b></p>
            <table width="100%" border="1">
              <thead>
                <tr>
                  <th> <p>No</p> </th>
                  <th> <p>Nama Barang</p> </th>
                  <th> <p>Banyak</p> </th>
                  <th> <p>Harga Beli</p> </th>
                  <th> <p>Total Retur</p> </th>
                </tr>
              </thead>
              <tbody>
                @foreach ($faktur->detail as $key)
                  <tr>
                    <td><p>{{$faktur->no++}}</p></td>
                    <td><p>{{$key['brg_nama']}}</p></td>
                    <td><p>{{number_format($key['qty_retur'], 0, "." ,".")}}</p></td>
                    <td><p>{{number_format($key['harga_jual'], 0, "." ,".")}}</p></td>
                    <td><p>{{number_format($key['total_retur'], 0, "." ,".")}}</p></td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <br>
          <br>
          <div style="float: left; width: 33%;">
            <table style="width: 100%;" border="0">
              <tbody>
                <tr>
                  <td width="50%" align="center">
                    {{-- <h5>Yang Menerima D/O</h5> --}}
                    <b><p>Mengetahui</p></b>
                  </td>
                </tr>
                <tr>
                  <td>
                    <br>
                    <br>
                    <br>
                  </td>
                </tr>
                <tr>
                  <td width="50%" align="center">
                    <p>(......................................)</p>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          <div style="float: left; width: 33%;">
            <table style="width: 100%;" border="0">
              <tbody>
                <tr>
                  <td width="50%" align="center">
                    {{-- <h5>Dari : ANGSA KUSUMA INDAH</h5> --}}
                    <b><p>Sopir</p></b>
                  </td>
                </tr>
                <tr>
                  <td>
                    <br>
                    <br>
                    <br>
                  </td>
                </tr>
                <tr>
                  <td width="50%" align="center">
                    <p>(......................................)</p>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          <div style="float: right; width: 30%;">
            <table style="width: 100%;" border="1">
              <tr>
                <td> <p>Total Retur</p> </td>
                {{-- <td> <p>:</p> </td> --}}
                <td> <p>{{number_format($faktur->total_retur, 0, "." ,".")}}</p> </td>
              </tr>
              <tr>
                <td> <p>Discount</p> </td>
                {{-- <td> <p>:</p> </td> --}}
                <td> <p>{{number_format(0, 0, "." ,".")}}</p> </td>
              </tr>
              <tr>
                <td> <p>PPN</p> </td>
                {{-- <td> <p>:</p> </td> --}}
                <td> <p>{{number_format(0, 0, "." ,".")}}</p> </td>
              </tr>
              <tr>
                <td> <p>Grand Total</p> </td>
                {{-- <td> <p>:</p> </td> --}}
                <td> <p>{{number_format($faktur->total_retur, 0, "." ,".")}}</p> </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
