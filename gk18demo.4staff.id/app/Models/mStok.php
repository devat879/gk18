<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mStok extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_stok';
  protected $primaryKey = 'stk_kode';
  public $timestamps = false;
  protected $fillable = ['brg_kode','gdg_kode','stok','stok_hpp','brg_no_seri','created_at','updated_at'];

  public function barang()
  {
    return $this->belongsTo('App\Models\mBarang', 'brg_kode');
  }

  public function gudang()
  {
    return $this->belongsTo('App\Models\mGudang', 'gdg_kode');
  }

  public function supplier()
  {
    return $this->belongsTo('App\Models\mSupplier', 'spl_kode');
  }

  // public function barangGudang1()
  // {
  //   return $this->hasManyThrough('App\Models\mBarang', 'App\Models\mGudang');
  // }
  //
  // public function barangGudang()
  // {
  //   return $this->hasManyThrough(
  //     'App\Models\mBarang',
  //     'App\Models\mGudang',
  //     'brg_kode',
  //     'gdg_kode',
  //     'brg_kode',
  //     'gdg_kode'
  //   );
  // }
}
