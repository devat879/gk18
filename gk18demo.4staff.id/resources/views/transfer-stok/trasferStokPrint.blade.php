<html>
  <head>
    {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> --}}
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}"> --}}

    <style>
    /* .tt  {border-collapse:collapse;border-spacing:0;width: 100%; }
    .tt td{font-family:Tahoma;font-size:11px;padding-top: 0px;overflow:hidden;word-break:normal;color:#333;background-color:#fff;}
    .tt th{font-family:Tahoma;font-size:11px;font-weight:bold;padding:1px 1px;overflow:hidden;word-break:normal;color:#333;background-color:#f0f0f0;}
    .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; padding:5px;}
    .tg td{font-family:Tahoma;font-size:11px;padding:5px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
    .tg th{font-family:Tahoma;font-size:12px;font-weight:bold;padding:5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;}
    .tg .tg-3wr7{font-weight:bold;font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
    .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
    .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;} */

    @media print {
      html, body {
      display: block;
      font-family: "Tahoma";
      margin: 0px 0px 0px 0px;
      }

      @page {
        size: 21.5cm 15cm;

      }
      #footer {
        position: fixed;
        bottom: 0;
      }
    }

    p {
      font-size: 14px;
      padding: 0 !important;
      margin: 0 !important;
    }
    table {
      border-collapse: collapse;
      padding: 0 !important;
      margin: 0 !important;
    }
    tr td{
      padding: 0 !important;
      margin: 0 !important;
    }
    </style>
  </head>
  <body>
    <table id="header" width="100%" border="0">
      <thead>
        <tr>
          <td colspan="8">
          <p><b>A.K.I.,</b> Jl. Gatot Subroto Barat No. 88A Tlp. (0361)416088, Fax.(0361)418933, No.HP. 0812 1611 8118, www.grahakita18.com</p>
          </td>
        </tr>
        <tr>
          <td width="60%" colspan="5"> <p>Transfer Barang</p> </td>
          <td width="20%"> <p>No. Transfer</p> </td>
          <td width="20%" colspan="2"> <p>: {{$dataTransfer->trf_kode}}</p> </td>
        </tr>
        <tr>
          <td colspan="5"></td>
          <td> <p>Tgl Transfer:</p> </td>
          <td colspan="2"> <p>: {{$dataTransfer->trf_tgl}}</p> </td>
        </tr>
        <tr>
          <td colspan="5"></td>

          <td> <p>Tujuan:</p> </td>
          <td colspan="2"> <p>: {{$dataTransfer->gudang['gdg_nama']}}</p> </td>
        </tr>
      </thead>
    </table>

    <table id="body" width="100%" border="0">
      <thead>
        <br>
        <tr>
          <th align="left"> <p>No</p> </th>
          <th align="left" colspan="3"> <p>Nama Stock</p> </th>
          <th align="center"> <p>Stok Transfer</p> </th>
          <th align="left"> <p>Satuan</p> </th>
          <th align="left" colspan="2"> <p>Asal Barang</p> </th>
        </tr>
        <tr>
          <td colspan="8">
            <hr>
          </td>
        </tr>
      </thead>
      <tbody>
        @foreach ($dataList as $row)
          <tr>
            <td align="left"><p>{{$row->barang['brg_barcode']}}</p></td>
            <td align="left" colspan="3"><p>{{$row->barang['brg_nama']}}</p></td>
            <td align="center"><p>{{number_format($row->trf_det_stok, 2, "." ,",")}}</p></td>
            <td align="left"><p>{{$row->barang->satuan['stn_nama']}}</p></td>
            <td align="left" colspan="2"><p>{{$row->gudang['gdg_nama']}}</p></td>
          </tr>
        @endforeach
      </tbody>
    </table>

    <table id="footer" width="100%" border="0">
      <thead>
        <tr>
          <td colspan="8">
            <hr>
          </td>
        </tr>
        <tr>
          <td colspan="8">
            <p>Keterangan : {{$dataTransfer->trf_keterangan}}</p>
          </td>
        </tr>
        <tr>
          <td colspan="8">
            <br>
          </td>
        </tr>
        <tr>
          <td align="center" colspan="3">
            <b><p>Yang Menerima Surat Transfer</p></b>
          </td>

          <td align="center" colspan="3">
            <b><p>Dari : ANGSA KUSUMA INDAH</p></b>
          </td>

          <td align="center" colspan="3">
            <b><p>Penerima Barang</p></b>
          </td>
        </tr>
        <tr>
          <td colspan="3">
            <br>
            <br>
            <br>
          </td>

          <td colspan="3">
            <br>
            <br>
            <br>
          </td>

          <td colspan="3">
            <br>
            <br>
            <br>
          </td>
        </tr>
        <tr>
          <td align="center" colspan="3">
            <p>(......................................)</p>
          </td>

          <td align="center" colspan="3">
            <p>(......................................)</p>
          </td>

          <td align="center" colspan="3">
            <p>(......................................)</p>
          </td>
        </tr>
      </thead>
    </table>
  </body>
</html>

<script type="text/javascript">
  window.print();
</script>
