<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mPerkiraan extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_ac_master';
  protected $primaryKey = 'master_id';
  public $timestamps = false;
  
  public function transaksi()
  {
    return $this->hasMany(mTransaksi::class,'master_id','master_id')->orderBy('tgl_transaksi','asc');
  }

  public function master_detail()
  {
    return $this->hasMany(mDetailPerkiraan::class,'master_id','master_id');
  }

  public function childs(){
    return $this->hasMany(mPerkiraan::class,'mst_master_id');
  }

  public function parent(){
    return $this->belongsTo(mPerkiraan::class,'mst_master_id');
  }
}
