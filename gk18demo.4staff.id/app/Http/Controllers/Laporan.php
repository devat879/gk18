<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use Meneses\LaravelMpdf\Facades\LaravelMpdf As MPDF;
use Carbon\Carbon;
use App\Helpers\Main;
use App\Models\mCustomer;
use App\Models\mKaryawan;
use App\Models\mPenjualanLangsung;
use App\Models\mDetailPenjualanLangsung;
use App\Models\mPenjualanTitipan;
use App\Models\mDetailPenjualanTitipan;
use App\Models\mHutangSuplier;
use App\Models\mHutangLain;
use App\Models\mPiutangPelanggan;
use App\Models\mPiutangLain;
use App\Models\mPembelianSupplier;
use App\Models\mSuratJalanPenjualanLangsung;
use App\Models\mSuratJalanPenjualanTitipan;
use App\Models\mBarang;
use App\Models\mKategoryProduct;
use App\Models\mGroupStok;
use App\Models\mMerek;
use App\Models\mSatuan;
use App\Models\mSupplier;
use App\Models\mGudang;
use App\Models\mStok;
use App\Models\mReturPenjualan;
use App\Models\mReturPenjualanDetail;
use App\Models\mHutangCek;
use App\Models\mCheque;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PenjualanStokExport;

set_time_limit(300);

class Laporan extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;
  private $menu;

  function __construct() {
    $this->main = new Main();
    $this->title = 'Laporan';
    $this->kodeLabel = '';
    $this->menu = 'laporan_penjualan';
  }

  function dataKategoryRow() {
    $data = mKategoryProduct::orderBy('ktg_nama', 'asc')->get();
    $option = '<option value="0">---Semua---</option>';
    foreach ($data as $key) {
      $option .= '<option value="'.$key->ktg_kode.'">'.$key->ktg_nama.'</option>';
    }

    $response = [
      'kategory'=>$option,
    ];

    return $response;
  }

  function dataGroupRow() {
    $data = mGroupStok::orderBy('grp_nama', 'asc')->get();
    $option = '<option value="0">---Semua---</option>';
    foreach ($data as $key) {
      $option .= '<option value="'.$key->grp_kode.'">'.$key->grp_nama.'</option>';
    }

    $response = [
      'group'=>$option,
    ];

    return $response;
  }

  function dataMerekRow() {
    $data = mMerek::orderBy('mrk_nama', 'asc')->get();
    $option = '<option value="0">---Semua---</option>';
    foreach ($data as $key) {
      $option .= '<option value="'.$key->mrk_kode.'">'.$key->mrk_nama.'</option>';
    }

    $response = [
      'merek'=>$option,
    ];

    return $response;
  }

  function dataSupplierRow() {
    $data = mSupplier::orderBy('spl_nama', 'asc')->get();
    $option = '<option value="0">---Semua---</option>';
    foreach ($data as $key) {
      $option .= '<option value="'.$key->spl_kode.'">'.$key->spl_nama.'</option>';
    }

    $response = [
      'supplier'=>$option,
    ];

    return $response;
  }

  function dataCustomerRow() {
    $data = mCustomer::orderBy('cus_nama', 'asc')->get();
    $option = '<option value="all">---Semua---</option>';
    foreach ($data as $key) {
      $option .= '<option value="'.$key->cus_kode.'">'.$key->cus_nama.'</option>';
    }

    $response = [
      'customer'=>$option,
    ];

    return $response;
  }

  private function request_date($request)
  {
    $time = Carbon::now();
    if ($request->start_date == null && $request->end_date == null) {
      $start_date = $time;
      $end_date = $time;
    }
    elseif ($request->start_date == null || $request->end_date == null) {
      if ($request->start_date == null) {
        $start_date = $time;
        $end_date = $request->end_date;
      }
      else {
        $start_date = $request->start_date;
        $end_date = $time;
      }
    }
    else {
      $start_date = $request->start_date;
      $end_date = $request->end_date;
    }

    return [
        'start' => $start_date,
        'end'=> $end_date
    ];
  }

  function viewPenjualan(Request $request) {
    $date = $this->request_date($request);

    if ($request->report == 'print') {
      return [
        'redirect'=>route('penjualanCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
      ];
    }
    else {
      return [
        'redirect'=>route('PrintExcel.all_penjualan_kasir', ['start'=>$date['start'], 'end'=>$date['end']]),
      ];
    }

  }

  function viewPembelian(Request $request) {
    $date = $this->request_date($request);

    return [
      'redirect'=>route('pembelianCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
    ];
  }

  function viewHutang(Request $request) {
    $date = $this->request_date($request);

    if ($request->report == 'print'){
      return [
          'redirect'=>route('hutangCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
      ];
    }else{
      return [
          'redirect'=>route('lapHutangExcel', ['start'=>$date['start'], 'end'=>$date['end']]),
      ];
    }


  }

  function viewPiutang(Request $request) {
    $date = $this->request_date($request);

    if ($request->report == 'print'){
      return [
          'redirect'=>route('piutangCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
      ];
    }else{
      return [
          'redirect'=>route('lapPiutangExcel', ['start'=>$date['start'], 'end'=>$date['end']]),
      ];
    }

  }

  function viewPenjualanLangsung(Request $request) {
    $date = $this->request_date($request);

    if ($request->report == 'print') {
      return [
        // 'redirect'=>route('penjualanLangsungCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
        'redirect'=>route('penjualanLangsungCetak', ['start'=>$date['start'], 'end'=>$date['end'], 'kode'=> $request->cus_kode]),
      ];
    }
    else {
      return [
        // 'redirect'=>route('PrintExcel.penjualan-langsung', ['start'=>$date['start'], 'end'=>$date['end']]),
        'redirect'=>route('PrintExcel.penjualan-langsung', ['start'=>$date['start'], 'end'=>$date['end'], 'kode'=> $request->cus_kode]),
      ];
    }
  }

  function viewRekapPenjualanLangsung(Request $request) {
    $date = $this->request_date($request);

    if ($request->report == 'print') {
      return [
        'redirect'=>route('rekapPenjualanLangsungCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
      ];
    }
    else {
      return [
        'redirect'=>route('PrintExcel.rekep-penjualan-langsung', ['start'=>$date['start'], 'end'=>$date['end']]),
      ];
    }
  }

  function viewPenjualanTitipan(Request $request) {
    $date = $this->request_date($request);

    if ($request->report == 'print') {
      return [
        // 'redirect'=>route('penjualanTitipanCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
        'redirect'=>route('penjualanTitipanCetak', ['start'=>$date['start'], 'end'=>$date['end'], 'kode'=>$request->cus_kode]),
      ];
    }
    else {
      return [
        // 'redirect'=>route('PrintExcel.penjualan-titipan', ['start'=>$date['start'], 'end'=>$date['end']]),
        'redirect'=>route('PrintExcel.penjualan-titipan', ['start'=>$date['start'], 'end'=>$date['end'], 'kode'=>$request->cus_kode]),
      ];
    }
  }

  function viewRekapPenjualanTitipan(Request $request) {
    $date = $this->request_date($request);

    if ($request->report == 'print') {
      return [
        'redirect'=>route('rekapPenjualanTitipanCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
      ];
    }
    else {
      return [
        'redirect'=>route('PrintExcel.rekep-penjualan-titipan', ['start'=>$date['start'], 'end'=>$date['end']]),
      ];
    }
  }

  function viewSuratJalanDetail(Request $request) {
    $date = $this->request_date($request);

    if ($request->report == 'print') {
      return [
        'redirect'=>route('suratJalanDetailCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
      ];
    }
    else {
      return [
        'redirect'=>route('PrintExcel.sj-detail', ['start'=>$date['start'], 'end'=>$date['end']]),
      ];
    }
  }

  function viewSuratJalanSisa(Request $request) {
    $date = $this->request_date($request);

    if ($request->report == 'print') {
      return [
        'redirect'=>route('suratJalanSisaCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
      ];
    }
    else {
      return [
        'redirect'=>route('PrintExcel.sj-sisa', ['start'=>$date['start'], 'end'=>$date['end']]),
      ];
    }
  }

  function viewPenjualanStok(Request $request) {
    $date = $this->request_date($request);

    return [
      'redirect'=>route('penjualanStokCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
    ];
  }

  function viewOmsetSalesDetail(Request $request) {
    $date = $this->request_date($request);

    if ($request->report == 'print') {
      return [
        // 'redirect'=>route('omsetSalesDetailCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
        'redirect'=>route('omsetSalesDetailCetak', ['start'=>$date['start'], 'end'=>$date['end'], 'kode' => $request->mrk_kode]),
      ];
    }
    else {
      return [
        // 'redirect'=>route('PrintExcel.omset-sales-detail', ['start'=>$date['start'], 'end'=>$date['end']]),
        'redirect'=>route('PrintExcel.omset-sales-detail', ['start'=>$date['start'], 'end'=>$date['end'], 'kode' => $request->mrk_kode]),
      ];
    }
  }

  function viewOmsetSalesRekap(Request $request) {
    $date = $this->request_date($request);

    if ($request->report == 'print') {
      return [
        // 'redirect'=>route('omsetSalesRekapCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
        'redirect'=>route('omsetSalesRekapCetak', ['start'=>$date['start'], 'end'=>$date['end'], 'kode'=>$request->mrk_kode]),
      ];
    }
    else {
      return [
        // 'redirect'=>route('PrintExcel.omset-sales-rekap', ['start'=>$date['start'], 'end'=>$date['end']]),
        'redirect'=>route('PrintExcel.omset-sales-rekap', ['start'=>$date['start'], 'end'=>$date['end'], 'kode'=>$request->mrk_kode]),
      ];
    }
  }


  function viewSales(Request $request) {
    $breadcrumb = [
        'Omset Sales'=>route('salesView'),
    ];
    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu'] = 'laporan_penjualan';
    $data['title'] = 'Omset Sales';
    $data['merek'] = mMerek::select('mrk_kode', 'mrk_nama')->orderBy('mrk_nama', 'asc')->get();
    return view('laporan/omset-sales/sales', $data);
  }

  function viewSalesTransaksi($kode='', $start='', $end='', $mrk_kode='') {
    $breadcrumb = [
        'Omset Sales'=>route('salesView'),
    ];
    $data = $this->main->data($breadcrumb, $this->kodeLabel);

    $data['menu'] = 'laporan_penjualan';
    $data['title'] = 'Omset Sales';

    if ($mrk_kode != 'all') {
      $rowPL = mKaryawan::
      select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', 'tb_penjualan_langsung.pl_tgl',
      'tb_penjualan_langsung.pl_no_faktur', 'tb_penjualan_langsung.cus_nama', 'tb_detail_penjualan_langsung.brg_kode',
      'tb_barang.brg_barcode', 'tb_detail_penjualan_langsung.qty', 'tb_detail_penjualan_langsung.harga_net',
      'tb_detail_penjualan_langsung.nama_barang')
      ->Join('tb_penjualan_langsung', 'tb_karyawan.kry_kode','=','tb_penjualan_langsung.pl_sales_person')
      ->Join('tb_detail_penjualan_langsung', 'tb_penjualan_langsung.pl_no_faktur','=','tb_detail_penjualan_langsung.pl_no_faktur')
      ->Join('tb_barang', 'tb_detail_penjualan_langsung.brg_kode','=','tb_barang.brg_kode')
      ->where('pl_tgl', '>=', $start)
      ->where('pl_tgl', '<=', $end)
      ->where('tb_barang.mrk_kode', '=', $mrk_kode)
      ->where('tb_penjualan_langsung.pl_sales_person', $kode)
      ->get();

      $rowPT = mKaryawan::
      select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', 'tb_penjualan_titipan.pt_tgl',
      'tb_penjualan_titipan.pt_no_faktur', 'tb_penjualan_titipan.cus_nama', 'tb_detail_penjualan_titipan.brg_kode',
      'tb_barang.brg_barcode', 'tb_detail_penjualan_titipan.qty', 'tb_detail_penjualan_titipan.harga_net',
      'tb_detail_penjualan_titipan.nama_barang')
      ->Join('tb_penjualan_titipan', 'tb_karyawan.kry_kode','=','tb_penjualan_titipan.pt_sales_person')
      ->Join('tb_detail_penjualan_titipan', 'tb_penjualan_titipan.pt_no_faktur','=','tb_detail_penjualan_titipan.pt_no_faktur')
      ->Join('tb_barang', 'tb_detail_penjualan_titipan.brg_kode','=','tb_barang.brg_kode')
      ->where('pt_tgl', '>=', $start)
      ->where('pt_tgl', '<=', $end)
      ->where('tb_barang.mrk_kode', '=', $mrk_kode)
      ->where('tb_penjualan_titipan.pt_sales_person', $kode)
      ->get();
    }
    else {
      $rowPL = mKaryawan::
      select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', 'tb_penjualan_langsung.pl_tgl',
      'tb_penjualan_langsung.pl_no_faktur', 'tb_penjualan_langsung.cus_nama', 'tb_detail_penjualan_langsung.brg_kode',
      'tb_barang.brg_barcode', 'tb_detail_penjualan_langsung.qty', 'tb_detail_penjualan_langsung.harga_net',
      'tb_detail_penjualan_langsung.nama_barang')
      ->Join('tb_penjualan_langsung', 'tb_karyawan.kry_kode','=','tb_penjualan_langsung.pl_sales_person')
      ->Join('tb_detail_penjualan_langsung', 'tb_penjualan_langsung.pl_no_faktur','=','tb_detail_penjualan_langsung.pl_no_faktur')
      ->Join('tb_barang', 'tb_detail_penjualan_langsung.brg_kode','=','tb_barang.brg_kode')
      ->where('pl_tgl', '>=', $start)
      ->where('pl_tgl', '<=', $end)
      ->where('tb_penjualan_langsung.pl_sales_person', $kode)
      ->get();

      $rowPT = mKaryawan::
      select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', 'tb_penjualan_titipan.pt_tgl',
      'tb_penjualan_titipan.pt_no_faktur', 'tb_penjualan_titipan.cus_nama', 'tb_detail_penjualan_titipan.brg_kode',
      'tb_barang.brg_barcode', 'tb_detail_penjualan_titipan.qty', 'tb_detail_penjualan_titipan.harga_net',
      'tb_detail_penjualan_titipan.nama_barang')
      ->Join('tb_penjualan_titipan', 'tb_karyawan.kry_kode','=','tb_penjualan_titipan.pt_sales_person')
      ->Join('tb_detail_penjualan_titipan', 'tb_penjualan_titipan.pt_no_faktur','=','tb_detail_penjualan_titipan.pt_no_faktur')
      ->Join('tb_barang', 'tb_detail_penjualan_titipan.brg_kode','=','tb_barang.brg_kode')
      ->where('pt_tgl', '>=', $start)
      ->where('pt_tgl', '<=', $end)
      ->where('tb_penjualan_titipan.pt_sales_person', $kode)
      ->get();
    }

    $data['dataListPL'] = $rowPL;
    $data['dataListPT'] = $rowPT;
    $data['sales'] = $kode;
    $data['start'] = $start;
    $data['end'] = $end;

    // return $data;

    return view('laporan/omset-sales/sales-transaksi', $data);
  }

  public function dataSalesMonthly($id='', $start='', $end='')
  {
    // $date = $this->request_date($request);
    $start = Carbon::parse($start);
    $end = Carbon::parse($end);

    $startYear = $start->startOfYear()->year;
    $endYear =  $end->year;

    $labelMonth = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    $labelYear = array();
    for ($i=$startYear; $i <= $endYear; $i++) {
      array_push($labelYear, $i);
    }

    $collectionMonth = collect($labelMonth)->unique();
    $collectionYear = collect($labelYear)->unique();

    $matrix = $collectionYear->crossJoin($collectionMonth);

    $label = array();
    foreach ($matrix as $key) {
      array_push($label, [$key[1].'-'.$key[0]]);
    }

    $PL = array();
    $PT = array();
    foreach ($collectionYear as $year) {
      foreach ($collectionMonth as $month) {
        $dataPL = mPenjualanLangsung::select(\DB::raw('SUM(grand_total) as total, MONTH(pl_tgl) as month, YEAR(pl_tgl) as year'))
        ->whereYear('pl_tgl', '=', $year)
        ->whereMonth('pl_tgl', '=', $month)
        ->where('pl_sales_person', '=', $id)
        ->groupBy('month', 'year')
        ->first();

        $dataPT = mPenjualanTitipan::select(\DB::raw('SUM(grand_total) as total, MONTH(pt_tgl) as month, YEAR(pt_tgl) as year'))
        ->whereYear('pt_tgl', '=', $year)
        ->whereMonth('pt_tgl', '=', $month)
        ->where('pt_sales_person', '=', $id)
        ->groupBy('month', 'year')
        ->first();

        if ($dataPL == null) {
          array_push($PL, ['date' => null, 'total' => null]);
          array_push($PT, ['date' => $dataPT['month'], 'total' => $dataPT['total']]);
        }
        elseif ($dataPT == null) {
          array_push($PL, ['date' => $dataPL['month'], 'total' => $dataPL['total']]);
          array_push($PT, ['date' => null, 'total' => null]);
        }
        elseif ($dataPL == null && $dataPT == null) {
          array_push($PL, ['date' => null, 'total' => null]);
          array_push($PT, ['date' => null, 'total' => null]);
        }
        else {
          array_push($PL, ['date' => $dataPL['month'], 'total' => $dataPL['total']]);
          array_push($PT, ['date' => $dataPT['month'], 'total' => $dataPT['total']]);
        }
      }
    }

    return response()->json(
      [
        'PL' => $PL,
        'PT' => $PT,
        'label' => $label,
      ]
    );
  }

  function viewSalesDetail($kode='') {
    $breadcrumb = [
        'Omset Sales'=>route('salesView'),
    ];
    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $kodePenjualan = $this->main->kodeLabel('penjualanLangsung');
    $data['menu'] = 'laporan_penjualan';
    $data['title'] = 'Omset Sales';

    $row = mPenjualanLangsung::where('pl_no_faktur', $kode)->first();
    $row->customer;
    $row['detail'] = $row->detail_PL;
    foreach ($row['detail'] as $key) {
      $key->barang;
    }
    unset($row->detail_PL);

    $data['kode'] = $kodePenjualan;
    $data['dataList'] = $row;

    // return $data;

    return view('laporan/omset-sales/sales-transaksi-detail', $data);
  }

  function penjualan($start='', $end='') {
    $data['no'] = 1;
    $data['no_2'] = 1;
    $data['no_3'] = 1;
      // $data = $this->main->data([], $this->kodeLabel);
      // $kodeCustomer = $this->main->kodeLabel('customer');
      // $kodePenjualanLangsung = $this->main->kodeLabel('penjualanLangsung');
      // $kodePenjualanTitipan = $this->main->kodeLabel('penjualanTitipan');
      // $kodeLaporan = $this->main->kodeLabel('laporan');
      // $date_start = $request->start_date;
      // $date_end = $request->end_date;
      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $disc_PL=0;
      $angkut_PL=0;
      $total_PL=0;

      $total_PL_cash=0;
      $total_PL_kembalian_uang=0;
      $total_PL_transfer=0;
      $total_PL_cek=0;
      $total_PL_edc=0;
      $total_PL_piutang=0;
      // $rowPL = mPenjualanLangsung::all();
      $rowPL = mPenjualanLangsung::where('pl_tgl', '>=', $start)->where('pl_tgl', '<=', $end)->get();
      foreach ($rowPL as &$tr) {
        $tr->customer;
        $tr->karyawan;
        $disc_PL += $tr->pl_disc_nom;
        $angkut_PL += $tr->pl_ongkos_angkut;
        // $total_PL += $tr->pl_subtotal;

        $total_PL_cash += $tr->cash;
        $total_PL_kembalian_uang += $tr->kembalian_uang;
        $total_PL_transfer += $tr->transfer;
        $total_PL_cek += $tr->cek_bg;
        $total_PL_edc += $tr->edc;
        $total_PL_piutang += $tr->piutang;
      }

      $disc_PT=0;
      $angkut_PT=0;
      $total_PT=0;

      $total_PT_cash=0;
      $total_PT_kembalian_uang=0;
      $total_PT_transfer=0;
      $total_PT_cek=0;
      $total_PT_edc=0;
      $total_PT_piutang=0;
      $total_PT=0;

      // $rowPT = mPenjualanTitipan::all();
      $rowPT = mPenjualanTitipan::where('pt_tgl', '>=', $start)->where('pt_tgl', '<=', $end)->get();
      foreach ($rowPT as &$tr) {
        $tr->customer;
        $tr->karyawan;
        $disc_PT += $tr->pt_disc_nom;
        $angkut_PT += $tr->pt_ongkos_angkut;
        // $total_PT += $tr->pt_subtotal;

        $total_PT_cash += $tr->cash;
        $total_PT_kembalian_uang += $tr->kembalian_uang;
        $total_PT_transfer += $tr->transfer;
        $total_PT_cek += $tr->cek_bg;
        $total_PT_edc += $tr->edc;
        $total_PT_piutang += $tr->piutang;
      }

      $disc_retur=0;
      $angkut_retur=0;
      $total_retur=0;
      $total_retur_cash=0;
      $total_retur_kembalian_uang=0;
      $total_retur_transfer=0;
      $total_retur_cek=0;
      $total_retur_edc=0;
      $total_retur_piutang=0;
      $total_retur=0;

      $rowRetur = mReturPenjualan::where('tgl_pengembalian', '>=', $start)->where('tgl_pengembalian', '<=', $end)->get();;
      foreach ($rowRetur as &$tr) {
        // $tr->customer;
        // $tr->karyawan;
        // $disc_retur += $tr->pt_disc_nom;
        // $angkut_retur += $tr->pt_ongkos_angkut;
        $total_retur_cash += $tr->cash;
        $total_retur_kembalian_uang += $tr->kembalian_uang;
        $total_retur_transfer += $tr->transfer;
        $total_retur_cek += $tr->cek_bg;
        $total_retur_edc += $tr->edc;
        $total_retur_piutang += $tr->piutang;
      }

      $data['start'] = $start;
      $data['end'] = $end;

      //Data Penjualan Langsung
      // $data['kode_PL'] = $kodePenjualanLangsung;
      $data['dataPL'] = $rowPL;
      $data['DiscPL'] = $disc_PL;
      $data['AngkutPL'] = $angkut_PL;
      // $data['GrandTotalPL'] = $total_PL;

      $data['TotalPLCash'] = $total_PL_cash - $total_PL_kembalian_uang;
      $data['TotalPLTransfer'] = $total_PL_transfer;
      $data['TotalPLCek'] = $total_PL_cek;
      $data['TotalPLEdc'] = $total_PL_edc;
      $data['TotalPLPiutang'] = $total_PL_piutang;
      $data['TotalPL'] = ($angkut_PL + ($total_PL_cash - $total_PL_kembalian_uang) + $total_PL_transfer + $total_PL_cek + $total_PL_edc + $total_PL_piutang);
      $data['TotalPenjualanL'] = ($angkut_PL + ($total_PL_cash - $total_PL_kembalian_uang) + $total_PL_transfer + $total_PL_cek + $total_PL_edc + $total_PL_piutang);

      //Data Penjualan Titipan
      // $data['kode_PT'] = $kodePenjualanTitipan;
      $data['dataPT'] = $rowPT;
      $data['DiscPT'] = $disc_PT;
      $data['AngkutPT'] = $angkut_PT;
      // $data['GrandTotalPT'] = $total_PT;

      $data['TotalPTCash'] = $total_PT_cash - $total_PT_kembalian_uang;
      $data['TotalPTTransfer'] = $total_PT_transfer;
      $data['TotalPTCek'] = $total_PT_cek;
      $data['TotalPTEdc'] = $total_PT_edc;
      $data['TotalPTPiutang'] = $total_PT_piutang;
      $data['TotalPT'] = ($angkut_PT + ($total_PT_cash - $total_PT_kembalian_uang) + $total_PT_transfer + $total_PT_cek + $total_PT_edc + $total_PT_piutang);
      $data['TotalPenjualanT'] = ($angkut_PT + ($total_PT_cash - $total_PT_kembalian_uang) + $total_PT_transfer + $total_PT_cek + $total_PT_edc + $total_PT_piutang);

      $data['dataRetur'] = $rowRetur;
      // $data['DiscRetur'] = $disc_Retur;
      // $data['AngkutRetur'] = $angkut_Retur;

      $data['TotalReturCash'] = $total_retur_cash - $total_retur_kembalian_uang;
      $data['TotalReturTransfer'] = $total_retur_transfer;
      $data['TotalReturCek'] = $total_retur_cek;
      $data['TotalReturEdc'] = $total_retur_edc;
      $data['TotalReturPiutang'] = $total_retur_piutang;
      $data['TotalRetur'] = (($total_retur_cash - $total_retur_kembalian_uang) + $total_retur_transfer + $total_retur_cek + $total_retur_edc + $total_retur_piutang);
      $data['TotalPenjualanRetur'] = (($total_retur_cash - $total_retur_kembalian_uang) + $total_retur_transfer + $total_retur_cek + $total_retur_edc + $total_retur_piutang);

      return view('laporan/laporan-penjualan', $data);

      // $pdf = PDF::loadView('laporan.laporan-penjualan', $data);
      // return $pdf->download('laporan-penjualan.pdf');

      $pdf = PDF::loadView('laporan.laporan-penjualan', $data)
                  ->setPaper('a4', 'potrait');
      return $pdf->stream();
  }

  function pembelian($start='', $end='') {
    $data['no'] = 1;
    $data['no_2'] = 1;
    // $data = $this->main->data([], $this->kodeLabel);
    // $kodeCustomer = $this->main->kodeLabel('supplier');
    // $kodePembelianSupplier = $this->main->kodeLabel('pembelianSupplier');
    // $date_start = $request->start_date;
    // $date_end = $request->end_date;
    $start = date('Y-m-d', strtotime($start));
    $end = date('Y-m-d', strtotime($end));

    $disc=0;
    $angkut=0;
    $total=0;
    // $row = mPembelianSupplier::all();
    $row = mPembelianSupplier::where('ps_tgl', '>=', $start)->where('ps_tgl', '<=', $end)->get();
    foreach ($row as &$spl) {
      $spl->supplier;
      $disc += $spl->ps_disc_nom;
      $angkut += $spl->biaya_lain;
      $total += $spl->ps_subtotal;
    }

    $data['start'] = $start;
    $data['end'] = $end;

    //Data Pembelian Supplier
    // $data['kode'] = $kodePembelianSupplier;
    $data['data'] = $row;
    $data['Disc'] = $disc;
    $data['Angkut'] = $angkut;
    $data['Total'] = $total;

    // return $data;

    return view('laporan/laporan-pembelian', $data);

    // $pdf = PDF::loadView('laporan.laporan-pembelian', $data);
    // return $pdf->download('laporan-pembelian.pdf');

    $pdf = PDF::loadView('laporan.laporan-pembelian', $data)
    ->setPaper('a4', 'potrait');
    return $pdf->stream();
  }

  function hutang($start='', $end='') {
    $data['no'] = 1;
    $data['no_2'] = 1;
    // $data = $this->main->data([], $this->kodeLabel);
    // $kodeCustomer = $this->main->kodeLabel('supplier');
    // $kodeHutangSupplier = $this->main->kodeLabel('hutangSuplier');
    // $kodeHutangLain = $this->main->kodeLabel('hutangLain');
    // $date_start = $request->start_date;
    // $date_end = $request->end_date;
    $start = date('Y-m-d', strtotime($start));
    $end = date('Y-m-d', strtotime($end));

    $disc_HS=0;
    $angkut_HS=0;
    $total_HS=0;
    $rowHS = mHutangSuplier::leftJoin('tb_pembelian_supplier', 'tb_hutang_supplier.ps_no_faktur', '=', 'tb_pembelian_supplier.no_pembelian')
    ->leftJoin('tb_supplier', 'tb_pembelian_supplier.spl_id', '=', 'tb_supplier.spl_kode')
    ->where('tb_hutang_supplier.kode_perkiraan', '=', '2101')
    ->where('ps_tgl', '>=', $start)->where('ps_tgl', '<=', $end)->get();

    foreach ($rowHS as $hutang) {
      $disc_HS += $hutang->ps_disc_nom;
      $angkut_HS += $hutang->biaya_lain;
      $total_HS += $hutang->hs_amount;
    }

    $total_HB=0;
    $rowHB = mHutangSuplier::leftJoin('tb_pembelian_supplier', 'tb_hutang_supplier.ps_no_faktur', '=', 'tb_pembelian_supplier.no_pembelian')
    ->leftJoin('tb_supplier', 'tb_pembelian_supplier.spl_id', '=', 'tb_supplier.spl_kode')
    ->where('tb_hutang_supplier.kode_perkiraan', '=', '2104')
    ->where('ps_tgl', '>=', $start)->where('ps_tgl', '<=', $end)->get();

    foreach ($rowHB as $hutang_biaya) {
      $total_HB += $hutang_biaya->hs_amount;
    }

    $total_HL=0;
    // $rowHL = mHutangLain::all();
    $rowHL = mHutangLain::where('hl_tgl', '>=', $start)->where('hl_tgl', '<=', $end)->get();
    foreach ($rowHL as &$hutang) {
      $total_HL += $hutang->hl_amount;
    }

    //data hutang cek/bg
    $total_HC=0;
    // $rowHL = mHutangLain::all();
    $rowHC = mHutangCek::where('tgl_cek', '>=', $start)->where('tgl_cek', '<=', $end)->get();
    foreach ($rowHC as $hutang_cek) {
      $total_HC += $hutang_cek->total_cek;
    }

    $data['start'] = $start;
    $data['end'] = $end;

    //Data Hutang Supplier
    // $data['kode_HS'] = $kodeHutangSupplier;
    $data['dataHS'] = $rowHS;
    $data['DiscHS'] = $disc_HS;
    $data['AngkutHS'] = $angkut_HS;
    $data['TotalHS'] = $total_HS;

    //Data Hutang Lain
    // $data['kode_HL'] = $kodeHutangLain;
    $data['dataHL'] = $rowHL;
    $data['TotalHL'] = $total_HL;

    //Data Hutang Biaya
    // $data['kode_HL'] = $kodeHutangLain;
    $data['dataHB'] = $rowHB;
    $data['TotalHB'] = $total_HB;

    //Data Hutang Biaya
    // $data['kode_HL'] = $kodeHutangLain;
    $data['dataHC'] = $rowHC;
    $data['TotalHC'] = $total_HC;

    // return $data;

    return view('laporan/laporan-hutang', $data);

    $pdf = PDF::loadView('laporan.laporan-hutang', $data)
    ->setPaper('a4', 'potrait');
    return $pdf->stream();
  }

  function piutang($start='', $end='') {
    $data['no'] = 1;
    $data['no_2'] = 1;
    $data['no_3'] = 1;
    $data['no_4'] = 1;

    $start = date('Y-m-d', strtotime($start));
    $end = date('Y-m-d', strtotime($end));

    $disc_PP=0;
    $angkut_PP=0;
    $total_PP=0;
    $rowPP = mPiutangPelanggan::leftJoin('tb_penjualan_langsung', 'tb_piutang_pelanggan.pp_no_faktur', '=', 'tb_penjualan_langsung.pl_no_faktur')
    ->leftJoin('tb_customer', 'tb_penjualan_langsung.cus_kode', '=', 'tb_customer.cus_kode')
    ->leftJoin('tb_karyawan', 'tb_penjualan_langsung.pl_sales_person', '=', 'tb_karyawan.kry_kode')
    ->select('pp_invoice', 'pp_no_faktur', 'pl_no_faktur', 'pl_tgl', 'pl_sales_person', 'pl_disc_nom', 'pl_ongkos_angkut', 'pl_subtotal', 'grand_total', 'tb_customer.cus_nama', 'kry_nama','pp_jatuh_tempo')
    ->where('pl_tgl', '>=', $start)->where('pl_tgl', '<=', $end)->get();

    foreach ($rowPP as $piutang) {
      $disc_PP += $piutang->pl_disc_nom;
      $angkut_PP += $piutang->pl_ongkos_angkut;
      $total_PP += $piutang->pl_subtotal;
    }

    $disc_PPPT=0;
    $angkut_PPPT=0;
    $total_PPPT=0;
    $rowPPPT = mPiutangPelanggan::leftJoin('tb_penjualan_titipan', 'tb_piutang_pelanggan.pp_no_faktur', '=', 'tb_penjualan_titipan.pt_no_faktur')
    ->leftJoin('tb_customer', 'tb_penjualan_titipan.cus_kode', '=', 'tb_customer.cus_kode')
    ->leftJoin('tb_karyawan', 'tb_penjualan_titipan.pt_sales_person', '=', 'tb_karyawan.kry_kode')
    ->select('pp_invoice', 'pp_no_faktur', 'pt_no_faktur', 'pt_tgl', 'pt_sales_person', 'pt_disc_nom', 'pt_ongkos_angkut', 'pt_subtotal', 'grand_total', 'tb_customer.cus_nama', 'kry_nama','pp_jatuh_tempo')
    ->where('pt_tgl', '>=', $start)->where('pt_tgl', '<=', $end)->get();

    foreach ($rowPPPT as $piutang) {
      $disc_PPPT += $piutang->pt_disc_nom;
      $angkut_PPPT += $piutang->pt_ongkos_angkut;
      $total_PPPT += $piutang->pt_subtotal;
    }

    $total_PL=0;
    // $rowPL = mPiutangLain::all();
    $rowPL = mPiutangLain::where('pl_tgl', '>=', $start)->where('pl_tgl', '<=', $end)->where('kode_perkiraan','1308')->get();
    foreach ($rowPL as &$piutang) {
      $piutang->customer;
      $total_PL += $piutang->pl_amount;
    }

    $total_PC=0;
    // $rowPL = mPiutangLain::all();
    $rowPC = mCheque::where('tgl_cek', '>=', $start)->where('tgl_cek', '<=', $end)->get();
    foreach ($rowPC as &$piutangCek) {
      // $piutang->customer;
      $total_PC += $piutangCek->cek_amount;
    }

    $data['start'] = $start;
    $data['end'] = $end;

    //Data Piutang Pelanggan
    // $data['kode_PP'] = $kodePenjualanTitipan;
    $data['dataPP'] = $rowPP;
    $data['DiscPP'] = $disc_PP;
    $data['AngkutPP'] = $angkut_PP;
    $data['TotalPP'] = $total_PP;

    $data['dataPPPT'] = $rowPPPT;
    $data['DiscPPPT'] = $disc_PPPT;
    $data['AngkutPPPT'] = $angkut_PPPT;
    $data['TotalPPPT'] = $total_PPPT;
    //Data Piutang Lain
    // $data['kode_PL'] = $kodePenjualanTitipan;
    $data['dataPL'] = $rowPL;
    $data['TotalPL'] = $total_PL;

    //Data Piutang Cek
    // $data['kode_PL'] = $kodePenjualanTitipan;
    $data['dataPC'] = $rowPC;
    $data['TotalPC'] = $total_PC;

    // return $data;

    return view('laporan/laporan-piutang', $data);

    $pdf = PDF::loadView('laporan.laporan-piutang', $data)
    ->setPaper('a4', 'potrait');
    return $pdf->stream();
  }

  function penjualan_langsung($start='', $end='', $cus_kode='') {
      // $data = $this->main->data([], $this->kodeLabel);
      // $kodeBarang = $this->main->kodeLabel('barang');
      $data['no'] = 1;
      $data['no_2'] = 1;
      // $kodePenjualan = $this->main->kodeLabel('penjualanLangsung');
      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      if ($cus_kode != 'all') {
        $row = mCustomer::select('cus_kode', 'cus_nama')->whereHas('PenjualanLangsung', function($q) use($start, $end){
          $q->where('pl_tgl', '>=', $start)->where('pl_tgl', '<=', $end);
        })
        ->where('cus_kode', $cus_kode)
        ->get();
      }
      else {
        $row = mCustomer::select('cus_kode', 'cus_nama')->whereHas('PenjualanLangsung', function($q) use($start, $end){
          $q->where('pl_tgl', '>=', $start)->where('pl_tgl', '<=', $end);
        })->get();
      }

      $grand_total=0;
      foreach ($row as $key) {
        $grand_total_cus=0;
        $key['penjualan'] = $key->PenjualanLangsung;
        foreach ($key['penjualan'] as $keyPenjualan) {
          $grand_total_cus+=$keyPenjualan->pl_subtotal;
          $grand_total+=$keyPenjualan->pl_subtotal;
          $keyPenjualan['detail'] = $keyPenjualan->detail_PL;
          // foreach ($keyPenjualan['detail'] as $keyDet) {
          //   $keyDet->barang->satuan;
          //   // $keyDet->barang;
          //   // $keyDet->satuanJ;
          // }
          unset($keyPenjualan->detail_PL);
        }
        $key['grand_total_cus'] = $grand_total_cus;
        unset($key->PenjualanLangsung);
      }

      $data['start'] = $start;
      $data['end'] = $end;

      //Data Penjualan Langsung
      // $data['kode'] = $kodePenjualan;
      $data['data'] = $row;
      $data['grand_total'] = $grand_total;

      // return $data;

      return view('laporan/laporan-penjualan-langsung', $data);

      // $pdf = PDF::loadView('laporan.laporan-penjualan-langsung', $data);
      // return $pdf->download('laporan-penjualan.pdf');

      $pdf = PDF::loadView('laporan.laporan-penjualan-langsung', $data)
                  ->setPaper('a4', 'potrait');
      return $pdf->stream();
  }

  function rekap_penjualan_langsung($start='', $end='') {
      // $data = $this->main->data([], $this->kodeLabel);
      // $kodeBarang = $this->main->kodeLabel('barang');
      $data['no'] = 1;
      $data['no_2'] = 1;

      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $rowPL = mDetailPenjualanLangsung::select('pl_no_faktur', 'qty', 'harga_net', 'brg_kode', 'gudang', \DB::raw('SUM(qty) as brg_qty'))
      ->whereHas('PL_tgl', function($q) use($start, $end){
        $q->where('pl_tgl', '>=', $start)->where('pl_tgl', '<=', $end);
      })
      ->groupBy('brg_kode', 'gudang')->get();
      $total=0;
      $grand_total=0;
      foreach ($rowPL as &$key) {
        $key->barang->satuan;
        $key->gdg;
        $key->PL_tgl;
        $total = $key->harga_net*$key->brg_qty;
        $key['total'] = $total;
        $grand_total += $total;
      }

      $data['start'] = $start;
      $data['end'] = $end;

      //Data Penjualan Langsung
      $data['dataPL'] = $rowPL;
      $data['grand_total'] = $grand_total;

      // return $data;

      return view('laporan/laporan-rekap-penjualan-langsung', $data);

      // $pdf = PDF::loadView('laporan.laporan-rekap-penjualan-langsung', $data);
      // return $pdf->download('laporan-penjualan.pdf');

      $pdf = PDF::loadView('laporan.laporan-rekap-penjualan-langsung', $data)
                  ->setPaper('a4', 'potrait');
      return $pdf->stream();
  }

  function penjualan_titipan($start='', $end='', $cus_kode='') {
      // $data = $this->main->data([], $this->kodeLabel);
      // $kodeBarang = $this->main->kodeLabel('barang');
      $data['no'] = 1;
      $data['no_2'] = 1;
      // $kodePenjualan = $this->main->kodeLabel('penjualanTitipan');
      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      if ($cus_kode != 'all') {
        $row = mCustomer::select('cus_kode', 'cus_nama')->whereHas('PenjualanTitipan', function($q) use($start, $end){
          $q->where('pt_tgl', '>=', $start)->where('pt_tgl', '<=', $end);
        })
        ->where('cus_kode', $cus_kode)
        ->get();
      }
      else {
        $row = mCustomer::select('cus_kode', 'cus_nama')->whereHas('PenjualanTitipan', function($q) use($start, $end){
          $q->where('pt_tgl', '>=', $start)->where('pt_tgl', '<=', $end);
        })->get();
      }

      $grand_total=0;
      foreach ($row as $key) {
        $grand_total_cus=0;
        $key['penjualan'] = $key->PenjualanTitipan;
        foreach ($key['penjualan'] as $keyPenjualan) {
          $grand_total_cus+=$keyPenjualan->pt_subtotal;
          $grand_total+=$keyPenjualan->pt_subtotal;
          $keyPenjualan['detail'] = $keyPenjualan->detail_PT;
          // foreach ($keyPenjualan['detail'] as $keyDet) {
          //   $keyDet->barang->satuan;
          //   // $keyDet->barang;
          //   // $keyDet->satuan;
          // }
          unset($keyPenjualan->detail_PT);
        }
        $key['grand_total_cus'] = $grand_total_cus;
        unset($key->PenjualanTitipan);
      }

      $data['start'] = $start;
      $data['end'] = $end;

      //Data Penjualan Langsung
      // $data['kode'] = $kodePenjualan;
      $data['data'] = $row;
      $data['grand_total'] = $grand_total;

      // return $data;

      return view('laporan/laporan-penjualan-titipan', $data);

      // $pdf = PDF::loadView('laporan.laporan-penjualan-titipan', $data);
      // return $pdf->download('laporan-penjualan.pdf');

      $pdf = PDF::loadView('laporan.laporan-penjualan-titipan', $data)
                  ->setPaper('a4', 'potrait');
      return $pdf->stream();
  }

  function rekap_penjualan_titipan($start='', $end='') {
    $data['no'] = 1;
    $data['no_2'] = 1;
    // $data = $this->main->data([], $this->kodeLabel);
    // $kodeBarang = $this->main->kodeLabel('barang');
    // $kodePenjualanTitipan = $this->main->kodeLabel('penjualanTitipan');
    $start = date('Y-m-d', strtotime($start));
    $end = date('Y-m-d', strtotime($end));

    $rowPT = mDetailPenjualanTitipan::select('pt_no_faktur', 'qty', 'harga_net', 'brg_kode', 'gudang', \DB::raw('SUM(qty) as brg_qty'))
    ->whereHas('PT_tgl', function($q) use($start, $end){
      $q->where('pt_tgl', '>=', $start)->where('pt_tgl', '<=', $end);
    })
    ->groupBy('brg_kode', 'gudang')->get();
    $total=0;
    $grand_total=0;
    foreach ($rowPT as &$key) {
      // $key->barang->satuan;
      $key->barang;
      $key->gdg;
      $key->PT_tgl;
      $total = $key->harga_net*$key->brg_qty;
      $key['total'] = $total;
      $grand_total += $total;
    }

    $data['start'] = $start;
    $data['end'] = $end;

    //Data Penjualan Langsung
    // $data['kode_PT'] = $kodePenjualanTitipan;
    $data['dataPT'] = $rowPT;
    $data['grand_total'] = $grand_total;

    // return $data;

    return view('laporan/laporan-rekap-penjualan-titipan', $data);

    // $pdf = PDF::loadView('laporan.laporan-rekap-penjualan-titipan', $data);
    // return $pdf->download('laporan-penjualan.pdf');

    $pdf = PDF::loadView('laporan.laporan-rekap-penjualan-titipan', $data)
    ->setPaper('a4', 'potrait');
    return $pdf->stream();
  }

  function surat_jalan_detail($start='', $end='') {
    $data['no'] = 1;
    $data['no_2'] = 1;
    // $data = $this->main->data([], $this->kodeLabel);
    // $kodeBarang = $this->main->kodeLabel('barang');
    // $kodeSJPL = $this->main->kodeLabel('suratJalanPL');
    // $kodeSJPT = $this->main->kodeLabel('suratJalanPT');
    // $kodeFakturPL = $this->main->kodeLabel('penjualanLangsung');
    // $kodeFakturPT = $this->main->kodeLabel('penjualanTitipan');
    $start = date('Y-m-d', strtotime($start));
    $end = date('Y-m-d', strtotime($end));

    // $rowPL = mSuratJalanPenjualanLangsung::where('sjl_tgl', '>=', $start)->where('sjl_tgl', '<=', $end)->get();
    // // return $row;
    // foreach ($rowPL as &$key) {
    //   $faktur = $key->faktur;
    //   $key['detail'] = $faktur->detail_PL;
    //   foreach ($key['detail'] as $keyDet) {
    //     $hpp=0;
    //     $harga_jual=0;
    //     $keyDet->barang->satuan;
    //     $keyDet->gdg;
    //     $hpp += $keyDet->qty*$keyDet->brg_hpp;
    //     $harga_jual += $keyDet->qty*$keyDet->harga_jual;
    //     $keyDet['hpp'] = $hpp;
    //     $keyDet['total_harga_jual'] = $harga_jual;
    //   }
    //   unset($faktur->detail_PL);
    // }

    $rowPT = mSuratJalanPenjualanTitipan::where('sjt_tgl', '>=', $start)->where('sjt_tgl', '<=', $end)->get();
    // return $row;
    foreach ($rowPT as &$key) {
      $faktur = $key->faktur;
      $key['detail'] = $faktur->detail_PT;
      foreach ($key['detail'] as $keyDet) {
        $hpp=0;
        $harga_jual=0;
        $keyDet->barang->satuan;
        $keyDet->gdg;
        $hpp += $keyDet->qty*$keyDet->brg_hpp;
        $harga_jual += $keyDet->qty*$keyDet->harga_jual;
        $keyDet['hpp'] = $hpp;
        $keyDet['total_harga_jual'] = $harga_jual;
      }
      unset($faktur->detail_PT);
    }

    $data['start'] = $start;
    $data['end'] = $end;

    //Data Penjualan Langsung
    // $data['kode_SJ_PL'] = $kodeSJPL;
    // $data['kode_SJ_PT'] = $kodeSJPT;
    // $data['kode_faktur_PL'] = $kodeFakturPL;
    // $data['kode_faktur_PT'] = $kodeFakturPT;
    // $data['dataPL'] = $rowPL;
    $data['dataPT'] = $rowPT;

    // return $data;

    return view('laporan/laporan-surat-jalan-detail', $data);

    // $pdf = PDF::loadView('laporan.laporan-surat-jalan-detail', $data);
    // return $pdf->download('laporan-penjualan.pdf');

    $pdf = PDF::loadView('laporan.laporan-surat-jalan-detail', $data)
    ->setPaper('a4', 'landscape');
    return $pdf->stream();
  }

  function surat_jalan_sisa($start='', $end='') {
    $data['no'] = 1;
    $data['no_2'] = 1;
    // $data = $this->main->data([], $this->kodeLabel);
    // $kodeBarang = $this->main->kodeLabel('barang');
    // $kodeSJPL = $this->main->kodeLabel('suratJalanPL');
    // $kodeSJPT = $this->main->kodeLabel('suratJalanPT');
    // $kodeFakturPL = $this->main->kodeLabel('penjualanLangsung');
    // $kodeFakturPT = $this->main->kodeLabel('penjualanTitipan');
    $start = date('Y-m-d', strtotime($start));
    $end = date('Y-m-d', strtotime($end));

    // $rowPL = mSuratJalanPenjualanLangsung::where('sjl_tgl', '>=', $start)->where('sjl_tgl', '<=', $end)
    // ->groupBy('sjl_kode')
    // ->get();
    // $total=0;
    // $grand_totalPL=0;
    // foreach ($rowPL as &$key) {
    //   $faktur = $key->faktur;
    //   $key['customer'] = $faktur->customer;
    //   $key['detail'] = $faktur->detail_PL;
    //   foreach ($key['detail'] as $keyDet) {
    //     $keyDet->barang->satuan;
    //     $keyDet['qty_akhir'] = $keyDet->qty - $keyDet->terkirim;
    //   }
    //   unset($key->faktur);
    //   unset($faktur->customer);
    //   unset($faktur->detail_PL);
    // }

    $rowPT = mSuratJalanPenjualanTitipan::where('sjt_tgl', '>=', $start)->where('sjt_tgl', '<=', $end)
    ->groupBy('sjt_kode')
    ->get();
    $total=0;
    $grand_totalPT=0;
    foreach ($rowPT as &$key) {
      $faktur = $key->faktur;
      $key['customer'] = $faktur->customer;
      $key['detail'] = $faktur->detail_PT;
      foreach ($key['detail'] as $keyDet) {
        $keyDet->barang->satuan;
        $keyDet['qty_akhir'] = $keyDet->qty - $keyDet->terkirim;
      }
      unset($key->faktur);
      unset($faktur->customer);
      unset($faktur->detail_PT);
    }

    $data['start'] = $start;
    $data['end'] = $end;

    //Data Penjualan Langsung
    // $data['kode_SJ_PL'] = $kodeSJPL;
    // $data['kode_SJ_PT'] = $kodeSJPT;
    // $data['kode_faktur_PL'] = $kodeFakturPL;
    // $data['kode_faktur_PT'] = $kodeFakturPT;
    // $data['dataPL'] = $rowPL;
    $data['dataPT'] = $rowPT;
    // $data['grand_total_PL'] = $grand_totalPL;
    $data['grand_total_PT'] = $grand_totalPT;

    // return $data;

    return view('laporan/laporan-surat-jalan-sisa', $data);

    // $pdf = PDF::loadView('laporan.laporan-surat-jalan-sisa', $data);
    // return $pdf->download('laporan-penjualan.pdf');

    $pdf = PDF::loadView('laporan.laporan-surat-jalan-sisa', $data)
    ->setPaper('a4', 'potrait');
    return $pdf->stream();
  }

  function penjualan_all_stok($start='', $end='') {
    $data['no'] = 1;
    $data['no_2'] = 1;
    // $data = $this->main->data([], $this->kodeLabel);
    // $kodeBarang = $this->main->kodeLabel('barang');
    // $kodeFaktur = $this->main->kodeLabel('penjualanLangsung');
    $start = date('Y-m-d', strtotime($start));
    $end = date('Y-m-d', strtotime($end));

    $rowPL = mPenjualanLangsung::where('pl_tgl', '>=', $start)->where('pl_tgl', '<=', $end)->get();
    $sub_total=0;
    $grand_total=0;
    $total_dic=0;
    $total_komisi=0;
    $hpp=0;
    foreach ($rowPL as &$key) {
      $key->customer;
      $key->karyawan;
      $key['detail'] = $key->detail_PL;
      foreach ($key['detail'] as $keyDet) {
        $keyDet->barang->satuan;
        $sub_total += $keyDet->total;
        $total_dic += $keyDet->disc_nom;
        $hpp += $keyDet->brg_hpp;
      }
      unset($key->detail_PL);
    }
    $grand_total = $sub_total - $total_dic;

    $data['sub_total'] = $sub_total;
    $data['grand_total'] = $grand_total;
    $data['total_komisi'] = $total_komisi;
    $data['HPP'] = $hpp;

    $rowPT = mPenjualanTitipan::where('pt_tgl', '>=', $start)->where('pt_tgl', '<=', $end)->get();
    $sub_totalPT=0;
    $grand_totalPT=0;
    $total_dicPT=0;
    $total_komisiPT=0;
    $hpp=0;
    foreach ($rowPT as &$key) {
      $key->customer;
      $key->karyawan;
      $key['detail'] = $key->detail_PT;
      foreach ($key['detail'] as $keyDet) {
        $keyDet->barang->satuan;
        $sub_totalPT += $keyDet->total;
        $total_dicPT += $keyDet->disc_nom;
        $hppPT += $keyDet->brg_hpp;
      }
      unset($key->detail_PT);
    }
    $grand_totalPT = $sub_totalPT - $total_dicPT;

    $data['sub_totalPT'] = $sub_totalPT;
    $data['grand_totalPT'] = $grand_totalPT;
    $data['total_komisiPT'] = $total_komisiPT;
    $data['HPPPT'] = $hppPT;

    $data['start'] = $start;
    $data['end'] = $end;

    //Data Penjualan Langsung
    // $data['kode_faktur'] = $kodeFaktur;
    $data['dataPL'] = $rowPL;
    $data['dataPT'] = $rowPT;

    // return $data;

    return view('laporan/laporan-penjualan-all-stok', $data);

    // $pdf = PDF::loadView('laporan.laporan-penjualan-all-stok', $data);
    // return $pdf->download('laporan-penjualan.pdf');

    $pdf = PDF::loadView('laporan.laporan-penjualan-all-stok', $data)
    ->setPaper('a4', 'landscape');
    return $pdf->stream();
  }

  function omset_sales_detail($start='', $end='', $mrk_kode='') {
      // $data = $this->main->data([], $this->kodeLabel);
      $data['no'] = 1;
      $data['no_2'] = 1;
      $data['no_3'] = 1;

      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      if ($mrk_kode != 'all') {
        $rowPL = mKaryawan::
        select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', 'tb_penjualan_langsung.pl_tgl', 'tb_penjualan_langsung.grand_total',
        'tb_penjualan_langsung.pl_no_faktur', 'tb_penjualan_langsung.cus_nama', 'tb_detail_penjualan_langsung.brg_kode',
        'tb_barang.brg_barcode', 'tb_detail_penjualan_langsung.qty', 'tb_detail_penjualan_langsung.harga_net',
        'tb_detail_penjualan_langsung.nama_barang', 'tb_piutang_pelanggan.pp_amount', 'tb_piutang_pelanggan.pp_sisa_amount')
        ->Join('tb_penjualan_langsung', 'tb_karyawan.kry_kode','=','tb_penjualan_langsung.pl_sales_person')
        ->leftJoin('tb_piutang_pelanggan', 'tb_penjualan_langsung.pl_no_faktur','=','tb_piutang_pelanggan.pp_no_faktur')
        ->Join('tb_detail_penjualan_langsung', 'tb_penjualan_langsung.pl_no_faktur','=','tb_detail_penjualan_langsung.pl_no_faktur')
        ->Join('tb_barang', 'tb_detail_penjualan_langsung.brg_kode','=','tb_barang.brg_kode')
        ->where('pl_tgl', '>=', $start)
        ->where('pl_tgl', '<=', $end)
        ->where('tb_barang.mrk_kode', '=', $mrk_kode)
        ->get();

        $rowPT = mKaryawan::
        select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', 'tb_penjualan_titipan.pt_tgl', 'tb_penjualan_titipan.grand_total',
        'tb_penjualan_titipan.pt_no_faktur', 'tb_penjualan_titipan.cus_nama', 'tb_detail_penjualan_titipan.brg_kode',
        'tb_barang.brg_barcode', 'tb_detail_penjualan_titipan.qty', 'tb_detail_penjualan_titipan.harga_net',
        'tb_detail_penjualan_titipan.nama_barang', 'tb_piutang_pelanggan.pp_amount', 'tb_piutang_pelanggan.pp_sisa_amount')
        ->Join('tb_penjualan_titipan', 'tb_karyawan.kry_kode','=','tb_penjualan_titipan.pt_sales_person')
        ->leftJoin('tb_piutang_pelanggan', 'tb_penjualan_titipan.pt_no_faktur','=','tb_piutang_pelanggan.pp_no_faktur')
        ->Join('tb_detail_penjualan_titipan', 'tb_penjualan_titipan.pt_no_faktur','=','tb_detail_penjualan_titipan.pt_no_faktur')
        ->Join('tb_barang', 'tb_detail_penjualan_titipan.brg_kode','=','tb_barang.brg_kode')
        ->where('pt_tgl', '>=', $start)
        ->where('pt_tgl', '<=', $end)
        ->where('tb_barang.mrk_kode', '=', $mrk_kode)
        ->get();
      }
      else {
        $rowPL = mKaryawan::
        select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', 'tb_penjualan_langsung.pl_tgl', 'tb_penjualan_langsung.grand_total',
        'tb_penjualan_langsung.pl_no_faktur', 'tb_penjualan_langsung.cus_nama', 'tb_detail_penjualan_langsung.brg_kode',
        'tb_barang.brg_barcode', 'tb_detail_penjualan_langsung.qty', 'tb_detail_penjualan_langsung.harga_net',
        'tb_detail_penjualan_langsung.nama_barang', 'tb_piutang_pelanggan.pp_amount', 'tb_piutang_pelanggan.pp_sisa_amount')
        ->Join('tb_penjualan_langsung', 'tb_karyawan.kry_kode','=','tb_penjualan_langsung.pl_sales_person')
        ->leftJoin('tb_piutang_pelanggan', 'tb_penjualan_langsung.pl_no_faktur','=','tb_piutang_pelanggan.pp_no_faktur')
        ->Join('tb_detail_penjualan_langsung', 'tb_penjualan_langsung.pl_no_faktur','=','tb_detail_penjualan_langsung.pl_no_faktur')
        ->Join('tb_barang', 'tb_detail_penjualan_langsung.brg_kode','=','tb_barang.brg_kode')
        ->where('pl_tgl', '>=', $start)
        ->where('pl_tgl', '<=', $end)
        ->get();

        $rowPT = mKaryawan::
        select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', 'tb_penjualan_titipan.pt_tgl', 'tb_penjualan_titipan.grand_total',
        'tb_penjualan_titipan.pt_no_faktur', 'tb_penjualan_titipan.cus_nama', 'tb_detail_penjualan_titipan.brg_kode',
        'tb_barang.brg_barcode', 'tb_detail_penjualan_titipan.qty', 'tb_detail_penjualan_titipan.harga_net',
        'tb_detail_penjualan_titipan.nama_barang', 'tb_piutang_pelanggan.pp_amount', 'tb_piutang_pelanggan.pp_sisa_amount')
        ->Join('tb_penjualan_titipan', 'tb_karyawan.kry_kode','=','tb_penjualan_titipan.pt_sales_person')
        ->leftJoin('tb_piutang_pelanggan', 'tb_penjualan_titipan.pt_no_faktur','=','tb_piutang_pelanggan.pp_no_faktur')
        ->Join('tb_detail_penjualan_titipan', 'tb_penjualan_titipan.pt_no_faktur','=','tb_detail_penjualan_titipan.pt_no_faktur')
        ->Join('tb_barang', 'tb_detail_penjualan_titipan.brg_kode','=','tb_barang.brg_kode')
        ->where('pt_tgl', '>=', $start)
        ->where('pt_tgl', '<=', $end)
        ->get();
      }

      $groupedPL = $rowPL->groupBy('kry_nama');
      $groupedPL->toArray();

      $groupedPT = $rowPT->groupBy('kry_nama');
      $groupedPT->toArray();

      // return $groupedPL;

      $data['start'] = $start;
      $data['end'] = $end;

      $data['dataPL'] = $groupedPL;
      $data['dataPT'] = $groupedPT;

      return view('laporan/laporan-omset-sales-detail', $data);

      // $pdf = PDF::loadView('laporan.laporan-omset-sales-detail', $data);
      // return $pdf->download('laporan-penjualan.pdf');

      $pdf = PDF::loadView('laporan.laporan-omset-sales-detail', $data)
                  ->setPaper('a4', 'landscape');
      return $pdf->stream();
  }

  function omset_sales_rekap($start='', $end='', $mrk_kode='') {
    $data['no'] = 1;
    $data['no_2'] = 1;

    $start = date('Y-m-d', strtotime($start));
    $end = date('Y-m-d', strtotime($end));
    $rekap_total_jualPL = 0;
    $rekap_total_jualPT = 0;

    if ($mrk_kode != 'all') {
      $rowPL = mKaryawan::
      select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', \DB::raw('sum(tb_detail_penjualan_langsung.total) as total_jual, sum(tb_detail_penjualan_langsung.qty) as qty_jual'))
      ->Join('tb_penjualan_langsung', 'tb_karyawan.kry_kode','=','tb_penjualan_langsung.pl_sales_person')
      ->Join('tb_detail_penjualan_langsung', 'tb_penjualan_langsung.pl_no_faktur','=','tb_detail_penjualan_langsung.pl_no_faktur')
      ->Join('tb_barang', 'tb_detail_penjualan_langsung.brg_kode','=','tb_barang.brg_kode')
      ->where('pl_tgl', '>=', $start)
      ->where('pl_tgl', '<=', $end)
      ->where('tb_barang.mrk_kode', '=', $mrk_kode)
      ->groupBy('kry_kode')
      ->orderBy('kry_kode')
      ->get();

      $rowPT = mKaryawan::
      select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', \DB::raw('sum(tb_detail_penjualan_titipan.total) as total_jual, sum(tb_detail_penjualan_titipan.qty) as qty_jual'))
      ->Join('tb_penjualan_titipan', 'tb_karyawan.kry_kode','=','tb_penjualan_titipan.pt_sales_person')
      ->Join('tb_detail_penjualan_titipan', 'tb_penjualan_titipan.pt_no_faktur','=','tb_detail_penjualan_titipan.pt_no_faktur')
      ->Join('tb_barang', 'tb_detail_penjualan_titipan.brg_kode','=','tb_barang.brg_kode')
      ->where('pt_tgl', '>=', $start)
      ->where('pt_tgl', '<=', $end)
      ->where('pt_tgl', '<=', $end)
      ->where('tb_barang.mrk_kode', '=', $mrk_kode)
      ->groupBy('kry_kode')
      ->orderBy('kry_kode')
      ->get();
    }
    else {
      $rowPL = mKaryawan::
      select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', \DB::raw('sum(tb_detail_penjualan_langsung.total) as total_jual, sum(tb_detail_penjualan_langsung.qty) as qty_jual'))
      ->Join('tb_penjualan_langsung', 'tb_karyawan.kry_kode','=','tb_penjualan_langsung.pl_sales_person')
      ->Join('tb_detail_penjualan_langsung', 'tb_penjualan_langsung.pl_no_faktur','=','tb_detail_penjualan_langsung.pl_no_faktur')
      ->where('pl_tgl', '>=', $start)
      ->where('pl_tgl', '<=', $end)
      ->groupBy('kry_kode')
      ->orderBy('kry_kode')
      ->get();

      $rowPT = mKaryawan::
      select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', \DB::raw('sum(tb_detail_penjualan_titipan.total) as total_jual, sum(tb_detail_penjualan_titipan.qty) as qty_jual'))
      ->Join('tb_penjualan_titipan', 'tb_karyawan.kry_kode','=','tb_penjualan_titipan.pt_sales_person')
      ->Join('tb_detail_penjualan_titipan', 'tb_penjualan_titipan.pt_no_faktur','=','tb_detail_penjualan_titipan.pt_no_faktur')
      ->where('pt_tgl', '>=', $start)
      ->where('pt_tgl', '<=', $end)
      ->groupBy('kry_kode')
      ->orderBy('kry_kode')
      ->get();
    }

    foreach ($rowPL as $key => $value) {
      $rekap_total_jualPL += $value->total_jual;
    }

    foreach ($rowPT as $key => $value) {
      $rekap_total_jualPT += $value->total_jual;
    }

    $data['rekapTJPL'] = $rekap_total_jualPL;
    $data['rekapTJPT'] = $rekap_total_jualPT;

    $data['start'] = $start;
    $data['end'] = $end;

    $data['dataPL'] = $rowPL;
    $data['dataPT'] = $rowPT;

    // return $data;

    return view('laporan/laporan-omset-sales-rekap', $data);

    // $pdf = PDF::loadView('laporan.laporan-omset-sales-rekap', $data);
    // return $pdf->download('laporan-penjualan.pdf');

    $pdf = PDF::loadView('laporan.laporan-omset-sales-rekap', $data)
                ->setPaper('a4', 'potrait');
    return $pdf->stream();
  }

  function salesRangePL(Request $request) {
    $date = $this->request_date($request);
    $start = date('Y-m-d', strtotime($date['start']));
    $end = date('Y-m-d', strtotime($date['end']));
    $mrk_kode = $request->mrk_kode;

    if ($mrk_kode != 'all') {
      $rowPL = mKaryawan::
      select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama',
      \DB::raw('sum(tb_detail_penjualan_langsung.total) as total_jual,
      sum(tb_detail_penjualan_langsung.qty) as qty_jual,
      sum(tb_detail_retur_penjualan.total_retur) as total_retur,
      sum(tb_detail_retur_penjualan.qty_retur) as qty_retur'))
      ->Join('tb_penjualan_langsung', 'tb_karyawan.kry_kode','=','tb_penjualan_langsung.pl_sales_person')
      ->Join('tb_detail_penjualan_langsung', 'tb_penjualan_langsung.pl_no_faktur','=','tb_detail_penjualan_langsung.pl_no_faktur')
      ->Join('tb_barang', 'tb_detail_penjualan_langsung.brg_kode','=','tb_barang.brg_kode')
      ->leftJoin('tb_retur_penjualan', 'tb_penjualan_langsung.pl_no_faktur','=','tb_retur_penjualan.no_faktur')
      ->leftJoin('tb_detail_retur_penjualan', 'tb_retur_penjualan.no_retur_penjualan','=','tb_detail_retur_penjualan.no_retur_penjualan')
      ->where('pl_tgl', '>=', $start)
      ->where('pl_tgl', '<=', $end)
      ->where('tb_barang.mrk_kode', '=', $mrk_kode)
      ->groupBy('kry_kode')
      ->orderBy('kry_kode')
      ->get();

      $rowPT = mKaryawan::
      select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', \DB::raw('sum(tb_detail_penjualan_titipan.total) as total_jual,
      sum(tb_detail_penjualan_titipan.qty) as qty_jual,
      sum(tb_detail_retur_penjualan.total_retur) as total_retur,
      sum(tb_detail_retur_penjualan.qty_retur) as qty_retur'))
      ->Join('tb_penjualan_titipan', 'tb_karyawan.kry_kode','=','tb_penjualan_titipan.pt_sales_person')
      ->Join('tb_detail_penjualan_titipan', 'tb_penjualan_titipan.pt_no_faktur','=','tb_detail_penjualan_titipan.pt_no_faktur')
      ->Join('tb_barang', 'tb_detail_penjualan_titipan.brg_kode','=','tb_barang.brg_kode')
      ->leftJoin('tb_retur_penjualan', 'tb_penjualan_titipan.pt_no_faktur','=','tb_retur_penjualan.no_faktur')
      ->leftJoin('tb_detail_retur_penjualan', 'tb_retur_penjualan.no_retur_penjualan','=','tb_detail_retur_penjualan.no_retur_penjualan')
      ->where('pt_tgl', '>=', $start)
      ->where('pt_tgl', '<=', $end)
      ->where('tb_barang.mrk_kode', '=', $mrk_kode)
      ->groupBy('kry_kode')
      ->orderBy('kry_kode')
      ->get();
    }
    else {
      $rowPL = mKaryawan::
      select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama',
      \DB::raw('sum(tb_detail_penjualan_langsung.total) as total_jual,
      sum(tb_detail_penjualan_langsung.qty) as qty_jual,
      sum(tb_detail_retur_penjualan.total_retur) as total_retur,
      sum(tb_detail_retur_penjualan.qty_retur) as qty_retur'))
      ->Join('tb_penjualan_langsung', 'tb_karyawan.kry_kode','=','tb_penjualan_langsung.pl_sales_person')
      ->Join('tb_detail_penjualan_langsung', 'tb_penjualan_langsung.pl_no_faktur','=','tb_detail_penjualan_langsung.pl_no_faktur')
      ->Join('tb_barang', 'tb_detail_penjualan_langsung.brg_kode','=','tb_barang.brg_kode')
      ->leftJoin('tb_retur_penjualan', 'tb_penjualan_langsung.pl_no_faktur','=','tb_retur_penjualan.no_faktur')
      ->leftJoin('tb_detail_retur_penjualan', 'tb_retur_penjualan.no_retur_penjualan','=','tb_detail_retur_penjualan.no_retur_penjualan')
      ->where('pl_tgl', '>=', $start)
      ->where('pl_tgl', '<=', $end)
      ->groupBy('kry_kode')
      ->orderBy('kry_kode')
      ->get();

      $rowPT = mKaryawan::
      select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', \DB::raw('sum(tb_detail_penjualan_titipan.total) as total_jual,
      sum(tb_detail_penjualan_titipan.qty) as qty_jual,
      sum(tb_detail_retur_penjualan.total_retur) as total_retur,
      sum(tb_detail_retur_penjualan.qty_retur) as qty_retur'))
      ->Join('tb_penjualan_titipan', 'tb_karyawan.kry_kode','=','tb_penjualan_titipan.pt_sales_person')
      ->Join('tb_detail_penjualan_titipan', 'tb_penjualan_titipan.pt_no_faktur','=','tb_detail_penjualan_titipan.pt_no_faktur')
      ->Join('tb_barang', 'tb_detail_penjualan_titipan.brg_kode','=','tb_barang.brg_kode')
      ->leftJoin('tb_retur_penjualan', 'tb_penjualan_titipan.pt_no_faktur','=','tb_retur_penjualan.no_faktur')
      ->leftJoin('tb_detail_retur_penjualan', 'tb_retur_penjualan.no_retur_penjualan','=','tb_detail_retur_penjualan.no_retur_penjualan')
      ->where('pt_tgl', '>=', $start)
      ->where('pt_tgl', '<=', $end)
      ->groupBy('kry_kode')
      ->orderBy('kry_kode')
      ->get();
    }


    $merged = $rowPL->concat($rowPT);
    $merged->all();
    $data = [];
    $merged->each(function ($item, $key) use(&$data){
      if (isset($data[$item['kry_kode']])) {
          $data[$item['kry_kode']]['total_jual'] += $item['total_jual'];
          $data[$item['kry_kode']]['qty_jual'] += $item['qty_jual'];
          $data[$item['kry_kode']]['total_retur'] += $item['total_retur'];
          $data[$item['kry_kode']]['qty_retur'] += $item['qty_retur'];
      } else {
          $data[$item['kry_kode']] = $item;
      }
    });

    $row = [];
    foreach ($data as $key => $value) {
      $row[] = $value;
    }
    return $row;
  }

  function salesRangePT(Request $request) {
    $date = $this->request_date($request);
    $start = date('Y-m-d', strtotime($date['start']));
    $end = date('Y-m-d', strtotime($date['end']));

    $row = mKaryawan::
    select('tb_karyawan.kry_kode', 'tb_karyawan.kry_nama', \DB::raw('sum(tb_detail_penjualan_titipan.total) as total_jual, sum(tb_detail_penjualan_titipan.qty) as qty_jual'))
    ->Join('tb_penjualan_titipan', 'tb_karyawan.kry_kode','=','tb_penjualan_titipan.pt_sales_person')
    ->Join('tb_detail_penjualan_titipan', 'tb_penjualan_titipan.pt_no_faktur','=','tb_detail_penjualan_titipan.pt_no_faktur')
    ->where('pt_tgl', '>=', $start)
    ->where('pt_tgl', '<=', $end)
    ->groupBy('kry_kode')
    ->orderBy('kry_kode')
    ->get();

    // $row = mKaryawan::whereHas('penjualanPT', function($q) use($start, $end){
    //   $q->where('pt_tgl', '>=', $start)->where('pt_tgl', '<=', $end);
    // })->Select('kry_kode', 'kry_nama')->get();
    // $rekap_total_jual=0;
    // foreach ($row as $key) {
    //   $total_jual=0;
    //   $qty_jual=0;
    //   $penjualan = $key->penjualanPT;
    //   $key['penjualan'] = $key->penjualanPT;
    //   foreach ($penjualan as $keyPen) {
    //     $keyPen->customer;
    //     $keyPen['detail'] = $keyPen->detail_PT;
    //     foreach ($keyPen['detail'] as $Det) {
    //       $total_jual+= $Det->total;
    //       $qty_jual+= $Det->qty;
    //     }
    //     unset($keyPen->piutangPelanggan);
    //     unset($keyPen->detail_PT);
    //     unset($key->penjualan);
    //   }
    //   unset($key->penjualanPT);
    //   $key['total_jual'] = $total_jual;
    //   $key['qty_jual'] = $qty_jual;
    // }
    return $row;
  }

  function PenjualanStok(Request $request) {
    // return $request;

    if ($request->report == 'excel') {
      $time = Carbon::now();
      return Excel::download(new PenjualanStokExport($request), 'penjualan-stok-'.$time.'.xlsx');
    }
    else {
      // $data = $this->main->data([], $this->kodeLabel);
      // $kodeBarang = $this->main->kodeLabel('barang');
      // $kodeFaktur = $this->main->kodeLabel('penjualanLangsung');
      $data['no'] = 1;
      $data['no_2'] = 1;
      $data['no_3'] = 1;
      $data['no_4'] = 1;

      $start = date('Y-m-d', strtotime($request->start_date));
      $end = date('Y-m-d', strtotime($request->end_date));

      $rowQB = mPenjualanLangsung::query();
      // if ($request->ppn != 'all') {
      //   $rowQB->whereHas('detail_PL', function ($query) use($request) {
      //     $query->where('ppn', $request->ppn);
      //   });
      // }

      if ($request->ktg_kode != 0) {
        $rowQB->whereHas('detail_PL.barang', function ($query) use($request) {
          $query->where('ktg_kode', $request->ktg_kode);
        });
      }
      if ($request->grp_kode != 0) {
        $rowQB->whereHas('detail_PL.barang', function ($query) use($request) {
          $query->where('grp_kode', $request->grp_kode);
        });
      }
      if ($request->mrk_kode != 0) {
        $rowQB->whereHas('detail_PL.barang', function ($query) use($request) {
          $query->where('mrk_kode', $request->mrk_kode);
        });
      }
      if ($request->spl_kode != 0) {
        $rowQB->whereHas('detail_PL.barang.stok', function ($query) use($request) {
          $query->where('spl_kode', $request->spl_kode);
        });
      }

      // $rowPL =  $rowQB->where('pl_tgl', '>=', $start)->where('pl_tgl', '<=', $end)->orderBy('cus_kode')->get();
      $rowPL =  $rowQB
      ->select('tb_penjualan_langsung.pl_no_faktur', 'pl_tgl', 'cus_nama', 'nama_barang',
       'qty', 'harga_net', 'harga_jual', 'tb_detail_penjualan_langsung.disc_nom',
       'tb_detail_penjualan_langsung.ppn_nom', 'tb_detail_penjualan_langsung.ppn',
       'tb_detail_penjualan_langsung.brg_hpp', 'tb_detail_penjualan_langsung.total',
       'satuan', 'mrk_kode', 'grp_kode', 'spl_kode')
      ->leftJoin('tb_detail_penjualan_langsung', 'tb_penjualan_langsung.pl_no_faktur','=','tb_detail_penjualan_langsung.pl_no_faktur')
      ->where('pl_tgl', '>=', $start)
      ->where('pl_tgl', '<=', $end)
      ->orderBy('pl_tgl')
      ->get();

      if ($request->ktg_kode != 0) {
        $rowPL = $rowPL->where('ktg_kode', $request->ktg_kode);
      }
      if ($request->grp_kode != 0) {
        $rowPL = $rowPL->where('grp_kode', $request->grp_kode);
      }
      if ($request->mrk_kode != 0) {
        $rowPL = $rowPL->where('mrk_kode', $request->mrk_kode);
      }
      if ($request->spl_kode != 0) {
        $rowPL = $rowPL->where('spl_kode', $request->spl_kode);
      }
      if ($request->ppn != 0) {
        $rowPL = $rowPL->where('ppn', $request->ppn);
      }

      $sub_total=0;
      $grand_total=0;
      $total_dic=0;
      $total_komisi=0;
      $hpp=0;
      foreach ($rowPL as &$key) {
        // $key->customer;
        // $key->karyawan;
        // $key['detail'] = $key->detail_PL;
        // foreach ($key['detail'] as $keyDet) {
          // $barangData = $keyDet->barang;
          // $barangData->satuan;
          // $barangData->kategoryProduct;
          // $barangData->groupStok;
          // $barangData->merek;
          // $stok = $barangData->stok;
          // foreach ($stok as $keyStok) {
          //   $keyStok->supplier;
          // }
          // $sub_total += $keyDet->total;
          // $total_dic += $keyDet->disc_nom;
          // $hpp += $keyDet->brg_hpp;
        // }
        // unset($key->detail_PL);

        $sub_total += $key->total;
        $total_dic += $key->disc_nom;
        $hpp += $key->brg_hpp;
      }
      $grand_total = $sub_total - $total_dic;

      $data['sub_total'] = $sub_total;
      $data['grand_total'] = $grand_total;
      $data['total_komisi'] = $total_komisi;
      $data['HPP'] = $hpp;

      $rowQBPT = mPenjualanTitipan::query();
      // if ($request->ppn != 'all') {
      //   $rowQBPT->whereHas('detail_PT', function ($query) use($request) {
      //     $query->where('ppn', $request->ppn);
      //   });
      // }

      if ($request->ktg_kode != 0) {
        $rowQBPT->whereHas('detail_PT.barang', function ($query) use($request) {
          $query->where('ktg_kode', $request->ktg_kode);
        });
      }
      if ($request->grp_kode != 0) {
        $rowQBPT->whereHas('detail_PT.barang', function ($query) use($request) {
          $query->where('grp_kode', $request->grp_kode);
        });
      }
      if ($request->mrk_kode != 0) {
        $rowQBPT->whereHas('detail_PT.barang', function ($query) use($request) {
          $query->where('mrk_kode', $request->mrk_kode);
        });
      }
      if ($request->spl_kode != 0) {
        $rowQBPT->whereHas('detail_PT.barang.stok', function ($query) use($request) {
          $query->where('spl_kode', $request->spl_kode);
        });
      }

      // $rowPT =  $rowQBPT->where('pt_tgl', '>=', $start)->where('pt_tgl', '<=', $end)->orderBy('cus_kode')->get();

      $rowPT =  $rowQBPT
      ->select('tb_penjualan_titipan.pt_no_faktur', 'pt_tgl', 'cus_nama', 'nama_barang',
       'qty', 'harga_net', 'harga_jual', 'tb_detail_penjualan_titipan.disc_nom',
       'tb_detail_penjualan_titipan.ppn_nom', 'tb_detail_penjualan_titipan.ppn',
       'tb_detail_penjualan_titipan.brg_hpp', 'tb_detail_penjualan_titipan.total',
       'satuan', 'mrk_kode', 'grp_kode', 'spl_kode')
      ->leftJoin('tb_detail_penjualan_titipan', 'tb_penjualan_titipan.pt_no_faktur','=','tb_detail_penjualan_titipan.pt_no_faktur')
      ->where('pt_tgl', '>=', $start)
      ->where('pt_tgl', '<=', $end)
      ->orderBy('pt_tgl')
      ->get();

      if ($request->ktg_kode != 0) {
        $rowPT = $rowPT->where('ktg_kode', $request->ktg_kode);
      }
      if ($request->grp_kode != 0) {
        $rowPT = $rowPT->where('grp_kode', $request->grp_kode);
      }
      if ($request->mrk_kode != 0) {
        $rowPT = $rowPT->where('mrk_kode', $request->mrk_kode);
      }
      if ($request->spl_kode != 0) {
        $rowPT = $rowPT->where('spl_kode', $request->spl_kode);
      }
      if ($request->ppn != 0) {
        $rowPT = $rowPT->where('ppn', $request->ppn);
      }

      $sub_totalPT=0;
      $grand_totalPT=0;
      $total_dicPT=0;
      $total_komisiPT=0;
      $hppPT=0;
      foreach ($rowPT as &$key) {
        // $key->customer;
        // $key->karyawan;
        // $key['detail'] = $key->detail_PT;
        // foreach ($key['detail'] as $keyDet) {
          // $barangData = $keyDet->barang;
          // $barangData->satuan;
          // $barangData->kategoryProduct;
          // $barangData->groupStok;
          // $barangData->merek;
          // $stok = $barangData->stok;
          // foreach ($stok as $keyStok) {
          //   $keyStok->supplier;
          // }
          // $sub_totalPT += $keyDet->total;
          // $total_dicPT += $keyDet->disc_nom;
          // $hppPT += $keyDet->brg_hpp;
        // }
        // unset($key->detail_PT);

        $sub_totalPT += $key->total;
        $total_dicPT += $key->disc_nom;
        $hppPT += $key->brg_hpp;
      }
      $grand_totalPT = $sub_totalPT - $total_dicPT;

      $data['sub_totalPT'] = $sub_totalPT;
      $data['grand_totalPT'] = $grand_totalPT;
      $data['total_komisiPT'] = $total_komisiPT;
      $data['HPPPT'] = $hppPT;

      $data['dataPL'] = $rowPL;
      $data['dataPT'] = $rowPT;

      $data['start'] = $start;
      $data['end'] = $end;
      $data['type_ppn'] = $request->ppn;

      // $data['ktg'] = $request->ktg_kode;
      // $data['grp'] = $request->grp_kode;
      // $data['mrk'] = $request->mrk_kode;


      //Data Penjualan Langsung
      // $data['kode_faktur'] = $kodeFaktur;

      // return $data;

      return view('laporan/laporan-penjualan-all-stok', $data);

      $pdf = PDF::loadView('laporan.laporan-penjualan-all-stok', $data)
                  ->setPaper('a4', 'landscape');
      return $pdf->stream();
    }
  }

  function viewReturPenjualan(Request $request) {
    $date = $this->request_date($request);

    if ($request->report == 'print') {
      return [
        'redirect'=>route('returPenjualanCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
      ];
    }
    else {
      return [
        'redirect'=>route('PrintExcel.retur-penjualan', ['start'=>$date['start'], 'end'=>$date['end']]),
      ];
    }
  }

  function returPenjualan($start='', $end='') {
    $data['no'] = 1;
    $data['no_2'] = 1;
    // $data = $this->main->data([], $this->kodeLabel);
    // $kodeCustomer = $this->main->kodeLabel('customer');
    // $kodePenjualanLangsung = $this->main->kodeLabel('penjualanLangsung');
    // $kodePenjualanTitipan = $this->main->kodeLabel('penjualanTitipan');

    $start = date('Y-m-d', strtotime($start));
    $end = date('Y-m-d', strtotime($end));

    $row = mReturPenjualan::where('tgl_pengembalian', '>=', $start)->where('tgl_pengembalian', '<=', $end)->get();
    $grand_total=0;
    foreach ($row as $key => $value) {
      $span=1;
      $det = $value->detail;
      foreach ($det as $keyDet => $valueDet) {
        $value['span'] = $span++;
      }
      $grand_total+= $value->total_retur;
    }

    $data['data'] = $row;
    $data['grand_total'] = $grand_total;
    // $data['label_penjualan'] = $kodePenjualanLangsung;
    $data['start'] = $start;
    $data['end'] = $end;
    // return $data;

    return view('laporan/laporan-retur-penjualan', $data);

    $pdf = PDF::loadView('laporan.laporan-retur-penjualan', $data)
    ->setPaper('a4', 'potrait');
    return $pdf->stream();
  }

}
