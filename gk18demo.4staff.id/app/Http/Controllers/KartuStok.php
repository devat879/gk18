<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use Carbon\Carbon;
use App\Models\mBarang;
use App\Models\mStok;
use App\Models\mArusStok;
use App\Models\mGudang;

class KartuStok extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Kartu Stok';
      $this->kodeLabel = 'barang';
  }

  function index() {
      $breadcrumb = [
          'Stok Alert'=>route('kartuStokList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'kartu_stok';
      $data['title'] = $this->title;
      $data['dataList'] = mArusStok::leftJoin('tb_barang', 'tb_barang.brg_kode', '=', 'tb_arus_stok.brg_kode')
      ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
      ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
      ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
      ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
      ->leftJoin('tb_gudang', 'tb_arus_stok.gdg_kode', '=', 'tb_gudang.gdg_kode')
      // ->leftJoin('tb_stok', 'tb_barang.brg_kode', '=', 'tb_stok.brg_kode')
      ->select('ars_stok_kode', 'ars_stok_date', 'brg_barcode', 'tb_barang.brg_kode', 'tb_barang.brg_nama', 'stn_nama', 'ktg_nama', 'grp_nama', 'mrk_nama', 'keterangan', 'stok_in', 'stok_out', 'gdg_nama', 'stok_prev')
      // ->groupBy('ars_stok_date')
      // ->selectRaw('sum(stok_in) as stokIn')
      // ->selectRaw('sum(stok_out) as stokOut')
      ->orderBy('ars_stok_kode', 'desc')
      ->get();

      $data['gudang'] = mGudang::select('gdg_kode', 'gdg_nama')->orderBy('gdg_nama', 'asc')->get();
      $data['barang'] = mBarang::select('brg_kode', 'brg_nama')->orderBy('brg_nama', 'asc')->get();

      // foreach ($data['dataList'] as &$barang) {
      //   $total = 0;
      //   $barangStok =  $barang->stok;
      //   foreach ($barangStok as $stok) {
      //     $total += $stok->stok;
      //   }
      //   $barang['QOH'] = $total;
      // }

      // return $data['dataList'];
      return view('kartu-stok/kartuStokList', $data);
  }

  function range(Request $request) {
      // return $request;
      $date_start = $request->start_date;
      $date_end = $request->end_date;

      $time = Carbon::now();
      if ($request->start_date == null) {
        $start = $time;
      }
      if ($request->end_date == null) {
        $end = $time;
      }
      $start = date('Y-m-d', strtotime($date_start));
      $end = date('Y-m-d', strtotime($date_end));


      $data = mArusStok::leftJoin('tb_barang', 'tb_barang.brg_kode', '=', 'tb_arus_stok.brg_kode')
      ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
      ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
      ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
      ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
      ->leftJoin('tb_gudang', 'tb_arus_stok.gdg_kode', '=', 'tb_gudang.gdg_kode')
      // ->leftJoin('tb_stok', 'tb_barang.brg_kode', '=', 'tb_stok.brg_kode')
      ->select('ars_stok_kode', 'ars_stok_date', 'brg_barcode', 'tb_barang.brg_kode', 'tb_barang.brg_nama', 'stn_nama', 'ktg_nama', 'grp_nama', 'mrk_nama', 'keterangan', 'stok_in', 'stok_out', 'gdg_nama', 'stok_prev')
      ->where('ars_stok_date', '>=', $start)
      ->where('ars_stok_date', '<=', $end)
      // ->groupBy('ars_stok_date')
      // ->selectRaw('sum(stok_in) as stokIn')
      // ->selectRaw('sum(stok_out) as stokOut')
      ->orderBy('ars_stok_kode', 'desc')
      ->get();

      // if ($request->start_date == null && $request->end_date == null) {
      //   $data = mBarang::leftJoin('tb_arus_stok', 'tb_barang.brg_kode', '=', 'tb_arus_stok.brg_kode')
      //   ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
      //   ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
      //   ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
      //   ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
      //   ->select('ars_stok_date', 'brg_barcode', 'tb_barang.brg_kode', 'tb_barang.brg_nama', 'stn_nama', 'ktg_nama', 'grp_nama', 'mrk_nama')
      //   ->whereNotNull('ars_stok_date')
      //   ->groupBy('ars_stok_date')
      //   ->selectRaw('sum(stok_in) as stokIn')
      //   ->selectRaw('sum(stok_out) as stokOut')
      //   ->get();
      // }
      // elseif ($request->start_date == null || $request->end_date == null) {
      //   $time = Carbon::now();
      //   if ($request->start_date == null) {
      //     $start = $time;
      //   }
      //   else {
      //     $end = $time;
      //   }
      //   $data = mBarang::leftJoin('tb_arus_stok', 'tb_barang.brg_kode', '=', 'tb_arus_stok.brg_kode')
      //   ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
      //   ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
      //   ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
      //   ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
      //   ->select('ars_stok_date', 'brg_barcode', 'tb_barang.brg_kode', 'tb_barang.brg_nama', 'stn_nama', 'ktg_nama', 'grp_nama', 'mrk_nama')
      //   ->whereNotNull('ars_stok_date')
      //   ->whereBetween('ars_stok_date', [$start, $end])
      //   ->groupBy('ars_stok_date')
      //   ->selectRaw('sum(stok_in) as stokIn')
      //   ->selectRaw('sum(stok_out) as stokOut')
      //   ->get();
      // }
      // else {
      //   $data = mBarang::leftJoin('tb_arus_stok', 'tb_barang.brg_kode', '=', 'tb_arus_stok.brg_kode')
      //   ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
      //   ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
      //   ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
      //   ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
      //   ->select('ars_stok_date', 'brg_barcode', 'tb_barang.brg_kode', 'tb_barang.brg_nama', 'stn_nama', 'ktg_nama', 'grp_nama', 'mrk_nama')
      //   ->whereNotNull('ars_stok_date')
      //   ->whereBetween('ars_stok_date', [$start, $end])
      //   ->groupBy('ars_stok_date')
      //   ->selectRaw('sum(stok_in) as stokIn')
      //   ->selectRaw('sum(stok_out) as stokOut')
      //   ->get();
      // }

      // foreach ($data as &$barang) {
      //   $total = 0;
      //   $barangStok =  $barang->stok;
      //   foreach ($barangStok as $stok) {
      //     $total += $stok->stok;
      //   }
      //   $barang['QOH'] = $total;
      // }

      return $data;
  }

  function viewKartuStok(Request $request) {
    $date_start = $request->start_date;
    $date_end = $request->end_date;
    $gudang = $request->gudang;
    $barang = $request->barang;

    $time = Carbon::now();
    if ($request->start_date == null) {
      $start = $time;
    }
    if ($request->end_date == null) {
      $end = $time;
    }
    $start = date('Y-m-d', strtotime($date_start));
    $end = date('Y-m-d', strtotime($date_end));

    if ($request->report == 'print') {
      return [
        'redirect'=>route('kartuStokCetak', ['start'=>$start, 'end'=>$end, 'gudang'=>$gudang, 'brg_kode'=>$barang]),
      ];
    }
    else {
      return [
        'redirect'=>route('PrintExcel.kartu-stok', ['start'=>$start, 'end'=>$end, 'gudang'=>$gudang, 'brg_kode'=>$barang]),
      ];
    }
  }

  function kartu_stok_cetak($start='', $end='', $gudang='', $brg_kode='') {
    $data['no'] = 1;
    $data['no_2'] = 1;

    $data['start'] = $start;
    $data['end'] = $end;

    if ($gudang != 'all') {
      $list = mArusStok::leftJoin('tb_barang', 'tb_barang.brg_kode', '=', 'tb_arus_stok.brg_kode')
      ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
      ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
      ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
      ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
      ->leftJoin('tb_gudang', 'tb_arus_stok.gdg_kode', '=', 'tb_gudang.gdg_kode')
      ->select('ars_stok_kode', 'ars_stok_date', 'brg_barcode', 'tb_barang.brg_kode', 'tb_barang.brg_nama', 'stn_nama', 'ktg_nama', 'grp_nama', 'mrk_nama', 'keterangan', 'stok_in', 'stok_out', 'gdg_nama', 'stok_prev')
      ->where('tb_arus_stok.gdg_kode', $gudang)
      ->where('ars_stok_date', '>=', $start)
      ->where('ars_stok_date', '<=', $end)
      ->orderBy('ars_stok_kode', 'asc')
      ->get();
    }
    else {
      $list = mArusStok::leftJoin('tb_barang', 'tb_barang.brg_kode', '=', 'tb_arus_stok.brg_kode')
      ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
      ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
      ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
      ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
      ->leftJoin('tb_gudang', 'tb_arus_stok.gdg_kode', '=', 'tb_gudang.gdg_kode')
      ->select('ars_stok_kode', 'ars_stok_date', 'brg_barcode', 'tb_barang.brg_kode', 'tb_barang.brg_nama', 'stn_nama', 'ktg_nama', 'grp_nama', 'mrk_nama', 'keterangan', 'stok_in', 'stok_out', 'gdg_nama', 'stok_prev')
      ->where('ars_stok_date', '>=', $start)
      ->where('ars_stok_date', '<=', $end)
      ->orderBy('ars_stok_kode', 'asc')
      ->get();
    }

    if ($brg_kode != 'all') {
      $data['dataList'] = $list->filter(function ($value, $key) use($brg_kode){
        return $value->brg_kode == $brg_kode;
      });

      $data['dataList']->all();
    }
    else {
      $data['dataList'] = $list;
    }


    return view('laporan.kartu-stok', $data);
  }
}
