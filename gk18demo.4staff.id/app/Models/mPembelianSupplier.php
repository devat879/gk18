<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mPembelianSupplier extends Model
{
  public $incrementing = false;
  protected $table = 'tb_pembelian_supplier';
  protected $primaryKey = 'ps_no_faktur';
  public $timestamps = false;
  protected $guarded=[];

  // protected $fillable=[
  //   'ps_no_faktur',
  //   'no_pembelian',
  //   'pos_no_po',
  //   'ps_tgl',
  //   'spl_kode',
  //   'ps_catatan',
  //   'ps_subtotal',
  //   'ps_disc',
  //   'ps_disc_nom',
  //   'ps_ppn',
  //   'ps_ppn_nom',
  //   'biaya_lain',
  //   'grand_total',
  //   'no_invoice',
  //   'tipe'
  // ];

  public function supplier()
  {
    return $this->belongsTo('App\Models\mSupplier', 'spl_id','spl_kode');
  }

  public function hutangSupplier()
  {
    return $this->hasOne('App\Models\mHutangSuplier', 'ps_no_faktur','no_pembelian');
  }

  public function detailPembelian()
  {
    return $this->hasMany(mDetailPembelianSupplier::class,'ps_no_faktur','no_pembelian');
  }

  public function woBarang()
  {
    return $this->belongsTo(mWoSupplier::class,'pos_no_po','no_wo');
  }
}
