<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mSatuan;

use Validator;

class Satuan extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Satuan';
      $this->kodeLabel = 'satuan';
  }

  function index() {
      $breadcrumb = [
          'Satuan'=>route('satuanList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'satuan';
      $data['title'] = $this->title;
      $data['dataList'] = mSatuan::all();
      return view('satuan/satuanList', $data);
  }

  function rules($request) {
      $rules = [
          'stn_nama' => 'required',
          'stn_description' => 'required',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
      $this->rules($request->all());
      mSatuan::insert($request->except('_token'));
      return [
          'redirect'=>route('satuanList')
      ];
  }

  function edit($stn_kode='') {
      $response = [
          'action'=>route('satuanUpdate', ['kode'=>$stn_kode]),
          'field'=>mSatuan::find($stn_kode)
      ];
      return $response;
  }

  function update(Request $request, $stn_kode) {
      $this->rules($request->all());
      mSatuan::where('stn_kode', $stn_kode)->update($request->except('_token'));
      return [
          'redirect'=>route('satuanList')
      ];
  }

  function delete($stn_kode) {
      mSatuan::where('stn_kode', $stn_kode)->delete();
  }
}
