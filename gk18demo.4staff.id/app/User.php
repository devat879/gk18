<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    // public $incrementing = false;
    // protected $table = 'tb_user';
    // protected $primaryKey = 'user_kode';
    // public $timestamps = false;
    // protected $fillable = [];

    public function Karyawan()
    {
      return $this->belongsTo('App\Models\mKaryawan', 'kry_kode');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
