<html moznomarginboxes mozdisallowselectionprint>
    <head>
    <title>Penyusutan Asset <?php echo e($thn); ?></title>
    <!-- <link href="<?php echo e(public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" /> -->
     <!-- <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/global/plugins/bootstrap/css/bootstrap.min.css')); ?>"> -->
    </head>
    <body>
    <style type="text/css">
                .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
                .tg td{font-family:Arial;font-size:10px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
                .tg th{font-family:Arial;font-size:12px;font-weight:normal;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
                .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="text-center">
                <h2><center><strong>Laporan Penyusutan Asset</strong></center></h2>
                        <h3><center><strong>Tahun <?php echo e($thn); ?></strong></center></h3>
          </div>
          <br>
          <div class="portlet light ">
          <table class="tg">
                                    <thead>
                                        <tr class="success">
                                            <th rowspan="2" align="center"> No </th>
                                            <th rowspan="2"> Keterangan</th>
                                            <th rowspan="2" width="5%"><center> Jumlah </center></th>
                                            <th rowspan="2" width="5%"> Thn Perolehan </th>
                                            <th rowspan="2" width="5%"> Nilai Perolehan </th>
                                            <th rowspan="2" width="5%">  Nilai Penyusutan </th>
                                            <th colspan="13"><center> Penyusutan <?php echo e($thn); ?></center></th>
                                            <th rowspan="2" width="5%"> Akumulasi Penyusutan </th>
                                            <th rowspan="2" width="5%"> Nilai Buku </th>
                                        </tr>
                                        <tr class="info">
                                            <th>Thn Lalu</th>
                                            <th>Jan</th>
                                            <th>Feb</th>
                                            <th>Mar</th>
                                            <th>Apr</th>
                                            <th>Mei</th>
                                            <th>Jun</th>
                                            <th>Jul</th>
                                            <th>Ags</th>
                                            <th>Sep</th>
                                            <th>Okt</th>
                                            <th>Nov</th>
                                            <th>Des</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $kategoriAsset; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ktgAsset): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($no++); ?></td>
                                            <td colspan="20"><?php echo e($ktgAsset->ka_nama); ?></td>
                                        </tr>
                                        <?php $__currentLoopData = $ktgAsset->Asset; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $asset): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td></td>
                                            <td><?php echo e($asset->nama); ?></td>
                                            <td><center><?php echo e($asset->qty); ?></center></td>
                                            <td><?php echo e(date('M Y', strtotime($asset->tanggal_beli))); ?></td>
                                            <td><?php echo e(number_format($asset->harga_beli)); ?></td>
                                            <td> <?php echo e(number_format($asset->beban_perbulan)); ?> </td>
                                            <td><?php echo e(number_format($asset->penyusutanAsset->where('tahun', '<',$thn)->sum('penyusutan_perbulan'),2)); ?></td>
                                            <?php for($i=1;$i<=12;$i++): ?>
                                            <td><?php echo e(number_format($asset->penyusutanAsset->where('bulan', '=',$i)->where('tahun', '=',$thn)->sum('penyusutan_perbulan'),2)); ?></td>
                                            <?php endfor; ?>
                                            
                                            <td> <?php echo e(number_format($asset->akumulasi_beban)); ?> </td>
                                            <td> <?php echo e(number_format($asset->nilai_buku)); ?> </td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table> 
          </div>
        </div>
      </div>
    </div>
    <script>
		window.print();
	</script>
  </body>
</html>
