<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/global/plugins/bootstrap/css/bootstrap.min.css')); ?>">
    <style media="screen">
    .float{
      position:fixed;
      width:60px;
      height:60px;
      bottom:40px;
      right:40px;
      border-radius:50px;
      text-align:center;
      box-shadow: 2px 2px 3px #999;
      z-index: 100000;
    }
    .my-float{
      margin-top:22px;
    }
    </style>

    <script>
    function printDiv(divName){
      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
    }
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="glyphicon glyphicon-print"></i></button>
    </div>
    <div class="container-fluid" id='printMe'>
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4 >Laporan Detail Omset Sales</h4>
            <h4>Tanggal <?php echo e($start); ?> S/D <?php echo e($end); ?></h4>
          </div>
          <br>
          <h4 >Penjualan Langsung</h4>
          <div class="portlet light ">
            <?php $__currentLoopData = $dataPL; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <table class="table table-bordered table-header-fixed">
              <thead>
                <tr>
                  <td colspan="10"><h5><b>Sales : <?php echo e($row->kry_nama); ?></b></h5></td>
                </tr>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">No. Faktur</th>
                  <th style="font-size:12px">Barang Barkode</th>
                  <th style="font-size:12px">Nama Barang</th>
                  <th style="font-size:12px">Pelanggan</th>
                  <th style="font-size:12px">QTY Jual</th>
                  <th style="font-size:12px">Rp. Jual</th>
                  
                  
                  
                  
                  
                  <th style="font-size:12px">Terbayar</th>
                  <th style="font-size:12px">Sisa Bayar</th>
                  
                </tr>
              </thead>
                <tbody>
                  <?php $__currentLoopData = $row->penjualanPL; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php
                      $no_2 = 0;
                    ?>
                    <?php $__currentLoopData = $key->detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $keyDet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <tr>
                        <?php if($no_2 < 1): ?>
                          <td rowspan="<?php echo e($key->span); ?>" style="font-size:12px; vertical-align: middle; text-align: center;"> <?php echo e($no++); ?>. </td>
                          <td rowspan="<?php echo e($key->span); ?>" style="font-size:12px; vertical-align: middle; text-align: center;"> <?php echo e(date('Y-m-d', strtotime($key->pl_tgl))); ?> </td>
                          <td rowspan="<?php echo e($key->span); ?>" style="font-size:12px; vertical-align: middle; text-align: center;"> <?php echo e($key->pl_no_faktur); ?> </td>
                          
                        <?php endif; ?>
                        <td style="font-size:12px"> <?php echo e($keyDet->barang['brg_barcode']); ?> </td>
                        <td style="font-size:12px"> <?php echo e($keyDet->barang['brg_nama']); ?> </td>
                        <td style="font-size:12px"> <?php echo e($key->customer['cus_nama']); ?> </td>
                        
                        <td style="font-size:12px" align="right"> <?php echo e(number_format($keyDet->qty, 2, "." ,",")); ?> </td>
                        
                        <td style="font-size:12px" align="right"> <?php echo e(number_format($keyDet->total, 2, "." ,",")); ?> </td>
                        <?php if($no_2 < 1): ?>
                          <td rowspan="<?php echo e($key->span); ?>" style="font-size:12px; vertical-align: middle; text-align: center;"> <?php echo e(number_format($key->terbayar, 0, "." ,".")); ?> </td>
                          <td rowspan="<?php echo e($key->span); ?>" style="font-size:12px; vertical-align: middle; text-align: center;"> <?php echo e(number_format($key->pp_amount, 0, "." ,".")); ?> </td>
                        <?php endif; ?>
                      </tr>
                      <?php
                        $no_2++;
                      ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <tr>
                    <td colspan="7" align="right" style="font-weight:bold">Total Total Omset Sales : </td>
                    <td style="font-weight:bold" align="right"><?php echo e(number_format($row->rekapTJ, 2, "." ,",")); ?></td>
                    <td style="font-weight:bold" align="right"><?php echo e(number_format($row->rekapTB, 2, "." ,",")); ?></td>
                    <td style="font-weight:bold" align="right"><?php echo e(number_format($row->rekapSisa, 2, "." ,",")); ?></td>
                  </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
          <br>
          <h4 >Penjualan Titipan</h4>
          <div class="portlet light ">
            <?php $__currentLoopData = $dataPT; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <table class="table table-bordered table-header-fixed">
              <tr>
                <td colspan="10"><h5><b>Sales : <?php echo e($row->kry_nama); ?></b></h5></td>
              </tr>
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">No. Faktur</th>
                  <th style="font-size:12px">Barang Barkode</th>
                  <th style="font-size:12px">Nama Barang</th>
                  <th style="font-size:12px">Pelanggan</th>
                  <th style="font-size:12px">QTY Jual</th>
                  <th style="font-size:12px">Rp. Jual</th>
                  
                  
                  
                  
                  
                  <th style="font-size:12px">Terbayar</th>
                  <th style="font-size:12px">Sisa Bayar</th>
                  
                </tr>
              </thead>
                <tbody>
                  <?php $__currentLoopData = $row->penjualanPT; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php
                      $no_4 = 0;
                    ?>
                    <?php $__currentLoopData = $key->detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $keyDet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <tr>
                        <?php if($no_4 < 1): ?>
                          <td rowspan="<?php echo e($key->span); ?>" style="font-size:12px; vertical-align: middle; text-align: center;"> <?php echo e($no_3++); ?>. </td>
                          <td rowspan="<?php echo e($key->span); ?>" style="font-size:12px; vertical-align: middle; text-align: center;"> <?php echo e(date('Y-m-d', strtotime($key->pt_tgl))); ?> </td>
                          <td rowspan="<?php echo e($key->span); ?>" style="font-size:12px; vertical-align: middle; text-align: center;"> <?php echo e($key->pt_no_faktur); ?> </td>
                          
                        <?php endif; ?>
                        <td style="font-size:12px"> <?php echo e($keyDet->barang['brg_barcode']); ?> </td>
                        <td style="font-size:12px"> <?php echo e($keyDet->barang['brg_nama']); ?> </td>
                        <td style="font-size:12px"> <?php echo e($key->customer['cus_nama']); ?> </td>
                        
                        <td style="font-size:12px" align="right"> <?php echo e(number_format($keyDet->qty, 2, "." ,",")); ?> </td>
                        
                        <td style="font-size:12px" align="right"> <?php echo e(number_format($keyDet->total, 2, "." ,",")); ?> </td>
                        <?php if($no_4 < 1): ?>
                          <td rowspan="<?php echo e($key->span); ?>" style="font-size:12px; vertical-align: middle; text-align: center;"> <?php echo e(number_format($key->terbayar, 0, "." ,".")); ?> </td>
                          <td rowspan="<?php echo e($key->span); ?>" style="font-size:12px; vertical-align: middle; text-align: center;"> <?php echo e(number_format($key->pp_amount, 0, "." ,".")); ?> </td>
                        <?php endif; ?>
                      </tr>
                      <?php
                        $no_4++;
                      ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <tr>
                    <td colspan="7" align="right" style="font-weight:bold">Total Total Omset Sales : </td>
                    <td style="font-weight:bold" align="right"><?php echo e(number_format($row->rekapTJ, 2, "." ,",")); ?></td>
                    <td style="font-weight:bold" align="right"><?php echo e(number_format($row->rekapTB, 2, "." ,",")); ?></td>
                    <td style="font-weight:bold" align="right"><?php echo e(number_format($row->rekapSisa, 2, "." ,",")); ?></td>
                  </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
