@extends('main/index')

@section('css')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>

  <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/poSupplier2.js') }}" type="text/javascript"></script>

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

  <!-- (Optional) Latest compiled and minified JavaScript translation files -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>

@stop

@section('body')

<span id="data-back"
  data-kode-customer="{{ $kodeSupplier }}"
  data-form-token="{{ csrf_token() }}"
  data-route-po-supplier-barang-row="{{ route('poSupplierBarangRow') }}"
  data-route-po-supplier-hitung-kredit="{{ route('poSupplierHitungWaktuKredit') }}">
</span>

<form class="form-send" action="{{ route('insertWo',['no_po'=>$pos_no_po]) }}" method="post">
{{ csrf_field() }}
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="row form-horizontal">
              <div class="col-xs-12 col-sm-6">
                <div class="form-body">
                    <div class="form-group">                        
                        <div class="col-xs-2">
                            <h5><b>No PO</b></h5>
                        </div>
                        <div class="col-xs-9">
                            <input type="text" class="form-control" placeholder="Kode" name="" value="{{$poSupplier->no}}" readonly="readonly">                            
                            <input type="hidden" class="form-control" placeholder="Kode" name="pos_no_po" value="{{ $poSupplier->no }}">
                            <input type="hidden" class="form-control" placeholder="Kode" name="id_po_spl" value="{{ $id_po_spl }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <h5><b>Supplier</b></h5>                        
                        </div>
                        <div class="col-xs-3">
                            <input type="text" class="form-control" placeholder="Kode" name="kode_spl" value="SPL{{ $poSupplier->spl_kode }}" readonly>
                            <input type="hidden" class="form-control" placeholder="Kode" name="spl_kode" value="{{ $poSupplier->spl_kode }}" readonly>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" placeholder="Kode" name="spl_name" value="{{ $poSupplier->suppliers->spl_nama }}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <h5><b>Alamat</b></h5>                        
                        </div>
                        <div class="col-xs-5">
                            : {{ $poSupplier->suppliers->spl_alamat }} 
                        </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6">
                <div class="form-body">
                    <div class="form-group">
                        <label class="col-md-3" style="padding-top: 10px;">Tanggal</label>
                        <div class="col-md-4">
                            <input type="text" name="tgl_pembelian" class="form-control date-picker" data-date-format="yyyy-mm-dd" value="{{date('Y-m-d')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3" style="padding-top: 10px;">No W/O</label>
                        <div class="col-md-4">
                            <input type="text" name="no_faktur_view" class="form-control" value="{{$ps_no_faktur_next}}" readonly>
                            <input type="hidden" name="no_faktur" class="form-control" value="{{$ps_no_faktur_next}}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                    <label class="col-md-{{ $col_label }}" style="padding-top: 10px;">No SJ Supplier</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="No SJ Supplier" name="no_sj" id="no_sj" required>
                        </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}" style="padding-top: 10px;">Transaksi</label>
                      <div class="col-md-{{ $col_form }}">
                        <div class="mt-radio-inline">
                          <label class="mt-radio mt-radio-outline">
                            <input type="radio" name="pl_transaksi" id="optionsRadios22" value="cash" checked="" <?php if($poSupplier->tipe_transaksi == 'cash') echo 'checked="" value="{{$poSupplier->tipe_transaksi}}"';?>> {{$poSupplier->tipe_transaksi}}
                            <span></span>
                          </label>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <hr>
            <div>
              <!-- <button type="button" class="btn btn-primary btn-add-item" data-toggle="modal"> 
                Add Item
              </button> -->
            </div>            
            <table class="table table-striped table-bordered table-hover table-header-fixed table-all-data table-po-supplier-detail">
              <thead>
                <tr class="">
                  <th><center> Kode </center></th>
                  <th><center> Nama Barang </center></th>
                  <th><center> No. Seri </center></th>
                  <th><center> Tujuan </center></th>
                  <th><center> Harga Beli </center></th>
                  <th><center> PPN(%) </center></th>
                  <th><center> Disc(%) </center></th>
                  <th><center> Disc. Nom </center></th>
                  <th><center> Harga Net </center></th>
                  <th><center> Qty </center></th>
                  <th><center> Satuan </center></th>
                  <th><center> Total </center></th>
                  <th><center> Keterangan </center></th>
                </tr>
              </thead>
              <tbody>
              
              @foreach($items as $item)
                      <tr>
                        <td><center>{{$item->brg_kode}}</center></td>
                        <td><center>{{$item->nama_barang}}</center></td>
                        <td><center>{{$item->brg_no_seri}}</center></td>
                        <td><center>{{$item->gudangs->gdg_nama}}</center></td>                        
                        <td><center>{{number_format($item->harga_beli,2)}}</center></td>
                        <td><center>{{number_format($item->ppn,2)}}%</center></td>
                        <td><center>{{number_format($item->disc,2)}}%</center></td>
                        <td><center>{{number_format($item->disc_nom,2)}}</center></td>
                        <td><center>{{number_format($item->harga_net,2)}}</center></td>
                        <td><center>{{number_format($item->qty,2)}}</center></td>
                        <td><center>{{$item->satuan}}</center></td>
                        <td><center>{{$item->total}}</center></td>
                        <td><center>{{$item->keterangan}}</center></td>
                      </tr>
                      @endforeach
              </tbody>
            </table>
            <hr />
            <div class="row">
              <div class="col-xs-12 col-sm-4 form-horizontal">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}">Kondisi</label>
                      <div class="col-md-{{ $col_form }}">
                        <select class="form-control" name="kondisi">
                          <option value="gudang" <?php if($poSupplier->kondisi=='gudang') echo 'selected';?>>Gudang</option>
                          <option value="customer" <?php if($poSupplier->kondisi=='customer') echo 'selected';?>>Customer</option>
                        </select>
                        <input type="hidden" name="tgl_kirim" class="form-control date-picker" data-date-format="yyyy-mm-dd" value="{{$poSupplier->pos_tgl_kirim}}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}">Catatan</label>
                        <div  class="col-md-{{ $col_form }}">
                          <textarea class="form-control" name="pl_catatan">{{$poSupplier->pos_catatan}}</textarea>
                        </div>
                    </div>
                  </div>
              </div>
              <div class="col-xs-12 col-sm-4 form-horizontal">
                <div class="form-body form-kredit hide">
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Lama Kredit</label>
                      <div class="col-md-{{ $col_label }}">
                      <input type="text" name="pl_lama_kredit" class="form-control">
                      </div>
                      <div class="col-md-{{ $col_label }}">
                      <select class="form-control" name="pl_waktu">
                          <option value="-Pilih Waktu-"></option>                          
                          <option value="hari">Hari</option>
                          <option value="bulan">Bulan</option>
                          <option value="tahun">Tahun</option>
                        </select>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Jatuh Tempo</label>
                      <div class="col-md-{{ $col_form }}">
                        <input type="text" name="pl_tgl_jatuh_tempo" class="form-control date-picker" readonly>
                      </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 form-horizontal">
                <div class="form-body">
                  <div class="form-group">
                    <label class="col-md-4">Sub Total</label>
                      <div class="col-md-8">
                          <input type="text" class="form-control" name="pl_subtotal" value="{{$sub_total}}" readonly step=".01">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Disc</label>
                    <div  class="col-md-3">
                      <input type="number" class="form-control" name="pl_disc" value="{{$poSupplier->pos_disc}}" readonly="readonly" step=".01">
                    </div>
                    <div  class="col-md-5">
                      <input type="number" class="form-control" name="pl_disc_nom" value="{{$poSupplier->pos_disc_nom}}" readonly step=".01">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Ppn</label>
                    <div  class="col-md-3">
                      <input type="number" class="form-control" name="pl_ppn" value="{{$poSupplier->pos_ppn}}" readonly="readonly" step=".01">
                    </div>
                    <div  class="col-md-5">
                      <input type="number" class="form-control" name="pl_ppn_nom" value="{{$poSupplier->pos_ppn_nom}}" readonly step=".01">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Ongkos Angkut</label>
                    <div  class="col-md-8">
                      <input type="number" class="form-control" name="pl_ongkos_angkut" value="{{$poSupplier->biaya_lain}}" readonly step=".01">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4"l>Grand Total</label>
                    <div   class="col-md-8">
                        <input type="number" class="form-control" name="grand_total" value="{{$grand_total}}" readonly step=".01">
                    </div>
                  </div>
                  <div class="form-action">
                    <button type="submit" class="btn btn-success btn-lg btn-block">SAVE</button>
                    <a href="{{ route('poSupplierDaftar') }}" class="btn btn-warning btn-lg btn-block">Batal</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</form>

@stop