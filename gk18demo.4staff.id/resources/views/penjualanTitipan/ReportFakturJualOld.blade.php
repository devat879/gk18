<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all"/> --}}
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}"> --}}
    <title></title>
    <style>
    p {
      font-size: 10px;
    }
    table {
      border-collapse: collapse;
    }
    </style>
  </head>
  <body>
    <div>
      <div>
        <div>
          <table width="100%">
            <tr>
              {{-- <td>
                <img src="http://grahakita.ganesh-demo.online/img/icon/aki.png" width="70">
              </td> --}}
              <td colspan="4">
                <p>Jl. Gatot Subroto Barat No. 88A Tlp. (0361)416088, Fax.(0361)418933, No.HP. 0812 1611 8118</p>
              </td>
            </tr>
          </table>
          <div style="float: left; width: 60%;">
            <table width="100%" border="0">
              <tr>
                <td width="30%"> <p>Syarat Pembayaran</p> </td>
                <td width="5%"> <p>:</p> </td>
                <td> <p>{{$faktur['pt_transaksi']}}</p> </td>
              </tr>
              <tr>
                <td width="30%"> <p>Sales</p> </td>
                <td width="5%"> <p>:</p> </td>
                <td> <p>{{$faktur->salesPrint['kry_nama']}}</p> </td>
              </tr>
            </table>
          </div>

          <div style="float: right; width: 40%;">
            <table width="100%" border="0">
              <tr>
                <td width="10%"> <p>No. Ftr</p> </td>
                <td width="10%"> <p>:</p> </td>
                <td width="10%"> <p>{{$faktur['kode_bukti_id']}}</p> </td>
              </tr>
              <tr>
                <td width="10%"> <p>Tgl.</p> </td>
                <td width="10%"> <p>:</p> </td>
                <td width="10%"> <p>{{$faktur['tglPrint']}}</p> </td>
              </tr>
              <tr>
                <td width="10%"> <p>Pelanggan</p> </td>
                <td width="10%"> <p>:</p> </td>
                <td width="10%"> <p>{{$faktur['cus_nama']}}</p> </td>
              </tr>
            </table>
          </div>

          <div class="index" style="clear: both; margin: 0pt; padding: 0pt;">
            {{-- <h5 align="center">Faktur Jual</h5> --}}
            <p align="center"><b>Faktur Jual</b></p>
            <table width="100%" border="1">
              <thead>
                <tr>
                  <th> <p>No</p> </th>
                  <th> <p>Nama Barang</p> </th>
                  <th> <p>Harga/sat(Rp.)</p> </th>
                  <th> <p>Disc.%</p> </th>
                  <th> <p>Disc.nom</p> </th>
                  <th> <p>Harga Bersih</p> </th>
                  <th> <p>Banyak</p> </th>
                  <th> <p>Total</p> </th>
                </tr>
              </thead>
              <tbody>
                @foreach ($faktur->detail as $key)
                  <tr>
                    <td><p>{{$faktur->no++}}</p></td>
                    <td><p>{{$key['nama_barang']}}</p></td>
                    <td><p>{{number_format($key['harga_jual'], 0, "." ,".")}}</p></td>
                    <td><p>{{number_format($key['disc'], 0, "." ,".")}}</p></td>
                    <td><p>{{number_format($key['disc_nom'], 0, "." ,".")}}</p></td>
                    <td><p>{{number_format($key['harga_net'], 0, "." ,".")}}</p></td>
                    <td><p>{{number_format($key['qty'], 0, "." ,".") }}</p></td>
                    <td><p>{{number_format($key['total'], 0, "." ,".")}}</p></td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>

          <div class="keterangan" style="clear: both; margin: 0pt; padding: 0pt;">
            <table width="20%" border="0">
              <tr>
                <td> <p>Terbilang</p> </td>
                <td> <p>:</p> </td>
              </tr>
              <tr>
                <td> <p>keterangan</p> </td>
                <td> <p>:</p> </td>
              </tr>
            </table>
          </div>

          <div style="float: left; width: 70%;">
            <table style="width: 100%;" border="0">
              <tbody>
                <tr>
                  <td width="50%" rowspan="4">
                    <p>- Pembayaran dengan CHEQUE/GIRO dianggap sah, setelah CHEQUE/GIRO tersebut telah dapat diuangkan/clearing</p>
                    <p>- Barang yang telah di beli tidak dapat dikembalikan/ditukar</p>
                  </td>
                </tr>
                <tr>
                  <td width="50%" align="center">
                    {{-- <h5>Penerima</h5> --}}
                    <b><p>Penerima</p></b>
                  </td>
                </tr>
                <tr>
                  <td>
                    <br>
                    <br>
                    <br>
                  </td>
                </tr>
                <tr>
                  <td width="50%" align="center">
                    <p>(......................................)</p>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          <div style="float: right; width: 30%;">
            <table style="width: 100%;" border="1">
              <tr>
                <td> <p>Total</p> </td>
                {{-- <td> <p>:</p> </td> --}}
                <td> <p>{{number_format($faktur['pt_subtotal'], 0, "." ,".")}}</p> </td>
              </tr>
              <tr>
                <td> <p>Discount</p> </td>
                {{-- <td> <p>:</p> </td> --}}
                <td> <p>{{number_format($faktur['pt_disc_nom'], 0, "." ,".")}}</p> </td>
              </tr>
              <tr>
                <td> <p>Grand Total</p> </td>
                {{-- <td> <p>:</p> </td> --}}
                <td> <p>{{number_format($faktur['grand_total'], 0, "." ,".")}}</p> </td>
              </tr>
              <tr>
                <td> <p>Kembali</p> </td>
                {{-- <td> <p>:</p> </td> --}}
                <td> <p>{{number_format(0, 0, "." ,".")}}</p> </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
