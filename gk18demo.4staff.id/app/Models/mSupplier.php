<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mSupplier extends Model
{
  public $incrementing = false;
  protected $table = 'tb_supplier';
  protected $primaryKey = 'spl_kode';
  public $timestamps = false;

  public function pembelianSupplier()
  {
    return $this->hasMany('App\Models\mPembelianSupplier','spl_id');
  }

  public function hutangSupplier()
  {
    return $this->hasMany('App\Models\mHutangSuplier', 'spl_kode');
  }

  public function returnPembelian()
  {
    return $this->hasMany('App\Models\mReturPembelian', 'spl_kode');
  }
}
