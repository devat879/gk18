<?php
use App\Models\mDeliveryOrder;
?>



<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
    
    
    
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="portlet light">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_1_1" data-toggle="tab"> Penjualan Langsung </a>
                </li>
                <li>
                  <a href="#tab_1_2" data-toggle="tab"> Penjualan Titipan </a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active in" id="tab_1_1">
                  <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                    <thead>
                      <tr class="">
                        <th width="10" style="font-size:10px"> No </th>
                        <th style="font-size:10px"> No Faktur </th>
                        <th style="font-size:10px"> Tanggal </th>
                        <th style="font-size:10px"> Jenis Pembayaran </th>
                        <th style="font-size:10px"> Pelanggan </th>
                        <th style="font-size:10px"> Action </th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        
                          <tr>
                            <td style="font-size:10px"> <?php echo e($no++); ?>. </td>
                            <td style="font-size:10px"> <?php echo e($row->pl_no_faktur); ?> </td>
                            <td style="font-size:10px"> <?php echo e($row->pl_tgl); ?> </td>
                            <td style="font-size:10px"> <?php echo e($row->pl_transaksi); ?> </td>
                            <td style="font-size:10px"> <?php echo e($row->cus_nama); ?> </td>
                            <td style="font-size:10px">
                              <div class="btn-group btn-group-xs">
                                <a type="button" class="btn btn-success" href="<?php echo e(route('ReturPenjualanEdit', ['kode'=>$row->pl_no_faktur, 'type'=>'langsung'])); ?>">
                                  <span class="icon-pencil"></span> Retur
                                </a>
                              </div>
                            </td>
                          </tr>
                        
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                  </table>
                </div>
                <div class="tab-pane" id="tab_1_2">
                  <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                    <thead>
                      <tr class="">
                        <th width="10" style="font-size:10px"> No </th>
                        <th style="font-size:10px"> No Faktur </th>
                        <th style="font-size:10px"> Tanggal </th>
                        <th style="font-size:10px"> Jenis Pembayaran </th>
                        <th style="font-size:10px"> Pelanggan </th>
                        <th style="font-size:10px"> Action </th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $__currentLoopData = $dataList_PT; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                          <td style="font-size:10px"> <?php echo e($no_2++); ?>. </td>
                          <td style="font-size:10px"> <?php echo e($row->pt_no_faktur); ?> </td>
                          <td style="font-size:10px"> <?php echo e($row->pt_tgl); ?> </td>
                          <td style="font-size:10px"> <?php echo e($row->pt_transaksi); ?> </td>
                          <td style="font-size:10px"> <?php echo e($row->cus_nama); ?> </td>
                          <td style="font-size:10px">
                            
                              <div class="btn-group btn-group-xs">
                                <a type="button" class="btn btn-success" href="<?php echo e(route('ReturPenjualanEdit', ['kode'=>$row->pt_no_faktur, 'type'=>'titipan'])); ?>">
                                  <span class="icon-pencil"></span> Retur
                                </a>
                              </div>
                            
                          </td>
                        </tr>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>