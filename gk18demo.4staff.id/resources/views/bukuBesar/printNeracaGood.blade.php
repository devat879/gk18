<html moznomarginboxes mozdisallowselectionprint>
    <head>
    <!-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> -->
     <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}"> -->
     <title>Laporan Neraca | Graha Kita</title>
    </head>
    <body>
    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
        .tg td{font-family:Arial;font-size:10px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
        .tg th{font-family:Arial;font-size:12px;font-weight:normal;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
        .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
        .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
        .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
        @media print {
        	.page-break	{ display: block; page-break-before: always; }
        }
    </style>
    <div class="container-fluid">
        <h2 style="text-align:justify;font-family:Arial;">   
            <img src="{{ asset('img/logo.png') }}" width='40px' heigth='40px' style=”float:left;”><i class="fa fa-file-o"></i> PT ANGSA KUSUMA INDAH
        </h2>
        <hr>
        <div class="row">
            <div class="col-xs-12">
                <div class="text-center">
                <h2><center><strong>LAPORAN Neraca</strong></center></h2>
                        <h3><center><strong>Periode {{$bln}} {{$tahun_periode}}</strong></center></h3>
          </div>
          <br>
          <div class="portlet light ">
          <table class="tg">
                            <thead>
                                <tr class="success">
                                    <th width="25%"><center>ASET</center></th>
                                    <th width="25%"><center>LIABILITAS</center></th>
                                    <th width="25%"><center>EKUITAS</center></th>
                                    <th width="25%"><center>STATUS</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{number_format($total_asset)}}</td>
                                    <td>{{number_format($total_liabilitas)}}</td>
                                    <td>{{number_format($total_ekuitas)}}</td>
                                    @if($status > 0)
                                    <td><font color="red"><strong>NOT BALANCE : {{number_format($status)}}</strong></font></td>
                                    @endif
                                    @if($status < 0)
                                    <td><font color="red"><strong>NOT BALANCE : {{number_format($status)}}</strong></font></td>
                                    @endif
                                    @if($status == 0)
                                    <td><font color="green"><strong>BALANCE</strong></font></td>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <h3><center><strong>DATA ASSET</strong></center></h3>
                        <table class="tg" width="100%">
                                        <thead>
                                            <tr class="success">
                                                <th width="25%"><center> Kode Rekening </center></th>
                                                <th width="45%"><center> Nama Rekening </center></th>
                                                <th width="20%"><center> Nominal </center></th>
                                            </tr>
                                        </thead>                                        
                                        <tbody>
                                        <?php
                                            

                                            $total_assets = 0;
                                            $total_liabilitass = 0;
                                            $total_ekuitass = 0;

                                        ?>
                                        @foreach($detail_perkiraan as $detail)
                                            @if($detail->perkiraan->mst_neraca_tipe == 'asset' && $detail->perkiraan->mst_master_id == '0' && $detail->perkiraan->mst_nama_rekening != 'AKTIVA TETAP')
                                            <tr>
                                                <td>{{$detail->perkiraan->mst_kode_rekening}}</td>
                                                <td>{{$detail->perkiraan->mst_nama_rekening}}</td>
                                                <?php
                                                    if($detail->perkiraan->mst_normal == 'kredit'){
                                                        $asset  = $detail->msd_awal_kredit;
                                                    }
                                                    if($detail->perkiraan->mst_normal == 'debet'){
                                                        $asset  = $detail->msd_awal_debet;
                                                    }
                                                    foreach($detail->perkiraan->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan) as $transaksi){
                                                        if($detail->perkiraan->mst_normal == 'kredit'){
                                                            $asset  = $asset+$transaksi->trs_kredit-$transaksi->trs_debet;
                                                        }
                                                        if($detail->perkiraan->mst_normal == 'debet'){
                                                            $asset  = $asset-$transaksi->trs_kredit+$transaksi->trs_debet;
                                                        }

                                                    }
                                                    $total_assets = $total_assets+$asset;
                                                ?>
                                                <td align="right">{{number_format($asset)}}</td>
                                            </tr>
                                            @foreach($detail->perkiraan->childs as $det_child)
                                            <tr>
                                                <td>{{$det_child->mst_kode_rekening}}</td>
                                                <td>{{$det_child->mst_nama_rekening}}</td>
                                                <?php
                                                    if($det_child->mst_normal == 'kredit'){
                                                        // $asset  = $det_child->master_detail->where('msd_year',$tahun_periode)->where('msd_month',$bulan)->select('msd_awal_kredit');
                                                        $assett  = $det_child->master_detail->where('msd_year',$tahun_periode)->where('msd_month',$bulan)->first();
                                                        $asset   = $assett->msd_awal_kredit;
                                                    }
                                                    if($det_child->mst_normal == 'debet'){
                                                        // $asset  = $det_child->msd_awal_debet;
                                                        $assett  = $det_child->master_detail->where('msd_year',$tahun_periode)->where('msd_month',$bulan)->first();
                                                        $asset   = $assett->msd_awal_debet;
                                                    }
                                                    foreach($det_child->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan) as $transaksi){
                                                        if($det_child->mst_normal == 'kredit'){
                                                            $asset  = $asset+$transaksi->trs_kredit-$transaksi->trs_debet;
                                                        }
                                                        if($det_child->mst_normal == 'debet'){
                                                            $asset  = $asset-$transaksi->trs_kredit+$transaksi->trs_debet;
                                                        }

                                                    }
                                                    $total_assets = $total_assets+$asset;
                                                ?>
                                                <td align="right">{{number_format($asset)}}</td>
                                            </tr>
                                            @endforeach
                                            @endif
                                        @endforeach
                                        <tr class="">
                                                <td colspan="2" align="right"><strong> TOTAL </strong></td>
                                                <td align="right"><strong>{{number_format($total_assets)}}</strong></td>
                                            </tr>
                                        </tbody>
                                        <!--<tfoot>-->
                                        <!--    <tr class="">-->
                                        <!--        <td colspan="2" align="right"><strong> TOTAL </strong></td>-->
                                        <!--        <td align="right"><strong>{{number_format($total_assets)}}</strong></td>-->
                                        <!--    </tr>-->
                                        <!--</tfoot>-->
                                    </table>
                                    <h4>Aktiva Tetap</h4>
                                    <table class="tg">
                                        <thead>
                                            <tr class="success">
                                                <th width="25%"><center> Kode Rekening </center></th>
                                                <th width="45%"><center> Nama Rekening </center></th>
                                                <th width="20%"><center> Nominal </center></th>
                                            </tr>
                                        </thead>                                        
                                        <tbody>
                                        <?php
                                            

                                            $total_aktiva = 0;

                                        ?>
                                        @foreach($detail_perkiraan as $detail)
                                            @if($detail->perkiraan->mst_neraca_tipe == 'asset' && $detail->perkiraan->mst_master_id == '0' && $detail->perkiraan->mst_nama_rekening == 'AKTIVA TETAP')
                                            <tr>
                                                <td>{{$detail->perkiraan->mst_kode_rekening}}</td>
                                                <td>{{$detail->perkiraan->mst_nama_rekening}}</td>
                                                <?php
                                                    if($detail->perkiraan->mst_normal == 'kredit'){
                                                        $activa  = $detail->msd_awal_kredit;
                                                    }
                                                    if($detail->perkiraan->mst_normal == 'debet'){
                                                        $activa  = $detail->msd_awal_debet;
                                                    }
                                                    foreach($detail->perkiraan->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan) as $transaksi){
                                                        if($detail->perkiraan->mst_normal == 'kredit'){
                                                            $activa  = $activa+$transaksi->trs_kredit-$transaksi->trs_debet;
                                                        }
                                                        if($detail->perkiraan->mst_normal == 'debet'){
                                                            $activa  = $activa-$transaksi->trs_kredit+$transaksi->trs_debet;
                                                        }

                                                    }
                                                    $total_aktiva = $total_aktiva+$activa;
                                                ?>
                                                <td align="right">{{number_format($activa)}}</td>
                                            </tr>
                                            @foreach($detail->perkiraan->childs as $det_child)
                                            <tr>
                                                <td>{{$det_child->mst_kode_rekening}}</td>
                                                <td>{{$det_child->mst_nama_rekening}}</td>
                                                <?php
                                                    if($det_child->mst_normal == 'kredit'){
                                                        $activa  = $det_child->msd_awal_kredit;
                                                    }
                                                    if($det_child->mst_normal == 'debet'){
                                                        $activa  = $det_child->msd_awal_debet;
                                                    }
                                                    foreach($det_child->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan) as $transaksi){
                                                        if($det_child->mst_normal == 'kredit'){
                                                            $activa  = $activa+$transaksi->trs_kredit-$transaksi->trs_debet;
                                                        }
                                                        if($det_child->mst_normal == 'debet'){
                                                            $activa  = $activa-$transaksi->trs_kredit+$transaksi->trs_debet;
                                                        }

                                                    }
                                                    $total_aktiva = $total_aktiva+$activa;
                                                ?>
                                                <td align="right">{{number_format($activa)}}</td>
                                            </tr>
                                            @foreach($det_child->childs as $child)
                                            <tr>
                                                <td>{{$child->mst_kode_rekening}}</td>
                                                <td>{{$child->mst_nama_rekening}}</td>
                                                <?php
                                                    if($child->mst_normal == 'kredit'){
                                                        $activa  = $child->msd_awal_kredit;
                                                    }
                                                    if($child->mst_normal == 'debet'){
                                                        $activa  = $child->msd_awal_debet;
                                                    }
                                                    foreach($child->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan) as $transaksi){
                                                        if($child->mst_normal == 'kredit'){
                                                            $activa  = $activa+$transaksi->trs_kredit-$transaksi->trs_debet;
                                                        }
                                                        if($child->mst_normal == 'debet'){
                                                            $activa  = $activa-$transaksi->trs_kredit+$transaksi->trs_debet;
                                                        }

                                                    }
                                                    $total_aktiva = $total_aktiva+$activa;
                                                ?>
                                                <td align="right">{{number_format($activa)}}</td>
                                            </tr>
                                            @endforeach
                                            @endforeach
                                            @endif
                                        @endforeach                                            
                                        </tbody>
                                        <tfoot>
                                            <tr class="">
                                                <td colspan="2" align="right"><strong> TOTAL </strong></td>
                                                <td align="right"><strong>{{number_format($total_aktiva)}}</strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <br><br>
                                    <hr>

                                    <br>
                                    <h3><center><strong>DATA LIABILITAS</strong></center></h3>
                                    <table class="tg"> 
                                        <thead>
                                            <tr class="success">
                                                <th width="25%"><center> Kode Rekening </center></th>
                                                <th width="45%"><center> Nama Rekening </center></th>
                                                <th width="20%"><center> Nominal </center></th>
                                            </tr>
                                        </thead>                                        
                                        <tbody>
                                        @foreach($detail_perkiraan as $detail)
                                            @if($detail->perkiraan->mst_neraca_tipe == 'liabilitas')
                                            <tr>
                                                <td width="25%">{{$detail->perkiraan->mst_kode_rekening}}</td>
                                                <td width="45%">{{$detail->perkiraan->mst_nama_rekening}}</td>
                                                <?php
                                                    if($detail->perkiraan->mst_normal == 'kredit'){
                                                        $liabilitas  = $detail->msd_awal_kredit;
                                                    }
                                                    if($detail->perkiraan->mst_normal == 'debet'){
                                                        $liabilitas  = $detail->msd_awal_debet;
                                                    }
                                                    foreach($detail->perkiraan->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan) as $transaksi){
                                                        if($detail->perkiraan->mst_normal == 'kredit'){
                                                            $liabilitas  = $liabilitas+$transaksi->trs_kredit-$transaksi->trs_debet;
                                                        }
                                                        if($detail->perkiraan->mst_normal == 'debet'){
                                                            $liabilitas  = $liabilitas-$transaksi->trs_kredit+$transaksi->trs_debet;
                                                        }
                                                                        
                                                    }
                                                    $total_liabilitass     = $total_liabilitass+$liabilitas;
                                                ?>
                                                <td width="20%">{{number_format($liabilitas)}}</td>
                                            </tr>
                                            @endif
                                        @endforeach                                           
                                        </tbody>
                                        <tfoot>
                                            <tr class="">
                                                <td colspan="2" align="right"><strong> TOTAL </strong></td>
                                                <td align="right"><strong>{{number_format($total_liabilitass)}}</strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <br><br>
                                    <hr>
                                    <div class="page-break">
                                        <h3><center><strong>DATA EKUITAS</strong></center></h3>                                 
                                    
                                    <table class="tg" width="100%">
                                        <thead>
                                            <tr class="success">
                                                <th width="25%"><center> Kode Rekening </center></th>
                                                <th width="45%"><center> Nama Rekening </center></th>
                                                <th width="20%"><center> Nominal </center></th>
                                            </tr>
                                        </thead>                                        
                                        <tbody>
                                        @foreach($detail_perkiraan as $detail)
                                            @if($detail->perkiraan->mst_neraca_tipe == 'ekuitas')
                                            <tr>
                                                <td width="25%">{{$detail->perkiraan->mst_kode_rekening}}</td>
                                                <td width="45%">{{$detail->perkiraan->mst_nama_rekening}}</td>
                                                <?php
                                                    if($detail->perkiraan->mst_normal == 'kredit'){
                                                        $ekuitas  = $detail->msd_awal_kredit;
                                                    }
                                                    if($detail->perkiraan->mst_normal == 'debet'){
                                                        $ekuitas  = $detail->msd_awal_debet;
                                                    }
                                                    foreach($detail->perkiraan->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan) as $transaksi){
                                                        if($detail->perkiraan->mst_normal == 'kredit'){
                                                            $ekuitas  = $ekuitas+$transaksi->trs_kredit-$transaksi->trs_debet;
                                                        }
                                                        if($detail->perkiraan->mst_normal == 'debet'){
                                                            $ekuitas  = $ekuitas-$transaksi->trs_kredit+$transaksi->trs_debet;
                                                            
                                                        }
                                                                        
                                                    }
                                                    $total_ekuitass     = $total_ekuitass+$ekuitas;
                                                ?>
                                                <td width="20%">{{number_format($ekuitas)}}</td>
                                            </tr>
                                            @endif
                                        @endforeach                                            
                                        </tbody>
                                        <tfoot>
                                            <tr class="">
                                                <td colspan="2" align="right"><strong> TOTAL </strong></td>
                                                <td align="right"><strong>{{number_format($total_ekuitass)}}</strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    </div>
                                    
          </div>
        </div>
      </div>
    </div>
    <script>
		window.print();
	</script>
  </body>
</html>
