<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mPoSupplier extends Model
{
  public $incrementing = false;
  protected $table = 'tb_po_supplier';
  protected $primaryKey = 'pos_no_po';
  public $timestamps = false;

  protected $guarded=[];
  

  public function suppliers(){
    return $this->belongsTo(mSupplier::class,'spl_kode','spl_kode');
  }

  public function detailPoSupplier(){
    return $this->hasMany(mDetailPoSupplier::class,'pos_no_po','no');
  }

}
