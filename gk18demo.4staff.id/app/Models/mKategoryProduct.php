<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mKategoryProduct extends Model
{
  public $incrementing = false;
  protected $table = 'tb_kategori_stok';
  protected $primaryKey = 'ktg_kode';
  public $timestamps = false;
}
