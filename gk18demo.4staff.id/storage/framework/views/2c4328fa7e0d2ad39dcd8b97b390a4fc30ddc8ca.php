

<?php $__env->startSection('css'); ?>
  <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
  
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
  <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
  
  
  
  
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="portlet light">
              <button class="btn btn-primary" data-toggle="modal" href="#modal-tambah">
                <i class="fa fa-plus"></i> Tambah Group Stok
              </button>
              <br /><br />
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                <thead>
                  <tr class="">
                    <th width="10" style="font-size:10px"> No </th>
                    <th style="font-size:10px"> Kode </th>
                    <th style="font-size:10px"> Nama Group Stok </th>
                    <th style="font-size:10px"> Deskripsi </th>
                    <th style="font-size:10px"> Action </th>
                  </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td style="font-size:10px"> <?php echo e($no++); ?>. </td>
                      <td style="font-size:10px"> <?php echo e($kodeLabel.$row->grp_kode); ?> </td>
                      <td style="font-size:10px"> <?php echo e($row->grp_nama); ?> </td>
                      <td style="font-size:10px"> <?php echo e($row->grp_description); ?> </td>
                      <td style="font-size:10px">
                        <div class="btn-group-xs">
                          <button class="btn btn-success btn-edit" data-href="<?php echo e(route('groupStokEdit', ['kode'=>$row->grp_kode])); ?>">
                            <span class="icon-pencil"></span> Edit
                          </button>
                          <button class="btn btn-danger btn-delete-new" data-href="<?php echo e(route('groupStokDelete', ['kode'=>$row->grp_kode])); ?>">
                            <span class="icon-trash"></span> Delete
                          </button>
                        </div>
                      </td>
                    </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modal-tambah" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-plus"></i> Tambah Group Stok
          </h4>
        </div>
        <div class="modal-body form">
          <form action="<?php echo e(route('groupStokInsert')); ?>" class="form-horizontal form-send" role="form" method="post">
            <?php echo e(csrf_field()); ?>

            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Nama Group Stok</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="grp_nama">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Deskripsi</label>
                <div class="col-md-9">
                  <textarea class="form-control" name="grp_description"></textarea>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal" id="modal-edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Edit Group Stok
          </h4>
        </div>
        <div class="modal-body form">
          <form action="" class="form-horizontal form-send" role="form" method="put">
            <?php echo e(csrf_field()); ?>

            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Nama Group Stok</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="grp_nama">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Deskripsi</label>
                <div class="col-md-9">
                  <textarea class="form-control" name="grp_description"></textarea>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>