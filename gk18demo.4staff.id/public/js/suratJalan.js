$(document).ready(function () {
  $('.form-send-SJ').submit(function(e) {
    e.preventDefault();
    var ini = $(this);
    //Save penjualan
    $.ajax({
      url: ini.attr('action'),
      type: ini.attr('method'),
      data: ini.serialize(),
      success: function(data) {
        console.log(data);
        if(data.redirect) {
          if (data.tipe == 'langsung') {
            //Surat Jalan
            var url_SJ = '../../suratJalan/surat-jalan/langsung/'+data.faktur;
            window.open(url_SJ, '_blank');

            //DO
            var url_DO = '../../suratJalan/do/langsung/'+data.faktur;
            window.open(url_DO, '_blank');
          }
          else {
            //Surat Jalan
            var url_SJ = '../../suratJalan/surat-jalan/titipan/'+data.faktur;
            window.open(url_SJ, '_blank');

            //DO
            var url_DO = '../../suratJalan/do/titipan/'+data.faktur;
            window.open(url_DO, '_blank');
          }

          window.location.href = data.redirect;
        }
      },
      error: function(request, status, error) {
        var json = JSON.parse(request.responseText);
        $('.form-group').removeClass('has-error');
        $('.help-block').remove();
        $.each(json.errors, function(key, value) {
          $('.form-send [name="'+key+'"]').parents('.form-group').addClass('has-error');
          $('.form-send [name="'+key+'"]').after('<span class="help-block">'+value+'</span>');
        });
      }
    });
    return false;
  });
});
