<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mKendaraan extends Model
{
  public $incrementing = false;
  protected $table = 'tb_kendaraan';
  protected $primaryKey = 'kdn_kode';
  public $timestamps = false;
}
