<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mReturPembelian extends Model
{
  public $incrementing = false;
  protected $table = 'tb_retur_pembelian';
  protected $primaryKey = 'id';
  public $timestamps = false;

  // protected $fillable=[
  //   'no_return_pembelian',
  //   'tgl_pembelian',
  //   'pelaksana',
  //   'alasan',
  //   'total_retur',
  //   'pembayaran',
  //   'ps_no_faktur'
  // ];

  protected $guarded = [];

  public function supplier()
  {
    return $this->belongsTo('App\Models\mSupplier', 'spl_kode');
  }

  public function hutangSupplier()
  {
    return $this->hasMany('App\Models\mHutangSupplier', 'ps_no_faktur');
  }

  public function detailReturPembelian()
  {
    return $this->hasMany(mDetailReturPembelian::class,'no_retur_pembelian','no_return_pembelian');
  }

  public function pembelianSupplier()
  {
    return $this->belongsTo('App\Models\mPembelianSupplier', 'ps_no_faktur','no_pembelian');
  }
}
