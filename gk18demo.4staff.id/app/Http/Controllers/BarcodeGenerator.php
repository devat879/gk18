<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\mBarang;

class BarcodeGenerator extends Controller
{
  public function barang($id='')
  {
    $barang = mBarang::select('brg_kode', 'brg_barcode', 'brg_nama', 'brg_harga_jual_eceran', 'brg_harga_beli_ppn', 'brg_ppn_dari_supplier_persen')->where('brg_kode', $id)->first();
    if ($barang != null) {
      return view('barcode-generator.index', compact('barang'));
    }
    else {
      return view('barcode-generator.index', compact('barang'));
    }
  }
}
