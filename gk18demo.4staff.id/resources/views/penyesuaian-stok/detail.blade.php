@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
  <style media="screen">
  td.wrapok {
    white-space:nowrap
  }

  td.fontsize{
    font-size:10px
  }
  </style>
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('js/penyesuaianSC.js') }}" type="text/javascript"></script> --}}
  <script type="text/javascript">
  var l = window.location;
  var base_url = l.protocol + "//" + l.host + "/";
  var table = $('#table_1').DataTable({
    destroy : true,
    processing: true,
    serverSide: true,
    ajax: base_url+'penyesuaian-stok/{{$brg_kode}}/stok',
    columns: [
      // {data: 'no', class:"fontsize"},
      {data: 'brg_no_seri', class:"wrapok fontsize"},
      {data: 'stok', class:"wrapok fontsize"},
      {data: 'gdg_nama', class:"wrapok fontsize"},
      { data: null, orderable: false, searchable: false, render: function ( data, type, row ) {
        return '<div style="font-size:10px; white-space: nowrap" class="btn-group-xs"> <button class="btn btn-info btn-edit-stok" data-brg_kode="'+data.brg_kode+'" data-brg_no_seri="'+data.brg_no_seri+'" data-stok="'+data.stok+'" data-gdg_nama="'+data.gdg_nama+'" data-stk_kode="'+data.stk_kode+'"><span class="icon-pencil"></span> Update Stok</button></div>';
        }
      },
    ]
  });

  $('#table_1').on( 'draw.dt', function () {
    $('.btn-edit-stok').click(function() {
      var stk_kode = $(this).data('stk_kode');
      var brg_kode = $(this).data('brg_kode');
      var brg_no_seri = $(this).data('brg_no_seri');
      var gdg_nama = $(this).data('gdg_nama');
      var stok = $(this).data('stok');
      var kode = brg_kode;

      $('#modal-edit-stok').modal('show');
      $('#stok-stk_kode').val(stk_kode);
      $('#stok-kode').val(kode);
      $('#stok-brg_kode').val(brg_kode);
      $('#stok-brg_no_seri').val(brg_no_seri);
      $('#stok-gudang').val(gdg_nama);
      // $('#stok-stok').val(stok);

    });
  });

  $('.stk_btn_save').click(function() {
    var url = $('#modal-edit-stok form').attr('action');

    var stk_kode = $('#stok-stk_kode').val();
    var brg_kode = $('#stok-brg_kode').val();
    var stok = $('#stok-stok').val();
    var keterangan = $('#stok-keterangan').val();
    var _token = $('[name="_token"]').val();

    var data_send = {
      stk_kode: stk_kode,
      brg_kode: brg_kode,
      stok: stok,
      keterangan: keterangan,
      _token: _token,
    };

    $.ajax({
      url: url,
      type: 'POST',
      data: data_send,
      success:function(data) {
        $('#modal-edit-stok').modal('hide');

        const toast = swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000
        });

        table.ajax.reload();

        toast({
          type: 'success',
          title: 'Item Berhasil Disimpan'
        })
      }
    });
  });

  // $('#table_2').DataTable({
  //   destroy : true,
  //   processing: true,
  //   serverSide: true,
  //   ajax: base_url+'penyesuaian-stok/history',
  //   columns: [
  //     {data: 'ps_tgl', class:"wrapok fontsize"},
  //     {data: 'brg_barcode', class:"wrapok fontsize"},
  //     {data: 'brg_nama', class:"wrapok fontsize"},
  //     {data: 'stn_nama', class:"wrapok fontsize"},
  //     {data: 'ktg_nama', class:"wrapok fontsize"},
  //     {data: 'grp_nama', class:"wrapok fontsize"},
  //     {data: 'mrk_nama', class:"wrapok fontsize"},
  //     {data: 'stok_awal', class:"wrapok fontsize"},
  //     {data: 'stok_akhir', class:"wrapok fontsize"},
  //     {data: 'keterangan', class:"wrapok fontsize"},
  //   ]
  // });
  </script>
@stop

@section('body')
  <span id="data-back"
  data-form-token="{{ csrf_token() }}"></span>

  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            {{-- <ul class="nav nav-tabs">
              <li class="active">
                <a href="#tab_1_1" data-toggle="tab"> Detail Penyesuaian </a>
              </li>
              <li>
                <a href="#tab_1_2" data-toggle="tab"> History Penyesuaian </a>
              </li>
            </ul> --}}
            <div class="tab-content">
              <div class="tab-pane active in" id="tab_1_1">
                <table class="table table-striped table-bordered table-hover table-header-fixed" id="table_1">
                  <thead>
                    <tr class="">
                      <th>No Seri</th>
                      <th>Qty</th>
                      <th>Gudang</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>

              {{-- <div class="tab-pane" id="tab_1_2">
                <table class="table table-striped table-bordered table-hover table-header-fixed" id="table_2">
                  <thead>
                    <tr class="">
                      <th>Tanggal</th>
                      <th>Barcode</th>
                      <th>Nama</th>
                      <th>Satuan</th>
                      <th>Kategori</th>
                      <th>Group</th>
                      <th>Merek</th>
                      <th>Stok Awal</th>
                      <th>Stok Akhir</th>
                      <th>Keterangan</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div> --}}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modal-edit-stok" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Stok
          </h4>
        </div>
        <div class="modal-body form">
          <form action="{{route('penyesuaianStok.store')}}" class="form-horizontal form-stok" role="form" method="post">
            {{ csrf_field() }}
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Kode Produk</label>
                <div class="col-md-9">
                  <input id="stok-stk_kode" type="hidden" name="stk_kode" class="form-control" readonly>
                  <input id="stok-kode" type="text" name="kode" class="form-control" readonly>
                  <input id="stok-brg_kode" type="hidden" name="brg_kode" class="form-control" readonly>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">No Seri</label>
                <div class="col-md-9">
                  <input id="stok-brg_no_seri" type="text" name="brg_no_seri" class="form-control" readonly>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Gudang</label>
                <div class="col-md-9">
                  <input id="stok-gudang" type="text" name="gudang" class="form-control" readonly>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">QTY</label>
                <div class="col-md-9">
                  <input id="stok-stok" type="number" min="0" class="form-control" name="stok" value="0" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Keterangan</label>
                <div class="col-md-9">
                  <textarea id="stok-keterangan" name="keterangan" rows="2" cols="20" class="form-control"></textarea>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="text-center">
                  <button type="button" class="btn green stk_btn_save">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@stop
