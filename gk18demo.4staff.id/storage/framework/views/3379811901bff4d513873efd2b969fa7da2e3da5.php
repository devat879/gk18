

<?php $__env->startSection('css'); ?>
  <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
  <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
  
  
  
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript">
    // $('.input-daterange').datepicker({
    //   todayBtn:'linked',
    //   format: "yyyy-mm-dd",
    //   autoclose: true
    // });

    $('#search-date').click(function() {
      var href = "<?php echo e(route('summaryStokRange')); ?>";
      var start = $('#start_date').val();
      var end = $('#end_date').val();
      var table = $('#sample_2').DataTable();
      table.clear();

      $('#sample_2').DataTable({
        destroy : true,
        processing: true,
        serverSide: true,
        ajax : {
          url: href,
          type: 'POST',
          data: { start_date: start, end_date: end , _token: "<?php echo e(csrf_token()); ?>" },
          dataSrc : ''
        },
        columns: [
          { data: null, render: function ( data, type, row ) {
            return data.brg_barcode;
          } },
          { data: "brg_nama" },
          { data: "stn_nama" },
          { data: "ktg_nama" },
          { data: "grp_nama" },
          { data: "mrk_nama" },
          { data: "gdg_nama" },
          { data: null, render: function ( data, type, row ) {
            if (data.stokIn == null) {
              return '-';
            }
            else {
              return data.stokIn;
            }
          } },
          { data: null, render: function ( data, type, row ) {
            if (data.stokOut == null) {
              return '-';
            }
            else {
              return data.stokOut;
            }
          } },
          { data: "QOH" },
        ]
      });
    });

    $('.btn-cetak').click(function() {
      var href = "<?php echo e(route('SummaryStok.print')); ?>";
      var type = $(this).data('type');
      var start = $('#start_date').val();
      var end = $('#end_date').val();

      $.ajax({
        url: href,
        type: 'POST',
        data: {
          report: type,
          start_date: start,
          end_date: end,
          _token: "<?php echo e(csrf_token()); ?>"
        },
          success: function(data) {
            // console.log(respond);
            // window.location.href = data.redirect;
            window.open(data.redirect,'_blank');
          },
          error: function(request, status, error) {
            // console.log(error);
          }
        });
    });
  </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="portlet light">
              <div class="row">
                
                  <div class="col-md-3">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                      <input type="date" name="start_date" id="start_date" class="form-control">
                    </div>
                  </div>
                  <div class="col-xs-1 text-center">
                    <h4>S/d</h4>
                  </div>
                  <div class="col-md-3">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                      <input type="date" name="end_date" id="end_date" class="form-control">
                    </div>
                  </div>

                  <div class="btn-group-md pull-right">
                    <button class="btn btn-success btn-cetak" data-type="print">
                      <i class="glyphicon glyphicon-print"></i> Print to PDF
                    </button>
                    <button class="btn btn-success btn-cetak" data-type="excel">
                      <i class="glyphicon glyphicon-print"></i> Print to Excel
                    </button>
                  </div>
                
                <div class="col-md-2">
                  <button type="button" name="button" id="search-date" class="btn btn-info">View</button>
                </div>
              </div>
              <br>
              <br>
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                <thead>
                  <tr class="">
                    <th> Kode Barang </th>
                    <th> Nama Barang </th>
                    <th> Satuan </th>
                    <th> Kategory </th>
                    <th> Group Stok </th>
                    <th> Merek </th>
                    <th> Lokasi </th>
                    <th> Total In </th>
                    <th> Total Out </th>
                    <th> Last Stok </th>
                  </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td> <?php echo e($row->brg_barcode); ?> </td>
                      <td> <?php echo e($row->brg_nama); ?> </td>
                      <td> <?php echo e($row->stn_nama); ?> </td>
                      <td> <?php echo e($row->ktg_nama); ?> </td>
                      <td> <?php echo e($row->grp_nama); ?> </td>
                      <td> <?php echo e($row->mrk_nama); ?> </td>
                      <td> <?php echo e($row->gdg_nama); ?> </td>
                      <?php if($row->stokIn == null): ?>
                        <td> - </td>
                      <?php else: ?>
                        <td> <?php echo e($row->stokIn); ?> </td>
                      <?php endif; ?>
                      <?php if($row->stokOut == null): ?>
                        <td> - </td>
                      <?php else: ?>
                        <td> <?php echo e($row->stokOut); ?> </td>
                      <?php endif; ?>
                      
                      <td> <?php echo e($row->QOH); ?> </td>
                    </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>