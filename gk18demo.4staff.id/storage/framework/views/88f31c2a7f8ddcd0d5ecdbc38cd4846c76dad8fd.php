<?php
 use App\Models\mDetailPenjualanLangsung;
 use App\Models\mDetailPenjualanTitipan;
?>



<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/piutangPelanggan.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn-unlock-edit').click(function(){
                var id_pembelian = $(this).data('todo').id;
                $('[name="id_pembelian"]').val(id_pembelian);
                $('#unlock-jurnal').modal("show");

            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
<div class="page-content-inner">
    <div class="mt-content-body">
        <?php if($message = Session::get('success')): ?>
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong><?php echo e($message); ?></strong>
            </div>
        <?php endif; ?>
        <?php if($message = Session::get('warning')): ?>
            <div class="alert alert-warning alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong><?php echo e($message); ?></strong>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <br /><br />
                            <h3><center>Daftar Pembelian ke Supplier</center></h3>
                            <table class="table table-striped table-bordered table-hover table-header-fixed" id="tb_daftar_pembelian">
                                <thead>
                                    <tr class="">
                                        <th width="10"> No </th>
                                        <th> Tanggal</th>
                                        <th> No Faktur </th>
                                        <th> No Inv Spl </th>
                                        <th> Nama Supplier </th>
                                        <th> Total </th>
                                        <th> Aksi </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $poS): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td align="center"> <?php echo e($no++); ?>. </td>
                                    <td> <?php echo e(date('d M Y', strtotime($poS->ps_tgl))); ?> </td>
                                    <td> <?php echo e($poS->no_pembelian); ?> </td>
                                    <td> <?php echo e($poS->no_invoice); ?> </td>
                                    <td> <?php echo e($poS->supplier->spl_nama); ?> </td>
                                    <td> <?php echo e(number_format($poS->grand_total,2)); ?> </td>
                                    <td>
                                        <!-- <div class="btn-group btn-group-xs">                                             -->
                                            <a type="button" name="btn-return" class="btn btn-danger btn-xs" href="<?php echo e(route('returPembelian',['kode'=>$poS->ps_no_faktur])); ?>">
                                                <span class="fa fa-mail-reply"></span>
                                            </a>
                                            <a type="button" name="btn-cart" class="btn btn-primary btn-xs" href="<?php echo e(route('detailPembelian',['id'=>$poS->ps_no_faktur])); ?>">
                                                <span class="fa fa-info"></span>
                                            </a>
                                            <?php if($poS->edit==1): ?>
                                            <a type="button" name="btn-edit" class="btn btn-success btn-xs" href="<?php echo e(route('editPembelian',['id_pembelian'=>$poS->ps_no_faktur])); ?>">
                                                <span class="fa fa-pencil"></span>
                                            </a>
                                            <?php endif; ?>
                                            <!-- <a class="btn btn-success btn-payment-hutang-supplier btn-xs" href="<?php echo e(route('view-unlock-edit', ['id'=>$poS->ps_no_faktur])); ?>" data-target="#unlock-jurnal" data-toggle="modal">
                                                <span class="fa fa-pencil"></span>
                                            </a>  -->
                                            <?php if($poS->edit==0): ?>
                                            <a class="btn btn-danger btn-payment-hutang-supplier btn-xs" href="<?php echo e(route('view-unlock-edit', ['id'=>$poS->ps_no_faktur])); ?>" data-target="#unlock-jurnal" data-toggle="modal">
                                                <span class="fa fa-pencil"></span>
                                            </a>
                                            <?php endif; ?>    
                                            <button class="btn btn-danger btn-delete btn-xs hide" data-href="<?php echo e(route('delete_pembelian', ['kode'=>$poS->ps_no_faktur])); ?>">
                                                <span class="icon-trash"></span>
                                            </button>
                                        <!-- </div> -->
                                    </td>
                                </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="unlock-jurnal" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-center modal-md">
        <div class="modal-content">
            <div class="modal-body">
                    
                <span> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>