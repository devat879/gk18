@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
  <script type="text/javascript">
  $(document).ready(function(){
    function getRandomColor() {
      var letters = '0123456789ABCDEF'.split('');
      var color = '#';
      for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
    }

    var route_sales_chart = $('#data-back').data('route-sales-chart');
    var Labels_monthly_gross = new Array();
    var jumlah_monthly_grossPL = new Array();
    var jumlah_monthly_grossPT = new Array();
    $.get(route_sales_chart, function(response){
      response.label.forEach(function(label) {
        Labels_monthly_gross.push(label);
      })
      response.PL.forEach(function(langsung) {
        jumlah_monthly_grossPL.push(langsung.total);
      })
      response.PT.forEach(function(titipan) {
        jumlah_monthly_grossPT.push(titipan.total);
      })
      var sales_monthly_gross = document.getElementById("sales_monthly_gross").getContext('2d');
      var salesChartGet = new Chart(sales_monthly_gross, {
        type: 'bar',
        data: {
          labels: Labels_monthly_gross,
          datasets: [{
            label: 'Penjualan Langsung',
            backgroundColor: getRandomColor(),
            borderColor: [],
            data: jumlah_monthly_grossPL,
            borderWidth: 2
          },
          {
            label: 'Penjualan Titipan',
            backgroundColor: getRandomColor(),
            borderColor: [],
            data: jumlah_monthly_grossPT,
            borderWidth: 2
          }]
        },
        options: {
          maintainAspectRatio: false,
          scales: {
            xAxes: [{
              time: {
                unit: 'day'
              }
            }],
            yAxes: [{
              ticks: {
                beginAtZero:true
              }
            }]
          },
          legend: { display: true },
          title: {
            display: false,
            text: 'Monthly Gross'
          }
        }
      });
    });
  });
  </script>
@stop

@section('body')
  <div class="page-content-inner">
    <div class="mt-content-body">
      <span id="data-back" data-form-token="{{ csrf_token() }}"
      data-route-sales-chart="{{ route('dataSalesMonthly', ['id'=>$sales, 'start'=> $start, 'end'=> $end]) }}"
      ></span>
      {{-- <h4><b>Monthly Gross Sales Amount</b></h4> --}}
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div id="div-sales_monthly_gross" class="portlet light chart-container">
            <canvas id="sales_monthly_gross"></canvas>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="portlet light">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_1_1" data-toggle="tab"> Penjualan Langsung </a>
                </li>
                <li>
                  <a href="#tab_1_2" data-toggle="tab"> Penjualan Titipan </a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active in" id="tab_1_1">
                  <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                    <thead>
                      <tr class="">
                        <th width="10"> No </th>
                        <th> Tanggal </th>
                        <th> No. Faktur </th>
                        <th> Pelanggan </th>
                        <th> Barang Barkode </th>
                        <th> Nama Barang </th>
                        <th> Qty </th>
                        <th> Harga </th>
                        <th> Total </th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($dataListPL as $row)
                        <tr>
                          <td> {{ $no++ }}. </td>
                          <td> {{ date('Y-m-d', strtotime($row->pl_tgl)) }} </td>
                          <td> {{ $row->pl_no_faktur }} </td>
                          <td> {{ $row->cus_nama }} </td>
                          <td> {{ $row->brg_barcode }} </td>
                          <td> {{ $row->nama_barang }} </td>
                          <td align="right"> {{ number_format($row->qty, 2, "." ,",") }} </td>
                          <td align="right"> {{ number_format(round($row->harga_net), 2, "." ,",") }} </td>
                          <td align="right"> {{ number_format(round($row->qty * $row->harga_net), 2, "." ,",") }} </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <div class="tab-pane" id="tab_1_2">
                  <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                    <thead>
                      <tr class="">
                        <th width="10"> No </th>
                        <th> Tanggal </th>
                        <th> No. Faktur </th>
                        <th> Pelanggan </th>
                        <th> Barang Barkode </th>
                        <th> Nama Barang </th>
                        <th> Qty </th>
                        <th> Harga </th>
                        <th> Total </th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($dataListPT as $row)
                        <tr>
                          <td> {{ $no_2++ }}. </td>
                          <td> {{ date('Y-m-d', strtotime($row->pt_tgl)) }} </td>
                          <td> {{ $row->pt_no_faktur }} </td>
                          <td> {{ $row->cus_nama }} </td>
                          <td> {{ $row->brg_barcode }} </td>
                          <td> {{ $row->nama_barang }} </td>
                          <td align="right"> {{ number_format($row->qty, 2, "." ,",") }} </td>
                          <td align="right"> {{ number_format(round($row->harga_net), 2, "." ,",") }} </td>
                          <td align="right"> {{ number_format(round($row->qty * $row->harga_net), 2, "." ,",") }} </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="fixed">
    {{-- <div class="row">
      <div class="col-xs-3 col-md-3 col-lg-3">
        <ul class="nav nav-tabs nav-lg nav-justified no-margin no-border-radius text-center" style="border: 2px solid black;">
          <li class="bg-info">
            <div class="text-white text-uppercase" aria-expanded="true">
              <h4><b>Total Penjualan</b></h4>
              <h4>Rp. {{ number_format($rekaptotal, 0, "." ,".") }}</h4>
            </div>
          </li>
        </ul>
      </div>
    </div> --}}
  </div>

<style media="screen">
div.fixed {
  position: fixed;
  bottom: 0;
  left: 0;
}
</style>
@stop
