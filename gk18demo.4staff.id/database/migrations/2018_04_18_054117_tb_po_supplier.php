<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbPoSupplier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_po_supplier', function (Blueprint $table) {
         $table->increments('pos_no_po');
         $table->datetime('pos_tgl')->nullable();
         $table->integer('spl_kode')->nullable();
         $table->datetime('pos_tgl_kirim')->nullable();
         $table->integer('pos_subtotal')->nullable();
         $table->float('pos_disc')->nullable();
         $table->integer('pos_disc_nom')->nullable();
         $table->float('pos_ppn')->nullable();
         $table->integer('pos_ppn_nom')->nullable();
         $table->integer('biaya_lain')->nullable();
         $table->integer('grand_total')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_po_supplier');
     }
}
