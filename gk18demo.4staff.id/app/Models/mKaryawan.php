<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mKaryawan extends Model
{
  public $incrementing = false;
  protected $table = 'tb_karyawan';
  protected $primaryKey = 'kry_kode';
  public $timestamps = false;

  public function User()
  {
    return $this->hasMany('App\Models\mUser', 'kry_kode');
  }

  public function Customer()
  {
    return $this->hasMany('App\Models\mCustomer', 'kry_kode');
  }

  public function penjualan()
  {
    return $this->hasMany('App\Models\mPenjualanLangsung', 'pl_sales_person', 'kry_kode');
  }

  public function penjualanPL()
  {
    return $this->hasMany('App\Models\mPenjualanLangsung', 'pl_sales_person', 'kry_kode');
  }

  public function penjualanPT()
  {
    return $this->hasMany('App\Models\mPenjualanTitipan', 'pt_sales_person', 'kry_kode');
  }
}
